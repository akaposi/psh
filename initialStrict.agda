{-# OPTIONS --prop --rewriting #-}

module initialStrict where

open import lib
open import model
open import morphism
open import depModel
open import initialObs
import strictifyCat
open import strictifyPrefascist
open import strictifyPrefascistMorphism
import strictifyIso

open str Ci Mi

-- strictified initial model
Ciₛₛ : CwF {lzero}{lzero}{lzero}{lzero}
Ciₛₛ = Cₛₛ

Miₛₛ : Model Ciₛₛ
Miₛₛ = Mₛₛ

module initElimₛₛ {i j k l : Level} (Cd : DepCwF {lzero}{i}{lzero}{j}{lzero}{k}{lzero}{l} Ciₛₛ)
                                    (Md : DepModel {lzero}{i}{lzero}{j}{lzero}{k}{lzero}{l} Ciₛₛ Cd Miₛₛ) where
  open initIte Ciₛₛ Miₛₛ
  open DepCwF Cd
  open DepModel Md
  open strictifyPrefascist→ Ci Mi
  open strictifyIso
  private module Cₛₛ = CwF Ciₛₛ
  private module Mₛₛ = Model Miₛₛ

  Cd' : DepCwF {lzero}{i}{lzero}{j}{lzero}{k}{lzero}{l} Ci
  Cd' =
    record
     { Con∙ = λ Γ → Con∙ (iteCon Γ)
     ; Sub∙ = λ Δ∙ Γ∙ γ → Sub∙ Δ∙ Γ∙ (iteSub γ)
     ; _∘∙_ = λ γ∙ δ∙ → γ∙ ∘∙ δ∙
     ; ass∙ = ass∙
     ; id∙ = id∙
     ; idl∙ = idl∙
     ; idr∙ = idr∙
     ; ◇∙ = ◇∙
     ; ε∙ = ε∙
     ; ◇η∙ = ◇η∙
     ; Ty∙ = λ Γ∙ A → Ty∙ Γ∙ (iteTy A)
     ; _[_]T∙ = λ A∙ γ∙ → A∙ [ γ∙ ]T∙
     ; [∘]T∙ = [∘]T∙
     ; [id]T∙ = [id]T∙
     ; Tm∙ = λ Γ∙ A∙ a → Tm∙ Γ∙ A∙ (iteTm a)
     ; _[_]t∙ = λ a∙ γ∙ → a∙ [ γ∙ ]t∙
     ; [∘]t∙ = [∘]t∙
     ; [id]t∙ = [id]t∙
     ; _▹∙_ = λ Γ∙ A∙ → Γ∙ ▹∙ A∙
     ; _,[_][_]∙_ = λ γ∙ e e∙ a∙ → γ∙ ,[ cong iteTy e ][ e∙ ]∙ a∙
     ; p∙ = p∙
     ; q∙ = q∙
     ; ▹β₁∙ = ▹β₁∙
     ; ▹β₂∙ = ▹β₂∙
     ; ▹η∙ = ▹η∙
     }

  Md' : DepModel {lzero}{i}{lzero}{j}{lzero}{k}{lzero}{l} Ci Cd' Mi
  Md' =
    record
     { Π∙ = λ A∙ B∙ → Π∙ A∙ B∙
     ; Π[]∙ = Π[]∙
     ; lam∙ = λ A∙ t∙ → lam∙ A∙ t∙
     ; lam[]∙ = lam[]∙
     ; app∙ = λ t∙ u∙ → app∙ t∙ u∙
     ; app[]∙ = app[]∙
     ; Πβ∙ = Πβ∙
     ; Πη∙ = Πη∙
     ; 𝔹∙ = 𝔹∙
     ; 𝕥∙ = 𝕥∙
     ; 𝕗∙ = 𝕗∙
     ; ifᵀ∙ = λ b∙ A∙ B∙ → ifᵀ∙ b∙ A∙ B∙
     ; ifᵀ[]∙ = ifᵀ[]∙
     ; ifᵀβ₁∙ = ifᵀβ₁∙
     ; ifᵀβ₂∙ = ifᵀβ₂∙
     ; ifᵗ∙ = λ P∙ P𝕥∙ P𝕗∙ b∙ → ifᵗ∙ P∙ P𝕥∙ P𝕗∙ b∙
     ; ifᵗ[]∙ = ifᵗ[]∙
     ; ifᵗβ₁∙ = ifᵗβ₁∙
     ; ifᵗβ₂∙ = ifᵗβ₂∙
     }

  open initElim Cd' Md'

  elimConₛₛ : (Γ : Cₛₛ.Con) → Con∙ Γ
  elimConₛₛ Γ = coe (cong Con∙ (Con-sec Γ)) (elimCon (Con→ Γ))

  elimSubₛₛ : ∀{Γ Δ}(γ : Cₛₛ.Sub Δ Γ) → Sub∙ (elimConₛₛ Δ) (elimConₛₛ Γ) γ
  elimSubₛₛ {Γ}{Δ} γ =
    coe (cong₅ (λ Δ Δ∙ Γ Γ∙ γ → Sub∙ {Δ} Δ∙ {Γ} Γ∙ γ)
               (Con-sec Δ)
               (coe≡-eq _ (elimCon (Con→ Δ)))
               (Con-sec Γ)
               (coe≡-eq _ (elimCon (Con→ Γ)))
               (Sub-sec Γ Δ γ))
        (elimSub {Con→ Δ}{Con→ Γ}(strictifyCat.Sub.γ (Sub→ {Γ}{Δ} γ) (CwF.id Ci)))

  elimTyₛₛ : ∀{Γ}(A : Cₛₛ.Ty Γ) → Ty∙ (elimConₛₛ Γ) A
  elimTyₛₛ {Γ} A =
    coe (cong₃ (λ Γ Γ∙ A → Ty∙ {Γ} Γ∙ A)
               (Con-sec Γ)
               (coe≡-eq _ (elimCon (Con→ Γ)))
               (Ty-sec Γ A))
        (elimTy (Ty→ Γ A))

  elimTmₛₛ : ∀{Γ}{A : Cₛₛ.Ty Γ}(a : Cₛₛ.Tm Γ A) → Tm∙ (elimConₛₛ Γ) (elimTyₛₛ A) a
  elimTmₛₛ {Γ}{A} a =
    coe (cong₅ (λ Γ Γ∙ A A∙ a → Tm∙ {Γ} Γ∙ {A} A∙ a)
               (Con-sec Γ)
               (coe≡-eq _ (elimCon (Con→ Γ)))
               (Ty-sec Γ A)
               (coe≡-eq _ (elimTy (Ty→ Γ A)))
               (Tm-sec Γ A a))
        (elimTm (Tm→ Γ A a))


  -- Now in order to finish transporting the induction principle, it remains to prove a bunch of equalities
  -- All of them follow from cong and coe-eq, using the fact that we have an isomorphism between Ci and Cₛₛ
  -- Thus they can be safely postulated (definitionally proof irrelevant postulates do not block computation)

  postulate
    elim◇ₛₛ   : elimConₛₛ Cₛₛ.◇ ~ ◇∙
    elim▹ₛₛ   : ∀{Γ}{A : Cₛₛ.Ty Γ} → elimConₛₛ (Γ Cₛₛ.▹ A) ~ elimConₛₛ Γ ▹∙ elimTyₛₛ A
    elim∘ₛₛ   : ∀{Γ Δ Θ} {f : Cₛₛ.Sub Δ Γ} {g : Cₛₛ.Sub Θ Δ}
                → elimSubₛₛ {Γ}{Θ} (Cₛₛ._∘_ {Γ}{Δ} f {Θ} g) ~ (elimSubₛₛ {Γ}{Δ} f) ∘∙ (elimSubₛₛ {Δ}{Θ} g)
    elim-idₛₛ : ∀{Γ} → elimSubₛₛ {Γ}{Γ} (Cₛₛ.id {Γ}) ~ id∙ {Γ}{elimConₛₛ Γ}
    elim[]Tₛₛ : ∀{Γ Δ}{f : Cₛₛ.Sub Δ Γ}{A : Cₛₛ.Ty Γ} → elimTyₛₛ {Δ} (Cₛₛ._[_]T {Γ} A {Δ} f) ~ (elimTyₛₛ {Γ} A) [ elimSubₛₛ {Γ}{Δ} f ]T∙
    elim,[]ₛₛ : ∀{Γ Δ}{f : Cₛₛ.Sub Δ Γ}{A : Cₛₛ.Ty Γ}{A' : Cₛₛ.Ty Δ}{a : Cₛₛ.Tm Δ A'}{e : Cₛₛ._[_]T {Γ} A {Δ} f ~ A'}
                → elimSubₛₛ {Γ Cₛₛ.▹ A}{Δ} (Cₛₛ._,[_]_ {Γ}{Δ} f {A}{A'} e a)
                  ~ (elimSubₛₛ {Γ}{Δ} f) ,[ e ][ elim[]Tₛₛ {Γ}{Δ}{f}{A} ⁻¹ ◼ cong (elimTyₛₛ {Δ}) e ]∙ (elimTmₛₛ {Δ}{A'} a)
    elimεₛₛ   : ∀ {Γ} → elimSubₛₛ {Cₛₛ.◇}{Γ} (Cₛₛ.ε {Γ}) ~ ε∙ {Γ}{elimConₛₛ Γ}
    elim[]tₛₛ : ∀{Γ Δ}{f : Cₛₛ.Sub Δ Γ}{A : Cₛₛ.Ty Γ}{a : Cₛₛ.Tm Γ A}
                → elimTmₛₛ {Δ}{Cₛₛ._[_]T {Γ} A {Δ} f}(Cₛₛ._[_]t {Γ}{A} a {Δ} f) ~ (elimTmₛₛ {Γ}{A} a) [ elimSubₛₛ {Γ}{Δ} f ]t∙
    elim-pₛₛ  : ∀{Γ}{A : Cₛₛ.Ty Γ} → elimSubₛₛ {Γ}{Γ Cₛₛ.▹ A} (Cₛₛ.p {Γ}{A}) ~ p∙ {Γ}{elimConₛₛ Γ}{A}{elimTyₛₛ {Γ} A}
    elim-qₛₛ  : ∀{Γ}{A : Cₛₛ.Ty Γ}
                → elimTmₛₛ {Γ Cₛₛ.▹ A}{Cₛₛ._[_]T {Γ} A {Γ Cₛₛ.▹ A} (Cₛₛ.p {Γ}{A})}(Cₛₛ.q {Γ}{A}) ~ q∙ {Γ}{elimConₛₛ Γ}{A}{elimTyₛₛ {Γ} A}

  -- TODO

  -- postulate
  --   elimΠ   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)} → elimTy (Π A B) ≡ Π∙ (elimTy A) (elimTy B)
  --   elim-lam : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{t : Tm (Γ ▹ A) B} → elimTm (lam A {B} t) ≡ lam∙ (elimTy A) (elimTm t)
  --   elim-app : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{t : Tm Γ (Π A B)}{u : Tm Γ A} → elimTm (app t u) ≡ app∙ (elimTm t) (elimTm u)

  --   elim𝔹   : elimTy 𝔹 ≡ 𝔹∙
  --   elim-𝕥   : elimTm 𝕥 ≡ 𝕥∙
  --   elim-𝕗   : elimTm 𝕗 ≡ 𝕗∙
  --   elim-ifᵀ : ∀{Γ}{b : Tm Γ (𝔹 [ ε ]T)}{A B : Ty Γ} → elimTy (ifᵀ b A B) ≡ ifᵀ∙ (elimTm b) (elimTy A) (elimTy B)
  --   elim-ifᵗ : ∀{Γ}{P : Ty (Γ ▹ 𝔹 [ ε ]T)}{P𝕥 : Tm Γ (P [ CwF-tools.sg Ci (𝕥 [ ε ]t) ]T)}
  --                  {P𝕗 : Tm Γ (P [ CwF-tools.sg Ci (𝕗 [ ε ]t) ]T)}{b : Tm Γ (𝔹 [ ε ]T)}
  --              → elimTm (ifᵗ P P𝕥 P𝕗 b) ≡ ifᵗ∙ (elimTy P) (elimTm P𝕥) (elimTm P𝕗) (elimTm b)
