{-# OPTIONS --prop --rewriting #-}

open import lib
open import model

module strictifyPrefascistYoneda (M : Model {lzero}{lzero}{lzero}{lzero}) where

import strictifyCat M as strictifyCat

-- First, we build the CwF of prefascists over SrictifyCat.M
-- Pretty much every equation in that category is strict, except η for substitutions
-- (Of course, the category of prefascists is not itself equivalent to M, but it is the correct framework for our construction)

variable i j k l : Level

Mₛ = strictifyCat.M

module M  = Model M
module Mₛ = Model Mₛ

Ob  = Mₛ.Con
Hom = Mₛ.Sub

module C where
  infixl 9 _∘_
  _∘_ : ∀ {x y z} → Hom y x → Hom z y → Hom z x
  f ∘ g = Mₛ._∘_ f g

  id : ∀ {x} → Hom x x
  id = Mₛ.id

  _▹_ : (x : Ob) → M.Ty x → Ob
  _▹_ = Mₛ._▹_

  p : ∀{x A} → Hom (x ▹ A) x
  p = Mₛ.p

  q : ∀{x A} → Mₛ.Tm (x ▹ A) (A Mₛ.[ p ]T)
  q = Mₛ.q

-- sections of a prefascist on C/x
record Y
  (x : Ob)
  (A : ∀ {y} → Hom y x → Set i)
  (A-rel : ∀ {y} (f : Hom y x) → (∀ {z} (g : Hom z y) → A (f C.∘ g)) → Prop i)
  : Set i
  where
  constructor mkY
  field
    a : ∀ {y} → (f : Hom y x) → A f
    rel : ∀ {y} → (f : Hom y x) → A-rel f λ g → a (f C.∘ g)
open Y public renaming (a to ∣_∣)

record Con (i : Level) : Set (lsuc i) where
  constructor mkCon
  field
    Γ : Ob → Set i
    rel : ∀ {x} → (∀ {y : Ob} → Hom y x → Γ y) → Prop i
open Con public renaming (Γ to ∣_∣)

-- converting Γ to ordinary presheaf
Yᶜ : Con i → Ob → Set i
Yᶜ Γ x = Y x (λ {y} _ → ∣ Γ ∣ y) (λ _ → Γ .rel)

Yᶜ-~ : ∀ {Γ : Con i} {x₀ x₁} (xₑ : x₀ ~ x₁) {γ₀ : Yᶜ Γ x₀} {γ₁ : Yᶜ Γ x₁} → (λ {x} → ∣ γ₀ ∣ {x}) ~ (λ {x} → ∣ γ₁ ∣ {x}) → γ₀ ~ γ₁
Yᶜ-~ xₑ γₑ = ~congₚₛ (cong₂ (λ x → mkY {x = x}) xₑ γₑ)

Yᶜ-≡ : ∀ {Γ : Con i} {x} → {γ₀ γ₁ : Yᶜ Γ x} → (λ {x} → ∣ γ₀ ∣ {x}) ~ (λ {x} → ∣ γ₁ ∣ {x}) → γ₀ ~ γ₁
Yᶜ-≡ e = ~congₚₛ (cong mkY e)

infixl 9 _|ᶜ_
_|ᶜ_ : ∀ {Γ : Con i} {x} → Yᶜ Γ x → ∀ {y} → Hom y x → Yᶜ Γ y
∣ γ |ᶜ f ∣ g = ∣ γ ∣ (f C.∘ g)
(γ |ᶜ f) .rel g = γ .rel (f C.∘ g)

record Sub (Δ : Con i) (Γ : Con j) : Set (i ⊔ j) where
  constructor mkSub
  field
    γ : ∀ {x} → Yᶜ Δ x → ∣ Γ ∣ x
    rel : ∀ {x} → (δ : Yᶜ Δ x) → Γ .rel (λ f → γ (δ |ᶜ f))
open Sub public renaming (γ to ∣_∣)

Sub-≡ : ∀ {Γ : Con i} {Δ : Con j}
  {γ₀ γ₁ : Sub Δ Γ} → (λ {x} → ∣ γ₀ ∣ {x}) ~ (λ {x} → ∣ γ₁ ∣ {x}) → γ₀ ~ γ₁
Sub-≡ e = ~congₚₛ (cong mkSub e)

-- converting a Sub into a natural transformation
Yˢ : ∀ {Γ : Con i} {Δ : Con j} → Sub Δ Γ → ∀ {x} → Yᶜ Δ x → Yᶜ Γ x
∣ Yˢ γ δ ∣ f =  ∣ γ ∣ (δ |ᶜ f)
Yˢ γ δ .rel f =  γ .rel (δ |ᶜ f)

infixl 9 _∘_
_∘_ : ∀ {Γ : Con i} {Δ : Con j} {Θ : Con k} → Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
∣ γ ∘ δ ∣ θ = ∣ γ ∣ (Yˢ δ θ)
(γ ∘ δ) .rel θ = γ .rel (Yˢ δ θ)

comp : ∀ (Γ : Con i) {Δ : Con i} → Sub Δ Γ → ∀{Θ : Con i} → Sub Θ Δ → Sub Θ Γ
comp Γ γ δ = _∘_ {Γ = Γ} γ δ
syntax comp Γ γ δ = γ ∘[ Γ ] δ

assoc : ∀ {Γ : Con i} {Δ : Con j} {Θ : Con k} {Ξ : Con l} (γ : Sub Δ Γ) (δ : Sub Θ Δ) (θ : Sub Ξ Θ) → γ ∘ (δ ∘ θ) ~ (γ ∘ δ) ∘ θ
assoc γ δ θ = ~refl

id : ∀ {Γ : Con i} → Sub Γ Γ
∣ id ∣ γ = ∣ γ ∣ C.id
id .rel γ = γ .rel C.id

idr : ∀ {Γ : Con i} {Δ : Con j} (γ : Sub Δ Γ) → γ ∘ id ~ γ
idr γ = ~refl

idl : ∀ {Γ : Con i} {Δ : Con j} (γ : Sub Δ Γ) → id ∘ γ ~ γ
idl γ = ~refl

record Ty (Γ : Con i) (j : Level) : Set (i ⊔ lsuc j) where
  field
    A : ∀ {x} → Yᶜ Γ x → Set j -- ordinary family
    rel : ∀ {x} → (γ : Yᶜ Γ x) → (∀ {y} (f : Hom y x) → A (γ |ᶜ f)) → Prop j
open Ty public renaming (A to ∣_∣)

Yᵀ : ∀ {Γ : Con i} → Ty Γ j → ∀ {x} → Yᶜ Γ x → Set j
Yᵀ A {x = x} γ = Y x (λ f → ∣ A ∣ (γ |ᶜ f)) (λ f → A .rel (γ |ᶜ f))

infixl 9 ⟨_,_⟩_|ᵀ_
⟨_,_⟩_|ᵀ_ : ∀ {Γ : Con i} (A : Ty Γ j) {x} (γ : Yᶜ Γ x) → Yᵀ A γ → ∀ {y} (f : Hom y x) → Yᵀ A (γ |ᶜ f)
∣ ⟨ A , γ ⟩ a |ᵀ f ∣ g = ∣ a ∣ (f C.∘ g)
(⟨ A , γ ⟩ a |ᵀ f) .rel g = a .rel (f C.∘ g)

infixl 9 _[_]ᵀ
_[_]ᵀ : ∀ {Γ : Con i} {Δ : Con j} → Ty Γ k → Sub Δ Γ → Ty Δ k
∣ A [ γ ]ᵀ ∣ δ = ∣ A ∣ (Yˢ γ δ)
(A [ γ ]ᵀ) .rel δ = A .rel (Yˢ γ δ)

[]ᵀ-∘ : ∀ {Γ : Con i} {Δ : Con j} {Θ : Con k}
  (A : Ty Γ l) (γ : Sub Δ Γ) (δ : Sub Θ Δ) → A [ γ ∘ δ ]ᵀ ~ A [ γ ]ᵀ [ δ ]ᵀ
[]ᵀ-∘ A γ δ = ~refl

[]ᵀ-id : ∀ {Γ : Con i} (A : Ty Γ j) → A [ id ]ᵀ ~ A
[]ᵀ-id A = ~refl

record Tm (Γ : Con i) (A : Ty Γ j) : Set (i ⊔ j) where
  constructor mkTm
  field
    a : ∀ {x} (γ : Yᶜ Γ x) → ∣ A ∣ γ
    rel : ∀ {x} (γ : Yᶜ Γ x) → A .rel γ (λ f → a (γ |ᶜ f))
open Tm public renaming (a to ∣_∣)

Tm-≡ : ∀ {Γ : Con i} {A : Ty Γ j} {a₀ a₁ : Tm Γ A} → (λ {x} → ∣ a₀ ∣ {x}) ~ (λ {x} → ∣ a₁ ∣ {x}) → a₀ ~ a₁
Tm-≡ e = ~congₚₛ (cong mkTm e)

Yᵗ : ∀ {Γ : Con i} {A : Ty Γ j} → Tm Γ A → ∀ {x} (γ : Yᶜ Γ x) → Yᵀ A γ
∣ Yᵗ a γ ∣ f = ∣ a ∣ (γ |ᶜ f)
Yᵗ a γ .rel f = a .rel (γ |ᶜ f)

infixl 9 _[_]ᵗ
_[_]ᵗ : ∀ {Γ : Con i} {Δ : Con j} {A : Ty Γ k} → Tm Γ A → (γ : Sub Δ Γ) → Tm Δ (A [ γ ]ᵀ)
∣ a [ γ ]ᵗ ∣ δ = ∣ a ∣ (Yˢ γ δ)
(a [ γ ]ᵗ) .rel δ = a .rel (Yˢ γ δ)

[]ᵗ-∘ : ∀ {Γ : Con i} {Δ : Con j} {Θ : Con k} {A : Ty Γ l}
  (a : Tm Γ A) (γ : Sub Δ Γ) (δ : Sub Θ Δ) → a [ γ ∘ δ ]ᵗ ~ a [ γ ]ᵗ [ δ ]ᵗ
[]ᵗ-∘ a γ δ = ~refl

[]ᵗ-id : ∀ {Γ : Con i} {A : Ty Γ j} (a : Tm Γ A) → a [ id ]ᵗ ~ a
[]ᵗ-id a = ~refl

record ∣▹∣ (Γ : Con i) (A : Ty Γ j) (x : Ob) : Set (i ⊔ j) where
  constructor mk▹
  field
    con : Yᶜ Γ x
    ty : ∣ A ∣ con
open ∣▹∣ public

record ▹-rel
  {Γ : Con i} {A : Ty Γ j} {x : Ob} (γa : ∀ {y} → Hom y x → ∣▹∣ Γ A y) : Prop (i ⊔ j)
  where
  constructor mk▹rel
  field
    con :
      (λ {y} (f : Hom y x) {z} (g : Hom z y) → ∣ γa f .con ∣ g) ~
      (λ {y} (f : Hom y x) {z} (g : Hom z y) → ∣ γa (f C.∘ g) .con ∣ C.id)
    ty :
      A .rel
        record
          { a = λ f → ∣ γa f .∣▹∣.con ∣ C.id
          ; rel = λ f → coeₚ (~to≡ (cong (λ γ → Γ .rel (γ f)) con)) (γa f .∣▹∣.con .rel C.id) }
        (λ f → coe (~to≡ (cong ∣ A ∣ (Yᶜ-≡ (~congᵢᵣ con ~refl (~refl {a = f}))))) (γa f .ty))
open ▹-rel public

infixl 2 _▹_
_▹_ : (Γ : Con i) → Ty Γ j → Con (i ⊔ j)
∣ Γ ▹ A ∣ = ∣▹∣ Γ A
(Γ ▹ A) .rel = ▹-rel

Y▹ : {Γ : Con i} {A : Ty Γ j} {x : Ob} (γ : Yᶜ Γ x) → Yᵀ A γ → Yᶜ (Γ ▹ A) x
∣ Y▹ γ a ∣ f .con = γ |ᶜ f
∣ Y▹ γ a ∣ f .ty = ∣ a ∣ f
Y▹ γ a .rel f .con = ~refl
Y▹ γ a .rel f .ty = a .rel f

p : {Γ : Con i} {A : Ty Γ j} → Sub (Γ ▹ A) Γ
∣ p ∣ γa = ∣ ∣ γa ∣ C.id .con ∣ C.id
p {Γ = Γ} .rel γa =
  let e = ~congᵢᵣ (γa .rel C.id .con) ~refl (~refl {a = C.id}) in
  coeₚ (~to≡ (cong (Γ .rel) e)) (∣ γa ∣ C.id .con .rel C.id)

q : {Γ : Con i} {A : Ty Γ j} → Tm (Γ ▹ A) (A [ p ]ᵀ)
∣ q {A = A} ∣ γa =
  let e = Yᶜ-≡ (~congᵢᵣ (γa .rel C.id .con) ~refl (~refl {a = C.id})) in
  coe (~to≡ (cong ∣ A ∣ e)) (∣ γa ∣ C.id .ty)
q .rel γa = γa .rel C.id .ty

infixl 4 _,_
_,_ : {Γ : Con i} {Δ : Con j} {A : Ty Γ k} (γ : Sub Δ Γ) → Tm Δ (A [ γ ]ᵀ) → Sub Δ (Γ ▹ A)
∣ γ , a ∣ δ .con = Yˢ γ δ
∣ γ , a ∣ δ .ty = ∣ a ∣ δ
(γ , a) .rel δ .con = ~refl
(_,_ {A = A} γ a) .rel δ .ty = a .rel δ

_,[_]_   : ∀{Γ : Con i}{Δ : Con j}(γ : Sub Δ Γ) {A : Ty Γ k} {A' : Ty Δ k} → A [ γ ]ᵀ ~ A' → Tm Δ A' → Sub Δ (Γ ▹ A)
∣ _,[_]_ {Γ = Γ}{Δ = Δ} γ {A}{A'} e a ∣ δ .con = Yˢ γ δ
∣ _,[_]_ {Γ = Γ}{Δ = Δ} γ {A}{A'} e a ∣ δ .ty = coe (~to≡ ((cong (λ (A : Ty Δ _) → ∣ A ∣ δ) e) ⁻¹)) (∣ a ∣ δ)
_,[_]_ {Γ = Γ}{Δ = Δ} γ {A}{A'} e a .rel δ .con = ~refl
_,[_]_ {Γ = Γ}{Δ = Δ} γ {A}{A'} e a .rel {x = x} δ .ty =
  let e = ~cong (cong (λ (A : Ty Δ _) → A .rel δ) (e ⁻¹))
                (funextᵢ λ xₑ →
                  funext λ {γ₀} {γ₁} γₑ →
                    cong₂ (λ y (γ : Hom y x) → ∣ a ∣ (δ |ᶜ γ)) xₑ γₑ ◼ coe-eq _ (∣ a ∣ (δ |ᶜ γ₁)))
  in coeₚ (~to≡ e) (a .rel δ)

infixl 4 _,⟨_⟩_
_,⟨_⟩_ : {Γ : Con i} {Δ : Con j} (γ : Sub Δ Γ) (A : Ty Γ k) → Tm Δ (A [ γ ]ᵀ) → Sub Δ (Γ ▹ A)
γ ,⟨ A ⟩ a = γ , a

,-∘ : {Γ : Con i} {Δ : Con j} {Θ : Con k} {A : Ty Γ l}
  (γ : Sub Δ Γ) (a : Tm Δ (A [ γ ]ᵀ)) (δ : Sub Θ Δ) →
  (γ ,⟨ A ⟩ a) ∘ δ ~ (γ ∘ δ , a [ δ ]ᵗ)
,-∘ γ a δ = ~refl

▹-β₁ : {Γ : Con i} {Δ : Con j} {A : Ty Γ k} (γ : Sub Δ Γ) (a : Tm Δ (A [ γ ]ᵀ)) → p ∘ (γ ,⟨ A ⟩ a) ~ γ
▹-β₁ γ a = ~refl

▹-β₂ : {Γ : Con i} {Δ : Con j} {A : Ty Γ k} (γ : Sub Δ Γ) (a : Tm Δ (A [ γ ]ᵀ)) → q [ γ ,⟨ A ⟩ a ]ᵗ ~ a
▹-β₂ γ a = ~refl

▹-η : {Γ : Con i} {A : Ty Γ j} → (p {A = A} ,⟨ A ⟩ q) ~ id {Γ = Γ ▹ A}
▹-η {Γ = Γ} {A = A} =
  Sub-≡ (funextᵢ λ {x} {x'} ex →
        funext λ {γa} {γa'} eγa →
          cong₃ (λ x → mk▹ {Γ = Γ} {A = A} {x}) ex
            ((cong₂ (λ z → Yˢ p {z}) ex eγa) ◼ (Yᶜ-≡ ((~congᵢᵣ (γa' .rel C.id .con) ~refl (~refl {a = C.id})) ⁻¹)))
            ((coe-eq _ (∣ γa ∣ C.id .ty)) ⁻¹ ◼ (cong₂ (λ x (γ : Yᶜ (Γ ▹ A) x) → ∣ γ ∣ C.id .ty) ex eγa)))

infixl 4 _↑
_↑ : {Γ : Con i} {Δ : Con j} {A : Ty Γ k} (γ : Sub Δ Γ) → Sub (Δ ▹ A [ γ ]ᵀ) (Γ ▹ A)
γ ↑ = γ ∘ p , q

◇ : Con lzero
∣ ◇ ∣ x = ⊤
◇ .rel γ = ⊤ₚ

ε : {Γ : Con i} → Sub Γ ◇
∣ ε ∣ γ = _
ε .rel γ = ttₚ

ε-∘ : {Γ : Con i} {Δ : Con j} (γ : Sub Δ Γ) → ε ∘ γ ~ ε
ε-∘ γ = ~refl

◇-η : (ε {Γ = ◇}) ~ (id {Γ = ◇})
◇-η = ~refl

-- Now we build a universe hierarchy for the CwF of prefascists
-- These universes come from the external universes Set i, not from the CwF structure on M

record ∣U∣ (i : Level) (x : Ob) : Set (lsuc i) where
  field
    t : ∀ {y} → Hom y x → Set i   -- this is simpler than just Ty (yoneda x)
    rel : (∀ {y} (f : Hom y x) → t f) → Prop i
open ∣U∣ public renaming (t to ∣_∣)

U : (i : Level) → {Γ : Con j} → Ty Γ (lsuc i)
∣ U i ∣ {x} γ = ∣U∣ i x
U i .rel {x} γ A =
  (λ {y} (f : Hom y x) {z} (g : Hom z y) → ∣ A f ∣ g) ~
  (λ {y} (f : Hom y x) {z} (g : Hom z y) → ∣ A (f C.∘ g) ∣ C.id)

U-[] : (i : Level) {Γ : Con j} {Δ : Con k} (γ : Sub Δ Γ) → U i [ γ ]ᵀ ~ U i
U-[] i γ = ~refl

El : {Γ : Con j} → Tm Γ (U i) → Ty Γ i
∣ El A ∣ γ = ∣ ∣ A ∣ γ ∣ C.id
El A .rel γ a =
  ∣ A ∣ γ .rel λ f → let e = (~congᵢᵣ (~congᵢᵣ ((A .rel γ) ⁻¹) ~refl (~refl {a = C.id})) ~refl (~refl {a = f})) in
                         coe (~to≡ e) (a f)

El-[] : {Γ : Con j} {Δ : Con k} (A : Tm Γ (U i)) (γ : Sub Δ Γ) → El A [ γ ]ᵀ ~ El (A [ γ ]ᵗ)
El-[] A γ = ~refl

c : {Γ : Con j} → Ty Γ i → Tm Γ (U i)
∣ ∣ c A ∣ γ ∣ f = ∣ A ∣ (γ |ᶜ f)
∣ c A ∣ γ .rel a = A .rel γ a
c A .rel γ = ~refl

c-[] : {Γ : Con j} {Δ : Con k} (A : Ty Γ i) (γ : Sub Δ Γ) → c A [ γ ]ᵗ ~ c (A [ γ ]ᵀ)
c-[] A γ = ~refl

U-β : {Γ : Con j} (A : Ty Γ i) → El (c A) ~ A
U-β A = ~refl

{-
U-η : (A : Tm Γ (U i)) → c (El A) ≡ A
U-η A = Tm-≡ (funextᵢ (funext λ γ → sym (∣U∣-≡ (ap (λ a → a C.id) (A .rel γ)) {!refl!})))
-}

-- Then we equip the CwF of prefascists with dependent products

Π : {Γ : Con i} (A : Ty Γ j) → Ty (Γ ▹ A) k → Ty Γ (j ⊔ k)
∣ Π A B ∣ γ = (a : Yᵀ A γ) → ∣ B ∣ (Y▹ γ a)
Π A B .rel γ t = (a : Yᵀ A γ) → B .rel (Y▹ γ a) λ f → t f (⟨ A , γ ⟩ a |ᵀ f)

Π-[] : {Γ : Con i} {Δ : Con j}
  (A : Ty Γ k) (B : Ty (Γ ▹ A) l) (γ : Sub Δ Γ) →
  Π A B [ γ ]ᵀ ~ Π (A [ γ ]ᵀ) (B [ γ ↑ ]ᵀ)
Π-[] A B γ = ~refl

app : {Γ : Con i}
  {A : Ty Γ j} (B : Ty (Γ ▹ A) k) →
  Tm Γ (Π A B) → (a : Tm Γ A) → Tm Γ (B [ id , a ]ᵀ)
∣ app B t a ∣ γ = ∣ t ∣ γ (Yᵗ a γ)
app B t a .rel γ = t .rel γ (Yᵗ a γ)

app-[] : {Γ : Con i} {Δ : Con j}
  {A : Ty Γ k} (B : Ty (Γ ▹ A) l) →
  (t : Tm Γ (Π A B)) (a : Tm Γ A) (γ : Sub Δ Γ) →
  app B t a [ γ ]ᵗ ~
  app (B [ γ ↑ ]ᵀ) (t [ γ ]ᵗ) (a [ γ ]ᵗ)
app-[] B t a γ = ~refl

lam : {Γ : Con i} {A : Ty Γ j} {B : Ty (Γ ▹ A) k} → Tm (Γ ▹ A) B → Tm Γ (Π A B)
∣ lam b ∣ γ a = ∣ b ∣ (Y▹ γ a)
lam b .rel γ a = b .rel (Y▹ γ a)

lam-[] : {Γ : Con i} {Δ : Con j} {A : Ty Γ k} {B : Ty (Γ ▹ A) l}
         (b : Tm (Γ ▹ A) B) (γ : Sub Δ Γ) → lam b [ γ ]ᵗ ~ lam (b [ γ ↑ ]ᵗ)
lam-[] b γ = ~refl

Π-β : {Γ : Con i}
  {A : Ty Γ j} (B : Ty (Γ ▹ A) k) (b : Tm (Γ ▹ A) B) (a : Tm Γ A) →
  app B (lam b) a ~ b [ id , a ]ᵗ
Π-β B b a = ~refl

Π-η :
  {Γ : Con i} {A : Ty Γ j} (B : Ty (Γ ▹ A) k) (t : Tm Γ (Π A B)) →
  lam (app (B [ p ↑ ]ᵀ) (t [ p ]ᵗ) (q {A = A})) ~ t
Π-η B t = ~refl

-- This concludes the definition of the CwF of prefascists on C.
-- Now we define the higher-order model in the CwF of prefascists, by internalising the CwF structure on C.

-- reorganising Mₛ.Ty as a prefascist set
CTy : Con lzero
CTy =
  record {
    Γ = Mₛ.Ty ;
    rel = λ {x} γ → ∀{y}(f : Hom y x) → (γ C.id Mₛ.[ f ]T) ~ γ f
  }

-- reorganising Mₛ.Tm as a dependent prefascist set
CTm : Ty CTy lzero
CTm = record { A = λ {x} γ → Mₛ.Tm x (∣ γ ∣ C.id)
             ; rel = λ {x} γ α →  ∀{y}(f : Hom y x) → (α C.id Mₛ.[ f ]t) ~ α f }

-- the new sort of types in our final model
Tyᵒ : Con i → Set i
Tyᵒ Γ = Sub Γ CTy

_[_]ᵀᵒ : ∀{Γ : Con i} → Tyᵒ Γ → ∀{Δ : Con i} → Sub Δ Γ → Tyᵒ Δ
Aᵒ [ γ ]ᵀᵒ = Aᵒ ∘ γ

[∘]ᵀᵒ  : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ}{Δ : Con i}{γ : Sub Δ Γ}{Θ : Con i}{δ : Sub Θ Δ}
       → Aᵒ [ γ ∘ δ ]ᵀᵒ ~ Aᵒ [ γ ]ᵀᵒ [ δ ]ᵀᵒ
[∘]ᵀᵒ  = ~refl

[id]ᵀᵒ : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ} → Aᵒ [ id ]ᵀᵒ ~ Aᵒ
[id]ᵀᵒ = ~refl

-- the new sort of terms in our final model
Tmᵒ : (Γ : Con i) → Tyᵒ Γ → Set i
Tmᵒ Γ Aᵒ = Tm Γ (CTm [ Aᵒ ]ᵀ)

_[_]ᵗᵒ : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ} → Tmᵒ Γ Aᵒ → ∀{Δ : Con i}(γ : Sub Δ Γ) → Tmᵒ Δ (Aᵒ [ γ ]ᵀᵒ)
a [ γ ]ᵗᵒ = a [ γ ]ᵗ

[∘]ᵗᵒ : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ}{aᵒ : Tmᵒ Γ Aᵒ}{Δ : Con i}{γ : Sub Δ Γ}{Θ : Con i}{δ : Sub Θ Δ}
      → _[_]ᵗᵒ {Aᵒ = Aᵒ} aᵒ (γ ∘[ Γ ] δ) ~ _[_]ᵗᵒ {Aᵒ = Aᵒ [ γ ]ᵀᵒ} (_[_]ᵗᵒ {Aᵒ = Aᵒ} aᵒ γ) δ
[∘]ᵗᵒ = ~refl

[id]ᵗᵒ : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ}{aᵒ : Tmᵒ Γ Aᵒ}
       → _[_]ᵗᵒ {Aᵒ = Aᵒ} aᵒ id ~ aᵒ
[id]ᵗᵒ = ~refl

_▹ᵒ_ : (Γ : Con i)(A : Tyᵒ Γ) → Con i
Γ ▹ᵒ Aᵒ = Γ ▹ (CTm [ Aᵒ ]ᵀ)

infixl 4 _,ᵒ_
_,ᵒ_ : ∀{Γ Δ : Con i}(γ : Sub Δ Γ){Aᵒ : Tyᵒ Γ} → Tmᵒ Δ (Aᵒ [ γ ]ᵀᵒ) → Sub Δ (Γ ▹ᵒ Aᵒ)
_,ᵒ_ γ {Aᵒ} aᵒ = _,_ {A = CTm [ Aᵒ ]ᵀ} γ aᵒ

pᵒ : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ} → Sub (Γ ▹ᵒ Aᵒ) Γ
pᵒ {Aᵒ = Aᵒ} = p {A = CTm [ Aᵒ ]ᵀ}

qᵒ : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ} → Tmᵒ (Γ ▹ᵒ Aᵒ) (Aᵒ [ pᵒ {Aᵒ = Aᵒ} ]ᵀᵒ)
qᵒ {Aᵒ = Aᵒ} = q {A = CTm [ Aᵒ ]ᵀ}

▹ᵒβ₁ : ∀{Γ Δ : Con i}{γ : Sub Δ Γ}{Aᵒ : Tyᵒ Γ}{aᵒ : Tmᵒ Δ (Aᵒ [ γ ]ᵀᵒ)}
     → pᵒ {Aᵒ = Aᵒ} ∘[ Γ ] (_,ᵒ_ γ {Aᵒ = Aᵒ} aᵒ) ~ γ
▹ᵒβ₁ = ~refl

▹ᵒβ₂ : ∀{Γ Δ : Con i}{γ : Sub Δ Γ}{Aᵒ : Tyᵒ Γ}{aᵒ : Tmᵒ Δ (Aᵒ [ γ ]ᵀᵒ)}
     → _[_]ᵗᵒ {Aᵒ = Aᵒ [ pᵒ {Aᵒ = Aᵒ} ]ᵀᵒ} (qᵒ {Aᵒ = Aᵒ}) (_,ᵒ_ γ {Aᵒ = Aᵒ} aᵒ) ~ aᵒ
▹ᵒβ₂ = ~refl

▹ᵒη : ∀{Γ : Con i}{Δ : Con i}{Aᵒ : Tyᵒ Γ}{γa : Sub Δ (Γ ▹ᵒ Aᵒ)}
      → (_,[_]_ (pᵒ {Aᵒ = Aᵒ} ∘ γa) {A = CTm [ Aᵒ ]ᵀ} {A' = CTm [ Aᵒ ∘ (pᵒ {Aᵒ = Aᵒ}) ∘ γa ]ᵀ} ~refl
                (_[_]ᵗᵒ {Aᵒ = Aᵒ [ pᵒ {Aᵒ = Aᵒ} ]ᵀᵒ} (qᵒ {Aᵒ = Aᵒ}) γa))
        ~ γa
▹ᵒη {Γ = Γ}{Δ = Δ}{Aᵒ = Aᵒ}{γa = γa} = cong (λ (x : Sub (Γ ▹ᵒ Aᵒ) (Γ ▹ᵒ Aᵒ)) → x ∘ γa) ▹-η

,ᵒ-∘ : {Γ Δ Θ : Con i} {Aᵒ : Tyᵒ Γ}
  (γ : Sub Δ Γ) (aᵒ : Tmᵒ Δ (Aᵒ [ γ ]ᵀᵒ)) (δ : Sub Θ Δ) →
  (_,ᵒ_  γ {Aᵒ} aᵒ) ∘ δ ~ (_,ᵒ_ (γ ∘ δ) {Aᵒ} (_[_]ᵗᵒ {Aᵒ = Aᵒ [ γ ]ᵀᵒ} aᵒ δ))
,ᵒ-∘ γ a δ = ~refl

Πᵒ : ∀ {Γ : Con i} → (A : Tyᵒ Γ) → (B : Tyᵒ (Γ ▹ᵒ A)) → Tyᵒ Γ
Πᵒ {Γ = Γ} A B =
  mkSub
    (λ {x} γ → Mₛ.Π
       (∣ A ∣ γ)
       (∣ B ∣ (γa γ)))
    (λ {x} γ {y} f → Mₛ.Π[] {γ = f}
      ◼ cong₂ Mₛ.Π (A .rel γ f) (B .rel (γa γ) (φ γ f) ◼ cong₂ (λ x → ∣ B ∣ {x}) (cong (Mₛ._▹_ y) (A .rel γ f)) (γaₑ γ f)))
  where
    e : ∀ {x} (γ : Yᶜ Γ x) {y} (f : Hom y (x Mₛ.▹ (∣ A ∣ γ)))
      → (∣ A ∣ γ) Mₛ.[ Mₛ.p ]T Mₛ.[ f ]T ~ (∣ A ∣ (γ |ᶜ (Mₛ.p Mₛ.∘ f)))
    e {x} γ {y} f = (Mₛ.[∘]T {γ = Mₛ.p} {δ = f}) ⁻¹ ◼ (A .rel γ (Mₛ.p Mₛ.∘ f))

    e₁ : ∀ {x} (γ : Yᶜ Γ x) {y} (f : Hom y (x Mₛ.▹ (∣ A ∣ γ))) {z} (g : Hom z y)
      → (coe (~to≡ (cong (Mₛ.Tm y) (e γ f))) (Mₛ.q Mₛ.[ f ]t)) Mₛ.[ g ]t ~ coe (~to≡ (cong (Mₛ.Tm z) (e γ (f Mₛ.∘ g)))) (Mₛ.q Mₛ.[ f Mₛ.∘ g ]t)
    e₁ {x} γ {y} f {z} g = cong₂ (λ A (a : Mₛ.Tm y A) → a Mₛ.[ g ]t) ((e γ f) ⁻¹) ((coe-eq _ (Mₛ.q Mₛ.[ f ]t)) ⁻¹)
      ◼ (Mₛ.[∘]t {γ = f} {δ = g}) ⁻¹
      ◼ coe-eq _ (Mₛ.q Mₛ.[ f Mₛ.∘ g ]t)

    e₂ : ∀ {x} (γ : Yᶜ Γ x) {y} (f : Hom y x) → y Mₛ.▹ (∣ A ∣ γ Mₛ.[ f ]T) ~ y Mₛ.▹ (∣ A ∣ (γ |ᶜ f))
    e₂ {x} γ {y} f = cong (Mₛ._▹_ y) (A .rel γ f)

    γa : ∀ {x} (γ : Yᶜ Γ x) → Yᶜ (Γ ▹ᵒ A) (x Mₛ.▹ (∣ A ∣ γ))
    γa {x} γ = (mkY (λ {y} f → mk▹ (γ |ᶜ (Mₛ.p Mₛ.∘ f)) (coe (~to≡ (cong (Mₛ.Tm y) (e γ f))) (Mₛ.q Mₛ.[ f ]t)))
                   (λ {y} f → mk▹rel ~refl (λ {z} g → e₁ γ f g)))

    φ : ∀ {x} (γ : Yᶜ Γ x) {y} (f : Hom y x) → Hom (y Mₛ.▹ ∣ A ∣ γ Mₛ.[ f ]T) (x Mₛ.▹ ∣ A ∣ γ)
    φ {x} γ {y} f = (f Mₛ.∘ Mₛ.p) Mₛ.,[ Mₛ.[∘]T {γ = f} {δ = Mₛ.p} ] Mₛ.q

    pφ : ∀ {x} (γ : Yᶜ Γ x) {y} (f : Hom y x)
      → (Mₛ.p {A = ∣ A ∣ γ}) Mₛ.∘ (φ γ f) ~ f Mₛ.∘ (Mₛ.p {A = ∣ A ∣ (γ |ᶜ f)})
    pφ {x} γ {y} f = (Additional.▹β₁-EX Mₛ) {e = Mₛ.[∘]T {γ = f} {δ = Mₛ.p}}
      ◼ cong₂ (λ z (g : Hom z y) → f Mₛ.∘ g) (e₂ γ f) (cong (λ A → Mₛ.p {A = A}) (A .rel γ f))

    qφ : ∀ {x} (γ : Yᶜ Γ x) {y} (f : Hom y x)
      → Mₛ.q {A = ∣ A ∣ γ} Mₛ.[ φ γ f ]t ~ Mₛ.q {A = ∣ A ∣ (γ |ᶜ f)}
    qφ {x} γ {y} f = (Additional.▹β₂-EX Mₛ {γ = f Mₛ.∘ Mₛ.p} {e = Mₛ.[∘]T {γ = f} {δ = Mₛ.p}})
      ◼ cong (λ A → Mₛ.q {A = A}) (A .rel γ f)

    γaₑ : ∀ {x} (γ : Yᶜ Γ x) {y} (f : Hom y x) → (γa γ) |ᶜ (φ γ f) ~ γa (γ |ᶜ f)
    γaₑ {x} γ {y} f = Yᶜ-~ (cong (Mₛ._▹_ y) (A .rel γ f))
      (funextᵢ λ {z₀} {z₁} zₑ →
        funext λ {g₀} {g₁} gₑ →
          cong₃ (λ x → mk▹ {x = x}) zₑ
                (cong₄ (λ z' (h : Hom z' x) z (g : Hom z z') → γ |ᶜ h |ᶜ g) (e₂ γ f) (pφ γ f) zₑ gₑ)
                ((coe-eq _ (Mₛ.q Mₛ.[ (φ γ f) Mₛ.∘ g₀ ]t)) ⁻¹
                  ◼ Mₛ.[∘]t {γ = φ γ f} {δ = g₀}
                  ◼ cong₅ (λ Γ A a Δ → Mₛ._[_]t {Γ = Γ} {A = A} a {Δ = Δ}) (e₂ γ f)
                          ((Mₛ.[∘]T {γ = Mₛ.p} {δ = φ γ f}) ⁻¹ ◼ A .rel γ (Mₛ.p Mₛ.∘ (φ γ f))
                            ◼ cong₂ (λ y (f : Hom y x) → ∣ A ∣ (γ |ᶜ f)) (e₂ γ f) (pφ γ f)
                            ◼ (A .rel (γ |ᶜ f) Mₛ.p) ⁻¹)
                          (qφ γ f) zₑ gₑ
                  ◼ coe-eq _ (Mₛ.q Mₛ.[ g₁ ]t)))

-- Yoneda is a strict functor from the strictified category to the category of prefascists

よ : Ob → Con lzero
よ x =
  record {
    Γ = λ y → Hom y x ;
    rel = λ {y} γ → ∀ {z} (f : Hom z y) → γ f ~ (γ {y} C.id) C.∘ f
  }

よ→ : ∀ {x y} → Hom y x → Sub (よ y) (よ x)
よ→ {x} {y} f = mkSub
  (λ {z} α → f C.∘ (∣ α ∣ C.id))
  (λ {z} α {z'} g → cong (λ (g : Hom z' y) → f C.∘ g) (α .rel C.id g))

よid : ∀ {x} → よ→ (C.id {x}) ~ id {Γ = よ x}
よid = ~refl

よ∘ : ∀ {x y z} {f : Hom y x} {g : Hom z y} → (よ→ f) ∘ (よ→ g) ~ (よ→ (f C.∘ g))
よ∘ = ~refl

record Iso (Γ : Con i) (Δ : Con j) : Set (i ⊔ j) where
  constructor mkIso
  field
    fun : Sub Γ Δ
    inv : Sub Δ Γ
    section : fun ∘ inv ~ id {Γ = Δ}
    retraction : inv ∘ fun ~ id {Γ = Γ}
open Iso public

id-iso : (Γ : Con i) → Iso Γ Γ
id-iso Γ .fun = id
id-iso Γ .inv = id
id-iso Γ .section = ~refl
id-iso Γ .retraction = ~refl

record よCon : Set₁ where
  constructor mkよCon
  field
    Γ : Con lzero
    ob : Ob
    iso : Iso Γ (よ ob)
open よCon public renaming (Γ to ∣_∣)

よ◇ : Iso ◇ (よ (Mₛ.◇))
よ◇ .fun = mkSub (λ _ → Mₛ.ε) λ γ f → Mₛ.◇η ⁻¹
よ◇ .inv = ε
よ◇ .section = Sub-≡ (funextHᵢᵣ λ {x} _ → Mₛ.◇η ⁻¹)
よ◇ .retraction = ◇-η

Hom-Yよ : ∀ {x y} → Hom y x → Yᶜ (よ x) y
Hom-Yよ f = mkY (λ g → f Mₛ.∘ g) (λ g h → ~refl)

Yよ-Hom : ∀ {x y} → Yᶜ (よ x) y → Hom y x
Yよ-Hom f = ∣ f ∣ Mₛ.id

Yよ-≡ : ∀ {x y} {f g : Yᶜ (よ x) y} → Yよ-Hom f ~ Yよ-Hom g → f ~ g
Yよ-≡ {f = f} {g = g} e = Yᶜ-≡ (funextHᵢᵣ λ h → f .rel Mₛ.id h ◼ cong (λ x → x Mₛ.∘ h) e ◼ (g .rel Mₛ.id h) ⁻¹)

toHom : ∀{x y} → Sub (よ x) (よ y) → Hom x y
toHom σ = ∣ σ ∣ (mkY (λ f → f) (λ f g → ~refl))

toHomid : ∀{x} → toHom (id {Γ = よ x}) ~ Mₛ.id {x}
toHomid = ~refl

_∘l_ : ∀ {x y z} → Hom y x → Yᶜ (よ y) z → Yᶜ (よ x) z
_∘l_ f g = Hom-Yよ (f Mₛ.∘ Yよ-Hom g)

Tyよ-Ty : ∀ {x} → Tyᵒ (よ x) → Mₛ.Ty x
Tyよ-Ty A = ∣ A ∣ (Hom-Yよ Mₛ.id)

Ty-Tyよ : ∀ {x} → Mₛ.Ty x → Tyᵒ (よ x)
Ty-Tyよ A = mkSub (λ {y} f → A Mₛ.[ Yよ-Hom f ]T)
                 (λ {y} f {z} g → (Mₛ.[∘]T {γ = Yよ-Hom f} {δ = g}) ⁻¹
                                  ◼ cong (λ x → A Mₛ.[ x ]T) ((f .rel Mₛ.id g) ⁻¹))

Tmよ-Tm : ∀ {x A} → Tmᵒ (よ x) A → Mₛ.Tm x (Tyよ-Ty A)
Tmよ-Tm a = ∣ a ∣ (Hom-Yよ Mₛ.id)

Tm-Tmよ : ∀ {x A} → Mₛ.Tm x A → Tmᵒ (よ x) (Ty-Tyよ A)
Tm-Tmよ {x} {A} a = mkTm (λ {y} f → a Mₛ.[ Yよ-Hom f ]t)
                        (λ {y} f {z} g → (Mₛ.[∘]t {γ = Yよ-Hom f} {δ = g}) ⁻¹
                                       ◼ cong (λ x → a Mₛ.[ x ]t) ((f .rel Mₛ.id g) ⁻¹))

Tyよ[Yよ] : ∀ {x : Ob} (A : Tyᵒ (よ x)) {y : Ob} (f : Yᶜ (よ x) y)
      → (Tyよ-Ty A) Mₛ.[ Yよ-Hom f ]T ~ ∣ A ∣ f
Tyよ[Yよ] {x} A {y} f = A .rel (Hom-Yよ Mₛ.id) (Yよ-Hom f) ◼ cong ∣ A ∣ (Yᶜ-≡ (funextHᵢᵣ λ {z} g → (f .rel C.id g) ⁻¹))

Tyよ-eq : ∀ {x y z} (A : Tyᵒ (よ x)) (g : Hom y x) (f : Yᶜ (よ y) z)
       → (Tyよ-Ty A) Mₛ.[ g ]T Mₛ.[ Yよ-Hom f ]T ~ (∣ A ∣ (g ∘l f))
Tyよ-eq A g f = (Mₛ.[∘]T {γ = g} {δ = Yよ-Hom f}) ⁻¹ ◼ A .rel (Hom-Yよ Mₛ.id) (g Mₛ.∘ Yよ-Hom f)

よ▹-iso : (x : Ob) (A : Tyᵒ (よ x)) → Iso (よ x ▹ᵒ A) (よ (x Mₛ.▹ (Tyよ-Ty A)))
∣ よ▹-iso x A .fun ∣ {y} f,a = (∣ ∣ f,a ∣ Mₛ.id .con ∣ Mₛ.id) Mₛ.,[ Tyよ[Yよ] A (∣ f,a ∣ Mₛ.id .con) ] (∣ f,a ∣ Mₛ.id .ty)
よ▹-iso x A .fun .rel {y} f,a {z} g =
  congᵣᵣᵣᵢᵣ (λ a b c d e → Mₛ._,[_]_ {x} {z} a {b} {c} d e)
    (~congᵢᵣ (~congᵢᵣ (f,a .rel Mₛ.id .con) ~refl (~refl {a = Mₛ.id})) ~refl (~refl {a = g}) ⁻¹ ◼ ∣ f,a ∣ Mₛ.id .con .rel Mₛ.id g)
    ~refl
    (cong ∣ A ∣ (Yᶜ-≡ (funextHᵢᵣ (λ g' → ~congᵢᵣ (~congᵢᵣ (f,a .rel Mₛ.id .con) ~refl (~refl {a = g})) ~refl (~refl {a = g'})
                                         ◼ (~congᵢᵣ (~congᵢᵣ (f,a .rel Mₛ.id .con) ~refl (~refl {a = C.id})) ~refl (~refl {a = g Mₛ.∘ g'})) ⁻¹)))
      ◼ (A .rel (∣ f,a ∣ Mₛ.id .con) g) ⁻¹)
    {d₀ = Tyよ[Yよ] A (∣ f,a ∣ g .con)}
    {d₁ = Mₛ.[∘]T {γ = ∣ ∣ f,a ∣ Mₛ.id .con ∣ Mₛ.id} {δ = g} ◼ cong (λ A → A Mₛ.[ g ]T) (Tyよ[Yよ] A (∣ f,a ∣ Mₛ.id .con))}
    (coe-eq _ (∣ f,a ∣ g .ty) ◼ (f,a .rel Mₛ.id .ty g) ⁻¹
      ◼ cong₂ (λ A (a : Mₛ.Tm y A) → a Mₛ.[ g ]t) (cong ∣ A ∣ (Yᶜ-≡ ((~congᵢᵣ (f,a .rel Mₛ.id .con) ~refl (~refl {a = Mₛ.id})) ⁻¹)))
              ((coe-eq _ (∣ f,a ∣ Mₛ.id .ty)) ⁻¹) )
  ◼ (Additional.,[∘] Mₛ (∣ ∣ f,a ∣ Mₛ.id .con ∣ Mₛ.id)
      (A .rel (Hom-Yよ Mₛ.id) (∣ ∣ f,a ∣ Mₛ.id .con ∣ Mₛ.id) ◼ cong ∣ A ∣ (Yᶜ-≡ (funextHᵢᵣ λ g' → ∣ f,a ∣ Mₛ.id .con .rel Mₛ.id g' ⁻¹)) )
      (∣ f,a ∣ Mₛ.id .ty) g) ⁻¹
∣ よ▹-iso x A .inv ∣ {y} f,a .con = Mₛ.p ∘l f,a
∣ よ▹-iso x A .inv ∣ {y} f,a .ty = coe (~to≡ (cong (M.Tm y) (Tyよ-eq A Mₛ.p f,a))) (Mₛ.q Mₛ.[ Yよ-Hom f,a ]t)
よ▹-iso x A .inv .rel {y} f,a = mk▹rel
  (funextHᵢᵣ λ g → funextHᵢᵣ λ g' → (cong (λ f → Mₛ.p Mₛ.∘ f) (f,a .rel g g')) ⁻¹)
  (λ {z} g → cong₂ (λ A (x : Mₛ.Tm _ A) → x Mₛ.[ g ]t)
                   (cong ∣ A ∣ (Yᶜ-≡ (funextHᵢᵣ (λ g' → cong (λ f → Mₛ.p Mₛ.∘ f) (f,a .rel Mₛ.id g')))) ◼ (Tyよ-eq A Mₛ.p f,a) ⁻¹)
                   (coe-eq _ (coe (~to≡ (cong (M.Tm y) (Tyよ-eq A Mₛ.p f,a))) (Mₛ.q Mₛ.[ Yよ-Hom f,a ]t)) ⁻¹
                    ◼ (coe-eq _ (Mₛ.q Mₛ.[ Yよ-Hom f,a ]t)) ⁻¹)
         ◼ (Mₛ.[∘]t {γ = Yよ-Hom f,a} {δ = g}) ⁻¹
         ◼ cong (λ f → Mₛ.q Mₛ.[ f ]t) ((f,a .rel Mₛ.id g) ⁻¹)
         ◼ coe-eq _ (Mₛ.q Mₛ.[ Yよ-Hom (f,a |ᶜ g) ]t)
         ◼ coe-eq _ (coe (~to≡ (cong (M.Tm z) (Tyよ-eq A Mₛ.p (f,a |ᶜ g)))) (Mₛ.q Mₛ.[ Yよ-Hom (f,a |ᶜ g) ]t)))
よ▹-iso x A .section = Sub-≡ (funextHᵢᵣ λ {y} f →
  congᵣᵣᵣᵢᵣ (λ a b c d e → Mₛ._,[_]_ {_} {_} a {b} {c} d e)
            {a₀ = ∣ Mₛ.p ∘l f ∣ Mₛ.id} {a₁ = Mₛ.p Mₛ.∘ ∣ f ∣ C.id}
            ~refl ~refl ((Tyよ-eq A Mₛ.p f) ⁻¹) {d₀ = Tyよ[Yよ] A (Mₛ.p ∘l f)} {d₁ = Mₛ.[∘]T {γ = Mₛ.p} {δ = ∣ f ∣ C.id}}
            ((coe-eq _ (Mₛ.q Mₛ.[ Yよ-Hom (f |ᶜ Mₛ.id) ]t)) ⁻¹) ◼ Mₛ.▹η)
よ▹-iso x A .retraction = Sub-≡ (funextHᵢᵣ λ {y} f →
  cong₂ mk▹ (Yよ-≡ (Additional.▹β₁-EX Mₛ {e = Tyよ[Yよ] A (con (∣ f ∣ C.id))}))
            ((coe-eq _ (Mₛ.q Mₛ.[ Yよ-Hom (Yˢ (fun (よ▹-iso x A)) f) ]t)) ⁻¹
              ◼ Additional.▹β₂-EX Mₛ {γ = Yよ-Hom (con (∣ f ∣ C.id))} {e = Tyよ[Yよ] A (con (∣ f ∣ C.id))}))

iso-▹ : {Γ Δ : Con i} (H : Iso Δ Γ) (A : Tyᵒ Δ) → Iso (Δ ▹ᵒ A) (Γ ▹ᵒ (A ∘ H .inv))
iso-▹ H A .fun = ((H .fun) ∘ p) ,[ cong (λ f → CTm [ A ∘ f ∘ (p {A = CTm [ A ]ᵀ}) ]ᵀ) {a = inv H ∘ fun H} {b = id} (H .retraction) ]
                 (q {A = CTm [ A ]ᵀ})
iso-▹ H A .inv = ((H .inv) ∘ p) ,[ ~refl ] (q {A = CTm [ A ∘ H .inv ]ᵀ})
iso-▹ {Γ = Γ} H A .section =
  congᵣᵣᵣᵢᵣ (λ a b c d e → _,[_]_ {_} {_} (a ∘ p {A = CTm [ A ∘ H .inv ]ᵀ}) {b} {c} d e)
            (H .section) ~refl ~refl
            {d₀ = cong (λ f → CTm [ A ∘ inv H ∘ f ∘ p {A = CTm [ A ∘ H .inv ]ᵀ} ]ᵀ) (H .section)} {d₁ = ~refl}
            (~refl {a = q {A = CTm [ A ∘ H .inv ]ᵀ}})
  ◼ ▹-η
iso-▹ H A .retraction =
  congᵣᵣᵣᵢᵣ (λ a b c d e → _,[_]_ {_} {_} (a ∘ p {A = CTm [ A ]ᵀ}) {b} {c} d e)
            (H .retraction) ~refl ~refl
            {d₀ = cong (λ f → CTm [ A ∘ f ∘ p {A = CTm [ A ]ᵀ} ]ᵀ) (H .retraction)} {d₁ = ~refl}
            (~refl {a = q {A = CTm [ A ]ᵀ}})
  ◼ ▹-η

iso-trans : {Γ Δ Θ : Con i} → Iso Γ Δ → Iso Δ Θ → Iso Γ Θ
iso-trans f g .fun = (g .fun) ∘ (f .fun)
iso-trans f g .inv = (f .inv) ∘ (g .inv)
iso-trans f g .section = cong (λ f → (g .fun) ∘ f ∘ (g .inv)) (f .section) ◼ (g .section)
iso-trans f g .retraction = cong (λ g → (f .inv) ∘ g ∘ (f .fun)) (g .retraction) ◼ (f .retraction)

_よ▹_ : (Γ : よCon) → Tyᵒ ∣ Γ ∣ → よCon
Γ よ▹ A = mkよCon (∣ Γ ∣ ▹ᵒ A) ((Γ .ob) Mₛ.▹ (Tyよ-Ty (A ∘ Γ .iso .inv))) (iso-trans (iso-▹ (Γ .iso) A) (よ▹-iso (Γ .ob) (A ∘ Γ .iso .inv)))

Mₛₛ : Model {lsuc lzero}{lzero}{lzero}{lzero}
Mₛₛ = record
       { Con = よCon
       ; Sub = λ Γ Δ → Sub ∣ Γ ∣ ∣ Δ ∣
       ; _∘_ = λ {_} {_} γ {_} δ → γ ∘ δ
       ; ass = ~refl
       ; id = id
       ; idl = ~refl
       ; idr = ~refl
       ; ◇ = mkよCon ◇ Mₛ.◇ よ◇
       ; ε = ε
       ; ◇η = ~refl
       ; Ty = λ Γ → Tyᵒ ∣ Γ ∣
       ; _[_]T = λ {_} A {_} γ → A [ γ ]ᵀᵒ
       ; [∘]T = ~refl
       ; [id]T = ~refl
       ; Tm = λ Γ A → Tmᵒ ∣ Γ ∣ A
       ; _[_]t = λ {_}{Aᵒ} a {_} γ → _[_]ᵗᵒ {Aᵒ = Aᵒ} a γ
       ; [∘]t = ~refl
       ; [id]t = ~refl
       ; _▹_ = λ Γ A → Γ よ▹ A
       ; _,[_]_ = λ {_}{Δ} γ {A}{A'} e a → γ ,[ cong (λ (γ : Tyᵒ ∣ Δ ∣) → CTm [ γ ]ᵀ) e ] a
       ; p = λ {_}{Aᵒ} → pᵒ {Aᵒ = Aᵒ}
       ; q = λ {_}{Aᵒ} → qᵒ {Aᵒ = Aᵒ}
       ; ▹β₁ = ~refl
       ; ▹β₂ = ~refl
       ; ▹η = λ {_}{_}{Aᵒ}{_} → ▹ᵒη {Aᵒ = Aᵒ} -- not reflexivity!
       ; Π = λ {Γ} A B → Πᵒ A B
       ; Π[] = ~refl
       }

module Mₛₛ = Model Mₛₛ

fCon : Mₛₛ.Con → M.Con
fCon = ob
fSub : ∀{Δ Γ} → Mₛₛ.Sub Δ Γ → M.Sub (fCon Δ) (fCon Γ)
fSub {Δ}{Γ} γ = strictifyCat.Sub.γ (toHom (fun (iso Γ) ∘ γ ∘ inv (iso Δ))) M.id
fTy : ∀{Γ} → Mₛₛ.Ty Γ → M.Ty (fCon Γ)
fTy {Γ} A = Tyよ-Ty (A ∘ inv (iso Γ))
fTm : ∀{Γ A} → Mₛₛ.Tm Γ A → M.Tm (fCon Γ) (fTy {Γ} A)
fTm {Γ}{A} t = Tmよ-Tm {fCon Γ}{A ∘ inv (iso Γ)} (t [ inv (iso Γ) ]ᵗ)
-- f∘ : ∀{Δ Γ Θ}{γ : Mₛₛ.Sub Δ Γ}{δ : Mₛₛ.Sub Θ Δ} → fSub {Θ}{Γ} (Mₛₛ._∘_ {Γ}{Δ} γ {Θ} δ) ~ fSub {Δ}{Γ} γ M.∘ fSub {Θ}{Δ} δ
-- f∘ = {!!}
-- fid : ∀{Γ} → fSub {Γ}{Γ} (Mₛₛ.id {Γ}) ~ M.id {fCon Γ}
-- fid {Γ} = {!!}
f◇ : fCon Mₛₛ.◇ ~ M.◇
f◇ = ~refl
f▹ : ∀{Γ A} → fCon (Γ Mₛₛ.▹ A) ~ fCon Γ M.▹ fTy {Γ} A
f▹ = ~refl

Conₛₛ : M.Con → Mₛₛ.Con
Conₛₛ Γ = mkよCon (よ Γ) Γ (id-iso (よ Γ))

Subₛₛ : ∀ {Δ Γ} → M.Sub Δ Γ → Mₛₛ.Sub (Conₛₛ Δ) (Conₛₛ Γ)
Subₛₛ f = よ→ (strictifyCat.toSub f)

∘ₛₛ : ∀ {Γ Δ Θ} {f : M.Sub Δ Γ} {g : M.Sub Θ Δ} → Subₛₛ (f M.∘ g) ~ (Subₛₛ f) ∘ (Subₛₛ g)
∘ₛₛ {f = f} {g = g} = cong よ→ (strictifyCat.toSub∘ {f = f} {g = g}) -- 

idₛₛ : ∀ {Γ} → Subₛₛ (M.id {Γ = Γ}) ~ Mₛₛ.id {Conₛₛ Γ}
idₛₛ {Γ = Γ} = cong よ→ (strictifyCat.toSub-id {Γ = Γ})

Tyₛₛ : ∀ {Γ} → M.Ty Γ → Mₛₛ.Ty (Conₛₛ Γ)
Tyₛₛ A = Ty-Tyよ A

Tmₛₛ : ∀ {Γ} {A : M.Ty Γ} → M.Tm Γ A → Mₛₛ.Tm (Conₛₛ Γ) (Tyₛₛ A)
Tmₛₛ a = Tm-Tmよ a
