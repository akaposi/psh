{-# OPTIONS --prop --rewriting #-}

module initial where

open import lib
open import model
open import depModel

-- Initial Model

postulate
  Ci : CwF {lzero}{lzero}{lzero}{lzero}
  Mi : Model {lzero}{lzero}{lzero}{lzero} Ci

module initElim {i j k l : Level} (C : DepCwF {lzero}{i}{lzero}{j}{lzero}{k}{lzero}{l} Ci)
                                  (M : DepModel {lzero}{i}{lzero}{j}{lzero}{k}{lzero}{l} Ci C Mi) where
  open CwF Ci
  open Model Mi
  open DepCwF C
  open DepCwF-tools Ci C
  open DepModel M
  postulate
    elimCon : (Γ : Con) → Con∙ Γ
    elimSub : ∀{Δ Γ}(γ : Sub Δ Γ) → Sub∙ (elimCon Δ) (elimCon Γ) γ
    elimTy  : ∀{Γ}(A : Ty Γ) → Ty∙ (elimCon Γ) A
    elimTm  : ∀{Γ A}(a : Tm Γ A) → Tm∙ (elimCon Γ) (elimTy A) a

    elim◇   : elimCon ◇ ≡ ◇∙
    elim▹   : ∀{Γ A} → elimCon (Γ ▹ A) ≡ elimCon Γ ▹∙ elimTy A
    elim∘   : ∀{Γ Δ Θ} {f : Sub Δ Γ} {g : Sub Θ Δ} → elimSub (f ∘ g) ≡ (elimSub f) ∘∙ (elimSub g)
    elim-id : ∀{Γ} → elimSub (id {Γ}) ≡ id∙
    elim[]T : ∀{Γ Δ}{f : Sub Δ Γ}{A : Ty Γ} → elimTy (A [ f ]T) ≡ (elimTy A) [ elimSub f ]T∙

  {-# REWRITE elim◇ elim▹ elim∘ elim-id elim[]T #-}

  postulate
    elimε   : ∀ {Γ} → elimSub (ε {Γ}) ≡ ε∙
    elim[]t : ∀{Γ Δ}{f : Sub Δ Γ}{A : Ty Γ}{a : Tm Γ A} → elimTm (a [ f ]t) ≡ (elimTm a) [ elimSub f ]t∙
    elim,[] : ∀{Γ Δ}{f : Sub Δ Γ}{A : Ty Γ}{A' : Ty Δ}{a : Tm Δ A'}{e : A [ f ]T ~ A'}
           → elimSub (f ,[ e ] a) ≡ (elimSub f) ,[ e ][ cong (elimTy {Δ}) e ]∙ (elimTm a)
    elim-p  : ∀{Γ}{A : Ty Γ} → elimSub (p {Γ}{A}) ≡ p∙
    elimΠ   : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)} → elimTy (Π A B) ≡ Π∙ (elimTy A) (elimTy B)
    elim𝔹   : elimTy 𝔹 ≡ 𝔹∙

  {-# REWRITE elimε elim[]t elim,[] elim-p elimΠ elim𝔹 #-}

  postulate
    elim-q   : ∀{Γ}{A : Ty Γ} → elimTm (q {Γ}{A}) ≡ q∙
    elim-lam : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{t : Tm (Γ ▹ A) B} → elimTm (lam A {B} t) ≡ lam∙ (elimTy A) (elimTm t)
    elim-app : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{t : Tm Γ (Π A B)}{u : Tm Γ A} → elimTm (app t u) ≡ app∙ (elimTm t) (elimTm u)
    elim-𝕥   : elimTm 𝕥 ≡ 𝕥∙
    elim-𝕗   : elimTm 𝕗 ≡ 𝕗∙
    elim-ifᵀ : ∀{Γ}{b : Tm Γ (𝔹 [ ε ]T)}{A B : Ty Γ} → elimTy (ifᵀ b A B) ≡ ifᵀ∙ (elimTm b) (elimTy A) (elimTy B)

  {-# REWRITE elim-q elim-lam elim-app elim-𝕥 elim-𝕗 elim-ifᵀ #-}

  postulate
    elim-ifᵗ : ∀{Γ}{P : Ty (Γ ▹ 𝔹 [ ε ]T)}{P𝕥 : Tm Γ (P [ CwF-tools.sg Ci (𝕥 [ ε ]t) ]T)}
                   {P𝕗 : Tm Γ (P [ CwF-tools.sg Ci (𝕗 [ ε ]t) ]T)}{b : Tm Γ (𝔹 [ ε ]T)}
               → elimTm (ifᵗ P P𝕥 P𝕗 b) ≡ ifᵗ∙ (elimTy P) (elimTm P𝕥) (elimTm P𝕗) (elimTm b)

  {-# REWRITE elim-ifᵗ #-}

-- nondependent eliminator (also called iterator)

module initIte {i j k l : Level} (C : CwF {i}{j}{k}{l})(M : Model C) where
  open CwF Ci
  open Model Mi
  private module C = CwF C
  private module M = Model M

  toDepCwF : DepCwF Ci
  toDepCwF = record
       { Con∙ = λ _ → C.Con
       ; Sub∙ = λ Δ Γ _ → C.Sub Δ Γ
       ; _∘∙_ = λ γ δ → γ C.∘ δ
       ; ass∙ = C.ass
       ; id∙ = C.id
       ; idl∙ = C.idl
       ; idr∙ = C.idr
       ; ◇∙ = C.◇
       ; ε∙ = C.ε
       ; ◇η∙ = C.◇η
       ; Ty∙ = λ Γ _ → C.Ty Γ
       ; _[_]T∙ = λ A γ → A C.[ γ ]T
       ; [∘]T∙ = C.[∘]T
       ; [id]T∙ = C.[id]T
       ; Tm∙ = λ Γ A _ → C.Tm Γ A
       ; _[_]t∙ = λ a γ → a C.[ γ ]t
       ; [∘]t∙ = C.[∘]t
       ; [id]t∙ = C.[id]t
       ; _▹∙_ = λ Γ A → Γ C.▹ A
       ; _,[_][_]∙_ = λ γ _ e a → γ C.,[ e ] a
       ; p∙ = C.p
       ; q∙ = C.q
       ; ▹β₁∙ = C.▹β₁
       ; ▹β₂∙ = C.▹β₂
       ; ▹η∙ = C.▹η
       }

  toDepModel : DepModel Ci toDepCwF Mi
  toDepModel = record
          { Π∙ = λ A B → M.Π A B
          ; Π[]∙ = M.Π[]
          ; lam∙ = λ A t → M.lam A t
          ; lam[]∙ = M.lam[]
          ; app∙ = λ t u → M.app t u
          ; app[]∙ = M.app[]
          ; Πβ∙ = M.Πβ
          ; Πη∙ = M.Πη
          ; 𝔹∙ = M.𝔹
          ; 𝕥∙ = M.𝕥
          ; 𝕗∙ = M.𝕗
          ; ifᵀ∙ = λ b A B → M.ifᵀ b A B
          ; ifᵀ[]∙ = M.ifᵀ[]
          ; ifᵀβ₁∙ = M.ifᵀβ₁
          ; ifᵀβ₂∙ = M.ifᵀβ₂
          ; ifᵗ∙ = λ P P𝕥 P𝕗 b → M.ifᵗ P P𝕥 P𝕗 b
          ; ifᵗ[]∙ = M.ifᵗ[]
          ; ifᵗβ₁∙ = M.ifᵗβ₁
          ; ifᵗβ₂∙ = M.ifᵗβ₂
          }

  private module toDepCwF = DepCwF toDepCwF
  private module toDepModel = DepModel toDepModel
  open initElim

  iteCon : (Γ : Con) → C.Con
  iteCon = elimCon toDepCwF toDepModel
  iteSub : ∀{Δ Γ}(γ : Sub Δ Γ) → C.Sub (iteCon Δ) (iteCon Γ)
  iteSub = elimSub toDepCwF toDepModel
  iteTy  : ∀{Γ}(A : Ty Γ) → C.Ty (iteCon Γ)
  iteTy = elimTy toDepCwF toDepModel
  iteTm  : ∀{Γ A}(a : Tm Γ A) → C.Tm (iteCon Γ) (iteTy A)
  iteTm = elimTm toDepCwF toDepModel

  ite◇   : iteCon ◇ ≡ C.◇
  ite◇   = ≡refl
  ite▹   : ∀{Γ A} → iteCon (Γ ▹ A) ≡ iteCon Γ C.▹ iteTy A
  ite▹   = ≡refl
  ite-id : ∀{Γ} → iteSub (id {Γ}) ≡ C.id
  ite-id = ≡refl
  ite[]T : ∀{Γ Δ}{f : Sub Δ Γ}{A : Ty Γ} → iteTy (A [ f ]T) ≡ (iteTy A) C.[ iteSub f ]T
  ite[]T = ≡refl

  iteε   : ∀ {Γ} → iteSub (ε {Γ}) ≡ C.ε
  iteε   = ≡refl
  ite[]t : ∀{Γ Δ}{f : Sub Δ Γ}{A : Ty Γ}{a : Tm Γ A} → iteTm (a [ f ]t) ≡ (iteTm a) C.[ iteSub f ]t
  ite[]t = ≡refl
  ite,[] : ∀{Γ Δ}{f : Sub Δ Γ}{A : Ty Γ}{A' : Ty Δ}{a : Tm Δ A'}{e : A [ f ]T ~ A'}
         → iteSub (f ,[ e ] a) ≡ (iteSub f) C.,[ cong (iteTy {Δ}) e ] (iteTm a)
  ite,[] = ≡refl
  ite-p  : ∀{Γ}{A : Ty Γ} → iteSub (p {Γ}{A}) ≡ C.p
  ite-p  = ≡refl
  ite-q  : ∀{Γ}{A : Ty Γ} → iteTm (q {Γ}{A}) ≡ C.q
  ite-q  = ≡refl

  iteΠ    : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)} → iteTy (Π A B) ≡ M.Π (iteTy A) (iteTy B)
  iteΠ    = ≡refl
  ite-lam : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{t : Tm (Γ ▹ A) B} → iteTm (lam A t) ≡ M.lam (iteTy A) (iteTm t)
  ite-lam = ≡refl
  ite-app : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{t : Tm Γ (Π A B)}{u : Tm Γ A} → iteTm (app t u) ≡ M.app (iteTm t) (iteTm u)
  ite-app = ≡refl

  ite𝔹    : iteTy 𝔹 ≡ M.𝔹
  ite𝔹    = ≡refl
  ite-𝕥   : iteTm 𝕥 ≡ M.𝕥
  ite-𝕥   = ≡refl
  ite-𝕗   : iteTm 𝕗 ≡ M.𝕗
  ite-𝕗   = ≡refl
  ite-ifᵀ : ∀{Γ}{b : Tm Γ (𝔹 [ ε ]T)}{A B : Ty Γ} → iteTy (ifᵀ b A B) ≡ M.ifᵀ (iteTm b) (iteTy A) (iteTy B)
  ite-ifᵀ = ≡refl
  ite-ifᵗ : ∀{Γ}{P : Ty (Γ ▹ (𝔹 [ ε ]T))}{P𝕥 : Tm Γ (P [ CwF-tools.sg Ci (𝕥 [ ε ]t) ]T)}
            {P𝕗 : Tm Γ (P [ CwF-tools.sg Ci (𝕗 [ ε ]t) ]T)}{b : Tm Γ (𝔹 [ ε ]T)}
            → iteTm (ifᵗ P P𝕥 P𝕗 b) ≡ M.ifᵗ (iteTy P) (iteTm P𝕥) (iteTm P𝕗) (iteTm b)
  ite-ifᵗ = ≡refl
