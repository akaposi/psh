{-# OPTIONS --prop #-}

-- list of tricks:
-- * pointfree
-- * parametricity definition of naturality and type restriction
-- * Yoneda
-- * equation in SProp
-- * local universe

open import Agda.Primitive
open import Cat

postulate
  C' : Cat
C : Cat
C = strictify C'
open Cat.Cat C

infix 4 _≡_
infixl 5 _,Σ_
data _≡_ {i}{A : Set i}(a : A) : A → Prop i where
  refl : a ≡ a
record ⊤ : Set where
record ⊤p : Prop where
record Σ {i}(A : Set i){j}(B : A → Set j) : Set (i ⊔ j) where
  constructor _,Σ_
  field
    π₁ : A
    π₂ : B π₁
open Σ
-- ,= : ∀{i}{A : Set i}{j}{B : A → Set j}{a₀ a₁ : A}(a₌ : a₀ ≡ a₁){b₀ : B a₀}{b₁ : B a₁} → b₀ ≡ b₁
record Lift {i}(A : Prop i) : Set i where
  constructor mk
  field un : A
open Lift
cong : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong f refl = refl
_◾_ : ∀{i}{A : Set i}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
refl ◾ refl = refl

record Con : Set₁ where
  field
    Γ    : Ob → Set
    _[_] : ∀{I} → Γ I → ∀{J} → Hom J I → Γ J
    [∘]  : ∀{I}(γI : Γ I){J}(f : Hom J I){K}(g : Hom K J) → γI [ f ∘c g ] ≡ γI [ f ] [ g ] -- TODO: change this to pointfree, then we try2fix should be fine
    [id] : ∀{I}(γI : Γ I) → γI [ idc ] ≡ γI
  infix 8 _[_]
open Con renaming (Γ to ∣_∣; _[_] to _∶_[_])

◆ : Con
∣_∣ ◆ I = ⊤
_∶_[_] ◆ _ _ = _
[∘] ◆ _ _ _ = refl
[id] ◆ _ = refl

yOb : Ob → Con
∣ yOb I ∣ J = Hom J I
yOb I ∶ f [ g ] = f ∘c g
[∘] (yOb I) f g h = refl
[id] (yOb I) f = refl

-- the category of presheaves
module A where
  record Sub (Δ Γ : Con) : Set where
    field
      γ   : ∀{I} → ∣ Δ ∣ I → ∣ Γ ∣ I
      nat : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I){δJ : ∣ Δ ∣ J} → Δ ∶ δI [ f ] ≡ δJ → Γ ∶ γ δI [ f ] ≡ γ δJ -- TODO: we don't need the tricky version, but we do need the point-free version
  open Sub renaming (γ to ∣_∣) public

  Sub= : {Γ Δ : Con} →
    {γ₀ γ₁ : ∀{I} → ∣ Δ ∣ I → ∣ Γ ∣ I}(γ₂ : (λ {I} → γ₀ {I}) ≡ γ₁) 
    {nat₀ : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I){δJ : ∣ Δ ∣ J} → Δ ∶ δI [ f ] ≡ δJ → Γ ∶ γ₀ δI [ f ] ≡ γ₀ δJ}
    {nat₁ : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I){δJ : ∣ Δ ∣ J} → Δ ∶ δI [ f ] ≡ δJ → Γ ∶ γ₁ δI [ f ] ≡ γ₁ δJ} →
    _≡_ {A = Sub Δ Γ} (record { γ = γ₀ ; nat = nat₀ }) (record { γ = γ₁ ; nat = nat₁ })
  Sub= refl = refl

  yHom : ∀{J I} → Hom J I → Sub (yOb J) (yOb I)
  ∣ yHom f ∣ g = f ∘c g
  nat (yHom f) g e = {!!}

  _∘_ : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
  ∣ γ ∘ δ ∣ θI = ∣ γ ∣ (∣ δ ∣ θI)
  nat (γ ∘ δ) f δf = nat γ f (nat δ f δf)

  id : ∀{Γ} → Sub Γ Γ
  ∣ id ∣ γI = γI
  nat id f γf = γf

  ass : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{Ξ}{θ : Sub Ξ Θ} → (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
  ass = refl

  idl : ∀{Γ Δ}{γ : Sub Δ Γ} → id ∘ γ ≡ γ
  idl = refl

  idr : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘ id ≡ γ
  idr = refl

  ε : ∀{Γ} → Sub Γ ◆
  ∣ ε ∣ = _
  nat ε _ _ = refl

-- strictification of a context which is equivalent by Yoneda lemma
⟨_⟩ : Con → Con
∣ ⟨ Γ ⟩ ∣ I = A.Sub (yOb I) Γ
⟨ Γ ⟩ ∶ γI [ f ]  = γI A.∘ A.yHom f
[∘] ⟨ Γ ⟩ _ _ _ = refl
[id] ⟨ Γ ⟩ _ = refl

Sub : Con → Con → Set
Sub Δ Γ = A.Sub ⟨ Δ ⟩ ⟨ Γ ⟩

_∘_ : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
γ ∘ δ = γ A.∘ δ

id : ∀{Γ} → Sub Γ Γ
id = A.id

ass : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{Ξ}{θ : Sub Ξ Θ} → (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
ass = refl

idl : ∀{Γ Δ}{γ : Sub Δ Γ} → id ∘ γ ≡ γ
idl = refl

idr : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘ id ≡ γ
idr = refl

ε : ∀{Γ} → Sub Γ ◆
A.Sub.γ (A.Sub.γ ε _) _ = _
A.Sub.nat (A.Sub.γ ε _) _ _ = refl
A.Sub.nat ε _ _ = refl

◆η : ∀{Γ}(σ : Sub Γ ◆) → σ ≡ ε
◆η _ = refl

yHom : ∀{J I} → Hom J I → Sub (yOb J) (yOb I)
A.Sub.γ (A.Sub.γ (yHom f) ⟨g⟩) h = f ∘c A.∣ ⟨g⟩ ∣ idc ∘c h
A.Sub.nat (A.Sub.γ (yHom f) ⟨g⟩) {_}{h} i {j} e = cong (λ z → f ∘c A.∣ ⟨g⟩ ∣ idc ∘c z) e
A.Sub.nat (yHom f) g {⟨h⟩} e = A.Sub= {!refl!} ◾ cong A.∣ yHom f ∣ e

yl : ∀{Γ I} → ∣ Γ ∣ I → Sub (yOb I) Γ
A.∣ A.∣ yl {Γ} γI ∣ ⟨f⟩ ∣ g = Γ ∶ γI [ A.∣ ⟨f⟩ ∣ idc ∘c g ]
A.nat (A.∣ yl {Γ} γI ∣ ⟨f⟩) = {!!}
A.nat (yl {Γ} γI) = {!!}

y∘ : ∀{Γ : Con}{I}{γI : ∣ Γ ∣ I}{J}{f : Hom J I} → yl {Γ} γI ∘ yHom f ≡ yl (Γ ∶ γI [ f ])
y∘ = A.Sub= {!!}

record Ty (Γ : Con) : Set₁ where
  field
    A     : (I : Ob) → ∣ ⟨ Γ ⟩ ∣ I → Set
    _[_]  : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I} → A I γI → ∀{J}(f : Hom J I) → A J (⟨ Γ ⟩ ∶ γI [ f ])
    [∘]   : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I}(aI : A I γI){J}(f : Hom J I){K}(g : Hom K J) → aI [ f ∘c g ] ≡ (aI [ f ]) [ g ]
    [id]  : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I}(aI : A I γI) → aI [ idc ] ≡ aI
  infix 8 _[_]
open Ty renaming (A to ∣_∣; _[_] to _∶_[_]T)
