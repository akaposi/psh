{-# OPTIONS --without-K #-}

module Cat where

open import Lib

record Cat : Set₁ where
  infixl 6  _∘c_
  field
    Ob   : Set
    Hom  : Ob → Ob → Set
    idc  : {I : Ob} → Hom I I
    _∘c_ : {I J K : Ob} → Hom J K → Hom I J → Hom I K
    idcl : {I J : Ob}{f : Hom I J} → idc ∘c f ≡ f
    idcr : {I J : Ob}{f : Hom I J} → f ∘c idc ≡ f
    assc : {K L : Ob}{h : Hom K L}{J : Ob}{g : Hom J K}{I : Ob}{f : Hom I J} →
           (h ∘c g) ∘c f ≡ h ∘c (g ∘c f)

strictify : Cat → Cat
strictify record {
  Ob   = Ob  ;
  Hom  = Hom ;
  idc  = idc  ;
  _∘c_ = _∘c_ ;
  idcl = idcl ;
  idcr = idcr ;
  assc = assc } = record {
  Ob  = Ob ;
  Hom = λ I J → Σ
    ({X : Ob} → Hom I X → Hom J X) λ α →
    {X₀ X₁ : Ob}{f : Hom  X₀ X₁}{g : Hom I X₀}{g' : Hom I X₁} →
      f ∘c g ≡ g' → f ∘c α g ≡ α g' ;
  idc  = (λ g → g) ,Σ (λ e → e) ;
  _∘c_ = λ { (α ,Σ β) (α' ,Σ β') → (λ g → α (α' g)) ,Σ (λ e → β (β' e)) } ;
  idcl = refl ;
  idcr = refl ;
  assc = refl }
