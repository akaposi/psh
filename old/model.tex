\documentclass[10pt, a4paper]{article}
\usepackage{geometry}
\geometry{left=3cm,right=3cm,top=1.5cm,bottom=1.5cm}
\usepackage[fleqn]{amsmath}
\usepackage{amssymb}
\usepackage{newunicodechar}
\newunicodechar{→}{$\to$}
\newunicodechar{Π}{$\Pi$}
\newunicodechar{β}{$\beta$}
\newunicodechar{η}{$\eta$}

\input{abbrevs.tex}

\allowdisplaybreaks

\begin{document}

\title{HOAS in type theory}

\maketitle

Model of type theory:
\begin{alignat*}{5}
  & \rlap{$\C$ is a category:} \\
  & |\C| && :\,\, && \Set \\
  & \C(\blank,\blank) && : && |\C|\ra|\C|\ra\Set \\
  & \id_\C && : && \C(\Gamma,\Gamma) \\
  & \blank\circ_\C\blank && : && \C(\Gamma,\Theta)\ra\C(\Theta,\Delta)\ra\C(\Gamma,\Delta) \\
  & \idl_\C && : && \id_\C\circ_\C\sigma = \sigma \\
  & \idr_\C && : && \sigma\circ_\C\id_\C = \sigma \\
  & \ass_\C && : && (\sigma\circ_\C\delta)\circ_\C\nu = \sigma\circ_C(\delta\circ_C\nu) \\
  & \rlap{$\C$ has a terminal object:} \\
  & \cdot_\C && : && |\C| \\
  & \epsilon_\C && : && \C(\Gamma,\cdot_\C) \\
  & {\cdot\eta}_\C && : && \sigma = \epsilon_\C \\
  & \rlap{$\C$ is a model of type theory (CwF):} \\
  & \Ty_\C && :\,\, && |\C|\ra\Set \\
  & \Tm_\C && : && (\Gamma:|\C|)\ra\Ty_\C\,\Gamma\ra\Set \\
  & \blank[\blank]_\C && : && \Ty_\C\,\Delta\ra\C(\Gamma,\Delta)\ra\Ty_\C\,\Gamma \\
  & \blank[\blank]_\C && : && \Tm_\C\,\Delta\,A\ra(\sigma:\C(\Gamma,\Delta))\ra\Tm_\C\,\Gamma\,(A[\sigma]_\C) \\
  & {[\id]}_\C && : && A[\id]_\C = A \\
  & {[\circ]}_\C && : && A[\sigma\circ\delta]_\C = A[\sigma]_\C[\delta]_\C \\
  & {[\id]}_\C && : && t[\id]_\C = t \\
  & {[\circ]}_\C && : && t[\sigma\circ\delta]_\C = t[\sigma]_\C[\delta]_\C \\
  & \blank\rhd_\C\blank && : && (\Gamma:|\C|)\ra\Ty_\C\,\Gamma\ra|\C| \\
  & \blank,_\C\blank && : && (\sigma:\C(\Gamma,\Delta))\ra\Tm_\C\,\Gamma\,(A[\sigma]_\C)\ra\C(\Gamma,\Delta\rhd_\C A) \\
  & \p_\C && : && \C(\Gamma\rhd_\C A,\Gamma) \\
  & \q_\C && : && \Tm\,(\Gamma\rhd_\C A)\,(A[\p_\C]_\C) \\
  & {{\rhd\beta}_1}_\C && : && \p_\C\circ_\C(\sigma,_\C t) = \sigma \\
  & {{\rhd\beta}_2}_\C && : && \q_\C[\sigma,_\C t]_\C = t \\
  & {\rhd\eta} && : && (\p_\C,_\C\q_\C) = \id_\C \\
  & {,\circ}_\C && : && (\sigma,_\C t)\circ_\C\delta = (\sigma\circ_\C\delta,_\C t[\delta]_\C) \\
  & \rlap{$\C$ has $\Pi$ types:} \\
  & \Pi_\C && : && (A:\Ty_\C\,\Gamma)\ra\Ty_\C\,(\Gamma\rhd_\C A)\ra\Ty_\C\,\Gamma \\
  & \lam_\C && : && \Tm_C\,(\Gamma\rhd_\C A)\,B\ra\Tm_\C\,\Gamma\,(\Pi_\C\,A\,B) \\
  & \app_\C && : && \Tm_\C\,\Gamma\,(\Pi_\C\,A\,B)\ra\Tm_C\,(\Gamma\rhd_\C A)\,B \\
  & {\Pi\beta}_\C && : && \app_\C\,(\lam_\C\,t) = t \\
  & {\Pi\eta}_\C && : && \lam_\C\,(\app_\C\,t) = t \\
  & {\Pi[]}_\C && : && \Pi_\C\,A\,B[\sigma]_\C = \Pi_\C\,(A[\sigma]_\C)\,(B[\sigma\circ_\C\p_\C,_\C\q_\C]_\C) \\
  & {\lam[]}_\C && : && \lam_\C\,t[\sigma]_\C = \lam_\C\,(t[\sigma\circ_\C\p_\C,_\C\q_\C]_\C) \\
  & \rlap{Derivable:} \\
  & {\app[]}_\C && : && \app_\C\,t[\sigma\circ_\C\p_\C,_\C\q_\C]_\C = \app_\C\,(t[\sigma]_\C)
\end{alignat*}

Presheaf model $\P^\C$ over $\C$. The metavariables for $\C$ change to
$\Omega$ and $\beta$. We don't write $_{\P^\C}$ indices (we open
$\P^\C$).
\begin{alignat*}{5}
  & (\Gamma:|\P^\C|) && :=\,\, && (|\Gamma| && : |\C|\ra\Set)\times \\
  & && && (\blank_\Gamma\lb\blank\rb && : |\Gamma|\,\Omega\ra\C(\Omega',\Omega)\ra|\Gamma|\,\Omega')\times \\
  & && && (\lb\id\rb_\Gamma && : \gamma_\Gamma\lb\id_\C\rb = \gamma)\times \\
  & && && (\lb\circ\rb_\Gamma && : \gamma_\Gamma\lb\beta\circ_\C\beta'\rb = \gamma_\Gamma\lb\beta\rb_\Gamma\lb\beta'\rb) \\
  & (\sigma:\P^\C(\Gamma,\Delta)) && :=\,\, && (|\sigma| && : |\Gamma|\,\Omega\ra|\Delta|\,\Omega)\times \\
  & && && (\blank_\sigma\lb\blank\rb && : (\gamma:|\Gamma|\,\Omega)(\beta:\C(\Omega',\Omega))\ra |\sigma|\,\gamma_\Delta\lb\beta\rb = |\sigma|\,(\gamma_\Gamma\lb\beta\rb)) \\
  & |\id|\,\gamma && :=\,\, && \rlap{$\gamma$} \\
  & |\sigma\circ\delta|\,\gamma && := && \rlap{$|\sigma|\,(|\delta|\,\gamma)$} \\
  & |\cdot|\,\Omega && := && \rlap{$\top$} \\
  & \tt _\cdot\lb\beta\rb && := && \tt \\
  & |\epsilon|\,\gamma && := && \rlap{$\tt$} \\
  & (A:\Ty\,\Gamma) && :=\,\, && (|A| && : |\Gamma|\,\Omega\ra\Set)\times \\
  & && && (\blank_A\lb\blank\rb && : |A|\,\gamma\ra(\beta:\C(\Omega',\Omega))\ra|A|\,(\gamma_\Gamma\lb\beta\rb))\times \\
  & && && (\lb\id\rb_A && : \alpha_A\lb\id_\C\rb = \alpha)\times \\
  & && && (\lb\circ\rb_A && : \alpha_A\lb\beta\circ_\C\beta'\rb = \alpha_A\lb\beta\rb_A\lb\beta'\rb) \\
  & (t:\Tm\,\Gamma\,A) && :=\,\, && (|t| && : (\gamma:|\Gamma|\,\Omega)\ra|A|\,\gamma)\times \\
  & && && (\blank_t\lb\blank\rb && : (\gamma:|\Gamma|\,\Omega)(\beta:\C(\Omega',\Omega))\ra |t|\,\gamma_A\lb\beta\rb = |t|\,(\gamma_\Gamma\lb\beta\rb)) \\
  & |A[\sigma]|\,\gamma && := && \rlap{$|A|\,(|\sigma|\,\gamma)$} \\
  & \alpha_{A[\sigma]}\lb\beta\rb && := && \rlap{$\alpha_A\lb\beta\rb$} \\
  & |t[\sigma]|\,\gamma && := && \rlap{$|t|\,(|\sigma|\,\gamma)$} \\
  & |\Gamma\rhd A|\,\Omega && := && \rlap{$(\gamma:|\Gamma|\,\Omega)\times|A|\,\gamma$} \\
  & (\gamma,\alpha)_{\Gamma\rhd A}\lb\beta\rb && := && \rlap{$(\gamma_\Gamma\lb\beta\rb,\alpha_A\lb\beta\rb)$} \\
  & |\p|\,(\gamma,\alpha) && := && \rlap{$\gamma$} \\
  & |\q|\,(\gamma,\alpha) && := && \rlap{$\alpha$} \\
  & (f:|\Pi\,A\,B|\,\gamma) && := && (|f| && :(\beta:\C(\Omega',\Omega))(\alpha:|A|\,(\gamma_\Gamma\lb\beta\rb))\ra|B|\,(\gamma_\Gamma\lb\beta\rb,\alpha))\times \\
  & && && (\blank_f\lb\blank\rb && : (\alpha:|A|\,(\gamma_\Gamma\lb\beta\rb))(\beta':\C(\Omega'',\Omega'))\ra|f|\,\beta\,\alpha _B\lb\beta'\rb = |f|\,(\beta\circ_\C\beta')\,(\alpha _A\lb\beta'\rb)) \\
  & f _{\Pi\,A\,B}\lb\beta\rb && := && \rlap{$(\lambda\beta'.|f|\,(\beta\circ_\C\beta'), \lambda\alpha\,\beta''.|f|\,(\beta\circ_C\beta')\,\alpha _B\lb\beta''\rb \overset{\alpha _f\lb\beta''\rb}{=} |f|\,(\beta\circ\beta'\circ\beta'')\,(\alpha _A\lb\beta''\rb))$} \\
  & |\lam\,t|\,\gamma && := && \rlap{$(\lambda\beta\,\alpha.|t|\,(\gamma _\Gamma\lb\beta\rb,\alpha), \lambda\alpha\beta'. |t|\,(\gamma _\Gamma\lb\beta\rb,\alpha) _B\lb\beta'\rb \overset{(\gamma _\Gamma\lb\beta\rb,\alpha) _t\lb\beta'\rb}{=} |t|\,(\gamma _\Gamma\lb\beta\circ\beta'\rb,\alpha _A\lb\beta'\rb))$} \\
  & |\app\,t|\,(\gamma,\alpha) && := && \rlap{$||t|\,\gamma|\,\id_\C\,\alpha$}
\end{alignat*}
% We have $|\P| \cong \Ty_\P\,\cdot$ and
% $\Tm_\P\,\Gamma\,(A[\epsilon]) \cong \P(\Gamma,A)$, we don't write
% these isomorphisms out explicitly (we already omitted the first one
% from the statement of the second one).

We fix a category $\C$. We write $\P$ for $\P^\C$ or we don't write
it. An internal model $\b\M$ supporting $\Pi$ types is given by the
following components (we don't write $\b\M$ indices).
\begin{alignat*}{5}
  & \b\Ty && :{} && \Ty_\P\,\cdot \\
  & \b\Tm && : && \Ty_\P\,(\cdot\rhd\b\Ty) \\
  & \b\Pi && : && \Tm_\P\,(\cdot\rhd\b\Ty\rhd\Pi\,\b\Tm\,(\b\Ty[\epsilon]))\,(\b\Ty[\epsilon]) \\
  & \b\lam && : && \Tm_\P\,(\cdot\rhd\b\Ty\rhd\Pi\,\b\Tm\,(\b\Ty[\epsilon])\rhd\Pi\,(\b\Tm[\p])\,(\b\Tm[\epsilon,\app\,\q]))\,(\b\Tm[\epsilon,\b\Pi][\p]) \\
  & \b\app && : && \Tm_\P\,(\cdot\rhd\b\Ty\rhd\Pi\,\b\Tm\,(\b\Ty[\epsilon])\rhd\b\Tm[\epsilon,\b\Pi])\,(\Pi\,(\b\Tm[\p])\,(\b\Tm[\epsilon,\app\,\q])[\p])) \\
  & \b{\Pi\beta} && : && \b\app[\p,\b\lam] = \q \\
  & \b{\Pi\eta} && : && \b\lam[\p,\b\app] = \q
\end{alignat*}
We turn $\b\M$ into an external model $\M$ as follows. $\P$
abbreviates $\P^\C$. $|\M|$ is given as the following
inductive-recursive type.
\begin{alignat*}{5}
  & |\M| && : \Set \\
  & \EL && : |\M|\ra|\P| \\
  & \cdot_\M && : |\M| \\
  & \blank\rhd_\M\blank && : (\Gamma:|\M|)\ra\Tm_\P\,(\EL\,\Gamma)\,(\b\Ty[\epsilon])\ra|\M| \\
  & \EL\,\cdot_\M && := \cdot_\P \\
  & \EL\,(\Gamma\rhd_\M A) && := \EL\,\Gamma\rhd_\P\b\Tm[\epsilon,A]_\P
\end{alignat*}
The rest of the model is as follows.
\begin{alignat*}{5}
  & \M(\Gamma,\Delta) && := \P(\EL\,\Gamma,\EL\,\Delta) \\
  & \id_\M && := \id_\P \\
  & \sigma\circ_\M\delta && := \sigma\circ_\P\delta \\
  & \Ty_\M\,\Gamma && := \Tm_\P\,(\EL\,\Gamma)\,(\b\Ty[\epsilon]) \\
  & A[\sigma]_\M && := A[\sigma]_\P \\
  & \Tm_\M\,\Gamma\,A && := \Tm_\P\,(\EL\,\Gamma)\,(\b\Tm[\epsilon,A]) \\
  & t[\sigma]_\M && := t[\sigma]_\P \\
  & \epsilon_\M && := \epsilon_\P \\
  & (\sigma,_\M t) && := (\sigma,_\P t) \\
  & \p_\M && := \p_\P \\
  & \q_\M && := \q_\P \\
  & \Pi_\M\,A\,B && := \b\Pi[\epsilon, A, \lam_\P\,B]_\P \\
  & \lam_\M\,t && := \b\lam[\epsilon, A,\lam_\P\,B,\lam_\P\,t]_\P \\
  & \app_\M\,t && := \app_\P\,(\b\app[\epsilon,A,\lam_\P\,B,t]_\P)
\end{alignat*}
Equalities are checked at the end.

If $\C$ is already a model of type theory, we can define an internal
version $\b\C$ as follows.
\begin{alignat*}{5}
  & \b\Ty && : && \Ty_\P\,\cdot \\
  & |\b\Ty|\,\{\Omega\}\,\tt && :=\,\, && \Ty_\C\,\Omega \\
  & A _{\b\Ty}\lb\beta\rb && := && A[\beta]_\C \\
  & \b\Tm && : && \Ty_\P\,(\cdot\rhd\b\Ty) \\
  & |\b\Tm|\,\{\Omega\}\,(\tt,A) && := && \Tm_\C\,\Omega\,A \\
  & t _{\b\Tm}\lb\beta\rb && := && t[\beta]_\C \\
  & \b\Pi && : && \Tm_\P\,(\cdot\rhd\b\Ty\rhd\Pi\,\b\Tm\,(\b\Ty[\epsilon]))\,(\b\Ty[\epsilon]) \\
  & |\b\Pi|\,\{\Omega\}\,(\tt,A,B) && := && \Pi_\C\,A\,(|B|\,\p_\C\,\q_\C) \\
  & \b\lam && : && \Tm_\P\,(\cdot\rhd\b\Ty\rhd\Pi\,\b\Tm\,(\b\Ty[\epsilon])\rhd\Pi\,(\b\Tm[\p])\,(\b\Tm[\epsilon,\app\,\q]))\,(\b\Tm[\epsilon,\b\Pi][\p]) \\
  & |\b\lam|\,(\tt,A,B,t) && := && \lam_\C\,(|t|\,\p\,\q) \\
  & \b\app && : && \Tm_\P\,(\cdot\rhd\b\Ty\rhd\Pi\,\b\Tm\,(\b\Ty[\epsilon])\rhd\b\Tm[\epsilon,\b\Pi])\,(\Pi\,(\b\Tm[\p^2])\,(\b\Tm[\epsilon,\app\,\q])[\p^2,\q]) \\
  & \app\,\b\app && : && \Tm_\P\,(\cdot\rhd\b\Ty\rhd\Pi\,\b\Tm\,(\b\Ty[\epsilon])\rhd\b\Tm[\epsilon,\b\Pi]\rhd(\b\Tm[\p^2]))\,(\b\Tm[\epsilon,\app\,\q][\p^2,\q]) \\
  & |\app\,\b\app|\,(\tt,A,B,t,u) && := && \app_\C\,t[\id,u]
\end{alignat*}
Now if we turn $\b\C$ into an external model using the externalisation
construction from above, we get the following $\C'$. $\P$ abbreviates
$\P^\C$. Contexts are given by the following inductive-recursive
definition.
\begin{alignat*}{5}
  & |\C'| && : \Set \\
  & \EL && : |\C'|\ra|\P| \\
  & \cdot_{\C'} && : |\C'| \\
  & \blank\rhd_{\C'}\blank && : (\Gamma:|\C'|)\ra\Tm_\P\,(\EL\,\Gamma)\,\Ty_\C xxx \ra|\C'| \\
  & \EL\,\cdot_{\C'} && := \cdot_\P \\
  & \EL\,(\Gamma\rhd_{\C'} A) && := \EL\,\Gamma\rhd_\P\b\Tm[\epsilon,A]_\P
\end{alignat*}
The rest of $\C'$.
\begin{alignat*}{5}
  & 
\end{alignat*}




The notion of internal displayed model (over $\C$).
\begin{alignat*}{6}
  & \Ty^* && :\, && \rlap{$ \Ty\,(\cdot\rhd\b\Ty)$} \\
  & \Tm^* && : && \rlap{$ \Ty\,(\cdot\rhd\b\Ty\rhd\Ty^*\rhd\b\Tm[\p])$} \\
  & \Pi^* && : && \rlap{$ \Tm\,(\cdot\rhd\b\Ty\rhd\Ty^*\rhd\Pi\,\b\Tm\,(\b\Ty[\epsilon])[\p]\rhd\Pi\,(\b\Tm[\p^2])\,(\Pi\,(\Tm^*[\p^2,\q])\,(\Ty^*[\epsilon,\app\,\q[\p]])))\,(\Ty^*[\epsilon,\b\Pi[\p^3,\q[\p]]])$} \\
  & \lam^* && : && \Tm\,&& ( && \cdot\rhd\b\Ty\rhd\Ty^*\rhd\Pi\,\b\Tm\,(\b\Ty[\epsilon])[\p]\rhd\Pi\,(\b\Tm[\p^2])\,(\Pi\,(\Tm^*[\p^2,\q])\,(\Ty^*[\epsilon,\app\,\q[\p]]))\rhd{} \\
  & && && && && \Pi\,(\b\Tm[\p^3])\,(\b\Tm[\epsilon,\app\,(\q[\p])])\rhd{} \\
  & && && && && \Pi\,(\b\Tm[\p^4])\,(\Pi\,(\Tm^*[\p^4,\q])\,(\Tm^*[\epsilon,\app\,(\q[\p^2])[\p],\app\,(\app\,(\q[\p])),\app\,\q[\p]]))) \\
  & && && && \rlap{$(\Tm^*[\epsilon,\b\Pi[\epsilon,\q[\p^5],\q[\p^3]],\Pi^*[\p^2],\b\lam[\p^5,\q[\p^3],\q[\p]]])$} \\
  & \app^* && : && \Tm\,&& ( && \cdot\rhd\b\Ty\rhd\Ty^*\rhd\Pi\,\b\Tm\,(\b\Ty[\epsilon])[\p]\rhd\Pi\,(\b\Tm[\p^2])\,(\Pi\,(\Tm^*[\p^2,\q])\,(\Ty^*[\epsilon,\app\,\q[\p]]))\rhd{} \\
  & && && && && \b\Tm[\epsilon,\b\Pi[\p^3,\q[\p]]]\rhd \Tm^*[\epsilon,\b\Pi[\p^4,\q[\p^2]],\Pi^*[\p],\q]) \\
  & && && && \rlap{$(\Pi\,(\b\Tm[\p^5])\,(\Pi\,(\Tm^*[\p^5,\q])\,(\Tm^*[\epsilon,\app\,(\q[\p^3])[\p],\app\,(\app\,(\q[\p^2])),\b\app[\p^7,\q[\p^5],\q[\p^3],\q[\p]]])))$} \\
  & {\Pi\beta}^* && : && \rlap{$\app^*[\p^2,\b\lam[\p^5,\q[\p^3],\q[\p]],\lam^*] = \q$} \\
  & {\Pi\eta}^* && : && \rlap{$\lam^*[\p^2,\b\app[\p^5,\q[\p^3],\q[\p]],\app^*] = \q$} \\
\end{alignat*}
What happens if we look at this model from the outside?




Internal model and displayed model using Agda syntax for the internal
language of the presheaf model $\P^\C$. We write \verb$Ty$ instead of
$\b\Ty$.
\begin{verbatim}
Ty   : Set
Tm   : Ty → Set
Π    : (A : Ty)(B : Tm A → Ty) → Ty
lam  : (A : Ty)(B : Tm A → Ty)(t : (u : Tm A) → Tm (B u)) → Tm (Π A B)
app  : (A : Ty)(B : Tm A → Ty)(t : Tm (Π A B)) → (u : Tm A) → Tm (B u)
Πβ   : (A : Ty)(B : Tm A → Ty)(t : (u : Tm A) → Tm (B u)) →
       app A B (lam A B t) = t
Πη   : (A : Ty)(B : Tm A → Ty)(t : Tm (Π A B)) →
       lam A B (app A B t) = t

Ty*  : Ty → Set
Tm*  : (A : Ty)(A* : Ty* A) → Tm A → Set
Π*   : (A : Ty)(A* : Ty* A)
       (B : Tm A → Ty)(B* : (t : Tm A)(t* : Tm* A A* t) → Ty* (B t)) →
       Ty* (Π A B)
lam* : (A : Ty)(A* : Ty* A)
       (B : Tm A → Ty)(B* : (t : Tm A)(t* : Tm* A A* t) → Ty* (B t)) →
       (t : (u : Tm A) → Tm (B u))
         (t* : (u : Tm A)(u* : Tm* A A* u) → Tm* (B u) (B* u u*) (t u)) →
       Tm* (Π A B) (Π* A A* B B*) (lam A B t)
app* : (A : Ty)(A* : Ty* A)
       (B : Tm A → Ty)(B* : (t : Tm A)(t* : Tm* A A* t) → Ty* (B t)) →
       (t : Tm (Π A B))(t* : Tm* (Π A B) (Π* A A* B B*) t) →
       (u : Tm A)(u* : Tm* A A* u) →
       Tm* (B u) (B* u u*) (app A B t u)
Πβ   : (A : Ty)(A* : Ty* A)
       (B : Tm A → Ty)(B* : (t : Tm A)(t* : Tm* A A* t) → Ty* (B t)) →
       (t : (u : Tm A) → Tm (B u))
         (t* : (u : Tm A)(u* : Tm* A A* u) → Tm* (B u) (B* u u*) (t u)) →
       app* A A* B B* (lam A B t) (lam* A A* B B* t t*) = t*
Πη   : (A : Ty)(A* : Ty* A)
       (B : Tm A → Ty)(B* : (t : Tm A)(t* : Tm* A A* t) → Ty* (B t)) →
       (t : Tm (Π A B))(t* : Tm* (Π A B) (Π* A A* B B*) t) →
       lam* A A* B B* (app A B t) (app* A A* B B* t t*) = t*       
\end{verbatim}

Equalities for the model $\M$ (the equalities for the substitution
calculus are trivial from those of the presheaf model). We open
$\P^\C$.
\begin{alignat*}{5}
  & {\Pi\beta}_\M && :=\,\, && \app_\M\,(\lam_\M\,t) = \\
  & && && \app\,(\b\app[\epsilon,A,\lam\,B,\b\lam[\epsilon, A,\lam\,B,\lam\,t]]) = \\
  & && && \app\,(\b\app[\p,\b\lam][\epsilon,A,\lam\,B,\lam\,t]) \overset{\b{\Pi\beta}}{=} \\
  & && && \app\,(\q[\epsilon,A,\lam\,B,\lam\,t]) = \\
  & && && \app\,(\lam\,t) \overset{{\Pi\beta}}{=} \\
  & && && t \\
  & {\Pi\eta}_\M && :=\,\, && \lam_\M\,(\app_\M\,t) = \\
  & && && \b\lam[\epsilon, A,\lam\,B,\lam\,(\app\,(\b\app[\epsilon,A,\lam\,B,t]))] \overset{{\Pi\eta}}{=} \\
  & && && \b\lam[\epsilon, A,\lam\,B,\b\app[\epsilon,A,\lam\,B,t]] = \\
  & && && \b\lam[\p,\b\app][\epsilon, A, \lam\,B, t] \overset{\b{\Pi\eta}}{=} \\
  & && && \q[\epsilon, A, \lam\,B, t] = \\
  & && && t \\
  & {\Pi[]}_\M && := && \Pi_\M\,A\,B[\sigma]_\M = \\
  & && && \b\Pi[\epsilon, A, \lam\,B][\sigma] = \\
  & && && \b\Pi[\epsilon, A[\sigma], \lam\,(B[\sigma\circ\p,\q])] = \\
  & && && \Pi_\M\,(A[\sigma]_\M)\,(B[\sigma\circ\p,\q]_\M) \\
  & {\lam[]}_\M && := && \lam_\M\,t[\sigma]_\M = \\
  & && && \b\lam[\epsilon, A,\lam\,B,\lam\,t][\sigma] = \\
  & && && \b\lam[\epsilon, A[\sigma],\lam\,(B[\sigma\circ\p,\q]),\lam\,(t[\sigma\circ\p,\q])] = \\
  & && && \lam_\M\,(t[\sigma]_\M)
\end{alignat*}

Checking equalities for the internalisation $\b\C$.
\begin{alignat*}{5}
  & \lb\id\rb_{\b\Ty} && :=\,\, && A _{\b\Ty}\lb\id_\C\rb = A[\id_C]_\C \overset{[\id]_\C}{=} A \\
  & \lb\circ\rb_{\b\Ty} && := && A _{\b\Ty}\lb\beta\circ_\C\beta'\rb = A[\beta\circ_\C\beta']_\C \overset{[\circ]_\C}{=} A[\beta]_\C[\beta']_\C = A _{\b\Ty}\lb\beta\rb _{\b\Ty}\lb\beta'\rb \\
  & \lb\id\rb_{\b\Tm} && := && t _{\b\Tm}\lb\id_\C\rb = t[\id_C]_\C \overset{[\id]_\C}{=} t \\
  & \lb\circ\rb_{\b\Tm} && := && t _{\b\Tm}\lb\beta\circ_\C\beta'\rb = t[\beta\circ_\C\beta']_\C \overset{[\circ]_\C}{=} t[\beta]_\C[\beta']_\C = t _{\b\Tm}\lb\beta\rb _{\b\Tm}\lb\beta'\rb \\
  & (\tt,A,B) _{\b\Pi}\lb\beta\rb && := && |\b\Pi|\,(\tt,A,B) _{\b\Ty[\epsilon]}\lb\beta\rb = \\
  & && && \Pi_\C\,A\,(|B|\,\p_\C\,\q_\C)[\beta]_\C \overset{{\Pi[]}}{=} \\
  & && && \Pi_\C\,(A[\beta]_\C)\,(|B|\,\p_\C\,\q_\C[\beta\circ\p,\q]) \overset{\q _B\lb\beta\circ\p,\q\rb}{=} \\
  & && && \Pi_\C\,(A[\beta]_\C)\,(|B|\,(\p\circ(\beta\circ\p,\q))\,(\q[\beta\circ\p,\q])) \overset{{\rhd\beta}_\C}{=} \\
  & && && \Pi_\C\,(A[\beta]_\C)\,(|B|\,(\beta\circ_\C\p_\C)\,\q_\C) = \\
  & && && |\b\Pi|\,(\tt,A[\beta]_\C,(\lambda\beta'.|B|\,(\beta\circ_\C\beta'), \dots)) = \\
  & && && |\b\Pi|\,(\tt,A[\beta]_\C,B _{\Pi\,\b\Tm\,(\b\Ty[\epsilon])}\lb\beta\rb) = \\
  & && && |\b\Pi|\,((\tt,A,B) _{\cdot\rhd\b\Ty\rhd\Pi\,\b\Tm\,(\b\Ty[\epsilon])}\lb\beta\rb) \\
  & (\tt,A,B,t) _{\b\lam}\lb\beta\rb && := && |\b\lam|\,(\tt,A,B,t) _{\b\Tm[\epsilon,\b\Pi[\p]]}\lb\beta\rb = \\
  & && && \lam_\C\,(|t|\,\p\,\q)[\beta] \overset{{\lam[]}_\C}{=} \\
  & && && \lam_\C\,(|t|\,\p\,\q[\beta\circ\p,\q]) \overset{\q _t\lb\beta\circ\p,\q\rb}{=} \\
  & && && \lam_\C\,(|t|\,(\p\circ(\beta\circ\p,\q))\,(\q[\beta\circ\p,\q])) = \\
  & && && \lam_\C\,(|t|\,(\beta\circ\p)\,\q) = \\
  & && && |\b\lam|\,(\tt,A[\beta],(\lambda\beta'.|B|\,(\beta\circ\beta'),\dots),(\lambda\beta'.|t|\,(\beta\circ\beta'),\dots)) \\
  & (\tt,A,B,t,u) _{\app\,\b\app}\lb\beta\rb && := && \app_\C\,t[\id,u][\beta] = \\
  & && && \app_\C\,t[\beta,u[\beta]] = \\
  & && && \app_\C\,t[\beta\circ\p,\q][\id,u[\beta]] = \\
  & && && \app_\C\,(t[\beta])[\id,u[\beta]] = \\
  & && && |\app\,\b\app|\,(\tt,A[\beta],(\lambda\beta'.|B|\,(\beta\circ\beta'),\dots),t[\beta],u[\beta]) \\
  & |\b{\Pi\beta}|\,(\tt,A,B,t) && := && |\b\app[\p,\b\lam]|\,(\tt,A,B,t) = \\
  & && && |\b\app|\,(\tt,A,B,|\b\lam|\,(\tt,A,B,t)) = \\
  & && && |\b\app|\,(\tt,A,B,\lam_\C\,(|t|\,\p\,\q)) = \\
  & && && |\lam\,(\app\,\b\app)|\,(\tt,A,B,\lam_\C\,(|t|\,\p\,\q)) = \\
  & && && (\lambda\beta\,u. |\app\,\b\app|\,(\tt,A[\beta],(\lambda\beta'.|B|\,(\beta\circ\beta'),\dots),\lam_\C\,(|t|\,\p\,\q)[\beta],u), \dots) = \\
  & && && (\lambda\beta\,u. \app_\C\,(\lam_\C\,(|t|\,\p\,\q)[\beta])[\id,u], \dots) \overset{\q _t\lb\beta\circ\p\,q\rb}{=} \\
  & && && (\lambda\beta\,u. \app_\C\,(\lam_\C\,(|t|\,(\beta\circ\p)\,\q))[\id,u], \dots) \overset{{\Pi\beta}_\C}{=} \\
  & && && (\lambda\beta\,u. |t|\,(\beta\circ\p)\,\q[\id,u], \dots) \overset{\q _t\lb\id,u\rb}{=} \\
  & && && (\lambda\beta\,u. |t|\,\beta\,u, \dots) = \\
  & && && (|t|, \blank _t\lb\blank\rb) = \\
  & && && t = \\
  & && && \q\,(\tt,A,B,t) \\
  & |\b{\Pi\eta}| && := && |\b\lam[\p,\b\app]|\,(\tt,A,B,t) = \\
  & && && |\b\lam|\,(\tt,A,B,|\b\app|\,(\tt,A,B,t)) = \\
  & && && |\b\lam|\,(\tt,A,B,|\lam\,(\app\,\b\app)|\,(\tt,A,B,t)) = \\
  & && && |\b\lam|\,(\tt,A,B,(\lambda\beta\,u.|\app\,\b\app|\,(\tt,A[\beta],(\lambda\beta'.|B|\,(\beta\circ\beta'),\dots),t[\beta],u),\dots)) = \\
  & && && |\b\lam|\,(\tt,A,B,(\lambda\beta\,u.\app_\C\,(t[\beta])[\id,u],\dots)) = \\
  & && && \lam_\C\,(\app_\C\,(t[\p])[\id,\q]) \overset{{\app[]}_\C}{=} \\
  & && && \lam_\C\,(\app_\C\,t[\p\circ\p,\q][\id,\q]) = \\
  & && && \lam_\C\,(\app_\C\,t[\p,\q]) = \\
  & && && \lam_\C\,(\app_\C\,t) = \\
  & && && \lam_\C\,(\app_\C\,t) \overset{{\Pi\eta}_\C}{=} \\
  & && && t = \\
  & && && \q\,(\tt,A,B,t) \\
\end{alignat*}

\end{document}