-- {-# OPTIONS --prop #-}

module CatOld where

open import Lib

record Cat : Set₂ where
  field
    Ob   : Set₁
    _⇒_  : (I : Ob)(J : Ob) → Set
    idc  : (I : Ob) → I ⇒ I
    _∘c_  : {I : Ob}{J : Ob}(f : J ⇒ I){K : Ob}(g : K ⇒ J) → K ⇒ I
    idlc : _≡_ {A = {I : Ob}{J : Ob} → I ⇒ J → I ⇒ J}
               (λ {I}{J} f → idc J ∘c f)
               (λ f → f)
    idrc : _≡_ {A = {I : Ob}{J : Ob} → I ⇒ J → I ⇒ J}
               (λ {I}{J} f → f ∘c idc I)
               (λ f → f)
    assc : _≡_ {A = {I : Ob}{J : Ob}(f : I ⇒ J){K : Ob}(g : K ⇒ J){L : Ob}(h : L ⇒ K) → L ⇒ I}
               {!λ f g h → (f ∘c g) ∘c h!}
               {!!}
--    assc : {Ω : Ob}{Ψ : Ob}{β : Ω ⇒ Ψ}{Ω' : Ob}{β' : Ω' ⇒ Ω}{Ω'' : Ob}{β'' : Ω'' ⇒ Ω'} → (β ∘ β') ∘ β'' ≡ β ∘ (β' ∘ β'')
  infix 5 _⇒_
  infix 6 _∘_
open Cat public
