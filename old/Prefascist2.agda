{-# OPTIONS --prop --rewriting --type-in-type #-}

module Prefascist2 where

open import Agda.Primitive
open import CatRewrite

infix 4 _≡_
infixl 5 _,Σ_
data _≡_ {i}{A : Set i}(a : A) : A → Prop i where
  refl : a ≡ a
record ⊤ : Set where
record ⊤p : Prop where
record Σ {i}(A : Set i){j}(B : A → Set j) : Set (i ⊔ j) where
  constructor _,Σ_
  field
    π₁ : A
    π₂ : B π₁
open Σ
-- ,= : ∀{i}{A : Set i}{j}{B : A → Set j}{a₀ a₁ : A}(a₌ : a₀ ≡ a₁){b₀ : B a₀}{b₁ : B a₁} → b₀ ≡ b₁
record Lift {i}(A : Prop i) : Set i where
  constructor mk
  field un : A
open Lift
cong : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong f refl = refl
_◾_ : ∀{i}{A : Set i}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
refl ◾ refl = refl

infixl 6  _∘_

Con : Set₁
Con = Ob → Set

Sub : Con → Con → Set
Sub Δ Γ = (I : Ob) → Δ I → Γ I

_∘_ : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
(γ ∘ δ) I θI = γ I (δ I θI)

id : ∀{Γ} → Sub Γ Γ
id I γI = γI

Ty : Con → Set₁
Ty Γ = (I : Ob) → Γ I → Set

_[_]T : ∀{Γ} → Ty Γ → ∀{Δ} → Sub Δ Γ → Ty Δ
(A [ γ ]T) I δI = A I (γ I δI)

Tm : (Γ : Con) → Ty Γ → Set
Tm Γ A = (I : Ob)(γI : Γ I) → A I γI

_[_]t : ∀{Γ}{A : Ty Γ} → Tm Γ A → ∀{Δ} → (γ : Sub Δ Γ) → Tm Δ (A [ γ ]T)
(a [ γ ]t) I δI = a I (γ I δI)

_▹_ : (Γ : Con) → Ty Γ → Con
(Γ ▹ A) I = Σ (Γ I) (A I)

_^_ : ∀{Γ Δ}(γ : Sub Δ Γ) → (A : Ty Γ) → Sub (Δ ▹ (A [ γ ]T)) (Γ ▹ A)
(γ ^ A) I (δI ,Σ aI) = γ I δI ,Σ aI

_,_ : ∀{Γ Δ}(γ : Sub Δ Γ){A : Ty Γ} → Tm Δ (A [ γ ]T) → Sub Δ (Γ ▹ A)
(γ , a) I δI = γ I δI ,Σ a I δI

-- ' is the presheaf model

record Con' : Set₁ where
  field
    Γ    : Con
    _[_] : ∀{I} → Γ I → ∀{J} → Hom J I → Γ J
    [∘]  : ∀{I}(γI : Γ I){J}(f : Hom J I){K}(g : Hom K J) → γI [ f ∘c g ] ≡ γI [ f ] [ g ] -- TODO: change this to pointfree, then we try2fix should be fine
    [id] : ∀{I}(γI : Γ I) → γI [ idc ] ≡ γI
  infix 8 _[_]
open Con' renaming (Γ to ∣_∣; _[_] to _∶_[_])

record Sub' (Δ Γ : Con') : Set where
  field
    γ   : Sub ∣ Δ ∣ ∣ Γ ∣
    nat : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I) → Γ ∶ γ I δI [ f ] ≡ γ J (Δ ∶ δI [ f ])
open Sub' renaming (γ to ∣_∣) public

yOb : Ob → Con'
∣ yOb I ∣ J = Hom J I
yOb I ∶ f [ g ] = f ∘c g
[∘] (yOb I) _ _ _ = refl
[id] (yOb I) _ = refl

yHom : ∀{I J} → Hom J I → Sub' (yOb J) (yOb I)
∣ yHom f ∣ _ g = f ∘c g
nat (yHom f) g = refl

-- take the cofree presheaf over the family
Y : Con → Con'
∣ Y Γ ∣ I = Sub ∣ yOb I ∣ Γ
(Y Γ ∶ γI [ f ]) _ g = γI _ (f ∘c g)
[∘] (Y Γ) _ _ _ = refl
[id] (Y Γ) _ = refl

Ys : ∀{Γ Δ} → Sub Δ Γ → Sub' (Y Δ) (Y Γ)
∣ Ys γ ∣ I δI = γ ∘ δI
nat (Ys γ) f = refl

yl : (Γ : Con') → Sub' Γ (Y ∣ Γ ∣)
∣ yl Γ ∣ _ γI _ g = Γ ∶ γI [ g ]
nat (yl Γ) f = {!!} -- [∘] in a point free way

record Ty' (Γ : Con') : Set₁ where
  field
    A     : Ty ∣ Γ ∣
    _[_]  : ∀{I}{γI : ∣ Γ ∣ I} → A I γI → ∀{J}(f : Hom J I) → A J (Γ ∶ γI [ f ])
    -- [∘]   : ∀{I}{γI : ∣ Γ ∣ I}(aI : A I γI){J}(f : Hom J I){K}(g : Hom K J) → aI [ f ∘c g ] ≡ (aI [ f ]) [ g ]
    -- [id]  : ∀{I}{γI : ∣ Γ ∣ I}(aI : A I γI) → aI [ idc ] ≡ aI
  infix 8 _[_]
open Ty' renaming (A to ∣_∣; _[_] to _∶_[_])

YT : ∀{Γ}(A : Ty Γ) → Ty' (Y Γ)
∣ YT {Γ} A ∣ I γI = Tm ∣ yOb I ∣ (A [ γI ]T)
YT A ∶ aI [ f ] = aI [ ∣ yHom f ∣ ]t

record Tm' (Γ : Con')(A : Ty' Γ) : Set where
  field
    a   : Tm ∣ Γ ∣ ∣ A ∣
    nat : ∀{I}{γI : ∣ Γ ∣ I}{J}(f : Hom J I) → A ∶ (a I γI) [ f ] ≡ a J (Γ ∶ γI [ f ])
open Tm' renaming (a to ∣_∣)

Yt : ∀{Γ A} → Tm Γ A → Tm' (Y Γ) (YT A)
∣ Yt a ∣ I γI = a [ γI ]t
nat (Yt a) f = refl

-- ∙ is the prefascist model

record Con∙ : Set₁ where
  field
    Γ  : Con                   -- Γ   : Ob → Set
    _R : Ty ∣ Y Γ ∣            -- Γ R : (I:Ob) → ∣ Y Γ ∣ → Set
open Con∙ renaming (Γ to ∣_∣)

yl∘ : ∀(Γ : Con'){I}(γI : ∣ Γ ∣ I){J}(f : Hom J I) → ∣ yl Γ ∣ I γI ∘ ∣ yHom f ∣ ≡ ∣ yl Γ ∣ J (Γ ∶ γI [ f ])
yl∘ Γ γI f = {!!}

yl∘' : ∀(Γ : Con'){I}(γI : ∣ Y ∣ Γ ∣ ∣ I){J}(f : Hom J I) → ∣ yl (Y ∣ Γ ∣) ∣ I γI ∘ ∣ yHom f ∣ ≡ ∣ yl (Y ∣ Γ ∣) ∣ J (Y ∣ Γ ∣ ∶ γI [ f ])
yl∘' Γ γI f = refl

⌜_⌝ : Con∙ → Con'
∣ ⌜ Γ ⌝ ∣ I = Σ (∣ Y ∣ Γ ∣ ∣ I) λ γI → Tm ∣ yOb I ∣ ((Γ R) [ ∣ yl (Y ∣ Γ ∣) ∣ I γI ]T)
⌜ Γ ⌝ ∶ (γJ ,Σ γR) [ f ] = (Y ∣ Γ ∣ ∶ γJ [ f ]) ,Σ γR [ ∣ yHom f ∣ ]t
[∘] ⌜ Γ ⌝ _ _ _ = refl
[id] ⌜ Γ ⌝ _ = refl

record Sub∙ (Δ Γ : Con∙) : Set where
  field
    γ  : Sub ∣ ⌜ Δ ⌝ ∣ ∣ Γ ∣ 
    _R : Tm  ∣ ⌜ Δ ⌝ ∣ (Γ R [ ∣ Ys γ ∣ ∘ ∣ yl ⌜ Δ ⌝ ∣ ]T)
open Sub∙ renaming (γ to ∣_∣)

⌜_⌝s : ∀{Γ Δ} → Sub∙ Δ Γ → Sub' ⌜ Δ ⌝ ⌜ Γ ⌝
∣ ⌜_⌝s {Γ}{Δ} γ ∣ I δI = (∣ Ys ∣ γ ∣ ∣ ∘ ∣ yl ⌜ Δ ⌝ ∣) I δI ,Σ _[_]t {A = (Γ R) [ ∣ Ys ∣ γ ∣ ∣ ∘ ∣ yl ⌜ Δ ⌝ ∣ ]T} (γ R) (∣ yl ⌜ Δ ⌝ ∣ I δI)
nat ⌜ γ ⌝s f = refl

_∘∙_ : ∀{Γ Δ} → Sub∙ Δ Γ → ∀{Θ} → Sub∙ Θ Δ → Sub∙ Θ Γ
∣ γ ∘∙ δ ∣ I θI = ∣ γ ∣ I (∣ ⌜ δ ⌝s ∣ I θI)
((γ ∘∙ δ) R) I θI = (γ R) I (∣ ⌜ δ ⌝s ∣ I θI )

id∙ : ∀{Γ} → Sub∙ Γ Γ
∣ id∙ ∣ I (γI ,Σ _) = γI I idc
(id∙ R) I (γI ,Σ γR) = γR I idc

ass : ∀{Γ Δ}{γ : Sub∙ Δ Γ}{Θ}{δ : Sub∙ Θ Δ}{Ξ}{θ : Sub∙ Ξ Θ} → (γ ∘∙ δ) ∘∙ θ ≡ γ ∘∙ (δ ∘∙ θ)
ass = refl

idl : ∀{Γ Δ}{γ : Sub∙ Δ Γ} → id∙ ∘∙ γ ≡ γ
idl = refl

idr : ∀{Γ Δ}{γ : Sub∙ Δ Γ} → γ ∘∙ id∙ ≡ γ
idr = refl

TY : Con
TY I = Σ (Ty ∣ yOb I ∣ ) λ X → Ty (∣ yOb I ∣ ▹ (∣ YT X ∣ [ ∣ yl (yOb I) ∣ ]T))

_[_]TY : ∀{I} → TY I → ∀{J} → Hom J I → TY J
(A ,Σ A') [ f ]TY = (A [ ∣ yHom f ∣ ]T) ,Σ (A' [ ∣ yHom f ∣ ^ _ ]T)

UU : Con∙
∣ UU ∣ = TY
(UU R) I A = ∀ J (f : Hom J I) → Lift (A J f ≡ (A I idc [ f ]TY))

Ty∙ : Con∙ → Set
Ty∙ Γ = Sub∙ Γ UU

_[_]T∙ : ∀{Γ} → Ty∙ Γ → ∀{Δ} → Sub∙ Δ Γ → Ty∙ Δ
A [ γ ]T∙ = A ∘∙ γ

[∘]T∙ : ∀{Γ}{A : Ty∙ Γ}{Δ}{γ : Sub∙ Δ Γ}{Θ}{δ : Sub∙ Θ Δ} → A [ γ ∘∙ δ ]T∙ ≡ A [ γ ]T∙ [ δ ]T∙
[∘]T∙ = refl

[id]T∙ : ∀{Γ}{A : Ty∙ Γ} → A [ id∙ {Γ} ]T∙ ≡ A
[id]T∙ = refl

-- TODO from here

Tm∙ : (Γ : Con∙) → Ty∙ Γ → Set
Tm∙ Γ A = {!!}

_▹∙_ : (Γ : Con∙) → Ty∙ Γ → Con∙
∣ Γ ▹∙ A ∣ = ∣ Γ ∣ ▹ λ I γI → {!!}
(Γ ▹∙ A) R = {!!}

{-

⌞_⌟ : Con' → Con∙
∣ ⌞ Γ ⌟ ∣ = ∣ Γ ∣
(⌞ Γ ⌟ R) I γI = Lift (∣ yl Γ ∣ I (γI I idc) ≡ γI)

ff : (Γ : Con') → Sub' Γ ⌜ ⌞ Γ ⌟ ⌝
∣ ff Γ ∣ I x = {!!}
nat (ff Γ) = {!!}

record Ty∙ (Γ : Con∙) : Set₁ where
  field
    A  : Ty ∣ ⌜ Γ ⌝ ∣
    _R : Ty (∣ ⌜ Γ ⌝ ∣ ▹ (∣ YT A ∣ [ ∣ yl ⌜ Γ ⌝ ∣ ]T))
open Ty∙ renaming (A to ∣_∣)

{-
-- TODO
⌜_⌝T : ∀{Γ} → Ty∙ Γ → Ty' ⌜ Γ ⌝
∣ ⌜ A ⌝T ∣ I (γI ,Σ γR) = Σ (∣ YT ∣ A ∣ ∣ I {!γI!}) {!!}
_∶_[_] ⌜ A ⌝T = {!!}
∣ ⌜ A ⌝ ∣ I = Σ (∣ Y ∣ Γ ∣ ∣ I) λ γI → Tm ∣ yOb I ∣ ((Γ R) [ ∣ yl (Y ∣ Γ ∣) ∣ I γI ]T)
⌜ Γ ⌝ ∶ (γJ ,Σ γR) [ f ] = (Y ∣ Γ ∣ ∶ γJ [ f ]) ,Σ γR [ ∣ yHom f ∣ ]t
-}

_[_]T∙ : ∀{Γ} → Ty∙ Γ → ∀{Δ} → Sub∙ Δ Γ → Ty∙ Δ
∣ A [ γ ]T∙ ∣ = ∣ A ∣ [ ∣ ⌜ γ ⌝s ∣ ]T
((_[_]T∙ {Γ} A γ) R) = (A R) [ ∣ ⌜ γ ⌝s ∣ ^ (∣ YT ∣ A ∣ ∣ [ ∣ yl ⌜ Γ ⌝ ∣ ]T) ]T


record Tm∙ (Γ : Con∙)(A : Ty∙ Γ) : Set where
  field
    a  : Tm ∣ ⌜ Γ ⌝ ∣ ∣ A ∣
    _R : Tm ∣ ⌜ Γ ⌝ ∣ (A R [ id , (∣ Yt a ∣ [ ∣ yl ⌜ Γ ⌝ ∣ ]t ) ]T)
open Tm∙ renaming (a to ∣_∣)

_[_]t∙ : ∀{Γ}{A : Ty∙ Γ} → Tm∙ Γ A → ∀{Δ}(γ : Sub∙ Δ Γ) → Tm∙ Δ (A [ γ ]T∙)
∣ a [ γ ]t∙ ∣ I δI = ∣ a ∣ I (∣ ⌜ γ ⌝s ∣ I δI)
((a [ γ ]t∙) R) I δI = (a R) I (∣ ⌜ γ ⌝s ∣ I δI)

[∘]t∙ : ∀{Γ}{A : Ty∙ Γ}{a : Tm∙ Γ A}{Δ}{γ : Sub∙ Δ Γ}{Θ}{δ : Sub∙ Θ Δ} → a [ γ ∘∙ δ ]t∙ ≡ a [ γ ]t∙ [ δ ]t∙
[∘]t∙ = refl

[id]t∙ : ∀{Γ}{A : Ty∙ Γ}{a : Tm∙ Γ A} → a [ id∙ ]t∙ ≡ a
[id]t∙ = refl

_▹∙_ : (Γ : Con∙) → Ty∙ Γ → Con'
∣ Γ ▹∙ A ∣ = ∣ Γ ∣ ▹ {!∣ A ∣!} -- (∣ ⌜ Γ ⌝ ∣ ▹ (∣ YT ∣ A ∣ ∣ [ ∣ yl ⌜ Γ ⌝ ∣ ]T)) ▹ (A R)
_∶_[_] (Γ ▹∙ A) = {!!}
[∘] (Γ ▹∙ A) = {!!}
[id] (Γ ▹∙ A) = {!!}
-}
