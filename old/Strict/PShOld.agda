module Strict.PShOld where

open import Agda.Primitive
import Lib as l
open import Cat

postulate
  C' : Cat
C : Cat
C = strictify C'
open Cat.Cat C

_⇒_ = Hom

UIP : ∀{i}{A : Set i}{a a' : A}{p q : a l.≡ a'} → p l.≡ q
UIP {p = l.refl}{q = l.refl} = l.refl

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_

-- why is the old style presheaf model not strict?

record Con (i : Level) : Set (lsuc i) where
  field
    ∣_∣    : (I : Ob) → Set i
    _∶_⟨_⟩ : {I : Ob}(γ : ∣_∣ I){J : Ob}(f : J ⇒ I) → ∣_∣ J
    ⟨id⟩   : l._≡_
      {A = {I : Ob}(γ : ∣_∣ I) → ∣_∣ I}
      (λ {I} γ → _∶_⟨_⟩ γ (idc I))
      (λ γ → γ)
    ⟨∘⟩    : l._≡_
      {A = {I : Ob}{γ : ∣_∣ I}{J : Ob}(f : J ⇒ I){K : Ob}(g : K ⇒ J) → ∣_∣ K}
      (λ {_}{γ} f g → _∶_⟨_⟩ γ (f ∘c g))
      (λ {_}{γ} f g → _∶_⟨_⟩ (_∶_⟨_⟩ γ f) g)
open Con

record Sub {i : Level}(Γ : Con i){j : Level}(Δ : Con j) : Set (i ⊔ j ⊔ lsuc lzero) where
  field
    ∣_∣ : {I : Ob}(γ : ∣ Γ ∣ I) → ∣ Δ ∣ I
    ⟨⟩  : l._≡_
      {A = {I : Ob}(γ : Con.∣ Γ ∣ I){J : Ob}(f : J ⇒ I) → Con.∣ Δ ∣ J}
      (λ {_} γ f → Δ ∶ ∣_∣ γ ⟨ f ⟩)
      (λ {_} γ f → ∣_∣ (Γ ∶ γ ⟨ f ⟩))
open Sub

record Ty {i : Level}(Γ : Con i)(j : Level) : Set (i ⊔ lsuc j) where
  field
    ∣_∣    : {I : Ob}(γ : Con.∣ Γ ∣ I) → Set j
    _∶_⟨_⟩ : {I : Ob}{γ : Con.∣ Γ ∣ I}(α : ∣_∣ γ){J : Ob}(f : J ⇒ I) → ∣_∣ (Γ ∶ γ ⟨ f ⟩)
    ⟨id⟩   : l._≡_
      {A = {I : Ob}{γ : Con.∣ Γ ∣ I}(α : ∣_∣ γ) → ∣_∣ γ}
      (λ {I}{γ : Con.∣ Γ ∣ I}(α : ∣_∣ γ) → l.tr ∣_∣ (l.ap (λ z → z γ) (⟨id⟩ Γ)) (_∶_⟨_⟩ {I} α (idc I)))
      (λ α → α)
    ⟨∘⟩    : l._≡_
      {A = {I : Ob}{γ : Con.∣ Γ ∣ I}{α : ∣_∣ γ}{J : Ob}(f : J ⇒ I){K : Ob}(g : K ⇒ J) → ∣_∣ (Γ Con.∶ (Γ Con.∶ γ ⟨ f ⟩) ⟨ g ⟩)}
      (λ {I}{γ : Con.∣ Γ ∣ I}{α : ∣_∣ γ}{J}(f : J ⇒ I){K}(g : K ⇒ J) → l.tr ∣_∣ (l.ap (λ z → z {_}{γ} f g) (⟨∘⟩ Γ)) (_∶_⟨_⟩ α (f ∘c g)))
      (λ {_}{γ}{α} f g → _∶_⟨_⟩ (_∶_⟨_⟩ α f) g)
open Ty

record Tm {i : Level}(Γ : Con i){j : Level}(A : Ty Γ j) : Set (i ⊔ j ⊔ lsuc lzero) where
  field
    ∣_∣ : {I : Ob}(γ :     ∣ Γ ∣ I) → ∣ A ∣ γ
    ⟨⟩  : l._≡_
      {A = {I : Ob}(γ : Con.∣ Γ ∣ I){J : Ob}(f : J ⇒ I) → Ty.∣ A ∣ (Γ ∶ γ ⟨ f ⟩)}
      (λ {_} γ f → A ∶ ∣_∣ γ ⟨ f ⟩)
      (λ {_} γ f → ∣_∣ (Γ ∶ γ ⟨ f ⟩))
open Tm

Sub=' :  {i : Level}{Γ : Con i}{j : Level}{Δ : Con j}
    {∣_∣₀ ∣_∣₁ : {I : Ob}(γ : ∣ Γ ∣ I) → ∣ Δ ∣ I}(∣_∣₂ : l._≡_ {A = {I : Ob}(γ : ∣ Γ ∣ I) → ∣ Δ ∣ I} ∣_∣₀ ∣_∣₁)
    {⟨⟩₀ : l._≡_ {A = {I : Ob}(γ : Con.∣ Γ ∣ I){J : Ob}(f : J ⇒ I) → Con.∣ Δ ∣ J} (λ {_} γ f → Δ ∶ ∣_∣₀ γ ⟨ f ⟩) (λ {_} γ f → ∣_∣₀ (Γ ∶ γ ⟨ f ⟩))}
    {⟨⟩₁ : l._≡_ {A = {I : Ob}(γ : Con.∣ Γ ∣ I){J : Ob}(f : J ⇒ I) → Con.∣ Δ ∣ J} (λ {_} γ f → Δ ∶ ∣_∣₁ γ ⟨ f ⟩) (λ {_} γ f → ∣_∣₁ (Γ ∶ γ ⟨ f ⟩))}
    (⟨⟩₂ : l._≡_ {A = l._≡_ {A = {I : Ob}(γ : Con.∣ Γ ∣ I){J : Ob}(f : J ⇒ I) → Con.∣ Δ ∣ J} (λ {_} γ f → Δ ∶ ∣_∣₁ γ ⟨ f ⟩) (λ {_} γ f → ∣_∣₁ (Γ ∶ γ ⟨ f ⟩))}
                 (l.tr (λ ∣_∣ → l._≡_ {A = {I : Ob}(γ : Con.∣ Γ ∣ I){J : Ob}(f : J ⇒ I) → Con.∣ Δ ∣ J} (λ {_} γ f → Δ ∶ ∣_∣ γ ⟨ f ⟩) (λ {_} γ f → ∣_∣ (Γ ∶ γ ⟨ f ⟩))) ∣_∣₂ ⟨⟩₀)
                 ⟨⟩₁) →
    l._≡_ {A = Sub Γ Δ}
          (record {∣_∣ = ∣_∣₀ ; ⟨⟩ = ⟨⟩₀ } )
          (record {∣_∣ = ∣_∣₁ ; ⟨⟩ = ⟨⟩₁ } )
Sub=' l.refl l.refl = l.refl

Sub= :  {i : Level}{Γ : Con i}{j : Level}{Δ : Con j}
    {∣_∣₀ ∣_∣₁ : {I : Ob}(γ : ∣ Γ ∣ I) → ∣ Δ ∣ I}
    (∣_∣₂ : l._≡_ {A = {I : Ob}(γ : ∣ Γ ∣ I) → ∣ Δ ∣ I} ∣_∣₀ ∣_∣₁)
    {⟨⟩₀ : l._≡_ {A = {I : Ob}(γ : Con.∣ Γ ∣ I){J : Ob}(f : J ⇒ I) → Con.∣ Δ ∣ J} (λ {_} γ f → Δ ∶ ∣_∣₀ γ ⟨ f ⟩) (λ {_} γ f → ∣_∣₀ (Γ ∶ γ ⟨ f ⟩))}
    {⟨⟩₁ : l._≡_ {A = {I : Ob}(γ : Con.∣ Γ ∣ I){J : Ob}(f : J ⇒ I) → Con.∣ Δ ∣ J} (λ {_} γ f → Δ ∶ ∣_∣₁ γ ⟨ f ⟩) (λ {_} γ f → ∣_∣₁ (Γ ∶ γ ⟨ f ⟩))} →
    l._≡_ {A = Sub Γ Δ}
          (record {∣_∣ = ∣_∣₀ ; ⟨⟩ = ⟨⟩₀ } )
          (record {∣_∣ = ∣_∣₁ ; ⟨⟩ = ⟨⟩₁ } )
Sub= ∣_∣₂ = Sub=' ∣_∣₂ UIP 

id : ∀{i}{Γ : Con i} → Sub Γ Γ
∣ id ∣ γ = γ
⟨⟩ id = l.refl

_∘_ : ∀{i}{Γ : Con i}{i'}{Γ' : Con i'}(σ : Sub Γ' Γ){i''}{Γ'' : Con i''}(σ' : Sub Γ'' Γ') → Sub Γ'' Γ
∣ σ ∘ σ' ∣ γ'' = ∣ σ ∣ (∣ σ' ∣ γ'')
⟨⟩ (_∘_ {_}{Γ}{_}{Γ'} σ {_}{Γ''} σ') = l.ap (λ z {_} γ → z (∣ σ' ∣ γ)) (⟨⟩ σ) l.◾ l.ap (λ z {I} γ {J} f → ∣ σ ∣ (z γ f)) (⟨⟩ σ')

idl : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → id ∘ σ l.≡ σ
idl = Sub= l.refl
idr : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → σ ∘ id l.≡ σ
idr = Sub= l.refl
ass : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Ξ : Con k}{δ : Sub Ξ Θ}{l}{Γ : Con l}{ν : Sub Γ Ξ} → (σ ∘ δ) ∘ ν l.≡ σ ∘ (δ ∘ ν)
ass = Sub= l.refl

_[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
∣ A [ σ ]T ∣ γ = ∣ A ∣ (∣ σ ∣ γ)
_∶_⟨_⟩ (A [ σ ]T) {_}{γ} α f = l.tr ∣ A ∣ (l.ap (λ z → z γ f) (⟨⟩ σ)) (A ∶ α ⟨ f ⟩)
⟨id⟩ (A [ σ ]T) = {!⟨id⟩ A!}
⟨∘⟩ (A [ σ ]T) = {!!}
