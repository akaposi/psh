{-# OPTIONS --prop --rewriting #-}

module SeTT.PSh where

open import Agda.Primitive
open import SeTT.lib

record Cat : Setω where
  field
    ∣_∣   : (i : Level) → Set (lsuc i)
    _∶_⇒_ : ∀{i}(Ω : ∣_∣ i){j}(Ψ : ∣_∣ j) → Set (i ⊔ j)
    idc   : ∀{i}(Ψ : ∣_∣ i) → _∶_⇒_ Ψ Ψ
    _∶_∘_ : ∀{i}{Ω : ∣_∣ i}{j}{Ψ : ∣_∣ j}(β : _∶_⇒_ Ω Ψ)
      {k}{Ω' : ∣_∣ k}(β' : _∶_⇒_ Ω' Ω) → _∶_⇒_ Ω' Ψ
    idlc  : ∀{i}{Ω : ∣_∣ i}{j}{Ψ : ∣_∣ j}{β : _∶_⇒_ Ω Ψ} →
      (_∶_∘_ (idc Ψ) β) ≡ β
    idrc  : ∀{i}{Ω : ∣_∣ i}{j}{Ψ : ∣_∣ j}{β : _∶_⇒_ Ω Ψ} →
      (_∶_∘_ β (idc Ω)) ≡ β
    assc  : ∀{i}{Ω : ∣_∣ i}{j}{Ψ : ∣_∣ j}{β : _∶_⇒_ Ω Ψ}
      {k}{Ω' : ∣_∣ k}{β' : _∶_⇒_ Ω' Ω}
      {l}{Ω'' : ∣_∣ l}{β'' : _∶_⇒_ Ω'' Ω'} →
      _∶_∘_ (_∶_∘_ β β') β'' ≡ _∶_∘_ β (_∶_∘_ β' β'')
  infix 5 _∶_⇒_
  infix 6 _∶_∘_
open Cat public

-- shallow embedding of a category
C : Cat
C = record
  { ∣_∣ = λ i → Set i
  ; _∶_⇒_ = λ Γ Δ → Γ → Δ
  ; idc = λ Ψ γ → γ
  ; _∶_∘_ = λ β β' γ → β (β' γ)
  ; idlc = refl
  ; idrc = refl
  ; assc = refl
  }

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_
infixr 6 _⇒_
infixl 7 _$_
infixl 6 _,Σ_

record Con j : Setω where
  field
    ∣_∣   : ∀{i}(Ψ : ∣ C ∣ i) → Set j
    _∶_⇒_ : ∀{i}{Ψ : Cat.∣ C ∣ i}(γ : ∣_∣ Ψ){k}{Ω : Cat.∣ C ∣ k}(β : C ∶ Ω ⇒ Ψ) → ∣_∣ Ω
    idc   : ∀{i}{Ψ : Cat.∣ C ∣ i}(γ : ∣_∣ Ψ) → _∶_⇒_ γ (idc C Ψ) ≡ γ
    _∶_∘_ : ∀{i}{Ω : Cat.∣ C ∣ i}{j}{Ψ : Cat.∣ C ∣ j}(β : C Cat.∶ Ω ⇒ Ψ)
      {k}{Ω' : Cat.∣ C ∣ k}(β' : C Cat.∶ Ω' ⇒ Ω){γ : ∣_∣ Ψ} →
      _∶_⇒_ γ (C Cat.∶ β ∘ β') ≡ _∶_⇒_ (_∶_⇒_ γ β) β'
  infix 5 _∶_⇒_
  infix 6 _∶_∘_
open Con public

record Ty {i}(Γ : Con i) j : Setω where
  field
    ∣_∣   : ∀{i}{Ψ : ∣ C ∣ i}(γ : ∣ Γ ∣ Ψ) → Set j
    _∶_⇒_ : ∀{i}{Ψ : Cat.∣ C ∣ i}{γ : Con.∣ Γ ∣ Ψ}(α : ∣_∣ γ){k}{Ω : Cat.∣ C ∣ k}(β : C ∶ Ω ⇒ Ψ) → ∣_∣ (Γ ∶ γ ⇒ β)
    idc   : ∀{i}{Ψ : Cat.∣ C ∣ i}{γ : Con.∣ Γ ∣ Ψ}(α : ∣_∣ γ) → ∣_∣ ∶ _∶_⇒_ α (idc C Ψ) ≡[ idc Γ γ ] α
    _∶_∘_ : ∀{i}{Ω : Cat.∣ C ∣ i}{j}{Ψ : Cat.∣ C ∣ j}(β : C Cat.∶ Ω ⇒ Ψ)
      {k}{Ω' : Cat.∣ C ∣ k}(β' : C Cat.∶ Ω' ⇒ Ω){γ : Con.∣ Γ ∣ Ψ}{α : ∣_∣ γ} →
      ∣_∣ ∶ _∶_⇒_ α (C ∶ β ∘ β') ≡[ Γ ∶ β ∘ β' ] _∶_⇒_ (_∶_⇒_ α β) β'
  infix 5 _∶_⇒_
  infix 6 _∶_∘_
open Ty public
{-
mkTy= : ∀{i}{Γ : Con i}{j}
  {∣_∣₀ ∣_∣₁ : ∀{i}{Ψ : ∣ C ∣ i}(γ : ∣ Γ ∣ Ψ) → Set j}(∣_∣₂ : _≡ω_ {A = ∀{i}{Ψ : ∣ C ∣ i}(γ : ∣ Γ ∣ Ψ) → Set j} ∣_∣₀ ∣_∣₁)
  {_∶_⇒_₀ : ∀{i}{Ψ : Cat.∣ C ∣ i}{γ : Con.∣ Γ ∣ Ψ}(α : ∣_∣₀ γ){k}{Ω : Cat.∣ C ∣ k}(β : C ∶ Ω ⇒ Ψ) → ∣_∣₀ (Γ ∶ γ ⇒ β)}
  {_∶_⇒_₁ : ∀{i}{Ψ : Cat.∣ C ∣ i}{γ : Con.∣ Γ ∣ Ψ}(α : ∣_∣₁ γ){k}{Ω : Cat.∣ C ∣ k}(β : C ∶ Ω ⇒ Ψ) → ∣_∣₁ (Γ ∶ γ ⇒ β)}
  (_∶_⇒_₂ : ? ∶ _∶_⇒_₀ ≡[ ? ] _∶_⇒_₁)
  {idc₀   : ∀{i}{Ψ : Cat.∣ C ∣ i}{γ : Con.∣ Γ ∣ Ψ}(α : ∣_∣₀ γ) → ∣_∣ ∶ _∶_⇒_ α (idc C Ψ) ≡[ idc Γ γ ] α}
  {idc₁   : ∀{i}{Ψ : Cat.∣ C ∣ i}{γ : Con.∣ Γ ∣ Ψ}(α : ∣_∣₁ γ) → ∣_∣ ∶ _∶_⇒_ α (idc C Ψ) ≡[ idc Γ γ ] α}
  {_∶_∘_₀ : ∀{i}{Ω : Cat.∣ C ∣ i}{j}{Ψ : Cat.∣ C ∣ j}(β : C Cat.∶ Ω ⇒ Ψ){k}{Ω' : Cat.∣ C ∣ k}(β' : C Cat.∶ Ω' ⇒ Ω){γ : Con.∣ Γ ∣ Ψ}{α : ∣_∣ γ} → ∣_∣ ∶ _∶_⇒_ α (C ∶ β ∘ β') ≡[ Γ ∶ β ∘ β' ] _∶_⇒_ (_∶_⇒_ α β) β'}
  {_∶_∘_₁ : ∀{i}{Ω : Cat.∣ C ∣ i}{j}{Ψ : Cat.∣ C ∣ j}(β : C Cat.∶ Ω ⇒ Ψ){k}{Ω' : Cat.∣ C ∣ k}(β' : C Cat.∶ Ω' ⇒ Ω){γ : Con.∣ Γ ∣ Ψ}{α : ∣_∣ γ} → ∣_∣ ∶ _∶_⇒_ α (C ∶ β ∘ β') ≡[ Γ ∶ β ∘ β' ] _∶_⇒_ (_∶_⇒_ α β) β'} →
  _≡_ {A = Ty Γ j}
      (record { ∣_∣ = ∣_∣₀ ; _∶_⇒_ = _∶_⇒_₀ ; idc = idc₀ ; _∶_∘_ = _∶_∘_₀ })
      (record { ∣_∣ = ∣_∣₁ ; _∶_⇒_ = _∶_⇒_₁ ; idc = idc₁ ; _∶_∘_ = _∶_∘_₁ })
mkTy= = ?
-}
record Sub {i}(Γ : Con i){j}(Δ : Con j) : Setω where
  field
    ∣_∣   : ∀{i}{Ψ : ∣ C ∣ i}(γ : ∣ Γ ∣ Ψ) → ∣ Δ ∣ Ψ
    _∶_⇒_ : ∀{i}{Ψ : Cat.∣ C ∣ i}(γ : Con.∣ Γ ∣ Ψ){k}{Ω : Cat.∣ C ∣ k}(β : C ∶ Ω ⇒ Ψ) →
      Δ ∶ ∣_∣ γ ⇒ β ≡ ∣_∣ (Γ ∶ γ ⇒ β)
  infix 5 _∶_⇒_
open Sub public

record Tm {i}(Γ : Con i){j}(A : Ty Γ j) : Setω where
  field
    ∣_∣   : ∀{i}{Ψ : ∣ C ∣ i}(γ : ∣ Γ ∣ Ψ) → ∣ A ∣ γ
    _∶_⇒_ : ∀{i}{Ψ : Cat.∣ C ∣ i}{γ : Con.∣ Γ ∣ Ψ}(α : Ty.∣ A ∣ γ){k}{Ω : Cat.∣ C ∣ k}(β : C ∶ Ω ⇒ Ψ) →
      A ∶ ∣_∣ γ ⇒ β ≡ ∣_∣ (Γ ∶ γ ⇒ β)
  infix 5 _∶_⇒_
open Tm public
  
id : ∀{i}{Γ : Con i} → Sub Γ Γ
id {Γ = Γ} = record { ∣_∣ = λ γ → γ ; _∶_⇒_ = λ γ β → refl }

_∘_ : ∀{i}{Θ : Con i}{j}{Δ : Con j}(σ : Sub Θ Δ){k}{Γ : Con k}(δ : Sub Γ Θ) →
  Sub Γ Δ
_∘_ {_}{Θ} σ δ = record {
  ∣_∣ = λ γ → ∣ σ ∣ (∣ δ ∣ γ) ;
  _∶_⇒_ = λ γ β → σ ∶ ∣ δ ∣ γ ⇒ β ◾ ap ∣ σ ∣ (δ ∶ γ ⇒ β) }

ass : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Ξ : Con k}{δ : Sub Ξ Θ}{l}{Γ : Con l}{ν : Sub Γ Ξ} → (σ ∘ δ) ∘ ν ≡ω σ ∘ (δ ∘ ν)
ass = reflω

idl : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → id ∘ σ ≡ω σ
idl = reflω

idr : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → σ ∘ id ≡ω σ
idr = reflω

_[_]T : ∀{i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
_[_]T {_}{Δ} A {_}{Γ} σ = record {
  ∣_∣ = λ γ → ∣ A ∣ (∣ σ ∣ γ) ;
  _∶_⇒_ = λ α β → coe ∣ A ∣ (σ ∶ _ ⇒ β) (A ∶ α ⇒ β) ;
  idc = λ α → ∣ A ∣ ∘ ∣ σ ∣ ∶ (∣ A ∣ ∶ coh ∣ A ∣ (σ ∶ _ ⇒ idc C _) (A ∶ α ⇒ idc C _) ◾ idc A α) ;
  _∶_∘_ = λ β β' {γ}{α} → ∣ A ∣ ∘ ∣ σ ∣ ∶ (∣ A ∣ ∶ (coh ∣ A ∣ (σ ∶ γ ⇒ C ∶ β ∘ β') (A ∶ α ⇒ C ∶ β ∘ β')) ◾ (∣ A ∣ ∶ ∣ A ∣ ∶ A ∶ β ∘ β' ◾ (∣ A ∣ ∘' (Δ ∶_⇒ β') ∶ (ap' ∣ A ∣ (λ z → A ∶ z ⇒ β') (∣ A ∣ ∶ coh ∣ A ∣ (σ ∶ γ ⇒ β) (A ∶ α ⇒ β) ⁻¹))) ◾ (∣ A ∣ ∶ (coh ∣ A ∣ (σ ∶ Γ ∶ γ ⇒ β ⇒ β') (A ∶ coe ∣ A ∣ (σ ∶ γ ⇒ β) (A ∶ α ⇒ β) ⇒ β')) ⁻¹)))
  }

_[_]t : ∀{i}{Δ : Con i}{j}{A : Ty Δ j}(t : Tm Δ A){k}{Γ : Con k}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
_[_]t {A = A} t σ = record {
  ∣_∣ = λ γ → ∣ t ∣ (∣ σ ∣ γ) ;
  _∶_⇒_ = λ {_}{_}{γ} α β → ap (coe ∣ A ∣ (σ ∶ γ ⇒ β)) (t ∶ α ⇒ β) ◾ {!!} } -- apd ∣ t ∣ (σ ∶ γ ⇒ β)

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T ≡ω A
[id]T = {!reflω!} -- reflω

[∘]T : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}
  {l}{A : Ty Δ l} → A [ σ ]T [ δ ]T ≡ω A [ σ ∘ δ ]T
[∘]T = {!reflω!}

