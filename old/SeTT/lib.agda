{-# OPTIONS --prop --rewriting #-}

module SeTT.lib where

open import Agda.Primitive

infix 4 _≡_
infix 4 _≡ω_
infix 4 _◾_

data _≡_ {i}{A : Set i}(a : A) : A → Prop i where
  refl : a ≡ a

{-# BUILTIN REWRITE _≡_ #-}

J : ∀{i}{A : Set i}{a : A}{j}(P : {a' : A} → a ≡ a' → Prop j) → P refl → {a' : A} → (a= : a ≡ a') → P a=
J P pr refl = pr

_◾_ : ∀{i}{A : Set i}{a a' : A}(a= : a ≡ a'){a'' : A}(a=' : a' ≡ a'') → a ≡ a''
refl ◾ refl = refl

_⁻¹ : ∀{i}{A : Set i}{a a' : A}(a= : a ≡ a') → a' ≡ a
refl ⁻¹ = refl

ap : ∀{i}{A : Set i}{j}{B : Set j}(f : A → B){a a' : A}(a= : a ≡ a') → f a ≡ f a'
ap f refl = refl

postulate
  coe  : ∀{i}{A : Set i}{j}(B : A → Set j){a a' : A}(a= : a ≡ a')(b : B a) → B a'
  coeR : ∀{i}{A : Set i}{j}{B : A → Set j}{a : A}{b : B a} → coe B refl b ≡ b
  coe◾ : ∀{i}{A : Set i}{j}{B : A → Set j}{a a' a'' : A}{a= : a ≡ a'}{a=' : a' ≡ a''}{b : B a} → coe B a=' (coe B a= b) ≡ coe B (a= ◾ a=') b

{-# REWRITE coeR #-}
{-# REWRITE coe◾ #-}

-- apd : ∀{i}{A : Set i}{j}{B : A → Set j}(f : (x : A) → B x){a a' : A}(a= : a ≡ a') → coe B a= (f a) ≡ f a'
-- apd f refl = refl

data _∶_≡[_]_ {i}{A : Set i}{j}(B : A → Set j){a : A}(b : B a) : {a' : A}(a= : a ≡ a')(b' : B a') → Prop j where
  refl : B ∶ b ≡[ refl ] b

postulate
  coh : ∀{i}{A : Set i}{j}(B : A → Set j){a a' : A}(a= : a ≡ a')(b : B a) → B ∶ coe B a= b ≡[ a= ⁻¹ ] b

postulate
  _≡ω_ : {A : Setω}(a : A)(a' : A) → Setω
  reflω : {A : Setω}{a : A} → a ≡ω a

_∶_◾_ : ∀{i}{A : Set i}{j}(B : A → Set j){a a' : A}{b : B a}{b' : B a'}{a= : a ≡ a'}(b= : B ∶ b ≡[ a= ] b')
  {a'' : A}{b'' : B a''}{a=' : a' ≡ a''}(b=' : B ∶ b' ≡[ a=' ] b'') → B ∶ b ≡[ a= ◾ a=' ] b''
B ∶ refl ◾ refl = refl

_∶_⁻¹ : ∀{i}{A : Set i}{j}(B : A → Set j){a a' : A}{b : B a}{b' : B a'}{a= : a ≡ a'}(b= : B ∶ b ≡[ a= ] b') →
  B ∶ b' ≡[ a= ⁻¹ ] b
B ∶ refl ⁻¹ = refl

_∘_∶_ : ∀{i}{A : Set i}{j}{B : Set j}{k}(C : B → Set k)(f : A → B){a a' : A}{c : C (f a)}{c' : C (f a')}{a= : a ≡ a'}(c= : C ∶ c ≡[ ap f a= ] c') →
  (λ z → C (f z)) ∶ c ≡[ a= ] c'
_∘_∶_ C f {a= = refl} refl = refl

_∘'_∶_ : ∀{i}{A : Set i}{j}{B : Set j}{k}(C : B → Set k)(f : A → B){a a' : A}{c : C (f a)}{c' : C (f a')}{a= : a ≡ a'}(c= : (λ z → C (f z)) ∶ c ≡[ a= ] c') →
  C ∶ c ≡[ ap f a= ] c'
_∘'_∶_ C f {a= = refl} refl = refl

ap' : ∀{i}{A : Set i}{j}(B : A → Set j){k}{C : A → Set k}(f : {a : A} → B a → C a)
  {a a' : A}{b : B a}{b' : B a'}{a= : a ≡ a'}(b= : B ∶ b ≡[ a= ] b') → C ∶ f b ≡[ a= ] f b'
ap' _ f refl = refl
