open import Agda.Primitive
open import Agda.Builtin.Nat renaming (Nat to ℕ)
open import Agda.Builtin.Unit
open import Agda.Builtin.Equality

cong : ∀{ℓ ℓ'}{A : Set ℓ}{B : Set ℓ'}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong f refl = refl

_◾_ : ∀{ℓ}{A : Set ℓ}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
refl ◾ refl = refl

data ⊥ : Set where

data Fin : ℕ → Set where
  zero : {n : ℕ} → Fin (suc n)
  suc  : {n : ℕ} → Fin n → Fin (suc n)

data _+2 (A : Set) : Set where
  inl : A → A +2
  inr0 : A +2
  inr1 : A +2

fmap : {A B : Set} → (A → B) → A +2 → B +2
fmap f (inl x) = inl (f x)
fmap f inr0 = inr0
fmap f inr1 = inr1

Ob = ℕ
variable
  I J K L : Ob

_≡ℕ_ : ℕ → ℕ → Set
zero ≡ℕ zero = ⊤
suc n ≡ℕ suc m = n ≡ m
_ ≡ℕ _ = ⊥

⌜_⌝ : {J : ℕ} → Fin J → ℕ
⌜ zero ⌝ = zero
⌜ suc x ⌝ = suc ⌜ x ⌝

_≡+2_ : ℕ +2 → ℕ +2 → Set
inl x ≡+2 inl y = x ≡ℕ y
inl x ≡+2 _ = ⊥
inr0  ≡+2 _ = ⊥
inr1  ≡+2 _ = ⊥

record Hom' (I J : Ob) : Set where
  field
    map : Fin J → Fin I +2
    inj : {x y : Fin J} → fmap ⌜_⌝ (map x) ≡+2 fmap ⌜_⌝ (map y) → ⌜ x ⌝ ≡ℕ ⌜ y ⌝
open Hom'

id' : Hom' I I
id' = record { map = inl ; inj = λ e → e }

_∘'_ : Hom' J I → Hom' K J → Hom' K I
map (f ∘' g) x with map f x
... | inl y = map g y
... | inr0 = inr0
... | inr1 = inr1
inj (f ∘' g) {zero} {zero} e = tt
inj (f ∘' g) {zero} {suc y} e = {!!}
inj (f ∘' g) {suc x} {zero} e = {!!}
inj (f ∘' g) {suc x} {suc y} e = {!inj (f ∘' g) !}

ass' : ∀{f : Hom' J I}{g : Hom' K J}{h : Hom' L K} → (f ∘' g) ∘' h ≡ f ∘' (g ∘' h)
ass' = {!!}

record Hom (I J : Ob) : Set where
  field
    map : {X : Ob} → Hom' X I → Hom' X J
    -- map : yI → yJ
    -- eq : map f [ g ]{yJ} = map (f [g]{yI})
    -- eq : map f ∘' g = map (f ∘' g)
    eq : ∀{X X'}{f : Hom' X I}{g : Hom' X' X}{g' : Hom' X' I} → f ∘' g ≡ g' → map f ∘' g ≡ map g'
open Hom

_∘_ : Hom J I → Hom K J → Hom K I
map (f ∘ g) h = map f (map g h)
eq  (f ∘ g) {f = h}{g = i}{g' = i'} e = eq f (eq g e)

id : Hom I I
id = record { map = λ f → f ; eq = λ e → e }

y : Hom' J I → Hom J I
y f = record { map = λ g → f ∘' g ; eq = λ e → ass' ◾ cong (f ∘'_) e }

record Con : Set₁ where
  field
    Γ : Ob → Set
    rel : (∀ {J} → Hom J I → Γ J) → Set
open Con public renaming (Γ to ∣_∣)

record PSh : Set₁ where
  field
    F    : Ob → Set
    _[_] : F I → Hom J I → F J
    [∘]  : {f : Hom J I}{g : Hom K J}{x : F I} → ((x [ f ]) [ g ]) ≡ (x [ f ∘ g ])
    [id] : {x : F I} → x [ id ] ≡ x
open PSh public renaming (F to ∣_∣; _[_] to _∶_[_])

record Y
  (I : Ob)
  (A : ∀ {J} → Hom J I → Set)
  (A-rel : ∀ {J} (f : Hom J I) → (∀ {K} (g : Hom K J) → A (f ∘ g)) → Set)
  : Set
  where
  field
    a   : (f : Hom J I) → A f
    rel : (f : Hom J I) → A-rel f λ g → a (f ∘ g)
open Y public renaming (a to ∣_∣)

-- converting Γ to ordinary presheaf, and this is the action on objects
Yᶜ : Con → Ob → Set
Yᶜ Γ I = Y I (λ {J} _ → ∣ Γ ∣ J) (λ _ → Γ .rel)

infixl 9 _|ᶜ_
_|ᶜ_ : ∀{Γ} → Yᶜ Γ I → Hom J I → Yᶜ Γ J
∣ γ |ᶜ f ∣ g = ∣ γ ∣ (f ∘ g)
(γ |ᶜ f) .rel g = γ .rel (f ∘ g)

ι : Con → PSh
PSh.F (ι Γ) = Yᶜ Γ
PSh._[_] (ι Γ) = _|ᶜ_
PSh.[∘] (ι Γ) = refl
PSh.[id] (ι Γ) = refl

O0 I0 : Hom 0 1
O0 = y (record { map = λ _ → inr0 ; inj = λ () })
I0 = y (record { map = λ _ → inr1 ; inj = λ () })

R0 : Hom 1 0
R0 = y (record { map = λ () ; inj = λ { {x = ()} } })

O0∘R0 : O0 ∘ R0 ≡ id
O0∘R0 = {!!}

postulate
  X : Con

X0 X1 X2 X3 : Set
X0 = ∣ ι X ∣ 0
X1 = ∣ ι X ∣ 1
X2 = ∣ ι X ∣ 2
X3 = ∣ ι X ∣ 3

XO0 XI0 : X1 → X0
XO0 = ι X ∶_[ O0 ]
XI0 = ι X ∶_[ I0 ]

XR0 : X0 → X1
XR0 = ι X ∶_[ R0 ]
