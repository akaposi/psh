{-# OPTIONS --without-K --rewriting #-}

module CatRewrite where

open import Lib

infixl 6  _∘c_

postulate
  Ob   : Set
  Hom  : Ob → Ob → Set
  idc  : {I : Ob} → Hom I I
  _∘c_ : {I J K : Ob} → Hom J K → Hom I J → Hom I K
  idcl : {I J : Ob}{f : Hom I J} → idc ∘c f ≡ f
  idcr : {I J : Ob}{f : Hom I J} → f ∘c idc ≡ f
  assc : {K L : Ob}{h : Hom K L}{J : Ob}{g : Hom J K}{I : Ob}{f : Hom I J} →
         (h ∘c g) ∘c f ≡ h ∘c (g ∘c f)

{-# BUILTIN REWRITE _≡_ #-}
{-# REWRITE idcl idcr assc #-}
