{-# OPTIONS --prop --type-in-type #-}

-- this is the nicest presheaf model

open import Agda.Primitive
open import Cat

postulate
  C' : Cat
C : Cat
C = strictify C'
open Cat.Cat C

infix 4 _≡_
infixl 5 _,Σ_
data _≡_ {i}{A : Set i}(a : A) : A → Prop i where
  refl : a ≡ a
record ⊤ : Set where
record ⊤p : Prop where
record Σ {i}(A : Set i){j}(B : A → Set j) : Set (i ⊔ j) where
  constructor _,Σ_
  field
    π₁ : A
    π₂ : B π₁
open Σ
-- ,= : ∀{i}{A : Set i}{j}{B : A → Set j}{a₀ a₁ : A}(a₌ : a₀ ≡ a₁){b₀ : B a₀}{b₁ : B a₁} → b₀ ≡ b₁
record Lift {i}(A : Prop i) : Set i where
  constructor mk
  field un : A
open Lift
cong : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong f refl = refl

record Con : Set₁ where
  field
    Γ    : Ob → Set
    _[_] : ∀{I} → Γ I → ∀{J} → Hom J I → Γ J
    [∘]  : ∀{I}(γI : Γ I){J}(f : Hom J I){K}(g : Hom K J) → γI [ f ∘c g ] ≡ γI [ f ] [ g ] -- TODO: change this to pointfree, then we try2fix should be fine
    [id] : ∀{I}(γI : Γ I) → γI [ idc ] ≡ γI
  infix 8 _[_]
open Con renaming (Γ to ∣_∣; _[_] to _∶_[_])

record Sub (Δ Γ : Con) : Set where
  field
    γ   : ∀{I} → ∣ Δ ∣ I → ∣ Γ ∣ I
--    nat : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I){δJ : ∣ Δ ∣ J} → Δ ∶ δI [ f ] ≡ δJ → Γ ∶ γ δI [ f ] ≡ γ δJ
    nat : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I) → Γ ∶ γ δI [ f ] ≡ γ (Δ ∶ δI [ f ])
open Sub renaming (γ to ∣_∣)

{-
Sub= : {Γ Δ : Con} →
  {γ₀ γ₁ : ∀{I} → ∣ Δ ∣ I → ∣ Γ ∣ I}(γ₂ : (λ {I} → γ₀ {I}) ≡ γ₁) 
  {nat₀ : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I){δJ : ∣ Δ ∣ J} → Δ ∶ δI [ f ] ≡ δJ → Γ ∶ γ₀ δI [ f ] ≡ γ₀ δJ}
  {nat₁ : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I){δJ : ∣ Δ ∣ J} → Δ ∶ δI [ f ] ≡ δJ → Γ ∶ γ₁ δI [ f ] ≡ γ₁ δJ} →
  _≡_ {A = Sub Δ Γ} (record { γ = γ₀ ; nat = nat₀ }) (record { γ = γ₁ ; nat = nat₁ })
Sub= refl = refl
-}
_∘_ : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
∣ γ ∘ δ ∣ θI = ∣ γ ∣ (∣ δ ∣ θI)
nat (γ ∘ δ) f = {!!}

id : ∀{Γ} → Sub Γ Γ
∣ id ∣ γI = γI
nat id f = refl

ass : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{Ξ}{θ : Sub Ξ Θ} → (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
ass = refl

idl : ∀{Γ Δ}{γ : Sub Δ Γ} → id ∘ γ ≡ γ
idl = refl

idr : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘ id ≡ γ
idr = refl

◆ : Con
∣_∣ ◆ I = ⊤
_∶_[_] ◆ _ _ = _
[∘] ◆ _ _ _ = refl
[id] ◆ _ = refl

ε : ∀{Γ} → Sub Γ ◆
∣ ε ∣ _ = _
nat ε _ = refl

◆η : ∀{Γ}(σ : Sub Γ ◆) → σ ≡ ε
◆η _ = refl

yOb : Ob → Con
∣ yOb I ∣ J = Hom J I
yOb I ∶ f [ g ] = f ∘c g
[∘] (yOb I) f g h = refl
[id] (yOb I) f = refl

yHom : ∀{J I} → Hom J I → Sub (yOb J) (yOb I)
∣ yHom f ∣ g = f ∘c g
nat (yHom f) g = {!!}

yl : ∀{Γ I} → ∣ Γ ∣ I → Sub (yOb I) Γ
∣ yl {Γ} γI ∣ f = Γ ∶ γI [ f ]
nat (yl γI) f = {!!}
module W where
  infix 7 _▹_

  record Ty (Γ : Con) : Set₁ where
    field
      A     : (I : Ob) → ∣ Γ ∣ I → Set
      _[_]_ : ∀{I}{γI : ∣ Γ ∣ I} → A I γI → ∀{J}(f : Hom J I) → {γJ : ∣ Γ ∣ J} → Γ ∶ γI [ f ] ≡ γJ → A J γJ
      [∘]   : ∀{I}{γI : ∣ Γ ∣ I}(aI : A I γI){J}(f : Hom J I){K}(g : Hom K J) → aI [ f ∘c g ] [∘] Γ γI f g ≡ (aI [ f ] refl) [ g ] refl
      [id]  : ∀{I}{γI : ∣ Γ ∣ I}(aI : A I γI) → aI [ idc ] [id] Γ γI ≡ aI
    infix 8 _[_]
  open Ty renaming (A to ∣_∣; _[_]_ to _∶_[_]_)
  
  _[_]T : ∀{Γ} → Ty Γ → ∀{Δ} → Sub Δ Γ → Ty Δ
  ∣ A [ γ ]T ∣ I δI = ∣ A ∣ I (∣ γ ∣ δI)
  (A [ γ ]T) ∶ aI [ f ] γf = A ∶ aI [ f ] {!!}
  [∘] (A [ γ ]T) aI f g = {!!}
  [id] (A [ γ ]T) aI = {!!}

  [∘]T : ∀{Γ}{A : Ty Γ}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → A [ γ ∘ δ ]T ≡ A [ γ ]T [ δ ]T
  [∘]T = refl

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id {Γ} ]T ≡ A
  [id]T = refl

  record Tm (Γ : Con)(A : Ty Γ) : Set where
    field
      a   : ∀{I}(γI : ∣ Γ ∣ I) → ∣ A ∣ I γI
      nat : ∀{I}{γI : ∣ Γ ∣ I}{J}(f : Hom J I) → A ∶ (a γI) [ f ] refl ≡ a (Γ ∶ γI [ f ])
  open Tm renaming (a to ∣_∣)

  _[_]t : ∀{Γ}{A : Ty Γ} → Tm Γ A → ∀{Δ}(γ : Sub Δ Γ) → Tm Δ (A [ γ ]T)
  ∣ a [ γ ]t ∣ δI = ∣ a ∣ (∣ γ ∣ δI)
  nat (a [ γ ]t) f = {!!}

  [∘]t : ∀{Γ}{A : Ty Γ}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → a [ γ ∘ δ ]t ≡ a [ γ ]t [ δ ]t
  [∘]t = refl

  [id]t : ∀{Γ}{A : Ty Γ}{a : Tm Γ A} → a [ id ]t ≡ a
  [id]t = refl

  _▹_ : (Γ : Con)(A : Ty Γ) → Con
  ∣ Γ ▹ A ∣ I = Σ (∣ Γ ∣ I) (∣ A ∣ I)
  (Γ ▹ A) ∶ (γI ,Σ aI) [ f ] = (Γ ∶ γI [ f ]) ,Σ (A ∶ aI [ f ] refl)
  [id] (Γ ▹ A) _ = {!!}
  [∘]  (Γ ▹ A) = {!!}

  p : ∀{Γ}{A : Ty Γ} → Sub (Γ ▹ A) Γ
  ∣ p ∣ (γI ,Σ aI) = γI
  nat p f = refl

  q : ∀{Γ}{A : Ty Γ} → Tm (Γ ▹ A) (A [ p {A = A} ]T)
  ∣ q ∣ (γI ,Σ aI) = aI
  nat q f = refl

  qp : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)} → Tm ((Γ ▹ A) ▹ B) (A [ p {A = A} ]T [ p {A = B} ]T)
  qp {A = A}{B = B} .∣_∣ = ∣ _[_]t (q {A = A}) (p {A = B}) ∣
  qp .nat _ = refl
{-
  _,_ : ∀{Γ Δ}(γ : Sub Δ Γ){A : Ty Γ} → Tm Δ (A [ γ ]T) → Sub Δ (Γ ▹ A)
  ∣ γ , a ∣ δI = ∣ γ ∣ δI ,Σ ∣ a ∣ δI
  nat (γ , a) f δf = {!nat γ f δf !}

  ▹β₁ : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Γ}{a : Tm Δ (A [ γ ]T)}→ p {A = A} ∘ (_,_ γ {A = A} a) ≡ γ
  ▹β₁ = refl

  ▹β₂ : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Γ}{a : Tm Δ (A [ γ ]T)}→ q {A = A} [ _,_ γ {A = A} a ]t ≡ a
  ▹β₂ = refl

  ▹η : ∀{Γ Δ}{A : Ty Γ}{γa : Sub Δ (Γ ▹ A)} → _,_ (p {A = A} ∘ γa) {A = A} (q {A = A} [ γa ]t) ≡ γa
  ▹η = refl

  _^_ : ∀{Γ Δ}(γ : Sub Δ Γ)(A : Ty Γ) → Sub (Δ ▹ (A [ γ ]T)) (Γ ▹ A)
  γ ^ A = _,_ (γ ∘ p {A = A [ γ ]T}) {A = A} (q {A = A [ γ ]T})

  Sigma : {Γ : Con}(A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
  ∣ Sigma A B ∣ I γI = Σ (∣ A ∣ I γI) λ aI → ∣ B ∣ I (γI ,Σ aI)
  Sigma A B ∶ (aI ,Σ bI) [ f ] γf  = (A ∶ aI [ f ] γf) ,Σ (B ∶ bI [ f ] {!!})
  [∘] (Sigma A B) = {!!}
  [id] (Sigma A B) = {!!}

  Σ[] : ∀{Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)}{Δ}{γ : Sub Δ Γ} → Sigma A B [ γ ]T ≡ Sigma (A [ γ ]T) (B [ γ ^ A ]T)
  Σ[] = refl

  proj₁ : ∀{Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)} → Tm Γ (Sigma A B) → Tm Γ A
  ∣ proj₁ ab ∣ γI = π₁ (∣ ab ∣ γI)
  nat (proj₁ ab) f γf = {!!}

  proj₂ : ∀{Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)}(ab : Tm Γ (Sigma A B)) → Tm Γ (B [ _,_ id {A = A} (proj₁ {B = B} ab) ]T)
  ∣ proj₂ ab ∣ γI = π₂ (∣ ab ∣ γI)
  nat (proj₂ ab) = {!!}

  y∘ : ∀{Γ : Con}{I}{γI : ∣ Γ ∣ I}{J}{f : Hom J I} → yl {Γ} γI ∘ yHom f ≡ yl (Γ ∶ γI [ f ])
  y∘ = Sub= {!!}
  -- this is not an issue in the PMP version
  -- we can prove y∘ if we use pointfree presheaves

  -- this is nice, but Π is not definable easily, 
  Π : {Γ : Con}(A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
  ∣ Π {Γ} A B ∣ I γI = Tm (yOb I ▹ A [ yl γI ]T) (B [ yl γI ^ A ]T)
  _∶_[_]_ (Π {Γ} A B) {I}{γI} α f γf = {!α [ yHom f ^ (A [ yl γI ]T) ]t!}
  [∘] (Π {Γ} A B) = {!!}
  [id] (Π {Γ} A B) = {!!}

  U : Con
  ∣ U ∣ I = Ty (yOb I)
  U ∶ A [ f ] = A [ yHom f ]T
  [∘] U A f g = refl
  [id] U A = refl

  El : ∀{Γ} → Sub Γ U → Ty Γ
  ∣ El A ∣ I γI = ∣ ∣ A ∣ γI ∣ I idc
  El A ∶ aI [ f ] γf = {! ∣ A ∣ _ ∶ aI [ f ] ?!} -- to get this, I need naturality of A strictly
  [∘] (El A) = {!!}
  [id] (El A) = {!!}

  c : ∀{Γ} → Ty Γ → Sub Γ U
  ∣ c A ∣ γI = A [ yl γI ]T
  nat (c A) = {!!}

module try2fix where
  -- idea : replace dependent presheaves by dependent presheaves over YonedaLemma of their context

  ⟨_⟩ : Con → Con
  ∣ ⟨ Γ ⟩ ∣ I = Sub (yOb I) Γ
  ⟨ Γ ⟩ ∶ γI [ f ]  = γI ∘ yHom f
  [∘] ⟨ Γ ⟩ _ _ _ = refl
  [id] ⟨ Γ ⟩ _ = refl

  ⟨_⟩s : ∀{Γ Δ} → Sub Δ Γ → Sub ⟨ Δ ⟩ ⟨ Γ ⟩
  ∣ ⟨ γ ⟩s ∣ δI = γ ∘ δI
  nat ⟨ γ ⟩s = {!!}

  un⟨_⟩ : (Γ : Con) → Sub ⟨ Γ ⟩ Γ
  ∣ un⟨ Γ ⟩ ∣ γI = ∣ γI ∣ idc
  nat un⟨ Γ ⟩ = {!!}

  mk⟨_⟩ : (Γ : Con) → Sub Γ ⟨ Γ ⟩
  ∣ mk⟨ Γ ⟩ ∣ γI = yl γI
  nat mk⟨ Γ ⟩ = {!!}

  unmk⟨_⟩ : (Γ : Con) → un⟨ Γ ⟩ ∘ mk⟨ Γ ⟩ ≡ id
  unmk⟨ Γ ⟩ = Sub= {!!}

  mkun⟨_⟩ : (Γ : Con) → mk⟨ Γ ⟩ ∘ un⟨ Γ ⟩ ≡ id
  mkun⟨ Γ ⟩ = Sub= {!!}

  Ty : Con → Set₁
  Ty Γ = W.Ty ⟨ Γ ⟩

  _[_]T : ∀{Γ} → Ty Γ → ∀{Δ} → Sub Δ Γ → Ty Δ
  A [ γ ]T = A W.[ ⟨ γ ⟩s ]T

  [∘]T : ∀{Γ}{A : Ty Γ}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → A [ γ ∘ δ ]T ≡ A [ γ ]T [ δ ]T
  [∘]T = refl

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id {Γ} ]T ≡ A
  [id]T = refl

  Tm : (Γ : Con) → Ty Γ → Set
  Tm Γ A = W.Tm ⟨ Γ ⟩ A

  _[_]t : ∀{Γ}{A : Ty Γ} → Tm Γ A → ∀{Δ}(γ : Sub Δ Γ) → Tm Δ (A [ γ ]T)
  a [ γ ]t = a W.[ ⟨ γ ⟩s ]t

  [∘]t : ∀{Γ}{A : Ty Γ}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → a [ γ ∘ δ ]t ≡ a [ γ ]t [ δ ]t
  [∘]t = refl

  [id]t : ∀{Γ}{A : Ty Γ}{a : Tm Γ A} → a [ id ]t ≡ a
  [id]t = refl

  _▹_ : (Γ : Con)(A : Ty Γ) → Con
  Γ ▹ A = ⟨ Γ ⟩ W.▹ A

  p : ∀{Γ}{A : Ty Γ} → Sub (Γ ▹ A) Γ
  p {Γ}{A} = un⟨ Γ ⟩ ∘ W.p {⟨ Γ ⟩}{A}

  q : ∀{Γ}{A : Ty Γ} → Tm (Γ ▹ A) (A [ p {A = A} ]T)
  q {Γ}{A} = {!W.q {⟨ Γ ⟩}{A}!}

  _,_ : ∀{Γ Δ}(γ : Sub Δ Γ){A : Ty Γ} → Tm Δ (A [ γ ]T) → Sub Δ (Γ ▹ A)
  _,_ {Γ}{Δ} γ {A} a = {!!} -- {!!} ∘ (W._,_ γ {A = A W.[ mk⟨ Γ ⟩ ]T} (a W.[ mk⟨ Δ ⟩ ]t))

  ▹β₁ : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Γ}{a : Tm Δ (A [ γ ]T)}→ p {A = A} ∘ (_,_ γ {A = A} a) ≡ γ
  ▹β₁ = {!!}

  ▹β₂ : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Γ}{a : Tm Δ (A [ γ ]T)}→ q {A = A} [ _,_ γ {A = A} a ]t ≡ a
  ▹β₂ = {!!}

  ▹η : ∀{Γ Δ}{A : Ty Γ}{γa : Sub Δ (Γ ▹ A)} → _,_ (p {A = A} ∘ γa) {A = A} (q {A = A} [ γa ]t) ≡ γa
  ▹η = {!!}

  _^_ : ∀{Γ Δ}(γ : Sub Δ Γ)(A : Ty Γ) → Sub (Δ ▹ (A [ γ ]T)) (Γ ▹ A)
  γ ^ A = {!!}

module LocalUniverse where
  -- just work with W, and replace Ty by 
  record Ty (Γ : Con) : Set₁ where
    field
      con : Con
      sub : Sub Γ con
      ty  : W.Ty con
  open Ty public

  Tm : (Γ : Con)(A : Ty Γ) → Set
  Tm Γ A = W.Tm Γ (ty A W.[ sub A ]T)

  -- this method strictifies [∘]T, [∘]t, Π[], etc, but it doesn't strictify Πβ, Πη

  -- it needs exponentials in contexts (maybe even Π), look in
  -- https://bitbucket.org/akaposi/prop/src/master/ModelStrict/Func.agda
  
module PMP where
  -- here we have to replace Con, Sub (and maybe Ty, Tm)
  -- then it seems that even Π works
-}
