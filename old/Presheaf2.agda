{-# OPTIONS --prop #-}

-- list of tricks:
-- * pointfree
-- * parametricity definition of naturality and type restriction
-- * Yoneda
-- * equation in SProp
-- * local universe

open import Agda.Primitive
open import Cat

postulate
  C' : Cat
C : Cat
C = strictify C'
open Cat.Cat C

infix 4 _≡_
infixl 5 _,Σ_
data _≡_ {i}{A : Set i}(a : A) : A → Prop i where
  refl : a ≡ a
record ⊤ : Set where
record ⊤p : Prop where
record Σ {i}(A : Set i){j}(B : A → Set j) : Set (i ⊔ j) where
  constructor _,Σ_
  field
    π₁ : A
    π₂ : B π₁
open Σ
-- ,= : ∀{i}{A : Set i}{j}{B : A → Set j}{a₀ a₁ : A}(a₌ : a₀ ≡ a₁){b₀ : B a₀}{b₁ : B a₁} → b₀ ≡ b₁
record Lift {i}(A : Prop i) : Set i where
  constructor mk
  field un : A
open Lift
cong : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong f refl = refl
_◾_ : ∀{i}{A : Set i}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
refl ◾ refl = refl
_⁻¹ : ∀{i}{A : Set i}{a a' : A} → a ≡ a' → a' ≡ a
refl ⁻¹ = refl

record Con : Set₁ where
  field
    Γ    : Ob → Set
    _[_] : ∀{I} → Γ I → ∀{J} → Hom J I → Γ J
    [∘]  : ∀{I}(γI : Γ I){J}(f : Hom J I){K}(g : Hom K J) → γI [ f ∘c g ] ≡ γI [ f ] [ g ] -- TODO: change this to pointfree, then we try2fix should be fine
    [id] : ∀{I}(γI : Γ I) → γI [ idc ] ≡ γI
  infix 8 _[_]
open Con renaming (Γ to ∣_∣; _[_] to _∶_[_])

◆ : Con
∣_∣ ◆ I = ⊤
_∶_[_] ◆ _ _ = _
[∘] ◆ _ _ _ = refl
[id] ◆ _ = refl

yOb : Ob → Con
∣ yOb I ∣ J = Hom J I
yOb I ∶ f [ g ] = f ∘c g
[∘] (yOb I) f g h = refl
[id] (yOb I) f = refl

record Sub (Δ Γ : Con) : Set where
  field
    γ   : ∀{I} → ∣ Δ ∣ I → ∣ Γ ∣ I
    nat : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I) → Γ ∶ γ δI [ f ] ≡ γ (Δ ∶ δI [ f ])
open Sub renaming (γ to ∣_∣) public

Sub= : {Γ Δ : Con} →
  {γ₀ γ₁ : ∀{I} → ∣ Δ ∣ I → ∣ Γ ∣ I}(γ₂ : (λ {I} → γ₀ {I}) ≡ γ₁) 
  {nat₀ : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I) → Γ ∶ γ₀ δI [ f ] ≡ γ₀ (Δ ∶ δI [ f ])}
  {nat₁ : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I) → Γ ∶ γ₁ δI [ f ] ≡ γ₁ (Δ ∶ δI [ f ])} →
  _≡_ {A = Sub Δ Γ} (record { γ = γ₀ ; nat = nat₀ }) (record { γ = γ₁ ; nat = nat₁ })
Sub= refl = refl

_∘_ : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
∣ γ ∘ δ ∣ θI = ∣ γ ∣ (∣ δ ∣ θI)
nat (γ ∘ δ) f = {!!}

id : ∀{Γ} → Sub Γ Γ
∣ id ∣ γI = γI
nat id f = refl

ass : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{Ξ}{θ : Sub Ξ Θ} → (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
ass = refl

idl : ∀{Γ Δ}{γ : Sub Δ Γ} → id ∘ γ ≡ γ
idl = refl

idr : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘ id ≡ γ
idr = refl

yHom : ∀{J I} → Hom J I → Sub (yOb J) (yOb I)
∣ yHom f ∣ g = f ∘c g
nat (yHom f) g = refl

yid : ∀{I} → yHom (idc {I}) ≡ id
yid = refl

-- representable presheaves and representable natural transformations are strict

-- ⟨ Γ ⟩ is the strict replacement of Γ
⟨_⟩ : Con → Con
∣ ⟨ Γ ⟩ ∣ I = Sub (yOb I) Γ
⟨ Γ ⟩ ∶ γI [ f ]  = γI ∘ yHom f
[∘] ⟨ Γ ⟩ _ _ _ = refl
[id] ⟨ Γ ⟩ _ = refl

yl : ∀{Γ I} → ∣ Γ ∣ I → Sub (yOb I) Γ
∣ yl {Γ} γI ∣ f = Γ ∶ γI [ f ]
nat (yl {Γ} γI) {_}{f} g = [∘] Γ γI f g ⁻¹

module equiv where
  mk⟨_⟩ : (Γ : Con) → Sub Γ ⟨ Γ ⟩
  ∣ mk⟨ Γ ⟩ ∣ γI = yl γI
  nat mk⟨ Γ ⟩ f = Sub= {!!}

  un⟨_⟩ : (Γ : Con) → Sub ⟨ Γ ⟩ Γ
  ∣ un⟨ Γ ⟩ ∣ γI = ∣ γI ∣ idc
  nat un⟨ Γ ⟩ {_}{γI} f = nat γI f

  ⟨_⟩β : (Γ : Con) → un⟨ Γ ⟩ ∘ mk⟨ Γ ⟩ ≡ id
  ⟨ Γ ⟩β = Sub= {![id] Γ!}
  
  ⟨_⟩η : (Γ : Con) → mk⟨ Γ ⟩ ∘ un⟨ Γ ⟩ ≡ id
  ⟨ Γ ⟩η = Sub= {![id] Γ!}
open equiv

⟨_⟩s : ∀{Γ Δ} → Sub Δ Γ → Sub ⟨ Δ ⟩ ⟨ Γ ⟩
∣ ⟨ γ ⟩s ∣ δ = γ ∘ δ
nat ⟨ γ ⟩s f = refl

record Ty (Γ : Con) : Set₁ where
  field
    A     : (I : Ob) → ∣ ⟨ Γ ⟩ ∣ I → Set
    _[_]  : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I} → A I γI → ∀{J}(f : Hom J I) → A J (⟨ Γ ⟩ ∶ γI [ f ])
    [∘]   : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I}(aI : A I γI){J}(f : Hom J I){K}(g : Hom K J) → aI [ f ∘c g ] ≡ (aI [ f ]) [ g ]
    [id]  : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I}(aI : A I γI) → aI [ idc ] ≡ aI
  infix 8 _[_]
open Ty renaming (A to ∣_∣; _[_] to _∶_[_])

_[_]T : ∀{Γ} → Ty Γ → ∀{Δ} → Sub Δ Γ → Ty Δ
∣ A [ γ ]T ∣ I δI = ∣ A ∣ I (∣ ⟨ γ ⟩s ∣ δI)
(A [ γ ]T) ∶ aI [ f ] = A ∶ aI [ f ]
[∘] (A [ γ ]T) aI f g = {!!}
[id] (A [ γ ]T) aI = {!!}

[∘]T : ∀{Γ}{A : Ty Γ}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → A [ γ ∘ δ ]T ≡ A [ γ ]T [ δ ]T
[∘]T = refl

[id]T : ∀{Γ}{A : Ty Γ} → A [ id {Γ} ]T ≡ A
[id]T = refl

record Tm (Γ : Con)(A : Ty Γ) : Set where
  field
    a   : ∀{I}(γI : ∣ ⟨ Γ ⟩ ∣ I) → ∣ A ∣ I γI
    nat : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I}{J}(f : Hom J I) → A ∶ (a γI) [ f ] ≡ a (⟨ Γ ⟩ ∶ γI [ f ])
open Tm renaming (a to ∣_∣)

_[_]t : ∀{Γ}{A : Ty Γ} → Tm Γ A → ∀{Δ}(γ : Sub Δ Γ) → Tm Δ (A [ γ ]T)
∣ a [ γ ]t ∣ δI = ∣ a ∣ (γ ∘ δI)
nat (a [ γ ]t) = {!!}

[∘]t : ∀{Γ}{A : Ty Γ}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → a [ γ ∘ δ ]t ≡ a [ γ ]t [ δ ]t
[∘]t = refl

[id]t : ∀{Γ}{A : Ty Γ}{a : Tm Γ A} → a [ id ]t ≡ a
[id]t = refl

⟨_⟩T : ∀{Γ} → Ty Γ → Ty Γ
∣ ⟨ A ⟩T ∣ I γI = Tm (yOb I) (A [ γI ]T)
⟨ A ⟩T ∶ aI [ f ] = aI [ yHom f ]t
[∘] ⟨ A ⟩T = {!!}
[id] ⟨ A ⟩T = {!!}

module try2 where
  _▹_ : (Γ : Con)(A : Ty Γ) → Con
  ∣ Γ ▹ A ∣ I = Σ (∣ ⟨ Γ ⟩ ∣ I) (∣ ⟨ A ⟩T ∣ I)
  (Γ ▹ A) ∶ (γI ,Σ aI) [ f ] = (γI ∘ yHom f) ,Σ (aI [ yHom f ]t)
  [id] (Γ ▹ A) _ = {!!}
  [∘]  (Γ ▹ A) = {!!}

  _,_ : ∀{Γ Δ}(γ : Sub Δ Γ){A : Ty Γ} → Tm Δ (A [ γ ]T) → Sub Δ (Γ ▹ A)
  ∣ γ , a ∣ δI = (γ ∘ yl δI) ,Σ (a [ yl δI ]t)
  nat (γ , a) f = {!!}

  π₁' : ∀{Γ Δ}{A : Ty Γ} → Sub Δ (Γ ▹ A) → Sub Δ Γ
  ∣ π₁' γa ∣ δI = ∣ π₁ (∣ γa ∣ δI) ∣ idc
  nat (π₁' γa) = {!!}

  p : ∀{Γ}{A : Ty Γ} → Sub (Γ ▹ A) Γ
  ∣ p {Γ} {A} ∣ (γI ,Σ aI) = ∣ γI ∣ idc
  nat (p {Γ} {A}) = {!!}

  q : ∀{Γ}{A : Ty Γ} → Tm (Γ ▹ A) (A [ p {A = A} ]T)
  ∣ q {Γ} {A} ∣ γaI = {!∣ γaI ∣ idc!} -- {!∣ π₂ (∣ γaI ∣ idc) ∣ (yHom idc)!}
  nat (q {Γ} {A}) = {!!}

  ▹β₁ : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Γ}{a : Tm Δ (A [ γ ]T)}→ p {A = A} ∘ (_,_ γ {A = A} a) ≡ γ
  ▹β₁ = Sub= {!!}

  ▹β₁' : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Γ}{a : Tm Δ (A [ γ ]T)}→ π₁' {A = A} (_,_ γ {A = A} a) ≡ γ
  ▹β₁' = Sub= {!!}

  _^_ : ∀{Γ Δ}(γ : Sub Δ Γ)(A : Ty Γ) → Sub (Δ ▹ (A [ γ ]T)) (Γ ▹ A)
  ∣ γ ^ A ∣ (δI ,Σ aI) = (γ ∘ δI) ,Σ aI
  nat (γ ^ A) = {!!}

  Π : {Γ : Con}(A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
  ∣ Π {Γ} A B ∣ I γI = Tm (yOb I ▹ (A [ γI ]T)) (B [ γI ^ A ]T)
  _∶_[_] (Π {Γ} A B) {I}{γI} α f = α [ yHom f ^ (A [ γI ]T) ]t
  [∘] (Π {Γ} A B) = {!!}
  [id] (Π {Γ} A B) = {!!}

  Π[] : ∀{Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)}{Δ}{γ : Sub Δ Γ} → Π A B [ γ ]T ≡ Π (A [ γ ]T) (B [ γ ^ A ]T)
  Π[] = refl
  
  lam : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)} → Tm (Γ ▹ A) B → Tm Γ (Π A B)
  ∣ lam {A = A} t ∣ γI = t [ γI ^ A ]t
  nat (lam t) = {!!}

  app : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)} → Tm Γ (Π A B) → Tm (Γ ▹ A) B
  ∣ app {A = A} t ∣ γaI = {!!}
  nat (app t) = {!!}

  Πη : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)}{t : Tm Γ (Π A B)} → lam {Γ}{A}{B} (app {Γ}{A}{B} t) ≡ t
  Πη {Γ}{A}{B} = {!!}

  Πβ : {Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)}{t : Tm (Γ ▹ A) B} → app {Γ}{A}{B} (lam {Γ}{A}{B} t) ≡ t
  Πβ {Γ}{A}{B} = {!!}


  -- TODO: can't we fix this by contextualisation?

module try1 where
  _▹_ : (Γ : Con)(A : Ty Γ) → Con
  ∣ Γ ▹ A ∣ I = Σ (∣ ⟨ Γ ⟩ ∣ I) (∣ A ∣ I)
  (Γ ▹ A) ∶ (γI ,Σ aI) [ f ] = (γI ∘ yHom f) ,Σ A ∶ aI [ f ]
  [id] (Γ ▹ A) _ = {!!}
  [∘]  (Γ ▹ A) = {!!}

  _,_ : ∀{Γ Δ}(γ : Sub Δ Γ){A : Ty Γ} → Tm Δ (A [ γ ]T) → Sub Δ (Γ ▹ A)
  ∣ γ , a ∣ δI = (γ ∘ yl δI) ,Σ ∣ a ∣ (yl δI)
  nat (γ , a) f = {!!}

  π₁' : ∀{Γ Δ}{A : Ty Γ} → Sub Δ (Γ ▹ A) → Sub Δ Γ
  ∣ π₁' γa ∣ δI = ∣ π₁ (∣ γa ∣ δI) ∣ idc
  nat (π₁' γa) = {!!}

  p : ∀{Γ}{A : Ty Γ} → Sub (Γ ▹ A) Γ
  ∣ p {Γ} {A} ∣ (γI ,Σ aI) = ∣ γI ∣ idc
  nat (p {Γ} {A}) = {!!}

  q : ∀{Γ}{A : Ty Γ} → Tm (Γ ▹ A) (A [ p {A = A} ]T)
  ∣ q {Γ} {A} ∣ γaI = {!π₂ (∣ γaI ∣ idc)!}
  nat (q {Γ} {A}) = {!!}

  ▹β₁ : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Γ}{a : Tm Δ (A [ γ ]T)}→ p {A = A} ∘ (_,_ γ {A = A} a) ≡ γ
  ▹β₁ = {!!}

  ▹β₁' : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Γ}{a : Tm Δ (A [ γ ]T)}→ π₁' {A = A} (_,_ γ {A = A} a) ≡ γ
  ▹β₁' = {!!}

module try3 where
  _▹_ : (Γ : Con)(A : Ty Γ) → Con
  ∣ Γ ▹ A ∣ I = Σ (∣ Γ ∣ I) λ γI → ∣ A ∣ I (∣ mk⟨ Γ ⟩ ∣ γI)
  (Γ ▹ A) ∶ (γI ,Σ aI) [ f ] = (Γ ∶ γI [ f ]) ,Σ {!A ∶ aI [ f ]!} -- (γI ∘ yHom f) ,Σ A ∶ aI [ f ]
  [id] (Γ ▹ A) _ = {!!}
  [∘]  (Γ ▹ A) = {!!}
