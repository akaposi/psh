\documentclass[10pt, a4paper]{article}
\usepackage{geometry}
\geometry{left=3cm,right=3cm,top=1.5cm,bottom=1.5cm}
\usepackage[fleqn]{amsmath}
\usepackage{amssymb}
\usepackage{newunicodechar}
\newunicodechar{→}{$\to$}
\newunicodechar{Π}{$\Pi$}
\newunicodechar{β}{$\beta$}
\newunicodechar{η}{$\eta$}

\input{abbrevs.tex}

\allowdisplaybreaks

\begin{document}

\title{Presheaf model over a CwF}

\maketitle

Notion of model of type theory (comes in 5 components the first 4 of
which together is called CwF: category, terminal object, families,
comprehension, $\Pi$ types). We write $|\C|$ instead of
$\mathsf{Con}_\C$ and $\C(\Gamma,\Delta)$ instead of
$\mathsf{Sub}_\C\,\Gamma\,\Delta$.
\begin{alignat*}{5}
  & \rlap{$\C$ is a category:} \\
  & |\C| && :\,\, && \Set \\
  & \C(\blank,\blank) && : && |\C|\ra|\C|\ra\Set \\
  & \id_\C && : && \C(\Gamma,\Gamma) \\
  & \blank\circ_\C\blank && : && \C(\Gamma,\Theta)\ra\C(\Theta,\Delta)\ra\C(\Gamma,\Delta) \\
  & \idl_\C && : && \id_\C\circ_\C\sigma = \sigma \\
  & \idr_\C && : && \sigma\circ_\C\id_\C = \sigma \\
  & \ass_\C && : && (\sigma\circ_\C\delta)\circ_\C\nu = \sigma\circ_C(\delta\circ_C\nu) \\
  & \rlap{$\C$ has a terminal object:} \\
  & \cdot_\C && : && |\C| \\
  & \epsilon_\C && : && \C(\Gamma,\cdot_\C) \\
  & {\cdot\eta}_\C && : && \sigma = \epsilon_\C \\
  & \rlap{Families:} \\
  & \Ty_\C && :\,\, && |\C|\ra\Set \\
  & \Tm_\C && : && (\Gamma:|\C|)\ra\Ty_\C\,\Gamma\ra\Set \\
  & \blank[\blank]_\C && : && \Ty_\C\,\Delta\ra\C(\Gamma,\Delta)\ra\Ty_\C\,\Gamma \\
  & \blank[\blank]_\C && : && \Tm_\C\,\Delta\,A\ra(\sigma:\C(\Gamma,\Delta))\ra\Tm_\C\,\Gamma\,(A[\sigma]_\C) \\
  & {[\id]}_\C && : && A[\id]_\C = A \\
  & {[\circ]}_\C && : && A[\sigma\circ\delta]_\C = A[\sigma]_\C[\delta]_\C \\
  & {[\id]}_\C && : && t[\id]_\C = t \\
  & {[\circ]}_\C && : && t[\sigma\circ\delta]_\C = t[\sigma]_\C[\delta]_\C \\
  & \rlap{Comprehension:} \\
  & \blank\rhd_\C\blank && : && (\Gamma:|\C|)\ra\Ty_\C\,\Gamma\ra|\C| \\
  & \blank,_\C\blank && : && (\sigma:\C(\Gamma,\Delta))\ra\Tm_\C\,\Gamma\,(A[\sigma]_\C)\ra\C(\Gamma,\Delta\rhd_\C A) \\
  & \p_\C && : && \C(\Gamma\rhd_\C A,\Gamma) \\
  & \q_\C && : && \Tm\,(\Gamma\rhd_\C A)\,(A[\p_\C]_\C) \\
  & {{\rhd\beta}_1}_\C && : && \p_\C\circ_\C(\sigma,_\C t) = \sigma \\
  & {{\rhd\beta}_2}_\C && : && \q_\C[\sigma,_\C t]_\C = t \\
  & {\rhd\eta} && : && (\p_\C,_\C\q_\C) = \id_\C \\
  & {,\circ}_\C && : && (\sigma,_\C t)\circ_\C\delta = (\sigma\circ_\C\delta,_\C t[\delta]_\C) \\
  & \rlap{$\Pi$ types:} \\
  & \Pi_\C && : && (A:\Ty_\C\,\Gamma)\ra\Ty_\C\,(\Gamma\rhd_\C A)\ra\Ty_\C\,\Gamma \\
  & \lam_\C && : && \Tm_C\,(\Gamma\rhd_\C A)\,B\ra\Tm_\C\,\Gamma\,(\Pi_\C\,A\,B) \\
  & \app_\C && : && \Tm_\C\,\Gamma\,(\Pi_\C\,A\,B)\ra\Tm_C\,(\Gamma\rhd_\C A)\,B \\
  & {\Pi\beta}_\C && : && \app_\C\,(\lam_\C\,t) = t \\
  & {\Pi\eta}_\C && : && \lam_\C\,(\app_\C\,t) = t \\
  & {\Pi[]}_\C && : && \Pi_\C\,A\,B[\sigma]_\C = \Pi_\C\,(A[\sigma]_\C)\,(B[\sigma\circ_\C\p_\C,_\C\q_\C]_\C) \\
  & {\lam[]}_\C && : && \lam_\C\,t[\sigma]_\C = \lam_\C\,(t[\sigma\circ_\C\p_\C,_\C\q_\C]_\C) \\
  & \rlap{Derivable:} \\
  & {\app[]}_\C && : && \app_\C\,t[\sigma\circ_\C\p_\C,_\C\q_\C]_\C = \app_\C\,(t[\sigma]_\C)
\end{alignat*}

Presheaf model $\hat{\C}$ over any category $\C$. The metavariables
for $\C$ change to $\Omega$ and $\beta$. We don't write $_{\hat{\C}}$
indices (we open $\hat{\C}$).
\begin{alignat*}{5}
  & (\Gamma:|\hat{\C}|) && :=\,\, && (|\Gamma| && : |\C|\ra\Set)\times \\
  & && && (\blank_\Gamma\lb\blank\rb && : |\Gamma|\,\Omega\ra\C(\Omega',\Omega)\ra|\Gamma|\,\Omega')\times \\
  & && && (\lb\id\rb_\Gamma && : \gamma_\Gamma\lb\id_\C\rb = \gamma)\times \\
  & && && (\lb\circ\rb_\Gamma && : \gamma_\Gamma\lb\beta\circ_\C\beta'\rb = \gamma_\Gamma\lb\beta\rb_\Gamma\lb\beta'\rb) \\
  & (\sigma:\hat{\C}(\Gamma,\Delta)) && :=\,\, && (|\sigma| && : |\Gamma|\,\Omega\ra|\Delta|\,\Omega)\times \\
  & && && (\blank_\sigma\lb\blank\rb && : (\gamma:|\Gamma|\,\Omega)(\beta:\C(\Omega',\Omega))\ra |\sigma|\,\gamma_\Delta\lb\beta\rb = |\sigma|\,(\gamma_\Gamma\lb\beta\rb)) \\
  & |\id|\,\gamma && :=\,\, && \rlap{$\gamma$} \\
  & |\sigma\circ\delta|\,\gamma && := && \rlap{$|\sigma|\,(|\delta|\,\gamma)$} \\
  & |\cdot|\,\Omega && := && \rlap{$\top$} \\
  & \tt _\cdot\lb\beta\rb && := && \tt \\
  & |\epsilon|\,\gamma && := && \rlap{$\tt$} \\
  & (A:\Ty\,\Gamma) && :=\,\, && (|A| && : |\Gamma|\,\Omega\ra\Set)\times \\
  & && && (\blank_A\lb\blank\rb && : |A|\,\gamma\ra(\beta:\C(\Omega',\Omega))\ra|A|\,(\gamma_\Gamma\lb\beta\rb))\times \\
  & && && (\lb\id\rb_A && : \alpha_A\lb\id_\C\rb = \alpha)\times \\
  & && && (\lb\circ\rb_A && : \alpha_A\lb\beta\circ_\C\beta'\rb = \alpha_A\lb\beta\rb_A\lb\beta'\rb) \\
  & (t:\Tm\,\Gamma\,A) && :=\,\, && (|t| && : (\gamma:|\Gamma|\,\Omega)\ra|A|\,\gamma)\times \\
  & && && (\blank_t\lb\blank\rb && : (\gamma:|\Gamma|\,\Omega)(\beta:\C(\Omega',\Omega))\ra |t|\,\gamma_A\lb\beta\rb = |t|\,(\gamma_\Gamma\lb\beta\rb)) \\
  & |A[\sigma]|\,\gamma && := && \rlap{$|A|\,(|\sigma|\,\gamma)$} \\
  & \alpha_{A[\sigma]}\lb\beta\rb && := && \rlap{$\alpha_A\lb\beta\rb$} \\
  & |t[\sigma]|\,\gamma && := && \rlap{$|t|\,(|\sigma|\,\gamma)$} \\
  & |\Gamma\rhd A|\,\Omega && := && \rlap{$(\gamma:|\Gamma|\,\Omega)\times|A|\,\gamma$} \\
  & (\gamma,\alpha)_{\Gamma\rhd A}\lb\beta\rb && := && \rlap{$(\gamma_\Gamma\lb\beta\rb,\alpha_A\lb\beta\rb)$} \\
  & |\p|\,(\gamma,\alpha) && := && \rlap{$\gamma$} \\
  & |\q|\,(\gamma,\alpha) && := && \rlap{$\alpha$} \\
  & (f:|\Pi\,A\,B|\,\gamma) && := && (|f| && :(\beta:\C(\Omega',\Omega))(\alpha:|A|\,(\gamma_\Gamma\lb\beta\rb))\ra|B|\,(\gamma_\Gamma\lb\beta\rb,\alpha))\times \\
  & && && (\blank_f\lb\blank\rb && : (\alpha:|A|\,(\gamma_\Gamma\lb\beta\rb))(\beta':\C(\Omega'',\Omega'))\ra|f|\,\beta\,\alpha _B\lb\beta'\rb = |f|\,(\beta\circ_\C\beta')\,(\alpha _A\lb\beta'\rb)) \\
  & f _{\Pi\,A\,B}\lb\beta\rb && := && \rlap{$(\lambda\beta'.|f|\,(\beta\circ_\C\beta'), \lambda\alpha\,\beta''.|f|\,(\beta\circ_C\beta')\,\alpha _B\lb\beta''\rb \overset{\alpha _f\lb\beta''\rb}{=} |f|\,(\beta\circ\beta'\circ\beta'')\,(\alpha _A\lb\beta''\rb))$} \\
  & |\lam\,t|\,\gamma && := && \rlap{$(\lambda\beta\,\alpha.|t|\,(\gamma _\Gamma\lb\beta\rb,\alpha), \lambda\alpha\beta'. |t|\,(\gamma _\Gamma\lb\beta\rb,\alpha) _B\lb\beta'\rb \overset{(\gamma _\Gamma\lb\beta\rb,\alpha) _t\lb\beta'\rb}{=} |t|\,(\gamma _\Gamma\lb\beta\circ\beta'\rb,\alpha _A\lb\beta'\rb))$} \\
  & |\app\,t|\,(\gamma,\alpha) && := && \rlap{$||t|\,\gamma|\,\id_\C\,\alpha$}
\end{alignat*}
If $\C$ is a CwF, then we have the following additional type formers in $\hat\C$:
\begin{alignat*}{5}
  & \b\U && : \Ty\,\Gamma \\
  & \b\El && : \Tm\,\Gamma\,\b\U\ra \Ty\,\Gamma \\
  & \b\Pi && : (a:\Tm\,\Gamma\,\b\U)\ra\Ty\,(\Gamma\rhd\b\El\,a) \\
  & \b\app && : \Tm\,(\Gamma\rhd\b\El\,a)\,B\ra\Tm\,\Gamma\,(\b\Pi\,a\,B)
\end{alignat*}
Their definition in the $\hat\C$:
\begin{alignat*}{5}
  & |\b\U|_\Omega\,\gamma && := \Ty_\C\,\Omega \\
  & A_{\b\U}\lb\beta\rb && := A[\beta]_\C \\
  & |\b\El\,a|_\Omega\,\gamma && := \Tm_\C\,\Omega\,(|a|_\Omega\,\gamma) \\
  & t_{\b\El\,a}\lb\beta\rb && := t[\beta]_\C \\
  & |\b\Pi\,a\,B|_\Omega\,\gamma && := |B|_{\Omega\rhd_\C|a|_\Omega\,\gamma}\,(\gamma_\Gamma\lb\p_\C\rb,\q_\C) \\
  & \text{TODO}
\end{alignat*}

\end{document}