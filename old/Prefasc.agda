{-# OPTIONS --without-K #-}

module Prefasc where

open import Agda.Primitive
open import Lib
open import Cat

-- we postulate a strict category C

postulate
  C' : Cat
C : Cat
C = strictify C'
open Cat.Cat C

-- the model of families

module F where

  Con : Set₁
  Con = Ob → Set

  Ty : Con → Set₁
  Ty Γ = (I : Ob) → Γ I → Set

  Sub : Con → Con → Set
  Sub Δ Γ = {I : Ob} → Δ I → Γ I

  Tm : (Γ : Con) → Ty Γ → Set
  Tm Γ A = {I : Ob}(γₓ : Γ I) → A I γₓ

  y : Ob → Con
  y I = λ J → Hom J I

  y' : ∀{J I} → Hom J I → Sub (y J) (y I)
  y' f = f ∘c_

  _∘_ : ∀{Δ Γ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
  γ ∘ δ = λ θₓ → γ (δ θₓ)

-- presheaves

module P where

  record Con : Set₁ where
    field
      ∣_∣  : Ob → Set
      _[_] : ∀{I} → ∣_∣ I → ∀{J} → Hom J I → ∣_∣ J
      [∘]  : (λ {I}(γₓ : ∣_∣ I){J}(i : Hom J I){K}(j : Hom K J) → γₓ [ i ∘c j ]) ≡
             (λ γₓ i j → γₓ [ i ] [ j ])
      [id] : (λ {I}(γₓ : ∣_∣ I) → γₓ [ idc ]) ≡ (λ γₓ → γₓ)
  open Con

  -- every family can be turned into a presheaf as follows
  toP : F.Con → Con
  ∣ toP Γ ∣    = λ I → F.Sub (F.y I) Γ
  _[_] (toP Γ) {I} γ i = F._∘_ {F.y I} γ (F.y' i)
  [∘]  (toP Γ) = refl
  [id] (toP Γ) = refl

  -- what if we start with a Γ : Con, then take ∣ Γ ∣ : F.Con, then
  -- toP ∣ Γ ∣ : Con? how do Γ and toP ∣ Γ ∣ relate?

module Prefasc where

  record Con : Set₁ where field
      ∣_∣ : (I : Ob) → Set
      sel : (I : Ob) → (F.Sub (F.y I) ∣_∣) → Set
