{-# OPTIONS --prop #-}

-- list of tricks:
-- * pointfree
-- * parametricity definition of naturality and type restriction
-- * Yoneda
-- * equation in SProp
-- * local universe

open import Agda.Primitive
open import Cat

postulate
  C' : Cat
C : Cat
C = strictify C'
open Cat.Cat C

infix 4 _≡_
infixl 5 _,Σ_
data _≡_ {i}{A : Set i}(a : A) : A → Prop i where
  refl : a ≡ a
record ⊤ : Set where
record ⊤p : Prop where
record Σ {i}(A : Set i){j}(B : A → Set j) : Set (i ⊔ j) where
  constructor _,Σ_
  field
    π₁ : A
    π₂ : B π₁
open Σ
-- ,= : ∀{i}{A : Set i}{j}{B : A → Set j}{a₀ a₁ : A}(a₌ : a₀ ≡ a₁){b₀ : B a₀}{b₁ : B a₁} → b₀ ≡ b₁
record Lift {i}(A : Prop i) : Set i where
  constructor mk
  field un : A
open Lift
cong : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong f refl = refl
_◾_ : ∀{i}{A : Set i}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
refl ◾ refl = refl

record Con : Set₁ where
  field
    Γ    : Ob → Set
    _[_] : ∀{I} → Γ I → ∀{J} → Hom J I → Γ J
    [∘]  : ∀{I}(γI : Γ I){J}(f : Hom J I){K}(g : Hom K J) → γI [ f ∘c g ] ≡ γI [ f ] [ g ] -- TODO: change this to pointfree, then we try2fix should be fine
    [id] : ∀{I}(γI : Γ I) → γI [ idc ] ≡ γI
  infix 8 _[_]
open Con renaming (Γ to ∣_∣; _[_] to _∶_[_])

◆ : Con
∣_∣ ◆ I = ⊤
_∶_[_] ◆ _ _ = _
[∘] ◆ _ _ _ = refl
[id] ◆ _ = refl

yOb : Ob → Con
∣ yOb I ∣ J = Hom J I
yOb I ∶ f [ g ] = f ∘c g
[∘] (yOb I) f g h = refl
[id] (yOb I) f = refl

record Sub (Δ Γ : Con) : Set where
  field
    γ   : ∀{I} → ∣ Δ ∣ I → ∣ Γ ∣ I
    nat : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I) → Γ ∶ γ δI [ f ] ≡ γ (Δ ∶ δI [ f ])
open Sub renaming (γ to ∣_∣) public

_∘_ : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
∣ γ ∘ δ ∣ θI = ∣ γ ∣ (∣ δ ∣ θI)
nat (γ ∘ δ) f = {!!}

id : ∀{Γ} → Sub Γ Γ
∣ id ∣ γI = γI
nat id f = refl

ass : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{Ξ}{θ : Sub Ξ Θ} → (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
ass = refl

idl : ∀{Γ Δ}{γ : Sub Δ Γ} → id ∘ γ ≡ γ
idl = refl

idr : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘ id ≡ γ
idr = refl

yHom : ∀{J I} → Hom J I → Sub (yOb J) (yOb I)
∣ yHom f ∣ g = f ∘c g
nat (yHom f) g = refl

yid : ∀{I} → yHom (idc {I}) ≡ id
yid = refl

-- representable presheaves and representable natural transformations are strict

-- ⟨ Γ ⟩ is the strict replacement of Γ
⟨_⟩ : Con → Con
∣ ⟨ Γ ⟩ ∣ I = Sub (yOb I) Γ
⟨ Γ ⟩ ∶ γI [ f ]  = γI ∘ yHom f
[∘] ⟨ Γ ⟩ _ _ _ = refl
[id] ⟨ Γ ⟩ _ = refl

⟨_⟩s : ∀{Γ Δ} → Sub Δ Γ → Sub ⟨ Δ ⟩ ⟨ Γ ⟩
∣ ⟨ γ ⟩s ∣ δ = γ ∘ δ
nat ⟨ γ ⟩s f = refl

yl : ∀{Γ I} → ∣ Γ ∣ I → Sub (yOb I) Γ
∣ yl {Γ} γI ∣ f = Γ ∶ γI [ f ]
nat (yl γI) f = {!!}

Sub' : Con → Con → Set
Sub' Δ Γ = Sub ⟨ Δ ⟩ ⟨ Γ ⟩

yl' : ∀{Γ I} → ∣ Γ ∣ I → Sub' (yOb I) Γ
yl' γI = ⟨ yl γI ⟩s

record Ty (Γ : Con) : Set₁ where
  field
    A     : (I : Ob) → ∣ ⟨ Γ ⟩ ∣ I → Set
    _[_]  : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I} → A I γI → ∀{J}(f : Hom J I) → A J (⟨ Γ ⟩ ∶ γI [ f ])
    [∘]   : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I}(aI : A I γI){J}(f : Hom J I){K}(g : Hom K J) → aI [ f ∘c g ] ≡ (aI [ f ]) [ g ]
    [id]  : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I}(aI : A I γI) → aI [ idc ] ≡ aI
  infix 8 _[_]
open Ty renaming (A to ∣_∣; _[_] to _∶_[_])

_[_]T : ∀{Γ} → Ty Γ → ∀{Δ} → Sub' Δ Γ → Ty Δ
∣ A [ γ ]T ∣ I δI = ∣ A ∣ I (∣ γ ∣ δI)
(A [ γ ]T) ∶ aI [ f ] = {!A ∶ aI [ f ]!}
[∘] (A [ γ ]T) aI f g = {!!}
[id] (A [ γ ]T) aI = {!!}

