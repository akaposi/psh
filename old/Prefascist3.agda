{-# OPTIONS
  --without-K
  --prop
  --rewriting
  --postfix-projections
  --show-irrelevant #-}

-- PMP's prefascist model following
-- https://github.com/ppedrot/vitef/blob/master/forcing_uip/cwf.v
-- simplification by Szumi in definition of _▹_ and U

module Prefascist3 where

open import Agda.Primitive

private variable
  i j k : Level

infix 4 _≡_
data _≡_ {A : Set i} (a : A) : A → Prop i where
  refl : a ≡ a
{-# BUILTIN REWRITE _≡_ #-}

≡-elim :
  {A : Set i} {a₀ : A}
  (P : ∀ a₁ → a₀ ≡ a₁ → Prop j) →
  P a₀ refl →
  ∀ {a₁} a₀₁ → P a₁ a₀₁
≡-elim P p refl = p

postulate
  ≡-elimₛ :
    {A : Set i} {a₀ : A}
    (P : ∀ a₁ → a₀ ≡ a₁ → Set j) →
    P a₀ refl →
    ∀ {a₁} a₀₁ → P a₁ a₀₁
  ≡-elimₛ-β :
    {A : Set i} {a₀ : A}
    (P : ∀ a₁ → a₀ ≡ a₁ → Set j) →
    (p : P a₀ refl) →
    ≡-elimₛ P p refl ≡ p
{-# REWRITE ≡-elimₛ-β #-}

ap : ∀ {A : Set i} {B : Set j} (f : A → B) {x y} → x ≡ y → f x ≡ f y
ap f refl = refl

sym : ∀ {A : Set i} {x y : A} → x ≡ y → y ≡ x
sym refl = refl

postulate
  funext :
    {A : Set i} {B : A → Set j} {f g : ∀ a → B a} → (∀ a → f a ≡ g a) → f ≡ g
  funextᵢ :
    {A : Set i} {B : A → Set j} {f g : ∀ {a} → B a} →
    (∀ {a} → f {a} ≡ g {a}) →
    (λ {a} → f {a}) ≡ (λ {a} → g {a})

record ⊤ : Set where
  constructor ⋆

record ⊤ₚ : Prop where
  constructor ⋆

postulate
  Ob : Set
  Hom₀ : Ob → Ob → Set

private variable
  x y z : Ob

Hom : Ob → Ob → Set
Hom x y = ∀ {z} → Hom₀ z x → Hom₀ z y

module C where
  infixl 9 _∘_
  _∘_ : Hom y x → Hom z y → Hom z x
  (f ∘ g) h = f (g h)

  id : Hom x x
  id f = f

record Y
  (x : Ob)
  (A : ∀ {y} → Hom y x → Set i)
  (A-rel : ∀ {y} (f : Hom y x) → (∀ {z} (g : Hom z y) → A (f C.∘ g)) → Prop i)
  : Set i
  where
  field
    a : (f : Hom y x) → A f
    rel : (f : Hom y x) → A-rel f λ g → a (f C.∘ g)
open Y public renaming (a to ∣_∣)

record Con (i : Level) : Set (lsuc i) where
  field
    Γ : Ob → Set i
    rel : (∀ {y} → Hom y x → Γ y) → Prop i
open Con public renaming (Γ to ∣_∣)

private variable
  Γ Δ Θ Ξ : Con i

-- converting Γ to ordinary presheaf, and this is the action on objects
Yᶜ : Con i → Ob → Set i
Yᶜ Γ x = Y x (λ {y} _ → ∣ Γ ∣ y) (λ _ → Γ .rel)

Yᶜ-≡ : {γ₀ γ₁ : Yᶜ Γ x} → (λ {x} → ∣ γ₀ ∣ {x}) ≡ ∣ γ₁ ∣ → γ₀ ≡ γ₁
Yᶜ-≡ refl = refl

infixl 9 _|ᶜ_
_|ᶜ_ : Yᶜ Γ x → Hom y x → Yᶜ Γ y
∣ γ |ᶜ f ∣ g = ∣ γ ∣ (f C.∘ g)
(γ |ᶜ f) .rel g = γ .rel (f C.∘ g)

record Sub (Δ : Con i) (Γ : Con j) : Set (i ⊔ j) where
  field
    γ : Yᶜ Δ x → ∣ Γ ∣ x                           -- this is not simply a natural transformation from Yᶜ Δ to Yᶜ Γ (not even this first component)
    rel : (δ : Yᶜ Δ x) → Γ .rel λ f → γ (δ |ᶜ f)
open Sub public renaming (γ to ∣_∣)

Sub-≡ :
  {γ₀ γ₁ : Sub Δ Γ} → (λ {x} → ∣ γ₀ ∣ {x}) ≡ (λ {x} → ∣ γ₁ ∣ {x}) → γ₀ ≡ γ₁
Sub-≡ refl = refl

-- converting a Sub into a natural transformation, only action on objects
Yˢ : Sub Δ Γ → Yᶜ Δ x → Yᶜ Γ x
∣ Yˢ γ δ ∣ f = ∣ γ ∣ (δ |ᶜ f)
Yˢ γ δ .rel f = γ .rel (δ |ᶜ f)

infixl 9 _∘_
_∘_ : Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
∣ γ ∘ δ ∣ θ = ∣ γ ∣ (Yˢ δ θ)
(γ ∘ δ) .rel θ = γ .rel (Yˢ δ θ)

assoc : (γ : Sub Δ Γ) (δ : Sub Θ Δ) (θ : Sub Ξ Θ) → γ ∘ (δ ∘ θ) ≡ (γ ∘ δ) ∘ θ
assoc γ δ θ = refl

id : Sub Γ Γ
∣ id ∣ γ = ∣ γ ∣ C.id
id .rel γ = γ .rel C.id

idr : (γ : Sub Δ Γ) → γ ∘ id ≡ γ
idr γ = refl

idl : (γ : Sub Δ Γ) → id ∘ γ ≡ γ
idl γ = refl

record Ty (Γ : Con i) (j : Level) : Set (i ⊔ lsuc j) where
  field
    A : Yᶜ Γ x → Set j -- ordinary family
    rel : (γ : Yᶜ Γ x) → (∀ {y} (f : Hom y x) → A (γ |ᶜ f)) → Prop j
open Ty public renaming (A to ∣_∣)

private variable
  A B : Ty Γ i

Yᵀ : Ty Γ j → Yᶜ Γ x → Set j
Yᵀ {x = x} A γ = Y x (λ f → ∣ A ∣ (γ |ᶜ f)) (λ f → A .rel (γ |ᶜ f))

infixl 9 ⟨_,_⟩_|ᵀ_
⟨_,_⟩_|ᵀ_ : (A : Ty Γ i) (γ : Yᶜ Γ x) → Yᵀ A γ → (f : Hom y x) → Yᵀ A (γ |ᶜ f)
∣ ⟨ A , γ ⟩ a |ᵀ f ∣ g = ∣ a ∣ (f C.∘ g)
(⟨ A , γ ⟩ a |ᵀ f) .rel g = a .rel (f C.∘ g)

infixl 9 _[_]ᵀ
_[_]ᵀ : Ty Γ i → Sub Δ Γ → Ty Δ i
∣ A [ γ ]ᵀ ∣ δ = ∣ A ∣ (Yˢ γ δ)
(A [ γ ]ᵀ) .rel δ = A .rel (Yˢ γ δ)

[]ᵀ-∘ :
  (A : Ty Γ i) (γ : Sub Δ Γ) (δ : Sub Θ Δ) → A [ γ ∘ δ ]ᵀ ≡ A [ γ ]ᵀ [ δ ]ᵀ
[]ᵀ-∘ A γ δ = refl

[]ᵀ-id : (A : Ty Γ i) → A [ id ]ᵀ ≡ A
[]ᵀ-id A = refl

record Tm (Γ : Con i) (A : Ty Γ j) : Set (i ⊔ j) where
  field
    a : (γ : Yᶜ Γ x) → ∣ A ∣ γ -- "in the domain you always convert into ordinary presheaves"
    rel : (γ : Yᶜ Γ x) → A .rel γ λ f → a (γ |ᶜ f)
open Tm public renaming (a to ∣_∣)

Tm-≡ : {a₀ a₁ : Tm Γ A} → (λ {x} → ∣ a₀ ∣ {x}) ≡ (λ {x} → ∣ a₁ ∣ {x}) → a₀ ≡ a₁
Tm-≡ refl = refl

Yᵗ : Tm Γ A → (γ : Yᶜ Γ x) → Yᵀ A γ
∣ Yᵗ a γ ∣ f = ∣ a ∣ (γ |ᶜ f)
Yᵗ a γ .rel f = a .rel (γ |ᶜ f)

infixl 9 _[_]ᵗ
_[_]ᵗ : Tm Γ A → (γ : Sub Δ Γ) → Tm Δ (A [ γ ]ᵀ)
∣ a [ γ ]ᵗ ∣ δ = ∣ a ∣ (Yˢ γ δ)
(a [ γ ]ᵗ) .rel δ = a .rel (Yˢ γ δ)

[]ᵗ-∘ :
  (a : Tm Γ A) (γ : Sub Δ Γ) (δ : Sub Θ Δ) → a [ γ ∘ δ ]ᵗ ≡ a [ γ ]ᵗ [ δ ]ᵗ
[]ᵗ-∘ a γ δ = refl

[]ᵗ-id : {A : Ty Γ i} (a : Tm Γ A) → a [ id ]ᵗ ≡ a
[]ᵗ-id a = refl

record ∣▹∣ (Γ : Con i) (A : Ty Γ j) (x : Ob) : Set (i ⊔ j) where
  field
    con : Yᶜ Γ x
    ty : ∣ A ∣ con
open ∣▹∣ public

∣▹∣-≡ :
  {γa₀ γa₁ : ∣▹∣ Γ A x}
  (con-≡ : γa₀ .con ≡ γa₁ .con) →
  ≡-elimₛ (λ γ _ → ∣ A ∣ γ) (γa₀ .ty) con-≡ ≡ γa₁ .ty →
  γa₀ ≡ γa₁
∣▹∣-≡ refl refl = refl

record ▹-rel
  {Γ : Con i} {A : Ty Γ j} (γa : ∀ {y} → Hom y x → ∣▹∣ Γ A y) : Prop (i ⊔ j)
  where
  field
    con :
      (λ {y} (f : Hom y x) {z} (g : Hom z y) → ∣ γa f .con ∣ g) ≡
      (λ {y} (f : Hom y x) {z} (g : Hom z y) → ∣ γa (f C.∘ g) .con ∣ C.id)
    ty :
      A .rel
        record
          { a = λ f → ∣ γa f .∣▹∣.con ∣ C.id
          ; rel = λ f →
            ≡-elim (λ γ _ → Γ .rel (γ f)) (γa f .∣▹∣.con .rel C.id) con }
        λ f → ≡-elimₛ (λ γ _ → ∣ A ∣ γ) (γa f .ty) (Yᶜ-≡ (ap (λ γ → γ f) con))
open ▹-rel public

infixl 2 _▹_
_▹_ : (Γ : Con i) → Ty Γ j → Con (i ⊔ j)
∣ Γ ▹ A ∣ = ∣▹∣ Γ A
(Γ ▹ A) .rel = ▹-rel

Y▹ : (γ : Yᶜ Γ x) → Yᵀ A γ → Yᶜ (Γ ▹ A) x
∣ Y▹ γ a ∣ f .con = γ |ᶜ f
∣ Y▹ γ a ∣ f .ty = ∣ a ∣ f
Y▹ γ a .rel f .con = refl
Y▹ γ a .rel f .ty = a .rel f

p : Sub (Γ ▹ A) Γ
∣ p ∣ γa = ∣ ∣ γa ∣ C.id .con ∣ C.id
p {Γ = Γ} .rel γa =
  ≡-elim
    (λ γ _ → Γ .rel (γ C.id))
    (∣ γa ∣ C.id .con .rel C.id)
    (γa .rel C.id .con)

q : Tm (Γ ▹ A) (A [ p ]ᵀ)
∣ q {A = A} ∣ γa =
  ≡-elimₛ
    (λ γ _ → ∣ A ∣ γ)
    (∣ γa ∣ C.id .ty)
    (Yᶜ-≡ (ap (λ γ → γ C.id) (γa .rel C.id .con)))
q .rel γa = γa .rel C.id .ty

infixl 4 _,_
_,_ : (γ : Sub Δ Γ) → Tm Δ (A [ γ ]ᵀ) → Sub Δ (Γ ▹ A)
∣ γ , a ∣ δ .con = Yˢ γ δ
∣ γ , a ∣ δ .ty = ∣ a ∣ δ
(γ , a) .rel δ .con = refl
(_,_ {A = A} γ a) .rel δ .ty = a .rel δ

infixl 4 _,⟨_⟩_
_,⟨_⟩_ : (γ : Sub Δ Γ) (A : Ty Γ i) → Tm Δ (A [ γ ]ᵀ) → Sub Δ (Γ ▹ A)
γ ,⟨ A ⟩ a = γ , a

,-∘ :
  (γ : Sub Δ Γ) (a : Tm Δ (A [ γ ]ᵀ)) (δ : Sub Θ Δ) →
  (γ ,⟨ A ⟩ a) ∘ δ ≡ (γ ∘ δ , a [ δ ]ᵗ)
,-∘ γ a δ = refl

▹-β₁ : (γ : Sub Δ Γ) (a : Tm Δ (A [ γ ]ᵀ)) → p ∘ (γ ,⟨ A ⟩ a) ≡ γ
▹-β₁ γ a = refl

▹-β₂ : (γ : Sub Δ Γ) (a : Tm Δ (A [ γ ]ᵀ)) → q [ γ ,⟨ A ⟩ a ]ᵗ ≡ a
▹-β₂ γ a = refl

▹-η : {A : Ty Γ i} → (p ,⟨ A ⟩ q) ≡ id
▹-η =
  Sub-≡
    (funextᵢ
      (funext λ γa →
        sym (∣▹∣-≡ (Yᶜ-≡ (ap (λ γ → γ C.id) (γa .rel C.id .con))) refl)))

infixl 4 _↑
_↑ : (γ : Sub Δ Γ) → Sub (Δ ▹ A [ γ ]ᵀ) (Γ ▹ A)
γ ↑ = γ ∘ p , q

◇ : Con lzero
∣ ◇ ∣ x = ⊤
◇ .rel γ = ⊤ₚ

ε : Sub Γ ◇
∣ ε ∣ γ = ⋆
ε .rel γ = ⋆

ε-∘ : (γ : Sub Δ Γ) → ε ∘ γ ≡ ε
ε-∘ γ = refl

◇-η : ε ≡ id
◇-η = refl

record ∣U∣ (i : Level) (x : Ob) : Set (lsuc i) where
  field
    t : Hom y x → Set i   -- this is simpler than just Ty (yoneda x)
    rel : (∀ {y} (f : Hom y x) → t f) → Prop i
open ∣U∣ public renaming (t to ∣_∣)

∣U∣-≡ :
  {A₀ A₁ : ∣U∣ i x}
  (A-≡ : (λ {y} → ∣ A₀ ∣ {y}) ≡ (λ {y} → ∣ A₁ ∣ {y})) →
  ≡-elimₛ (λ A _ → (∀ {y} (f : Hom y x) → A f) → Prop i) (A₀ .rel) A-≡ ≡
  A₁ .rel →
  A₀ ≡ A₁
∣U∣-≡ refl refl = refl

U : (i : Level) → Ty Γ (lsuc i)
∣ U i ∣ {x} γ = ∣U∣ i x
U i .rel {x} γ A =
  (λ {y} (f : Hom y x) {z} (g : Hom z y) → ∣ A f ∣ g) ≡
  (λ {y} (f : Hom y x) {z} (g : Hom z y) → ∣ A (f C.∘ g) ∣ C.id)

U-[] : (i : Level) (γ : Sub Δ Γ) → U i [ γ ]ᵀ ≡ U i
U-[] i γ = refl

El : Tm Γ (U i) → Ty Γ i
∣ El A ∣ γ = ∣ ∣ A ∣ γ ∣ C.id
El A .rel γ a =
  ∣ A ∣ γ .rel λ f → ≡-elimₛ (λ a _ → a C.id f) (a f) (sym (A .rel γ))

El-[] : (A : Tm Γ (U i)) (γ : Sub Δ Γ) → El A [ γ ]ᵀ ≡ El (A [ γ ]ᵗ)
El-[] A γ = refl

c : Ty Γ i → Tm Γ (U i)
∣ ∣ c A ∣ γ ∣ f = ∣ A ∣ (γ |ᶜ f)
∣ c A ∣ γ .rel a = A .rel γ a
c A .rel γ = refl

c-[] : (A : Ty Γ i) (γ : Sub Δ Γ) → c A [ γ ]ᵗ ≡ c (A [ γ ]ᵀ)
c-[] A γ = refl

U-β : (A : Ty Γ i) → El (c A) ≡ A
U-β A = refl
{-
U-η : (A : Tm Γ (U i)) → c (El A) ≡ A
U-η A = Tm-≡ (funextᵢ (funext λ γ → sym (∣U∣-≡ (ap (λ a → a C.id) (A .rel γ)) {!refl!})))
-}
Π : (A : Ty Γ i) → Ty (Γ ▹ A) j → Ty Γ (i ⊔ j)
∣ Π A B ∣ γ = (a : Yᵀ A γ) → ∣ B ∣ (Y▹ γ a)
Π A B .rel γ t = (a : Yᵀ A γ) → B .rel (Y▹ γ a) λ f → t f (⟨ A , γ ⟩ a |ᵀ f)

Π-[] :
  (A : Ty Γ i) (B : Ty (Γ ▹ A) j) (γ : Sub Δ Γ) →
  Π A B [ γ ]ᵀ ≡ Π (A [ γ ]ᵀ) (B [ γ ↑ ]ᵀ)
Π-[] A B γ = refl

app :
  {A : Ty Γ i} (B : Ty (Γ ▹ A) j) →
  Tm Γ (Π A B) → (a : Tm Γ A) → Tm Γ (B [ id , a ]ᵀ)
∣ app B t a ∣ γ = ∣ t ∣ γ (Yᵗ a γ)
app B t a .rel γ = t .rel γ (Yᵗ a γ)

app-[] :
  {A : Ty Γ i} (B : Ty (Γ ▹ A) j) →
  (t : Tm Γ (Π A B)) (a : Tm Γ A) (γ : Sub Δ Γ) →
  app B t a [ γ ]ᵗ ≡
  app (B [ γ ↑ ]ᵀ) (t [ γ ]ᵗ) (a [ γ ]ᵗ)
app-[] B t a γ = refl

lam : Tm (Γ ▹ A) B → Tm Γ (Π A B)
∣ lam b ∣ γ a = ∣ b ∣ (Y▹ γ a)
lam b .rel γ a = b .rel (Y▹ γ a)

lam-[] : (b : Tm (Γ ▹ A) B) (γ : Sub Δ Γ) → lam b [ γ ]ᵗ ≡ lam (b [ γ ↑ ]ᵗ)
lam-[] b γ = refl

Π-β :
  {A : Ty Γ i} (B : Ty (Γ ▹ A) j) (b : Tm (Γ ▹ A) B) (a : Tm Γ A) →
  app B (lam b) a ≡ b [ id , a ]ᵗ
Π-β B b a = refl

Π-η :
  {Γ : Con i} {A : Ty Γ j} (B : Ty (Γ ▹ A) k) (t : Tm Γ (Π A B)) →
  lam (app (B [ p ↑ ]ᵀ) (t [ p ]ᵗ) (q {A = A})) ≡ t
Π-η B t = refl

module Psh where
  private variable
    f g : Hom y x

  record Con′ (i : Level) : Set (lsuc i) where
    infixl 9 _⟨_⟩
    field
      Γ′ : Ob → Set i
      _⟨_⟩ : Γ′ x → Hom y x → Γ′ y
      ⟨⟩-∘ : ∀ {γ} → γ ⟨ f C.∘ g ⟩ ≡ γ ⟨ f ⟩ ⟨ g ⟩
      ⟨⟩-id : {γ : Γ′ x} → γ ⟨ C.id ⟩ ≡ γ
  open Con′ public renaming (Γ′ to ∣_∣; _⟨_⟩ to infix 9 _!_⟨_⟩)

  ⌜_⌝ᶜ : Con i → Con′ i
  ∣ ⌜ Γ ⌝ᶜ ∣ = Yᶜ Γ
  ⌜ Γ ⌝ᶜ ._!_⟨_⟩ = _|ᶜ_
  ⌜ Γ ⌝ᶜ .⟨⟩-∘ = refl
  ⌜ Γ ⌝ᶜ .⟨⟩-id = refl

  ⌞_⌟ᶜ : Con′ i → Con i
  ∣ ⌞ Γ ⌟ᶜ ∣ = ∣ Γ ∣
  ⌞ Γ ⌟ᶜ .rel {x} k = ∀ {y} (f : Hom y x) → Γ ! k C.id ⟨ f ⟩ ≡ k f

  record Ty′ (Γ : Con i) (j : Level) : Set (i ⊔ lsuc j) where
    infixl 9 _⟨_⟩
    field
      A′ : Yᶜ Γ x → Set j
      _⟨_⟩ : ∀ {γ} → A′ γ → (f : Hom y x) → A′ (γ |ᶜ f)
      ⟨⟩-∘ : ∀ {γ} {a : A′ γ} → a ⟨ f C.∘ g ⟩ ≡ a ⟨ f ⟩ ⟨ g ⟩
      ⟨⟩-id : {γ : Yᶜ Γ x} {a : A′ γ} → a ⟨ C.id ⟩ ≡ a
  open Ty′ public renaming (A′ to ∣_∣; _⟨_⟩ to infix 9 _!_⟨_⟩)
{-
  ⌜_⌝ᵀ : Ty Γ j → Ty′ Γ j
  ∣ ⌜ A ⌝ᵀ ∣ = Yᵀ A
  ⌜ A ⌝ᵀ ._!_⟨_⟩ {γ} = ⟨ A , γ ⟩_|ᵀ_
  ⌜ A ⌝ᵀ .⟨⟩-∘ = refl
  ⌜ A ⌝ᵀ .⟨⟩-id = refl

  ⌞_⌟ᵀ : Ty′ Γ j → Ty Γ j
  ∣ ⌞ A ⌟ᵀ ∣ = ∣ A ∣
  ⌞ A ⌟ᵀ .rel {x} γ k = ∀ {y} (f : Hom y x) → A ! k C.id ⟨ f ⟩ ≡ k f

  record Ty′′ (Γ : Con′ i) (j : Level) : Set (i ⊔ lsuc j) where
    infixl 9 _⟨_⟩
    field
      A′ : Yᶜ Γ x → Set j
      _⟨_⟩ : ∀ {γ} → A′ γ → (f : Hom y x) → A′ (γ |ᶜ f)
      ⟨⟩-∘ : ∀ {γ} {a : A′ γ} → a ⟨ f C.∘ g ⟩ ≡ a ⟨ f ⟩ ⟨ g ⟩
      ⟨⟩-id : {γ : Yᶜ Γ x} {a : A′ γ} → a ⟨ C.id ⟩ ≡ a
  open Ty′ public renaming (A′ to ∣_∣; _⟨_⟩ to infix 9 _!_⟨_⟩)

-- TODO: define Ty⁺ (having context extension) and Π⁺ (domain in Ty⁺)

Ty⁺ Γ := (A : Ty Γ) × (_▹_ : (I : Ob) → Γ I → Ob) × Mor J (I ▹ γI) ≅ (f:Mor J I) × A J (γI [f])

Π⁺ ...
-}
