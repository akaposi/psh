{-# OPTIONS --rewriting #-}

module Internal where

open import Agda.Primitive
open import Lib using (_≡_)
import Lib as l
import WrappedStandard as P

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_

postulate
  Tyˢ  : P.Ty P.∙ lzero
  --     Set
  Tmˢ  : P.Ty (P.∙ P.▷ Tyˢ) lzero
  --     Ty → Set
  Πˢ   : P.Tm (P.∙ P.▷ Tyˢ P.▷ P.Π Tmˢ (Tyˢ P.[ P.ε ]T)) (Tyˢ P.[ P.ε ]T)
  --     (A : Ty)(B : Tm A → Ty) → Ty
  lamˢ : P.Tm (P.∙ P.▷ Tyˢ P.▷ P.Π Tmˢ (Tyˢ P.[ P.ε ]T) P.▷ P.Π (Tmˢ P.[ P.p ]T) (Tmˢ P.[ P.ε P., P.v¹ P.$ P.v⁰ ]T)) (Tmˢ P.[ P.ε P., Πˢ ]T P.[ P.p ]T)
  --     (A : Ty)(B : Tm A → Ty)(t : (x : Tm A) → Tm (B x)) → Tm (Π A B)
  appˢ : P.Tm (P.∙ P.▷ Tyˢ P.▷ P.Π Tmˢ (Tyˢ P.[ P.ε ]T) P.▷ Tmˢ P.[ P.ε P., Πˢ ]T) (P.Π (Tmˢ P.[ P.p ]T) (Tmˢ P.[ P.ε P., P.v¹ P.$ P.v⁰ ]T) P.[ P.p ]T)
  --     (A : Ty)(B : Tm A → Ty)(t : Tm (Π A B)) → (x : Tm A) → Tm (B x)
  Πβˢ  : appˢ P.[ P.p P., lamˢ ]t ≡ P.q
  --     app A B (lam t) ≡ t
  Πηˢ  : lamˢ P.[ P.p P., appˢ ]t ≡ P.q
  --     lam A B (app t) ≡ t
