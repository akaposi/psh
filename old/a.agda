{-# OPTIONS --prop #-}

open import Agda.Primitive
open import Cat

postulate
  C' : Cat
C : Cat
C = strictify C'
open Cat.Cat C

infix 4 _≡_
infixl 5 _,Σ_
data _≡_ {i}{A : Set i}(a : A) : A → Prop i where
  refl : a ≡ a
record ⊤ : Set where
record ⊤p : Prop where
record Σ {i}(A : Set i){j}(B : A → Set j) : Set (i ⊔ j) where
  constructor _,Σ_
  field
    π₁ : A
    π₂ : B π₁
open Σ
-- ,= : ∀{i}{A : Set i}{j}{B : A → Set j}{a₀ a₁ : A}(a₌ : a₀ ≡ a₁){b₀ : B a₀}{b₁ : B a₁} → b₀ ≡ b₁
record Lift {i}(A : Prop i) : Set i where
  constructor mk
  field un : A
open Lift

record Con : Set₁ where
  field
    _T : Ob → Set
    _R : ∀{I} → (∀{J}(f : Hom J I) → _T J) → Prop
open Con
    
record PSh : Set₁ where
  field
    Γ    : Ob → Set
    _[_] : ∀{I} → Γ I → ∀{J} → Hom J I → Γ J
    [∘]  : ∀{I}(γI : Γ I){J}(f : Hom J I){K}(g : Hom K J) → γI [ f ∘c g ] ≡ γI [ f ] [ g ]
    [id] : ∀{I}(γI : Γ I) → γI [ idc ] ≡ γI
  infix 8 _[_]
open PSh renaming (Γ to ∣_∣; _[_] to _∶_[_])

_∥_ : ∀{i I}{A : {J : Ob}(f : Hom J I) → Set i}
  (α : {J : Ob}(f : Hom J I) → A f){J}
  (f : Hom J I){K}(g : Hom K J) → A (f ∘c g)
(α ∥ f) g = α (f ∘c g)

⌜_⌝ : Con → PSh
∣ ⌜ Γ ⌝ ∣ I =  Σ
  (∀{J}(f : Hom J I) → (Γ T) J) λ γT →
  (∀{J}(f : Hom J I) → Lift ((Γ R) (γT ∥ f)))
⌜ Γ ⌝ ∶ (γT ,Σ γR) [ f ] = (γT ∥ f) ,Σ (γR ∥ f)
[∘] ⌜ Γ ⌝ _ _ _ = refl
[id] ⌜ Γ ⌝ _ = refl

⌞_⌟ : PSh → Con
⌞ F ⌟ T = ∣ F ∣
(⌞ F ⌟ R) {I} γT = (λ {J}(f : Hom J I) → F ∶ γT idc [ f ]) ≡ γT

record Sub' (Δ Γ : PSh) : Set where
  field
    γ   : ∀{I} → ∣ Δ ∣ I → ∣ Γ ∣ I
    nat : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I){δJ : ∣ Δ ∣ J} → Δ ∶ δI [ f ] ≡ δJ → Γ ∶ γ δI [ f ] ≡ γ δJ
open Sub' renaming (γ to ∣_∣)

Sub : Con → Con → Set
Sub Δ Γ = Sub' ⌜ Δ ⌝ ⌜ Γ ⌝

_∘_ : ∀{Γ Δ} → Sub' Δ Γ → ∀{Θ} → Sub' Θ Δ → Sub' Θ Γ
∣ γ ∘ δ ∣ θI = ∣ γ ∣ (∣ δ ∣ θI)
nat (γ ∘ δ) f δf = nat γ f (nat δ f δf)

id : ∀{Γ} → Sub' Γ Γ
∣ id ∣ γI = γI
nat id f γf = γf

ass : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{Ξ}{θ : Sub Ξ Θ} → (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
ass = refl

idl : ∀{Γ Δ}{γ : Sub Δ Γ} → id ∘ γ ≡ γ
idl = refl

idr : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘ id ≡ γ
idr = refl
{-
◆ : Con
(◆ T) _ = ⊤
(◆ R) _ = ⊤p

ε : ∀{Γ} → Sub Γ ◆
∣ ε ∣ _ = _
nat ε _ _ = refl

◆η : ∀{Γ}(σ : Sub Γ ◆) → σ ≡ ε
◆η _ = refl

record Ty' (Γ : PSh) : Set₁ where
  field
    A     : (I : Ob) → ∣ Γ ∣ I → Set
    _[_]_ : ∀{I}{γI : ∣ Γ ∣ I} → A I γI → ∀{J}(f : Hom J I) → {γJ : ∣ Γ ∣ J} → Γ ∶ γI [ f ] ≡ γJ → A J γJ
    [∘]   : ∀{I}{γI : ∣ Γ ∣ I}(aI : A I γI){J}(f : Hom J I){K}(g : Hom K J) → aI [ f ∘c g ] [∘] Γ γI f g ≡ (aI [ f ] refl) [ g ] refl
    [id]  : ∀{I}{γI : ∣ Γ ∣ I}(aI : A I γI) → aI [ idc ] [id] Γ γI ≡ aI
  infix 8 _[_]
open Ty' renaming (A to ∣_∣; _[_]_ to _∶_[_]_)

Ty : Con → Set₁
Ty Γ = Ty' ⌜ Γ ⌝

_[_]T : ∀{Γ} → Ty Γ → ∀{Δ} → Sub Δ Γ → Ty Δ
∣ A [ γ ]T ∣ I δI = ∣ A ∣ I (∣ γ ∣ δI)
(A [ γ ]T) ∶ aI [ f ] γf = A ∶ aI [ f ] nat γ f γf
[∘] (A [ γ ]T) aI f g = {![∘] A aI f g !}
[id] (A [ γ ]T) aI = [id] A aI

[∘]T : ∀{Γ}{A : Ty Γ}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → A [ γ ∘ δ ]T ≡ A [ γ ]T [ δ ]T
[∘]T = refl

[id]T : ∀{Γ}{A : Ty Γ} → A [ id {Γ} ]T ≡ A
[id]T = refl

record Tm (Γ : Con)(A : Ty Γ) : Set where
  field
    a   : ∀{I}(γI : ∣ ⌜ Γ ⌝ ∣ I) → ∣ A ∣ I γI
    nat : ∀{I}{γI : ∣ ⌜ Γ ⌝ ∣ I}{J}(f : Hom J I){γJ : ∣ ⌜ Γ ⌝ ∣ J}(γf : ⌜ Γ ⌝ ∶ γI [ f ] ≡ γJ) → A ∶ (a γI) [ f ] γf ≡ a γJ
open Tm renaming (a to ∣_∣)

_[_]t : ∀{Γ}{A : Ty Γ} → Tm Γ A → ∀{Δ}(γ : Sub Δ Γ) → Tm Δ (A [ γ ]T)
∣ a [ γ ]t ∣ δI = ∣ a ∣ (∣ γ ∣ δI)
nat (a [ γ ]t) f γf = nat a f (nat γ f γf)

[∘]t : ∀{Γ}{A : Ty Γ}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → a [ γ ∘ δ ]t ≡ a [ γ ]t [ δ ]t
[∘]t = refl

[id]t : ∀{Γ}{A : Ty Γ}{a : Tm Γ A} → a [ id ]t ≡ a
[id]t = refl

infix 7 _▹_
_▹'_ : (Γ : PSh)(A : Ty' Γ) → PSh
∣ Γ ▹' A ∣ I = Σ (∣ Γ ∣ I) (∣ A ∣ I)
(Γ ▹' A) ∶ (γI ,Σ aI) [ f ] = (Γ ∶ γI [ f ]) ,Σ (A ∶ aI [ f ] refl)
[id] (Γ ▹' A) _ = {!!}
[∘]  (Γ ▹' A) = {!!}

_▹_ : (Γ : Con) → Ty Γ → Con
Γ ▹ A = ⌞ ⌜ Γ ⌝ ▹' A ⌟

-- ⌜_⌝ : Con ≃ PSh : ⌞_⌟

p : ∀{Γ}{A : Ty Γ} → Sub (Γ ▹ A) Γ
∣ p ∣ (γaT ,Σ γaR) = (λ {J} f → π₁ (π₁ (γaT f)) idc) ,Σ λ {J} f → mk {!un (π₂ (π₁ (γaT f)) idc)!}
nat p f γaf = {!!}
-}
postulate
  Γ : PSh

yOb : Ob → PSh
∣ yOb I ∣ J = Hom J I
yOb I ∶ f [ g ] = f ∘c g
[∘] (yOb I) f g h = refl
[id] (yOb I) f = refl

yHom : ∀{J I} → Hom J I → Sub' (yOb J) (yOb I)
∣ yHom f ∣ g = f ∘c g
nat (yHom f) g h = {!!}

yl : ∀{Γ I} → ∣ Γ ∣ I → Sub' (yOb I) Γ
∣ yl {Γ} γI ∣ f = Γ ∶ γI [ f ]
nat (yl γI) f {g} gf = {!!}

Γ' : PSh
∣ Γ' ∣ I = Sub' (yOb I) Γ
Γ' ∶ γI [ f ] = γI ∘ yHom f
[∘] Γ' _ _ _ = refl
[id] Γ' _ = refl

{-
_,_ : ∀{Γ Δ}(γ : Sub Δ Γ){A : Ty Γ} → Tm Δ (A [ γ ]T) → Sub Δ (Γ ▹ A)
∣ γ , a ∣ δI = {!∣ γ ∣ δI!} -- ∣ γ ∣ δI ,Σ ∣ a ∣ δI
nat (γ , a) f δf = {!nat γ f δf !}



q : ∀{Γ}{A : Ty Γ} → Tm (Γ ▹ A) (A [ p {A = A} ]T)
∣ q ∣ (γI ,Σ aI) = aI
nat q = {!!}

▹β₁ : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Γ}{a : Tm Δ (A [ γ ]T)}→ p {A = A} ∘ (_,_ γ {A = A} a) ≡ γ
▹β₁ = refl

▹β₂ : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Γ}{a : Tm Δ (A [ γ ]T)}→ q {A = A} [ _,_ γ {A = A} a ]t ≡ a
▹β₂ = refl

▹η : ∀{Γ Δ}{A : Ty Γ}{γa : Sub Δ (Γ ▹ A)} → _,_ (p {A = A} ∘ γa) {A = A} (q {A = A} [ γa ]t) ≡ γa
▹η = refl

_^_ : ∀{Γ Δ}(γ : Sub Δ Γ)(A : Ty Γ) → Sub (Δ ▹ (A [ γ ]T)) (Γ ▹ A)
γ ^ A = _,_ (γ ∘ p {A = A [ γ ]T}) {A = A} (q {A = A [ γ ]T})


Sigma : {Γ : Con}(A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
∣ Sigma A B ∣ I γI = Σ (∣ A ∣ I γI) λ aI → ∣ B ∣ I (γI ,Σ aI)
Sigma A B ∶ (aI ,Σ bI) [ f ] γf  = (A ∶ aI [ f ] γf) ,Σ (B ∶ bI [ f ] {!!})
[∘] (Sigma A B) = {!!}
[id] (Sigma A B) = {!!}

Σ[] : ∀{Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)}{Δ}{γ : Sub Δ Γ} → Sigma A B [ γ ]T ≡ Sigma (A [ γ ]T) (B [ γ ^ A ]T)
Σ[] = refl

proj₁ : ∀{Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)} → Tm Γ (Sigma A B) → Tm Γ A
∣ proj₁ ab ∣ γI = π₁ (∣ ab ∣ γI)
nat (proj₁ ab) f γf = {!!}

proj₂ : ∀{Γ : Con}{A : Ty Γ}{B : Ty (Γ ▹ A)}(ab : Tm Γ (Sigma A B)) → Tm Γ (B [ _,_ id {A = A} (proj₁ {B = B} ab) ]T)
∣ proj₂ ab ∣ γI = π₂ (∣ ab ∣ γI)
nat (proj₂ ab) = {!!}

Sub= : {Γ Δ : Con} →
  {γ₀ γ₁ : ∀{I} → ∣ Δ ∣ I → ∣ Γ ∣ I}(γ₂ : (λ {I} → γ₀ {I}) ≡ γ₁) 
  {nat₀ : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I){δJ : ∣ Δ ∣ J} → Δ ∶ δI [ f ] ≡ δJ → Γ ∶ γ₀ δI [ f ] ≡ γ₀ δJ}
  {nat₁ : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I){δJ : ∣ Δ ∣ J} → Δ ∶ δI [ f ] ≡ δJ → Γ ∶ γ₁ δI [ f ] ≡ γ₁ δJ} →
  _≡_ {A = Sub Δ Γ} (record { γ = γ₀ ; nat = nat₀ }) (record { γ = γ₁ ; nat = nat₁ })
Sub= refl = refl

y∘ : ∀{Γ : Con}{I}{γI : ∣ Γ ∣ I}{J}{f : Hom J I} → yl {Γ} γI ∘ yHom f ≡ yl (Γ ∶ γI [ f ])
y∘ = Sub= {!!}

Π : {Γ : Con}(A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
∣ Π {Γ} A B ∣ I γI = Tm (yOb I ▹ (A [ yl γI ]T)) (B [ yl γI ^ A ]T)
_∶_[_]_ (Π A B) {I}{γI} t f γf = {!t [ yHom f ^ (A [ yl γI ]T) ]t!}
{-
∣ Π {Γ} A B ∣ I γI = Σ (∀{J}(f : Hom J I)(aJ : ∣ A ∣ J (Γ ∶ γI [ f ])) → ∣ B ∣ J ((Γ ∶ γI [ f ]) ,Σ aJ)) λ α →
                       ∀{J}(f : Hom J I){K}(g : Hom K J)(aJ :  ∣ A ∣ J (Γ ∶ γI [ f ])) → Lift (B ∶ α f aJ [ g ] refl ≡ {!α (f ∘c g) ?!})
Π A B ∶ (α ,Σ natα) [ f ] γf = (λ g aK → {!α (f ∘c g) ?!}) ,Σ {!!}
-}
[∘] (Π A B) = {!!}
[id] (Π A B) = {!!}
-}
