{-# OPTIONS --prop --rewriting #-}

module Lib.Props where

open import Agda.Primitive
open import Lib public

infix 4 _≡p_
infixl 4 _◾p_
infix 5 _⁻¹p
infix 4 _≡P_
infixl 5 _,ΣP_
infixl 5 _,ΣSP_
infixl 5 _,ΣPS_

record ElP {ℓ}(A : Prop ℓ) : Set ℓ where
  constructor mkElP
  field
    unElP : A

open ElP public

record ⊤P : Prop where

ttP : ⊤P   -- \st
ttP = record {}

record ΣSP {ℓ ℓ'}(A : Set ℓ)(B : A → Prop ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,ΣSP_
  field
    fstSP : A
    sndSP : B fstSP
open ΣSP public

record ΣPS {ℓ ℓ'}(A : Prop ℓ)(B : A → Set ℓ') : Set (ℓ ⊔ ℓ') where
  constructor _,ΣPS_
  field
    fstPS : A
    sndPS : B fstPS
open ΣPS public

record ΣP {ℓ ℓ'}(A : Prop ℓ)(B : A → Prop ℓ') : Prop (ℓ ⊔ ℓ') where
  constructor _,ΣP_
  field
    fstP : A
    sndP : B fstP
open ΣP public

data ⊥P : Prop where

exfalsoP : ∀{i}{C : Set i} → ⊥P → C
exfalsoP ()

data _≡P_ {ℓ}{A : Prop ℓ} (x : A) : A → Set ℓ where
  reflP : x ≡P x

data _≡p_ {ℓ}{A : Set ℓ} (x : A) : A → Prop ℓ where
  reflp : x ≡p x

_◾p_ : ∀{ℓ}{A : Set ℓ}{x y z : A} → x ≡p y → y ≡p z → x ≡p z
reflp ◾p reflp = reflp

ap-p : ∀{ℓ ℓ'}{A : Set ℓ}{B : Set ℓ'}(f : A → B){a₀ a₁ : A}(a₂ : a₀ ≡p a₁)
    → f a₀ ≡p f a₁
ap-p f reflp = reflp

_⁻¹p : ∀{ℓ}{A : Set ℓ}{x y : A} → x ≡p y → y ≡p x
reflp ⁻¹p = reflp
