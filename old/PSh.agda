{-# OPTIONS --prop #-}

module PSh where

open import Agda.Primitive
open import Cat

-- strictification of presheaves using Yoneda (we get presheaves where the laws are refl; this uses Prop essentially)

infix 4 _≡_

data _≡_ {ℓ}{A : Set ℓ} (x : A) : A → Prop ℓ where
  refl : x ≡ x

ap : ∀{ℓ ℓ'}{A : Set ℓ}{B : Set ℓ'}(f : A → B){a₀ a₁ : A}(a₂ : a₀ ≡ a₁)
    → f a₀ ≡ f a₁
ap f refl = refl

postulate
  C' : Cat
C : Cat
C = strictify C'
open Cat.Cat C

_⇒_ = Hom

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_

record Con : Set₁ where
  field
    !      : (I : Ob) → Set
    _⟨_⟩   : {I : Ob}(γ : ! I){J : Ob}(f : J ⇒ I) → ! J
    ⟨id⟩   : {I : Ob}{γ : ! I} → γ ⟨ idc ⟩ ≡ γ
    ⟨∘⟩    : {I : Ob}{γ : ! I}{J : Ob}{f : Hom J I}{K : Ob}{g : Hom K J} → γ ⟨ f ∘c g ⟩ ≡ γ ⟨ f ⟩ ⟨ g ⟩
open Con renaming (! to ∣_∣; _⟨_⟩ to _∶_⟨_⟩)

record Sub (Γ : Con)(Δ : Con) : Set where
  field
    ! : {I : Ob}(γ : ∣ Γ ∣ I) → ∣ Δ ∣ I
    ⟨⟩  : {I : Ob}{γ : ∣ Γ ∣ I}{J : Ob}{f : J ⇒ I}{γ' : ∣ Γ ∣ J} → Γ ∶ γ ⟨ f ⟩ ≡ γ' → Δ ∶ (! γ) ⟨ f ⟩ ≡ ! γ'
open Sub renaming (! to ∣_∣)

id : {Γ : Con} → Sub Γ Γ
∣ id ∣ γ = γ
⟨⟩ id = λ e → e

_∘_ : {Γ : Con}{Γ' : Con}(σ : Sub Γ' Γ){Γ'' : Con}(σ' : Sub Γ'' Γ') → Sub Γ'' Γ
∣ σ ∘ σ' ∣ γ'' = ∣ σ ∣ (∣ σ' ∣ γ'')
⟨⟩ (σ ∘ σ') = λ e → ⟨⟩ σ (⟨⟩ σ' e)

idl : {Γ : Con}{Δ : Con}{σ : Sub Γ Δ} → id ∘ σ ≡ σ
idl = refl
idr : {Γ : Con}{Δ : Con}{σ : Sub Γ Δ} → σ ∘ id ≡ σ
idr = refl
ass : {Θ : Con}{Δ : Con}{σ : Sub Θ Δ}{Ξ : Con}{δ : Sub Ξ Θ}{Γ : Con}{ν : Sub Γ Ξ} → (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
ass = refl

yOb : Ob → Con
∣ yOb I ∣ J = Hom J I
yOb I ∶ f ⟨ g ⟩ = f ∘c g
⟨id⟩ (yOb I) = refl
⟨∘⟩ (yOb I) = refl

yHom : ∀{J I} → Hom J I → Sub (yOb J) (yOb I)
∣ yHom f ∣ g = f ∘c g
⟨⟩ (yHom f) = ap (f ∘c_)

⌜_⌝ : Con → Con
∣ ⌜ Γ ⌝ ∣ I = Sub (yOb I) Γ
⌜ Γ ⌝ ∶ γ ⟨ f ⟩ = γ ∘ yHom f
⟨id⟩ ⌜ Γ ⌝ = refl
⟨∘⟩ ⌜ Γ ⌝ = refl

-- how to do types with _[_] satisfying [∘],[id]??

