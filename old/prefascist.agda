{-# OPTIONS --without-K #-}

module Prefascist where

open import Agda.Primitive
open import Lib
open import Cat

infix 7 _∥_
infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_

postulate
  C' : Cat

C : Cat
C = strictify C'

open Cat.Cat C

variable
  i j k l : Level
  I J K L : Ob
  f g h : Hom J I

record Con (i : Level) : Set (lsuc i) where
  field
    _T : Ob → Set i
    _R : ∀{I} → (∀{J}(f : Hom J I) → _T J) → Set i
open Con

record PSh (i : Level) : Set (lsuc i) where
  field
    ∣_∣    : Ob → Set i
    _∶_⟨_⟩ : ∣_∣ I → Hom J I → ∣_∣ J
    ⟨id⟩   : (λ (γ : ∣_∣ I) → _∶_⟨_⟩ γ idc) ≡ (λ γ → γ)
    ⟨∘⟩    : (λ (γ : ∣_∣ I)(f : Hom J I)(g : Hom K J) →
             _∶_⟨_⟩ γ (f ∘c g)) ≡ (λ γ f g → _∶_⟨_⟩ (_∶_⟨_⟩ γ f) g)
open PSh

_∥_ : ∀{i}{A : {J : Ob}(f : Hom J I) → Set  i}
  (α : {J : Ob}(f : Hom J I) → A f)
  (f : Hom J I)(g : Hom K J) → A (f ∘c g)
(α ∥ f) g = α (f ∘c g)

_∥'_ : ∀{i}{A : {J : Ob} → Set  i}
  (α : {J : Ob}(f : Hom J I) → A {J})
  (f : Hom J I)(g : Hom K J) → A {K}
(α ∥' f) g = α (f ∘c g)

⌜_⌝ : ∀{i} → Con i → PSh i
∣ ⌜ Γ ⌝ ∣ I = Σ
  (∀{J}(f : Hom J I) → (Γ T) J) λ γT →
  (∀{J}(f : Hom J I) → (Γ R) (γT ∥' f))
⌜ Γ ⌝ ∶ (γT ,Σ γR) ⟨ f ⟩ = (γT ∥' f) ,Σ (γR ∥ f)
⟨id⟩ ⌜ Γ ⌝ = refl
⟨∘⟩ ⌜ Γ ⌝ = refl

⌞_⌟ : ∀{i} → PSh i → Con i
⌞ F ⌟ T = ∣ F ∣
(⌞ F ⌟ R) {I} γT = (λ {J}(f : Hom J I) → F ∶ γT idc ⟨ f ⟩) ≡ γT

postulate
  Γ : PSh lzero

Γ' : PSh lzero
Γ' = ⌜ ⌞ Γ ⌟ ⌝

Γ'' = ∣ Γ' ∣

{-
variable
  Γ Δ Θ : Con _
  γ γ' γ'' : ∣ ⌜ Γ ⌝ ∣ I

record Sub {i j : Level}(Γ : Con i)(Δ : Con j) : Set (i ⊔ j) where
  field
    _T : ∣ ⌜ Γ ⌝ ∣ I → (Δ T) I
    _R : (γ : ∣ ⌜ Γ ⌝ ∣ I) → (Δ R) {I} (λ {J} f → _T {J} (⌜ Γ ⌝ ∶ γ ⟨ f ⟩))
open Sub
{-
record Sub {i j : Level}(Γ : Con i)(Δ : Con j) : Set (i ⊔ j) where
  field
    ∣_∣ : ∣ ⌜ Γ ⌝ ∣ I → ∣ ⌜ Δ ⌝ ∣ I
    ⟨_⟩ : {γ : PSh.∣ ⌜ Γ ⌝ ∣ I}(f : Hom J I){γ' : PSh.∣ ⌜ Γ ⌝ ∣ J} → γ' ≡ ⌜ Γ ⌝ ∶ γ ⟨ f ⟩ → 
          ∣_∣ {J} γ' ≡ ⌜ Δ ⌝ ∶ ∣_∣ {I} γ ⟨ f ⟩
    -- _∶⟨_⟩ : (λ (γ : Con.∣ Γ ∣ I)(f : Hom J I) → Δ ∶ ∣_∣ {I} γ ⟨ f ⟩) ≡ λ γ f → ∣_∣ {J} (Γ ∶ γ ⟨ f ⟩)
open Sub
-}
variable
  σ δ ν : Sub Γ Δ

id : Sub Γ Γ
id T = λ { (γT ,Σ γR) → γT idc }
id R = λ { (γT ,Σ γR) → γR idc }

_∘_ : Sub Θ Δ → Sub Γ Θ → Sub Γ Δ
(_∘_ {Γ = Γ} σ δ T) γ = (σ T) ((λ f → (δ T) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩)) ,Σ (λ f → (δ R) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩)))
(_∘_ {Γ = Γ} σ δ R) γ = (σ R) ((λ f → (δ T) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩)) ,Σ (λ f → (δ R) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩)))

{-
id : Sub Γ Γ
∣ id ∣ = λ γ → γ
⟨ id ⟩ f e = e

_∘_ : Sub Θ Δ → Sub Γ Θ → Sub Γ Δ
∣ σ ∘ δ ∣ = λ γ → ∣ σ ∣ (∣ δ ∣ γ)
⟨ σ ∘ δ ⟩ f e = ⟨ σ ⟩ f (⟨ δ ⟩ f e)
-}

idl : id ∘ σ ≡ σ
idl = refl
idr : σ ∘ id ≡ σ
idr = refl
ass : (σ ∘ δ) ∘ ν ≡ σ ∘ (δ ∘ ν)
ass = refl

∙ : Con lzero
(∙ T) I  = ⊤
(∙ R) γT = ⊤

{-
⌜∙⌝ : PSh lzero
∣ ⌜∙⌝ ∣ I = ⊤
⌜∙⌝ ∶ _ ⟨ _ ⟩  = tt
⟨id⟩ ⌜∙⌝ = refl
⟨∘⟩ ⌜∙⌝ = refl

• : Con lzero
• = ⌞ ⌜∙⌝ ⌟
-}

ε : Sub Γ ∙
(ε T) γ = tt
(ε R) γ = tt

{-
ε : Sub Γ ⌞ ⌜∙⌝ ⌟
∣ ε ∣ _ = (λ _ → tt) ,Σ (λ _ _ → refl)
⟨ ε ⟩ f e = refl
-}

∙η : σ ≡ ε
∙η = refl

record Ty {i : Level}(Γ : Con i)(j : Level) : Set (i ⊔ lsuc j) where
  field
    _T : ∣ ⌜ Γ ⌝ ∣ I → Set j
    _R : (γ : ∣ ⌜ Γ ⌝ ∣ I) → (∀{J}(f : Hom J I) → _T (⌜ Γ ⌝ ∶ γ ⟨ f ⟩)) → Set j
open Ty

_⌜▷⌝_ : ∀{i}(Γ : Con i){j}(A : Ty Γ j) → PSh _
∣ Γ ⌜▷⌝ A ∣ I = Σ
  (∣ ⌜ Γ ⌝ ∣ I) λ γ → Σ
  (∀{J}(f : Hom J I) → (A T) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩)) λ αT →
  (∀{J}(f : Hom J I) → (A R) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩) (αT ∥ f))
(Γ ⌜▷⌝ A) ∶ (γ ,Σ (αT ,Σ αR)) ⟨ f ⟩ = (⌜ Γ ⌝ ∶ γ ⟨ f ⟩) ,Σ (αT ∥ f ,Σ αR ∥ f)
⟨id⟩ (Γ ⌜▷⌝ A) = refl
⟨∘⟩  (Γ ⌜▷⌝ A) = refl

_▷_ : ∀{i}(Γ : Con i){j}(A : Ty Γ j) → Con _
(Γ ▷ A) = ⌞ Γ ⌜▷⌝ A ⌟

-- ⌞ F ⌟ T = ∣ F ∣
-- (⌞ F ⌟ R) {I} γT = (λ {J}(f : Hom J I) → F ∶ γT idc ⟨ f ⟩) ≡ γT

-- ((Γ ▷ A) T) I = Σ (Σ
--   (∀{J}(f : Hom J I) → (Γ T) J) λ γT →
--   (∀{J}(f : Hom J I) → (Γ R) (γT ∥ f))) λ γ → Σ
--   (∀{J}(f : Hom J I) → (A T) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩)) λ αT →
--   (∀{J}(f : Hom J I) → (A R) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩) (αT ∥ f))
-- ((Γ ▷ A)     R) {I} γT =
-- (⌞ Γ ⌜▷⌝ A ⌟ R) {I} γT =
-- (λ {J}(f : Hom J I) → Γ ⌜▷⌝ A ∶ γT idc ⟨ f ⟩) ≡ γT =
-- (λ {J}(f : Hom J I) → (⌜ Γ ⌝ ∶ γT idc ₁ ⟨ f ⟩) ,Σ (γT idc ₂ ₁ ∥ f ,Σ γT idc ₂ ₂ ∥ f)) ≡ γT =
-- (λ {J}(f : Hom J I) → (γT idc ₁ ₁ ∥ f ,Σ γT idc ₁ ₂ ∥ f) ,Σ (γT idc ₂ ₁ ∥ f ,Σ γT idc ₂ ₂ ∥ f)) ≡ γT

_[_]T : ∀{i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
((_[_]T A {Γ = Γ} σ) T) γ = (A T) (
  (λ f → (σ T) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩)) ,Σ
  (λ f → (σ R) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩)))
((_[_]T A {Γ = Γ} σ) R) γ αT = (A R) (
  (λ f → (σ T) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩)) ,Σ
  (λ f → (σ R) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩))) αT

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T ≡ A
[id]T = refl
[∘]T : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}{l}{A : Ty Δ l} → A [ σ ∘ δ ]T ≡ A [ σ ]T [ δ ]T
[∘]T = refl

record Tm {i : Level}(Γ : Con i){j : Level}(A : Ty Γ j) : Set (i ⊔ j) where
  field
    _T : (γ : ∣ ⌜ Γ ⌝ ∣ I) → (A T) γ
    _R : (γ : ∣ ⌜ Γ ⌝ ∣ I) → (A R) γ (λ f → _T (⌜ Γ ⌝ ∶ γ ⟨ f ⟩))
open Tm

_,_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : Ty Δ k}(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▷ A)
((_,_ {Γ = Γ} σ t) T) γ = (
  (λ f → (σ T) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩)) ,Σ
  (λ f → (σ R) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩))) ,Σ (
  (λ f → (t T) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩)) ,Σ
  (λ f → (t R) (⌜ Γ ⌝ ∶ γ ⟨ f ⟩)))
((σ , t) R) γ = refl

p : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Sub (Γ ▷ A) Γ
(p {Γ = Γ}{A = A} T) (γT ,Σ γR) = (γT idc ₁ ₁) idc
(p {Γ = Γ}{A = A} R) {I} (γT ,Σ γR) = tr
  (Γ R)
  (ap (λ z {J} f → (z f ₁ ₁) idc) (γR idc))
  ((γT idc ₁ ₂) idc)

q : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ p {A = A} ]T)
(q {Γ = Γ} {A = A} T) (γT ,Σ γR) = tr
  (A T)
  {!!}
  -- (,Σ= (ap (λ z {J} f → (z f ₁ ₁) idc) (γR idc))
  --     {!apd (λ z {J} f → (z f ₁ ₂) idc) (γR idc)!})
  ((γT idc ₂ ₁) idc)
q {Γ = Γ} {A = A} R = {!!}
{- 
(γT idc ₁ ₁) ∥ idc ,Σ (γT idc ₁ ₂) ∥ idc ≡
(λ f → (γT f ₁ ₁) idc) ,Σ (λ f → tr (Γ R) (ap (λ z {J} f₁ → (z f₁ ₁ ₁) idc) (γR f)) ((γT f ₁ ₂) idc))







(λ {J} f → (γT idc ₁ ₁) f) ,Σ (λ {J} f → (γT idc ₁ ₂) f) ≡
(λ {J} f → (γT f ₁ ₁) idc) ,Σ (λ {J} f → (γT f ₁ ₂) idc)

kell :
(λ f → (γT idc ₁ ₁) f) ,Σ (λ f → (γT idc ₁ ₂) f) ≡
(λ f → (γT f ₁ ₁) idc) ,Σ (λ f → tr (Γ R) (ap (λ z {J} g → (z g ₁ ₁) idc) (γR f)) ((γT f ₁ ₂) idc))

(α ∥ f) g = α (f ∘c g)

ap (λ z {J} g → (z g ₁ ₁) idc) (γR idc) : (λ {J} g → (γT idc ₁ ₁) g ≡ (λ {J} g → (γT g ₁ ₁) idc)
-}
▷β₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → p {A = A} ∘ (_,_ σ {A = A} t) ≡ σ
▷β₁ = refl
{-
record YCon (i : Level)(I : Ob) : Set (lsuc i) where
  field
    _T : {J : Ob}(f : J ⇒ I) → Set i
    _R : ({J : Ob}(f : J ⇒ I) → _T f) → Prop i
open YCon

U : (i : Level) → Con (lsuc i)
U i T = YCon i
(U i R) {I} Γ = {J : Ob}(f : J ⇒ I) → (Γ (idc I) T) f ≡p (Γ f T) (idc J)

Ty : {i : Level}(Γ : Con i)(j : Level) → Set (i ⊔ lsuc j)
Ty Γ j = Sub Γ (U j)

_▷_ : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j ⊔ lsuc lzero)
((Γ ▷ A) T) I = Σ ((Γ T) I) λ γT → {!!}
(Γ ▷ A) R = {!!}

{-
record Tm {i}(Γ : Con i){j}(A : Ty Γ j) : Set (i ⊔ j ⊔ lsuc lzero) where
  field
    _T : {I : Ob}(γT : ∀{J} → J ⇒ I → (Γ T)     J)(γR : ∀{J}(f : J ⇒ I) → (Γ R) (γT ∣ f)) → (A T) γT γR
    _R : {I : Ob}(γT : ∀{J} → J ⇒ I → (Γ Con.T) J)(γR : ∀{J}(f : J ⇒ I) → (Γ R) (γT ∣ f)) → (A R) γT γR λ f → _T (γT ∣ f) λ g → γR (f ∘c g)
open Tm

id : ∀{i}{Γ : Con i} → Sub Γ Γ
id T = λ γT γR → γT (idc _)
id R = λ γT γR → γR (idc _)

_∘_ : ∀{i}{Γ : Con i}{i'}{Γ' : Con i'}(σ : Sub Γ' Γ){i''}{Γ'' : Con i''}(σ' : Sub Γ'' Γ') → Sub Γ'' Γ
(σ ∘ σ') T = λ γT'' γR'' → (σ T)  (λ f → (σ' T) (γT'' ∣ f) (λ g → γR'' (f ∘c g)))
  (λ f → (σ' R) (γT'' ∣ f) (λ g → γR'' (f ∘c g)))
(σ ∘ σ') R = λ γT'' γR'' → (σ R)
  (λ f → (σ' T) (γT'' ∣ f) (λ g → γR'' (f ∘c g)))
  (λ f → (σ' R) (γT'' ∣ f) (λ g → γR'' (f ∘c g)))

idl : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → id ∘ σ l.≡ σ
idl = l.refl
idr : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → σ ∘ id l.≡ σ
idr = l.refl
ass : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Ξ : Con k}{δ : Sub Ξ Θ}{l}{Γ : Con l}{ν : Sub Γ Ξ} → (σ ∘ δ) ∘ ν l.≡ σ ∘ (δ ∘ ν)
ass = l.refl

_[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
(A [ σ ]T) T = λ γT γR    → (A T) (λ f → (σ T) (γT ∣ f) (λ g → γR (f ∘c g))) (λ f → (σ R) (γT ∣ f) (λ g → γR (f ∘c g)))
(A [ σ ]T) R = λ γT γR αT → (A R) (λ f → (σ T) (γT ∣ f) (λ g → γR (f ∘c g))) (λ f → (σ R) (γT ∣ f) (λ g → γR (f ∘c g))) αT

_[_]t : ∀{i}{Δ : Con i}{j}{A : Ty Δ j}(t : Tm Δ A){k}{Γ : Con k}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
(t [ σ ]t) Tm.T = λ γT γR → (t Tm.T) (λ f → (σ T) (γT ∣ f) λ g → γR (f ∘c g)) (λ f → (σ R) (γT ∣ f) λ g → γR (f ∘c g))
(t [ σ ]t) Tm.R = λ γT γR → (t Tm.R) (λ f → (σ T) (γT ∣ f) λ g → γR (f ∘c g)) (λ f → (σ R) (γT ∣ f) λ g → γR (f ∘c g))

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T l.≡ A
[id]T = l.refl
[∘]T : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}{l}{A : Ty Δ l} → A [ σ ]T [ δ ]T l.≡ A [ σ ∘ δ ]T
[∘]T = l.refl
[id]t : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{t : Tm Γ A} → t [ id ]t l.≡ t
[id]t = l.refl
[∘]t : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}{l}{A : Ty Δ l}{t : Tm Δ A} → t [ σ ]t [ δ ]t l.≡ t [ σ ∘ δ ]t
[∘]t = l.refl

∙ : Con lzero
∙ T = λ _ → l.⊤
∙ R = λ _ → l.⊤P

ε : ∀{i}{Γ : Con i} → Sub Γ ∙
ε T = λ _ _ → l.tt
ε R = λ _ _ → l.ttP

∙η : ∀{i}{Γ : Con i}{σ : Sub Γ ∙} → σ l.≡ ε
∙η = l.refl

_▷_ : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j ⊔ lsuc lzero)
(Γ ▷ A) T = λ I → l.Σ   (∀{J}(f : J ⇒ I) → (Γ T) J) λ γT →
                  l.ΣPS (∀{J}(f : J ⇒ I) → (Γ R) (γT ∣ f)) λ γR →
                        (∀{J}(f : J ⇒ I) → (A T) (γT ∣ f) (λ g → γR (f ∘c g)))
(Γ ▷ A) R = λ γαT → (A R)
  (λ f → l.fst (γαT (idc _)) f)
  (λ f → l.fstPS (l.snd (γαT (idc _))) f)
  (λ f → l.sndPS (l.snd (γαT (idc _))) f)

_,_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : Ty Δ k}(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▷ A)
(σ , t) T = λ γT γR →
  (λ f → (σ T) (γT ∣ f) (λ g → γR (f ∘c g))) l.,Σ (
  (λ f → (σ R) (γT ∣ f) (λ g → γR (f ∘c g))) l.,ΣPS
  (λ f → (t T) (γT ∣ f) (λ g → γR (f ∘c g))))
(σ , t) R = λ γT γR → (t R) γT γR

π₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{k}{A : Ty Δ k}(σ : Sub Γ (Δ ▷ A)) → Sub Γ Δ
π₁ σ T = λ γT γR → l.fst ((σ T) γT γR) (idc _)
π₁ σ R = λ γT γR → {!l.fstPS (l.snd ((σ T) γT γR)) ?!}

p : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Sub (Γ ▷ A) Γ
p T = λ γT γR → l.fst (γT (idc _)) (idc _)
p R = λ γT γR → {!l.fstPS (l.snd (γT (idc _))) (idc _)!}
{-
  q : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ p ]T)
  ▷β₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → p ∘ (_,_ σ {A = A} t) l.≡ σ
{-# REWRITE [id]t [∘]t ▷β₁  #-}

postulate
  ▷β₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → q [ _,_ σ {A = A} t ]t l.≡ t
  ▷η : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → (p {A = A} , q {A = A}) l.≡ id
  ,∘ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)}{l}{θ : Con l}{δ : Sub θ Γ} →
    (_,_ σ {A = A} t) ∘ δ l.≡ (σ ∘ δ) , (t [ δ ]t)
{-# REWRITE ▷β₂ ,∘ ▷η #-}

-- abbreviations
p² :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k} →
   Sub (Γ ▷ A ▷ B) Γ
p² = p ∘ p

p³ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l} →
   Sub (Γ ▷ A ▷ B ▷ C) Γ
p³ = p ∘ p ∘ p

p⁴ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D) Γ
p⁴ = p ∘ p ∘ p ∘ p

p⁵ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) Γ
p⁵ = p ∘ p ∘ p ∘ p ∘ p

v⁰ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ p ]T)
v⁰ = q

v¹ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k} →
   Tm (Γ ▷ A ▷ B) (A [ p² ]T)
v¹ = v⁰ [ p ]t

v² :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l} →
   Tm (Γ ▷ A ▷ B ▷ C) (A [ p³ ]T)
v² = v⁰ [ p² ]t

v³ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m} →
   Tm (Γ ▷ A ▷ B ▷ C ▷ D) (A [ p⁴ ]T)
v³ = v⁰ [ p³ ]t

_^_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}(A : Ty Δ k) →
  Sub (Γ ▷ A [ σ ]T) (Δ ▷ A)
_^_ {i}{Γ}{j}{Δ} σ {k} A = σ ∘ p , v⁰ {i}{Γ}{_}{A [ σ ]T}

-- Identity
postulate
  Id : ∀{i}{Γ : Con i}{j}(A : Ty Γ j)(u v : Tm Γ A) → Ty Γ j
  Id[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u v : Tm Γ A}{k}{Θ : Con k}{σ : Sub Θ Γ} →
    Id A u v [ σ ]T l.≡ Id (A [ σ ]T) (u [ σ ]t) (v [ σ ]t)
{-# REWRITE Id[] #-}

postulate
  refl : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}(u : Tm Γ A) → Tm Γ (Id A u u)
  refl[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}{k}{Θ : Con k}{σ : Sub Θ Γ} →
    refl u [ σ ]t l.≡ refl (u [ σ ]t)

  J :
    ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
     {k}(C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k)
     (w : Tm Γ (C [ id , u , refl u ]T))
     {v : Tm Γ A}(t : Tm Γ (Id A u v)) → Tm Γ (C [ id , v , t ]T)

  Idβ :
    ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
     {k}{C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k}
     {w : Tm Γ (C [ id , u , refl u ]T)} →
     J C w (refl u) l.≡ w
{-# REWRITE refl[] Idβ #-}

-- postulate
--   J[] :
--     ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
--      {k}{C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k}
--      {w : Tm Γ (C [ id , u , refl u ]T)}
--      {v : Tm Γ A}{t : Tm Γ (Id A u v)}{l}{θ : Con l}{σ : Sub θ Γ} →
--      l.coe {!!}
--        (J C w t [ σ ]t)
--      ≡ J {l}{θ}{j}{A [ σ ]T}{u [ σ ]t}{k}
--          (C [ σ ^ A ^ Id (A [ p ]T) (u [ p ]t) v⁰ ]T)
--          {!!}
--          {!!}
-}
-}
-}
-}
