{-# OPTIONS --rewriting --prop --type-in-type #-}

module PSh where

open import Agda.Primitive
import Lib as l
import Lib.Props as l
open import Cat

infixl 5 _▷_
infixl 7 _[_]T
infixl 5 _,_
infixr 6 _∘_
infixl 8 _[_]t
infixl 5 _^_

record Con (i : Level) : Set (lsuc i) where
  field
    _T : (I : Ob) → Set i
    _R : (I : Ob)(γT : {J : Ob} → J ⇒ I → _T J) → Prop i
open Con

_∣_ : ∀{i}{ΓT : Ob → Set i}{I} →
  (∀{J} → J ⇒ I → ΓT J) → ∀{J} → J ⇒ I →
  (∀{K} → K ⇒ J → ΓT K)
γT ∣ f = λ g → γT (f ∘c g)

_∥_ : ∀{i}{ΓT : Ob → Set i}{ΓR : (I : Ob)(γT : {J : Ob} → J ⇒ I → ΓT J) → Prop i}{I}{γT : ∀{J} → J ⇒ I → ΓT J} → 
  (∀{J}(f : J ⇒ I) → ΓR J (γT ∣ f)) → ∀{J}(f : J ⇒ I) →
  (∀{K}(g : K ⇒ J) → ΓR K (γT ∣ (f ∘c g)))
γR ∥ f = λ g → γR (f ∘c g)

record Sub {i j}(Γ : Con i)(Δ : Con j) : Set (i ⊔ j ⊔ lsuc lzero) where
  field
    _T : (I : Ob) → (Γ T) I → (Δ T) I
    _R : (I : Ob)(γT : ∀{J} → J ⇒ I → (Γ Con.T) J) → (Γ Con.R) I γT → (Δ Con.R) I (λ f → _T _ (γT f))
open Sub

record Ty {i}(Γ : Con i) j : Set (i ⊔ lsuc j) where
  field
    _T : (I : Ob)(γT : ∀{J} → J ⇒ I → (Γ T)     J)(γR : ∀{J}(f : J ⇒ I) → (Γ R) J (γT ∣ f)) → Set j
    _R : (I : Ob)(γT : ∀{J} → J ⇒ I → (Γ Con.T) J)(γR : ∀{J}(f : J ⇒ I) → (Γ R) J (γT ∣ f))
         (αT : ∀{J}(f : J ⇒ I) → _T J (γT ∣ f) (λ g → γR (f ∘c g))) → Prop j
open Ty         

record Tm {i}(Γ : Con i){j}(A : Ty Γ j) : Set (i ⊔ j ⊔ lsuc lzero) where
  field
    _T : (I : Ob)(γT : ∀{J} → J ⇒ I → (Γ T)     J)(γR : ∀{J}(f : J ⇒ I) → (Γ R) J (γT ∣ f)) → (A T) I γT γR
    _R : (I : Ob)(γT : ∀{J} → J ⇒ I → (Γ Con.T) J)(γR : ∀{J}(f : J ⇒ I) → (Γ R) J (γT ∣ f)) → (A R) I γT γR λ f → _T _ (γT ∣ f) λ g → γR (f ∘c g)

id : ∀{i}{Γ : Con i} → Sub Γ Γ
id T = λ I γT → γT
id R = λ I γT γR → γR

_∘_ : ∀{i}{Γ : Con i}{i'}{Γ' : Con i'}(σ : Sub Γ' Γ){i''}{Γ'' : Con i''}(σ' : Sub Γ'' Γ') → Sub Γ'' Γ
(σ ∘ σ') T = λ I γT'' → (σ T) I ((σ' T) I γT'')
(σ ∘ σ') R = λ I γT γR'' → (σ R) I (λ f → (σ' T) _ (γT f)) ((σ' R) I γT γR'')

idl : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → id ∘ σ l.≡ σ
idl = l.refl
idr : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ} → σ ∘ id l.≡ σ
idr = l.refl
ass : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Ξ : Con k}{δ : Sub Ξ Θ}{l}{Γ : Con l}{ν : Sub Γ Ξ} → (σ ∘ δ) ∘ ν l.≡ σ ∘ (δ ∘ ν)
ass = l.refl

_[_]T : ∀ {i}{Δ : Con i}{j}(A : Ty Δ j){k}{Γ : Con k}(σ : Sub Γ Δ) → Ty Γ j
(A [ σ ]T) T = λ I γT γR    → (A T) I (λ f → (σ T) _ (γT f)) (λ f → (σ R) _ _ (γR f))
(A [ σ ]T) R = λ I γT γR αT → (A R) I (λ f → (σ T) _ (γT f)) (λ f → (σ R) _ _ (γR f)) αT

_[_]t : ∀{i}{Δ : Con i}{j}{A : Ty Δ j}(t : Tm Δ A){k}{Γ : Con k}(σ : Sub Γ Δ) → Tm Γ (A [ σ ]T)
(t [ σ ]t) Tm.T = λ I γT γR → (t Tm.T) I (λ f → (σ T) _ (γT f)) (λ f → (σ R) _ _ (γR f))
(t [ σ ]t) Tm.R = λ I γT γR → (t Tm.R) I (λ f → (σ T) _ (γT f)) (λ f → (σ R) _ _ (γR f))

[id]T : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → A [ id ]T l.≡ A
[id]T = l.refl
[∘]T : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}{l}{A : Ty Δ l} → A [ σ ]T [ δ ]T l.≡ A [ σ ∘ δ ]T
[∘]T = l.refl
[id]t : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{t : Tm Γ A} → t [ id ]t l.≡ t
[id]t = l.refl
[∘]t : ∀{i}{Θ : Con i}{j}{Δ : Con j}{σ : Sub Θ Δ}{k}{Γ : Con k}{δ : Sub Γ Θ}{l}{A : Ty Δ l}{t : Tm Δ A} → t [ σ ]t [ δ ]t l.≡ t [ σ ∘ δ ]t
[∘]t = l.refl

∙ : Con lzero
∙ T = λ _ → l.⊤
∙ R = λ _ _ → l.⊤P

ε : ∀{i}{Γ : Con i} → Sub Γ ∙
ε T = λ _ _ → l.tt
ε R = λ _ _ _ → l.ttP

∙η : ∀{i}{Γ : Con i}{σ : Sub Γ ∙} → σ l.≡ ε
∙η = l.refl

_▷_ : ∀ {i}(Γ : Con i){j}(A : Ty Γ j) → Con (i ⊔ j ⊔ lsuc lzero)
(Γ ▷ A) T = λ I → l.Σ   (∀{J} → J ⇒ I → (Γ T) J) λ γT →
                  l.ΣPS (∀{J}(f : J ⇒ I) → (Γ R) J (γT ∣ f)) λ γR →
                        (∀{J}(f : J ⇒ I) → (A T) J (γT ∣ f) (λ g → γR (f ∘c g)))
(Γ ▷ A) R = λ I γαT → (A R) I
  (λ f → l.fst (γαT (idc _)) f)
  (λ f → l.fstPS (l.snd (γαT (idc _))) f)
  (λ f → l.sndPS (l.snd (γαT (idc _))) f)

_,_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}{A : Ty Δ k}(t : Tm Γ (A [ σ ]T)) → Sub Γ (Δ ▷ A)
(σ , t) T = λ I γT →
  (λ {J} f → (σ T) J {!γT!} ) l.,Σ (
  {!!} l.,ΣPS
  {!!})
(σ , t) R = {!!}


{-
  p : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Sub (Γ ▷ A) Γ
  q : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ p ]T)
  ▷β₁ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → p ∘ (_,_ σ {A = A} t) l.≡ σ
{-# REWRITE [id]t [∘]t ▷β₁  #-}

postulate
  ▷β₂ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)} → q [ _,_ σ {A = A} t ]t l.≡ t
  ▷η : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → (p {A = A} , q {A = A}) l.≡ id
  ,∘ : ∀{i}{Γ : Con i}{j}{Δ : Con j}{σ : Sub Γ Δ}{k}{A : Ty Δ k}{t : Tm Γ (A [ σ ]T)}{l}{θ : Con l}{δ : Sub θ Γ} →
    (_,_ σ {A = A} t) ∘ δ l.≡ (σ ∘ δ) , (t [ δ ]t)
{-# REWRITE ▷β₂ ,∘ ▷η #-}

-- abbreviations
p² :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k} →
   Sub (Γ ▷ A ▷ B) Γ
p² = p ∘ p

p³ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l} →
   Sub (Γ ▷ A ▷ B ▷ C) Γ
p³ = p ∘ p ∘ p

p⁴ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D) Γ
p⁴ = p ∘ p ∘ p ∘ p

p⁵ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m}
   {n}{E : Ty (Γ ▷ A ▷ B ▷ C ▷ D) n} →
   Sub (Γ ▷ A ▷ B ▷ C ▷ D ▷ E) Γ
p⁵ = p ∘ p ∘ p ∘ p ∘ p

v⁰ : ∀{i}{Γ : Con i}{j}{A : Ty Γ j} → Tm (Γ ▷ A) (A [ p ]T)
v⁰ = q

v¹ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k} →
   Tm (Γ ▷ A ▷ B) (A [ p² ]T)
v¹ = v⁰ [ p ]t

v² :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l} →
   Tm (Γ ▷ A ▷ B ▷ C) (A [ p³ ]T)
v² = v⁰ [ p² ]t

v³ :
  ∀{i}{Γ : Con i}
   {j}{A : Ty Γ j}
   {k}{B : Ty (Γ ▷ A) k}
   {l}{C : Ty (Γ ▷ A ▷ B) l}
   {m}{D : Ty (Γ ▷ A ▷ B ▷ C) m} →
   Tm (Γ ▷ A ▷ B ▷ C ▷ D) (A [ p⁴ ]T)
v³ = v⁰ [ p³ ]t

_^_ : ∀{i}{Γ : Con i}{j}{Δ : Con j}(σ : Sub Γ Δ){k}(A : Ty Δ k) →
  Sub (Γ ▷ A [ σ ]T) (Δ ▷ A)
_^_ {i}{Γ}{j}{Δ} σ {k} A = σ ∘ p , v⁰ {i}{Γ}{_}{A [ σ ]T}

-- Identity
postulate
  Id : ∀{i}{Γ : Con i}{j}(A : Ty Γ j)(u v : Tm Γ A) → Ty Γ j
  Id[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u v : Tm Γ A}{k}{Θ : Con k}{σ : Sub Θ Γ} →
    Id A u v [ σ ]T l.≡ Id (A [ σ ]T) (u [ σ ]t) (v [ σ ]t)
{-# REWRITE Id[] #-}

postulate
  refl : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}(u : Tm Γ A) → Tm Γ (Id A u u)
  refl[] : ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}{k}{Θ : Con k}{σ : Sub Θ Γ} →
    refl u [ σ ]t l.≡ refl (u [ σ ]t)

  J :
    ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
     {k}(C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k)
     (w : Tm Γ (C [ id , u , refl u ]T))
     {v : Tm Γ A}(t : Tm Γ (Id A u v)) → Tm Γ (C [ id , v , t ]T)

  Idβ :
    ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
     {k}{C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k}
     {w : Tm Γ (C [ id , u , refl u ]T)} →
     J C w (refl u) l.≡ w
{-# REWRITE refl[] Idβ #-}

-- postulate
--   J[] :
--     ∀{i}{Γ : Con i}{j}{A : Ty Γ j}{u : Tm Γ A}
--      {k}{C : Ty (Γ ▷ A ▷ Id (A [ p ]T) (u [ p ]t) v⁰) k}
--      {w : Tm Γ (C [ id , u , refl u ]T)}
--      {v : Tm Γ A}{t : Tm Γ (Id A u v)}{l}{θ : Con l}{σ : Sub θ Γ} →
--      l.coe {!!}
--        (J C w t [ σ ]t)
--      ≡ J {l}{θ}{j}{A [ σ ]T}{u [ σ ]t}{k}
--          (C [ σ ^ A ^ Id (A [ p ]T) (u [ p ]t) v⁰ ]T)
--          {!!}
--          {!!}
-}
