{-# OPTIONS --prop --rewriting --type-in-type #-}

open import Agda.Primitive

-- what if we strictify the standard model using presheaves? do presheaves over the standard model have strict Π⁺?

--f why not? it seems that we need definitionally natural presheaves for this (our presheaves are not definitional at all)

-- we should check the version where the standard model is hardcoded, so there are no extra equalities in the notion of model

infixl 2 _◾_
infix 5 _⁻¹
infix 4 _≡_
infixl 5 _,Σ_
data _≡_ {i}{A : Set i}(a : A) : A → Prop i where
  refl : a ≡ a
_◾_ : ∀{i}{A : Set i}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
refl ◾ refl = refl
_⁻¹ : ∀{i}{A : Set i}{a a' : A} → a ≡ a' → a' ≡ a
refl ⁻¹ = refl
record ⊤ : Set where
record ⊤p : Prop where
record Σ {i}(A : Set i){j}(B : A → Set j) : Set (i ⊔ j) where
  constructor _,Σ_
  field
    π₁ : A
    π₂ : B π₁
open Σ
record Lift {i}(A : Prop i) : Set i where
  constructor mk
  field un : A
open Lift
cong : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong f refl = refl

postulate coe     : ∀{ℓ}{A B : Set ℓ} → A ≡ B → A → B
postulate coerefl : {A : Set}{e : A ≡ A}{a : A} → coe e a ≡ a
{-# BUILTIN REWRITE _≡_ #-}
{-# REWRITE coerefl   #-}
transp : ∀{ℓ}{A : Set ℓ}{ℓ'}(P : A → Set ℓ'){a a' : A} → a ≡ a' → P a → P a'
transp P e p = coe (cong P e) p

record Model {i}{j}{k}{l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _∘_
  infixl 5 _▹_
  infixl 5 _,[_]_
  field
    Con      : Set i
    Sub      : Con → Con → Set j
    _∘_      : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
    ass      : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{Ξ}{θ : Sub Ξ Θ} → (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
    id       : ∀{Γ} → Sub Γ Γ
    idl      : ∀{Γ Δ}{γ : Sub Δ Γ} → id ∘ γ ≡ γ
    idr      : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘ id ≡ γ    
    Ty       : Con → Set k
    _[_]T    : ∀{Γ} → Ty Γ → ∀{Δ} → Sub Δ Γ → Ty Δ
    [∘]T     : ∀{Γ}{A : Ty Γ}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → A [ γ ∘ δ ]T ≡ A [ γ ]T [ δ ]T
    [id]T    : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
    Tm       : (Γ : Con) → Ty Γ → Set l
    _[_][_]t : ∀{Γ}{A : Ty Γ} → Tm Γ A → ∀{Δ}(γ : Sub Δ Γ){A' : Ty Δ} → A [ γ ]T ≡ A' → Tm Δ A'
    [∘]t     : ∀{Γ}{A : Ty Γ}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → a [ γ ∘ δ ][ [∘]T ]t ≡ a [ γ ][ refl ]t [ δ ][ refl ]t
    [id]t    : ∀{Γ}{A : Ty Γ}{a : Tm Γ A} → a [ id ][ [id]T ]t ≡ a
    _▹_      : (Γ : Con) → Ty Γ → Con
    _,[_]_   : ∀{Γ Δ}(γ : Sub Δ Γ) → ∀{A A'} → A [ γ ]T ≡ A' → Tm Δ A' → Sub Δ (Γ ▹ A)
    p        : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q        : ∀{Γ A A'} → (A [ p ]T) ≡ A' → Tm (Γ ▹ A) A'
    ▹β₁      : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{a : Tm Δ (A [ γ ]T)} → p ∘ (γ ,[ refl ] a) ≡ γ
    ▹β₂      : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{a : Tm Δ (A [ γ ]T)} → q refl [ γ ,[ refl ] a ][ [∘]T ⁻¹ ◾ cong (A [_]T) ▹β₁ ]t ≡ a
    ▹η       : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → (p ∘ γa ,[ refl ] q refl [ γa ][ [∘]T ⁻¹ ]t) ≡ γa
    Π        : ∀{Γ}(A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
    Π[]      : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{Δ}{γ : Sub Δ Γ} → (Π A B) [ γ ]T ≡ Π (A [ γ ]T) (B [ γ ∘ p ,[ [∘]T ] q refl ]T)

St : Model
St = record
  { Con = Set
  ; Sub = λ Δ Γ → Δ → Γ
  ; _∘_ = λ γ δ x → γ (δ x)
  ; ass = refl
  ; id = λ x → x
  ; idl = refl
  ; idr = refl
  ; Ty = λ Γ → Γ → Set
  ; _[_]T = λ A γ x → A (γ x)
  ; [∘]T = refl
  ; [id]T = refl
  ; Tm = λ Γ A → (x : Γ) → A x
  ; _[_][_]t = λ a γ e x → transp (λ z → z x) e (a (γ x))
  ; [∘]t = refl
  ; [id]t = refl
  ; _▹_ = λ Γ A → Σ Γ A
  ; _,[_]_ = λ γ e a x → γ x ,Σ transp (λ z → z x) (e ⁻¹) (a x)
  ; p = π₁
  ; q = λ {Γ}{A}{A'} e x → transp (λ z → z x) e (π₂ x)
  ; ▹β₁ = refl
  ; ▹β₂ = refl
  ; ▹η = refl
  ; Π = λ A B x → (y : A x) → B (x ,Σ y)
  ; Π[] = refl
  }

module C = Model St

record Con : Set₁ where
  field
    Γ    : C.Con → Set
    _[_] : ∀{I} → Γ I → ∀{J} → C.Sub J I → Γ J
    [∘]  : ∀{I}{γI : Γ I}{J}{f : C.Sub J I}{K}{g : C.Sub K J} → γI [ f C.∘ g ] ≡ γI [ f ] [ g ]
    [id] : ∀{I}{γI : Γ I} → γI [ C.id ] ≡ γI
  infix 8 _[_]
open Con renaming (Γ to ∣_∣; _[_] to _∶_[_])

record Ty (Γ : Con) : Set₁ where
  field
    A       : (I : C.Con) → ∣ Γ ∣ I → Set
    _[_][_] : ∀{I}{γI : ∣ Γ ∣ I} → A I γI → ∀{J}(f : C.Sub J I) → {γJ : ∣ Γ ∣ J} → Γ ∶ γI [ f ] ≡ γJ → A J γJ
    [∘]     : ∀{I}{γI : ∣ Γ ∣ I}{aI : A I γI}{J}{f : C.Sub J I}{K}{g : C.Sub K J} → aI [ f C.∘ g ][ [∘] Γ ] ≡ aI [ f ][ refl ] [ g ][ refl ]
    [id]    : ∀{I}{γI : ∣ Γ ∣ I}{aI : A I γI} → aI [ C.id ][ [id] Γ ] ≡ aI
  infix 8 _[_]
open Ty renaming (A to ∣_∣; _[_][_] to _∶_[_][_])

K : Con → ∀{Γ} → Ty Γ
K Δ = record { A = λ I _ → ∣ Δ ∣ I ; _[_][_] = λ δI f _ → Δ ∶ δI [ f ] ; [∘] = {!!} ; [id] = {!!} }

record Tm (Γ : Con)(A : Ty Γ) : Set₁ where
  field
    a   : ∀{I}(γI : ∣ Γ ∣ I) → ∣ A ∣ I γI
    nat : ∀{I}{γI : ∣ Γ ∣ I}{J}{f : C.Sub J I}{γJ : ∣ Γ ∣ J}{γf : Γ ∶ γI [ f ] ≡ γJ} → A ∶ (a γI) [ f ][ γf ] ≡ a γJ
open Tm renaming (a to ∣_∣)

Sub : Con → Con → Set₁
Sub Δ Γ = Tm Δ (K Γ)

_∘_ : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
∣ γ ∘ δ ∣ θI = ∣ γ ∣ (∣ δ ∣ θI)
nat (γ ∘ δ) {γf = δf} = nat γ {γf = nat δ {γf = δf}}
comp : ∀ Γ {Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
comp Γ γ δ = _∘_ {Γ} γ δ
syntax comp Γ γ δ = γ ∘[ Γ ] δ

_∶_∘_ : ∀ Γ {Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
Γ ∶ γ ∘ δ = _∘_ {Γ} γ δ

id : ∀{Γ} → Sub Γ Γ
∣ id ∣ γI = γI
nat id {γf = γf} = γf

ass : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{Ξ}{θ : Sub Ξ Θ} → (γ ∘[ Γ ] δ) ∘[ Γ ] θ ≡ γ ∘[ Γ ] (δ ∘[ Δ ] θ)
ass = refl

idl : ∀{Γ Δ}{γ : Sub Δ Γ} → id {Γ} ∘[ Γ ] γ ≡ γ
idl = refl

idr : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘[ Γ ] id ≡ γ
idr = refl

◇ : Con
∣_∣ ◇ I = ⊤
_∶_[_] ◇ _ _ = _
[∘] ◇ = refl
[id] ◇ = refl

ε : ∀{Γ} → Sub Γ ◇
∣ ε ∣ _ = _
nat ε = refl

◇η : ∀{Γ}(σ : Sub Γ ◇) → σ ≡ ε
◇η _ = refl

_[_]T : ∀{Γ} → Ty Γ → ∀{Δ} → Sub Δ Γ → Ty Δ
∣ A [ γ ]T ∣ I δI = ∣ A ∣ I (∣ γ ∣ δI)
(A [ γ ]T) ∶ aI [ f ][ γf ] = A ∶ aI [ f ][ nat γ {γf = γf} ]
[∘] (A [ γ ]T) {I}{δI}{aI}{J}{f}{K}{g} = {![∘] A {I}{∣ γ ∣ δI}{aI}{J}{f}{K}{g}!}
[id] (A [ γ ]T) {I}{δI}{aI} = [id] A {I}{∣ γ ∣ δI}{aI}
[∘]T : ∀{Γ}{A : Ty Γ}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → A [ γ ∘[ Γ ] δ ]T ≡ A [ γ ]T [ δ ]T
[∘]T = refl
[id]T : ∀{Γ}{A : Ty Γ} → A [ id {Γ} ]T ≡ A
[id]T = refl

_[_]t : ∀{Γ}{A : Ty Γ} → Tm Γ A → ∀{Δ}(γ : Sub Δ Γ) → Tm Δ (A [ γ ]T)
∣ a [ γ ]t ∣ δI = ∣ a ∣ (∣ γ ∣ δI)
nat (a [ γ ]t) = {!!}
[∘]t : ∀{Γ}{A : Ty Γ}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → a [ γ ∘[ Γ ] δ ]t ≡ a [ γ ]t [ δ ]t
[∘]t = refl
[id]t : ∀{Γ}{A : Ty Γ}{a : Tm Γ A} → a [ id ]t ≡ a
[id]t = refl
_▹_ : (Γ : Con)(A : Ty Γ) → Con
∣ Γ ▹ A ∣ I = Σ (∣ Γ ∣ I) (∣ A ∣ I)
(Γ ▹ A) ∶ (γI ,Σ aI) [ f ] = (Γ ∶ γI [ f ]) ,Σ (A ∶ aI [ f ][ refl ])
[id] (Γ ▹ A) = {!!}
[∘]  (Γ ▹ A) = {!!}
p : ∀{Γ}{A : Ty Γ} → Sub (Γ ▹ A) Γ
∣ p ∣ (γI ,Σ aI) = γI
nat p = {!!}
q : ∀{Γ}{A : Ty Γ} → Tm (Γ ▹ A) (A [ p {A = A} ]T)
∣ q ∣ (γI ,Σ aI) = aI
nat q = {!!}
_,_ : ∀{Γ Δ}(γ : Sub Δ Γ){A : Ty Γ} → Tm Δ (A [ γ ]T) → Sub Δ (Γ ▹ A)
∣ γ , a ∣ δI = ∣ γ ∣ δI ,Σ ∣ a ∣ δI
nat (γ , a) = {!!}
▹β₁ : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Γ}{a : Tm Δ (A [ γ ]T)}→ p {A = A} ∘[ Γ ] (_,_ γ {A = A} a) ≡ γ
▹β₁ = refl
▹β₂ : ∀{Γ Δ}{γ : Sub Δ Γ}{A : Ty Γ}{a : Tm Δ (A [ γ ]T)}→ q {A = A} [ _,_ γ {A = A} a ]t ≡ a
▹β₂ = refl
▹η : ∀{Γ Δ}{A : Ty Γ}{γa : Sub Δ (Γ ▹ A)} → _,_ (p {A = A} ∘[ Γ ] γa) {A = A} (q {A = A} [ γa ]t) ≡ γa
▹η = refl

CTy : Con
CTy = record { Γ = C.Ty ; _[_] = C._[_]T ; [∘] = C.[∘]T ; [id] = C.[id]T }
CTm : Ty CTy
CTm = record { A = C.Tm ; _[_][_] = C._[_][_]t ; [∘] = C.[∘]t ; [id] = C.[id]t }

Π⁺ : ∀{Γ} → (A : Sub Γ CTy) → Ty (Γ ▹ (CTm [ A ]T)) → Ty Γ
Π⁺ {Γ} Aᵒ B = record
  { A = λ I γI → ∣ B ∣ (I C.▹ ∣ Aᵒ ∣ γI) (Γ ∶ γI [ C.p ] ,Σ C.q (nat Aᵒ {γf = refl}))
  ; _[_][_] = λ {I}{γI} bI {J} f {γJ} e → B ∶ bI [ f C.∘ C.p C.,[ refl ] C.q {!!} ][ {!!} ]
  ; [∘] = {!!}
  ; [id] = {!!} }

lam⁺ : ∀{Γ}{Aᵒ : Sub Γ CTy}{B : Ty (Γ ▹ (CTm [ Aᵒ ]T))} → Tm (Γ ▹ (CTm [ Aᵒ ]T)) B → Tm Γ (Π⁺ Aᵒ B)
lam⁺ {Γ}{Aᵒ} t = record { a = λ γI → ∣ t ∣ (Γ ∶ γI [ C.p ] ,Σ C.q (nat Aᵒ {γf = refl})) ; nat = {!!} }

Π⁺[] : ∀{Γ}{A : Sub Γ CTy}{B : Ty (Γ ▹ (CTm [ A ]T))}{Δ}{γ : Sub Δ Γ} → Π⁺ A B [ γ ]T ≡ Π⁺ (A ∘[ CTy ] γ) (B [ (_,_ (γ ∘[ Γ ] p {A = CTm [ A ∘[ CTy ] γ ]T}) {CTm [ A ]T} (q {A = CTm [ A ∘[ CTy ] γ ]T})) ]T)
Π⁺[] = {!!}
