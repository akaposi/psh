{-# OPTIONS
  --without-K
  --prop
  --type-in-type
  --rewriting
  --postfix-projections
  --hidden-argument-puns
#-}

module Presheaf5 where

module L where
  private variable
    A A₀ A₁ B C : Set
    a a₀ a₁ a₂ a₃ b₀ b₁ : A

  record ⊤ : Set where
    constructor ⋆

  infixl 4 _,_
  record Σ (A : Set) (B : A → Set) : Set where
    constructor _,_
    field
      fst : A
      snd : B fst
  open Σ public

  infixl 4 _≡_
  data _≡_ (a : A) : A → Prop where
    refl : a ≡ a
  {-# BUILTIN REWRITE _≡_ #-}

  sym : a₀ ≡ a₁ → a₁ ≡ a₀
  sym refl = refl

  infixr 9 _∙_
  _∙_ : a₀ ≡ a₁ → a₁ ≡ a₂ → a₀ ≡ a₂
  refl ∙ a₁₂ = a₁₂

  cong : (f : A → B) → a₀ ≡ a₁ → f a₀ ≡ f a₁
  cong f refl = refl

  cong₂ : (f : A → B → C) → a₀ ≡ a₁ → b₀ ≡ b₁ → f a₀ b₀ ≡ f a₁ b₁
  cong₂ f refl refl = refl

  private postulate
    coe₀ : A₀ ≡ A₁ → A₀ → A₁
    coe₀-refl : coe₀ refl a ≡ a
    {-# REWRITE coe₀-refl #-}

  opaque
    coe : A₀ ≡ A₁ → A₀ → A₁
    coe = coe₀

    coe-refl : coe refl a ≡ a
    coe-refl = refl

  private variable A₀₁ A₁₀ A₁₂ A₂₁ A₃₂ : A₀ ≡ A₁

  infix 4 _≡[_]_
  _≡[_]_ : A₀ → A₀ ≡ A₁ → A₁ → Prop _
  a₀ ≡[ A₀₁ ] a₁ = coe A₀₁ a₀ ≡ a₁

  opaque
    unfolding coe

    reflᵈ : a₀ ≡[ refl ] a₀
    reflᵈ = refl

    symᵈ : a₀ ≡[ A₀₁ ] a₁ → a₁ ≡[ sym A₀₁ ] a₀
    symᵈ {A₀₁ = refl} refl = refl

    infixr 9 _∙ᵈ_
    _∙ᵈ_ : a₀ ≡[ A₀₁ ] a₁ → a₁ ≡[ A₁₂ ] a₂ → a₀ ≡[ A₀₁ ∙ A₁₂ ] a₂
    _∙ᵈ_ {A₀₁ = refl} refl a₁₂ = a₁₂

    dep : a₀ ≡ a₁ → a₀ ≡[ refl ] a₁
    dep a₀₁ = a₀₁

    undep : a₀ ≡[ refl ] a₁ → a₀ ≡ a₁
    undep a₀₁ = a₀₁

    splitl : a₀ ≡[ A₀₁ ∙ A₁₂ ] a₂ → coe A₀₁ a₀ ≡[ A₁₂ ] a₂
    splitl {A₀₁ = refl} a₀₂ = a₀₂

    splitr : a₀ ≡[ A₀₁ ∙ sym A₂₁ ] a₂ → a₀ ≡[ A₀₁ ] coe A₂₁ a₂
    splitr {A₀₁ = refl} {A₂₁ = refl} a₀₂ = a₀₂

    splitlr : a₀ ≡[ A₀₁ ∙ A₁₂ ∙ sym A₃₂ ] a₃ → coe A₀₁ a₀ ≡[ A₁₂ ] coe A₃₂ a₃
    splitlr {A₀₁ = refl} {A₁₂ = refl} {A₃₂ = refl} a₀₃ = a₀₃

    mergel : coe A₀₁ a₀ ≡[ A₁₂ ] a₂ → a₀ ≡[ A₀₁ ∙ A₁₂ ] a₂
    mergel {A₀₁ = refl} a₀₂ = a₀₂

    merger : a₀ ≡[ A₀₁ ] coe A₂₁ a₂ → a₀ ≡[ A₀₁ ∙ sym A₂₁ ] a₂
    merger {A₀₁ = refl} {A₂₁ = refl} a₀₂ = a₀₂

    pushr : a₀ ≡[ sym A₁₀ ] a₁ → a₀ ≡ coe A₁₀ a₁
    pushr {A₁₀ = refl} a₀₁ = a₀₁

    pullr : a₀ ≡ coe A₁₀ a₁ → a₀ ≡[ sym A₁₀ ] a₁
    pullr {A₁₀ = refl} a₀₁ = a₀₁

  infixl 0 Π-≡_ Πᵢ-≡_
  postulate
    Π-≡_ : {B : A → Set} {f₀ f₁ : ∀ a → B a} → (∀ a → f₀ a ≡ f₁ a) → f₀ ≡ f₁
    Πᵢ-≡_ :
      {B : A → Set} {f₀ f₁ : ∀ {a} → B a} →
      (∀ {a} → f₀ {a} ≡ f₁ {a}) → (λ {a} → f₀ {a}) ≡ (λ {a} → f₁ {a})

  opaque
    unfolding coe

    Σ-≡ :
      {B : A → Set} {t₀ t₁ : Σ A B} →
      (fst₀₁ : t₀ .fst ≡ t₁ .fst) → t₀ .snd ≡[ cong B fst₀₁ ] t₁ .snd → t₀ ≡ t₁
    Σ-≡ refl refl = refl

    private
      Σ-≡ᵈ₀ :
        {B₀ B₁ : A → Set} {t₀ : Σ A B₀} {t₁ : Σ A B₁} (B₀₁ : B₀ ≡ B₁) →
        (fst₀₁ : t₀ .fst ≡ t₁ .fst) →
        t₀ .snd ≡[ cong₂ (λ B fst → B fst) B₀₁ fst₀₁ ] t₁ .snd →
        t₀ ≡[ cong (Σ A) B₀₁ ] t₁
      Σ-≡ᵈ₀ refl refl refl = refl

  Σ-≡ᵈ :
    {B₀ B₁ : A → Set} {t₀ : Σ A B₀} {t₁ : Σ A B₁} (B₀₁ : ∀ a → B₀ a ≡ B₁ a) →
    (fst₀₁ : t₀ .fst ≡ t₁ .fst) →
    t₀ .snd ≡[ B₀₁ (t₀ .fst) ∙ cong B₁ fst₀₁ ] t₁ .snd →
    t₀ ≡[ cong (Σ A) (Π-≡ B₀₁) ] t₁
  Σ-≡ᵈ B₀₁ = Σ-≡ᵈ₀ (Π-≡ B₀₁)

open L hiding (_,_)

module C where
  postulate
    Ob : Set
    Hom : Ob → Ob → Set

  private variable
    I J K : Ob
    f g h : Hom J I

  infixl 9 _∘_
  postulate
    _∘_ : Hom J I → Hom K J → Hom K I
    assoc : f ∘ (g ∘ h) ≡ (f ∘ g) ∘ h

    id : Hom I I
    idr : f ∘ id ≡ f
    idl : id ∘ f ≡ f

module Psh where
  private variable
    I J : C.Ob
    f f₀ f₁ g : C.Hom J I

  record Con : Set where
    no-eta-equality

    infixl 9 _⟨_⟩
    field
      Γ : C.Ob → Set
      _⟨_⟩ : Γ I → C.Hom J I → Γ J
      ⟨⟩-∘ : ∀ {γᴵ} → γᴵ ⟨ f C.∘ g ⟩ ≡ γᴵ ⟨ f ⟩ ⟨ g ⟩
      ⟨⟩-id : {γᴵ : Γ I} → γᴵ ⟨ C.id ⟩ ≡ γᴵ

    ap-⟨⟩ₗ : ∀ {γᴵ₀ γᴵ₁} → γᴵ₀ ≡ γᴵ₁ → γᴵ₀ ⟨ f ⟩ ≡ γᴵ₁ ⟨ f ⟩
    ap-⟨⟩ₗ refl = refl

    ap-⟨⟩ᵣ : ∀ {γᴵ} → f₀ ≡ f₁ → γᴵ ⟨ f₀ ⟩ ≡ γᴵ ⟨ f₁ ⟩
    ap-⟨⟩ᵣ refl = refl

    ap-⟨⟩ : ∀ {γᴵ₀ γᴵ₁} → γᴵ₀ ≡ γᴵ₁ → f₀ ≡ f₁ → γᴵ₀ ⟨ f₀ ⟩ ≡ γᴵ₁ ⟨ f₁ ⟩
    ap-⟨⟩ refl refl = refl

  open Con
    renaming
      ( Γ to ∣_∣
      ; _⟨_⟩ to infix 9 _!_⟨_⟩
      ; ap-⟨⟩ₗ to ap-⟨⟩ᶜₗ
      ; ap-⟨⟩ᵣ to ap-⟨⟩ᶜᵣ
      ; ap-⟨⟩ to ap-⟨⟩ᶜ)
  private variable Γ Γ₀ Γ₁ Δ Δ₀ Δ₁ Θ Θ₀ Θ₁ Ξ : Con

  ap-∣∣ᶜ-Γ : Γ₀ ≡ Γ₁ → ∣ Γ₀ ∣ I ≡ ∣ Γ₁ ∣ I
  ap-∣∣ᶜ-Γ refl = refl

  record Sub (Δ Γ : Con) : Set where
    field
      γ : ∣ Δ ∣ I → ∣ Γ ∣ I
      ⟨⟩ : ∀ {δᴵ} → γ (Δ ! δᴵ ⟨ f ⟩) ≡ Γ ! γ δᴵ ⟨ f ⟩

    ap-γ : {δᴵ₀ δᴵ₁ : ∣ Δ ∣ I} → δᴵ₀ ≡ δᴵ₁ → γ δᴵ₀ ≡ γ δᴵ₁
    ap-γ refl = refl

  open Sub renaming (γ to ∣_∣; ap-γ to ap-∣∣ˢ)
  private variable γ γ₀ γ₁ δ₀ δ₁ : Sub Δ Γ

  ap-Subₗ : Δ₀ ≡ Δ₁ → Sub Δ₀ Γ ≡ Sub Δ₁ Γ
  ap-Subₗ refl = refl

  ap-Sub : Δ₀ ≡ Δ₁ → Γ₀ ≡ Γ₁ → Sub Δ₀ Γ₀ ≡ Sub Δ₁ Γ₁
  ap-Sub refl refl = refl

  private
    Sub-≡₀ :
      {γ₀ γ₁ : Sub Δ Γ} → (λ {I} → ∣ γ₀ ∣ {I}) ≡ (λ {I} → ∣ γ₁ ∣) → γ₀ ≡ γ₁
    Sub-≡₀ refl = refl

  Sub-≡ :
    {γ₀ γ₁ : Sub Δ Γ} → (∀ {I} (δᴵ : ∣ Δ ∣ I) → ∣ γ₀ ∣ δᴵ ≡ ∣ γ₁ ∣ δᴵ) → γ₀ ≡ γ₁
  Sub-≡ ∣∣₀₁ = Sub-≡₀ (Πᵢ-≡ Π-≡ λ δᴵ → ∣∣₀₁ δᴵ)

  infixl 9 _∘_
  _∘_ : Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
  ∣ γ ∘ δ ∣ θᴵ = ∣ γ ∣ (∣ δ ∣ θᴵ)
  (γ ∘ δ) .⟨⟩ = ap-∣∣ˢ γ (δ .⟨⟩) ∙ γ .⟨⟩

  assoc : (γ : Sub Δ Γ) (δ : Sub Θ Δ) (θ : Sub Ξ Θ) → γ ∘ (δ ∘ θ) ≡ (γ ∘ δ) ∘ θ
  assoc γ δ θ = refl

  id : Sub Γ Γ
  ∣ id ∣ γᴵ = γᴵ
  id .⟨⟩ = refl

  idr : γ ∘ id ≡ γ
  idr = refl

  idl : id ∘ γ ≡ γ
  idl = refl

  opaque
    unfolding coe

    ap-∘ : (γ : Sub Δ Γ) → δ₀ ≡ δ₁ → γ ∘ δ₀ ≡ γ ∘ δ₁
    ap-∘ γ refl = refl

    apᵈ-∘ :
      (γ : Sub Δ Γ) (Θ₀₁ : Θ₀ ≡ Θ₁) →
      δ₀ ≡[ ap-Subₗ Θ₀₁ ] δ₁ → γ ∘ δ₀ ≡[ ap-Subₗ Θ₀₁ ] γ ∘ δ₁
    apᵈ-∘ γ refl refl = refl

  private module Ty-util (Γ : Con) (A : (I : C.Ob) → ∣ Γ ∣ I → Set) where
    ap-A : ∀ {γᴵ₀ γᴵ₁} → γᴵ₀ ≡ γᴵ₁ → A I γᴵ₀ ≡ A I γᴵ₁
    ap-A refl = refl

  record Ty (Γ : Con) : Set where
    infixl 9 _⟨_⟩
    field
      A : ∀ I → ∣ Γ ∣ I → Set
      _⟨_⟩ : ∀ {γᴵ} → A I γᴵ → (f : C.Hom J I) → A J (Γ ! γᴵ ⟨ f ⟩)

    ap-A : ∀ {γᴵ₀ γᴵ₁} → γᴵ₀ ≡ γᴵ₁ → A I γᴵ₀ ≡ A I γᴵ₁
    ap-A = Ty-util.ap-A Γ A

    field
      ⟨⟩-∘ :
        ∀ {γᴵ} {aᴵ : A I γᴵ} → aᴵ ⟨ f C.∘ g ⟩ ≡[ ap-A (Γ .⟨⟩-∘) ] aᴵ ⟨ f ⟩ ⟨ g ⟩
      ⟨⟩-id : ∀ {γᴵ} {aᴵ : A I γᴵ} → aᴵ ⟨ C.id ⟩ ≡[ ap-A (Γ .⟨⟩-id) ] aᴵ

    opaque
      unfolding coe

      ap-⟨⟩ : ∀ {γᴵ} {aᴵ₀ aᴵ₁ : A I γᴵ} → aᴵ₀ ≡ aᴵ₁ → aᴵ₀ ⟨ f ⟩ ≡ aᴵ₁ ⟨ f ⟩
      ap-⟨⟩ refl = refl

      apᵈ-⟨⟩ₗ :
        ∀ {γᴵ₀ γᴵ₁ aᴵ₀ aᴵ₁} (γᴵ₀₁ : γᴵ₀ ≡ γᴵ₁) →
        aᴵ₀ ≡[ ap-A γᴵ₀₁ ] aᴵ₁ → aᴵ₀ ⟨ f ⟩ ≡[ ap-A (ap-⟨⟩ᶜₗ Γ γᴵ₀₁) ] aᴵ₁ ⟨ f ⟩
      apᵈ-⟨⟩ₗ refl refl = refl

      apᵈ-⟨⟩ᵣ :
        ∀ {γᴵ} {aᴵ : A I γᴵ} (f₀₁ : f₀ ≡ f₁) →
        aᴵ ⟨ f₀ ⟩ ≡[ ap-A (ap-⟨⟩ᶜᵣ Γ f₀₁) ] aᴵ ⟨ f₁ ⟩
      apᵈ-⟨⟩ᵣ refl = refl

      apᵈ-⟨⟩ :
        ∀ {γᴵ₀ γᴵ₁ aᴵ₀ aᴵ₁} (γᴵ₀₁ : γᴵ₀ ≡ γᴵ₁) → aᴵ₀ ≡[ ap-A γᴵ₀₁ ] aᴵ₁ →
        (f₀₁ : f₀ ≡ f₁) → aᴵ₀ ⟨ f₀ ⟩ ≡[ ap-A (ap-⟨⟩ᶜ Γ γᴵ₀₁ f₀₁) ] aᴵ₁ ⟨ f₁ ⟩
      apᵈ-⟨⟩ refl refl refl = refl

  open Ty
    renaming
      ( A to ∣_∣
      ; _⟨_⟩ to infix 9 _!_⟨_⟩
      ; ap-A to ap-∣∣ᵀ
      ; ap-⟨⟩ to ap-⟨⟩ᵀ
      ; apᵈ-⟨⟩ₗ to apᵈ-⟨⟩ᵀₗ
      ; apᵈ-⟨⟩ᵣ to apᵈ-⟨⟩ᵀᵣ
      ; apᵈ-⟨⟩ to apᵈ-⟨⟩ᵀ)
  private variable A A₀ A₁ B : Ty Γ

  opaque
    unfolding coe

    ap-Ty : Γ₀ ≡ Γ₁ → Ty Γ₀ ≡ Ty Γ₁
    ap-Ty refl = refl

    ap-∣∣ᵀ-A : ∀ {γᴵ} → A₀ ≡ A₁ → ∣ A₀ ∣ I γᴵ ≡ ∣ A₁ ∣ I γᴵ
    ap-∣∣ᵀ-A refl = refl

    ap-∣∣ᵀ-Γ :
      ∀ {γᴵ₀ γᴵ₁} (Γ₀₁ : Γ₀ ≡ Γ₁) → A₀ ≡[ ap-Ty Γ₀₁ ] A₁ →
      γᴵ₀ ≡[ ap-∣∣ᶜ-Γ Γ₀₁ ] γᴵ₁ → ∣ A₀ ∣ I γᴵ₀ ≡ ∣ A₁ ∣ I γᴵ₁
    ap-∣∣ᵀ-Γ refl refl refl = refl

    apᵈ-⟨⟩ᵀₗ-A :
      ∀ {A₀ A₁ : Ty Γ} {γᴵ} {aᴵ₀ : ∣ A₀ ∣ I γᴵ} {aᴵ₁ : ∣ A₁ ∣ I γᴵ}
      (A₀₁ : A₀ ≡ A₁) →
      aᴵ₀ ≡[ ap-∣∣ᵀ-A A₀₁ ] aᴵ₁ →
      A₀ ! aᴵ₀ ⟨ f ⟩ ≡[ ap-∣∣ᵀ-A A₀₁ ] A₁ ! aᴵ₁ ⟨ f ⟩
    apᵈ-⟨⟩ᵀₗ-A refl refl = refl

    private
      apᵀ-⟨⟩ᵀ :
        (A₀ A₁ : Ty Γ) →
        ∣ A₀ ∣ ≡ ∣ A₁ ∣ →
        (∀ {I J γᴵ} → ∣ A₀ ∣ I γᴵ → (f : C.Hom J I) → ∣ A₀ ∣ J (Γ ! γᴵ ⟨ f ⟩)) ≡
        (∀ {I J γᴵ} → ∣ A₁ ∣ I γᴵ → (f : C.Hom J I) → ∣ A₁ ∣ J (Γ ! γᴵ ⟨ f ⟩))
      apᵀ-⟨⟩ᵀ A₀ A₁ refl = refl

      Ty-≡₀ :
        {A₀ A₁ : Ty Γ}
        (∣∣₀₁ : ∣ A₀ ∣ ≡ ∣ A₁ ∣) →
        (λ {I J γᴵ} → A₀ ._!_⟨_⟩ {I} {J} {γᴵ}) ≡[ apᵀ-⟨⟩ᵀ A₀ A₁ ∣∣₀₁ ]
        (λ {I J γᴵ} → A₁ ._!_⟨_⟩ {I} {J} {γᴵ}) →
        A₀ ≡ A₁
      Ty-≡₀ refl refl = refl

      apᵖ-∣∣ᵀ :
        (A₀ A₁ : Ty Γ) →
        ∣ A₀ ∣ ≡ ∣ A₁ ∣ → (∀ I (γᴵ : ∣ Γ ∣ I) → ∣ A₀ ∣ I γᴵ ≡ ∣ A₁ ∣ I γᴵ)
      apᵖ-∣∣ᵀ A₀ A₁ refl I γᴵ = refl

      Π-≡-∣∣ᵀ :
        (A₀ A₁ : Ty Γ) →
        (∀ I (γᴵ : ∣ Γ ∣ I) → ∣ A₀ ∣ I γᴵ ≡ ∣ A₁ ∣ I γᴵ) → ∣ A₀ ∣ ≡ ∣ A₁ ∣
      Π-≡-∣∣ᵀ A₀ A₁ ∣∣₀₁ = Π-≡ λ I → Π-≡ λ γᴵ → ∣∣₀₁ I γᴵ

      Π-≡-⟨⟩ᵀ :
        (A₀ A₁ : Ty Γ)
        (∣∣₀₁ : ∣ A₀ ∣ ≡ ∣ A₁ ∣) →
        ( ∀ {I J γᴵ aᴵ₀ aᴵ₁}
          (aᴵ₀₁ : aᴵ₀ ≡[ apᵖ-∣∣ᵀ A₀ A₁ ∣∣₀₁ I γᴵ ] aᴵ₁) (f : C.Hom J I) →
          A₀ ! aᴵ₀ ⟨ f ⟩ ≡[ apᵖ-∣∣ᵀ A₀ A₁ ∣∣₀₁ J (Γ ! γᴵ ⟨ f ⟩) ]
          A₁ ! aᴵ₁ ⟨ f ⟩) →
        (λ {I J γᴵ} → A₀ ._!_⟨_⟩ {I} {J} {γᴵ}) ≡[ apᵀ-⟨⟩ᵀ A₀ A₁ ∣∣₀₁ ]
        (λ {I J γᴵ} → A₁ ._!_⟨_⟩ {I} {J} {γᴵ})
      Π-≡-⟨⟩ᵀ A₀ A₁ refl ⟨⟩₀₁ = Πᵢ-≡ Πᵢ-≡ Πᵢ-≡ Π-≡ λ aᴵ → Π-≡ λ f → ⟨⟩₀₁ refl f

    Ty-≡ :
      {A₀ A₁ : Ty Γ}
      (∣∣₀₁ : ∀ I (γᴵ : ∣ Γ ∣ I) → ∣ A₀ ∣ I γᴵ ≡ ∣ A₁ ∣ I γᴵ) →
      ( ∀ {I J γᴵ aᴵ₀ aᴵ₁} (aᴵ₀₁ : aᴵ₀ ≡[ ∣∣₀₁ I γᴵ ] aᴵ₁) (f : C.Hom J I) →
        A₀ ! aᴵ₀ ⟨ f ⟩ ≡[ ∣∣₀₁ J (Γ ! γᴵ ⟨ f ⟩) ] A₁ ! aᴵ₁ ⟨ f ⟩) →
      A₀ ≡ A₁
    Ty-≡ {A₀} {A₁} ∣∣₀₁ ⟨⟩₀₁ =
      Ty-≡₀
        (Π-≡-∣∣ᵀ A₀ A₁ ∣∣₀₁)
        (Π-≡-⟨⟩ᵀ A₀ A₁ (Π-≡-∣∣ᵀ A₀ A₁ ∣∣₀₁) ⟨⟩₀₁)

  infixl 9 _[_]ᵀ
  _[_]ᵀ : Ty Γ → Sub Δ Γ → Ty Δ
  ∣ A [ γ ]ᵀ ∣ I δᴵ = ∣ A ∣ I (∣ γ ∣ δᴵ)
  (A [ γ ]ᵀ) ! aᴵ ⟨ f ⟩ = coe (ap-∣∣ᵀ A (sym (γ .⟨⟩))) (A ! aᴵ ⟨ f ⟩)
  (A [ γ ]ᵀ) .⟨⟩-∘ = splitlr (A .⟨⟩-∘ ∙ᵈ apᵈ-⟨⟩ᵀₗ A (sym (γ .⟨⟩)) refl)
  (A [ γ ]ᵀ) .⟨⟩-id = splitl (A .⟨⟩-id)

  []ᵀ-∘ :
    (A : Ty Γ) (γ : Sub Δ Γ) (δ : Sub Θ Δ) → A [ γ ∘ δ ]ᵀ ≡ A [ γ ]ᵀ [ δ ]ᵀ
  []ᵀ-∘ A γ δ =
    Ty-≡
      (λ I θᴵ → refl)
      (λ aᴵ₀₁ f → splitlr (splitr (dep (ap-⟨⟩ᵀ A (undep aᴵ₀₁)))))

  []ᵀ-id : A [ id ]ᵀ ≡ A
  []ᵀ-id {A} =
    Ty-≡ (λ I γᴵ → refl) (λ aᴵ₀₁ f → splitl (dep (ap-⟨⟩ᵀ A (undep aᴵ₀₁))))

  opaque
    unfolding coe

    ap-[]ᵀ : A₀ ≡ A₁ → (γ : Sub Δ Γ) → A₀ [ γ ]ᵀ ≡ A₁ [ γ ]ᵀ
    ap-[]ᵀ refl γ = refl

    ap-[]ᵀᵣ : (A : Ty Γ) → γ₀ ≡ γ₁ → A [ γ₀ ]ᵀ ≡ A [ γ₁ ]ᵀ
    ap-[]ᵀᵣ A refl = refl

    apᵈ-[]ᵀᵣ :
      (A : Ty Γ) (Δ₀₁ : Δ₀ ≡ Δ₁) → γ₀ ≡[ ap-Subₗ Δ₀₁ ] γ₁ →
      A [ γ₀ ]ᵀ ≡[ ap-Ty Δ₀₁ ] A [ γ₁ ]ᵀ
    apᵈ-[]ᵀᵣ A refl refl = refl

    apᵈ-[]ᵀ-AΔ :
      A₀ ≡ A₁ → (Δ₀₁ : Δ₀ ≡ Δ₁) → γ₀ ≡[ ap-Subₗ Δ₀₁ ] γ₁ →
      A₀ [ γ₀ ]ᵀ ≡[ ap-Ty Δ₀₁ ] A₁ [ γ₁ ]ᵀ
    apᵈ-[]ᵀ-AΔ refl refl refl = refl

    apᵈ-[]ᵀ :
      (Γ₀₁ : Γ₀ ≡ Γ₁) → A₀ ≡[ ap-Ty Γ₀₁ ] A₁ →
      (Δ₀₁ : Δ₀ ≡ Δ₁) → γ₀ ≡[ ap-Sub Δ₀₁ Γ₀₁ ] γ₁ →
      A₀ [ γ₀ ]ᵀ ≡[ ap-Ty Δ₀₁ ] A₁ [ γ₁ ]ᵀ
    apᵈ-[]ᵀ refl refl refl refl = refl

  record Tm (Γ : Con) (A : Ty Γ) : Set where
    field
      a : (γᴵ : ∣ Γ ∣ I) → ∣ A ∣ I γᴵ
      ⟨⟩ : ∀ {γᴵ} → a (Γ ! γᴵ ⟨ f ⟩) ≡ A ! a γᴵ ⟨ f ⟩

    opaque
      unfolding coe

      apᵈ-a :
        ∀ {γᴵ₀ γᴵ₁ : ∣ Γ ∣ I}
        (γᴵ₀₁ : γᴵ₀ ≡ γᴵ₁) → a γᴵ₀ ≡[ ap-∣∣ᵀ A γᴵ₀₁ ] a γᴵ₁
      apᵈ-a refl = refl

  open Tm renaming (a to ∣_∣; apᵈ-a to apᵈ-∣∣ᵗ)
  private variable a a₀ a₁ : Tm Γ A

  opaque
    unfolding coe

    ap-Tm : A₀ ≡ A₁ → Tm Γ A₀ ≡ Tm Γ A₁
    ap-Tm refl = refl

    ap-Tm₂ : (Γ₀₁ : Γ₀ ≡ Γ₁) → A₀ ≡[ ap-Ty Γ₀₁ ] A₁ → Tm Γ₀ A₀ ≡ Tm Γ₁ A₁
    ap-Tm₂ refl refl = refl

    apᵈ-∣∣ᵗ-A :
      ∀ {a₀ : Tm Γ A₀} {a₁ : Tm Γ A₁} {γᴵ : ∣ Γ ∣ I} (A₀₁ : A₀ ≡ A₁) →
      a₀ ≡[ ap-Tm A₀₁ ] a₁ → ∣ a₀ ∣ γᴵ ≡[ ap-∣∣ᵀ-A A₀₁ ] ∣ a₁ ∣ γᴵ
    apᵈ-∣∣ᵗ-A refl refl = refl

    apᵈ-∣∣ᵗ-Γ :
      {γᴵ₀ : ∣ Γ₀ ∣ I} {γᴵ₁ : ∣ Γ₁ ∣ I}
      (Γ₀₁ : Γ₀ ≡ Γ₁) (A₀₁ : A₀ ≡[ ap-Ty Γ₀₁ ] A₁) → a₀ ≡[ ap-Tm₂ Γ₀₁ A₀₁ ] a₁ →
      (γᴵ₀₁ : γᴵ₀ ≡[ ap-∣∣ᶜ-Γ Γ₀₁ ] γᴵ₁) →
      ∣ a₀ ∣ γᴵ₀ ≡[ ap-∣∣ᵀ-Γ Γ₀₁ A₀₁ γᴵ₀₁ ] ∣ a₁ ∣ γᴵ₁
    apᵈ-∣∣ᵗ-Γ refl refl refl refl = refl

    private
      Tm-≡₀ :
        {a₀ a₁ : Tm Γ A} → (λ {I} → ∣ a₀ ∣ {I}) ≡ (λ {I} → ∣ a₁ ∣ {I}) → a₀ ≡ a₁
      Tm-≡₀ refl = refl

    Tm-≡ :
      {a₀ a₁ : Tm Γ A} →
      (∀ {I} (γᴵ : ∣ Γ ∣ I) → ∣ a₀ ∣ γᴵ ≡ ∣ a₁ ∣ γᴵ) → a₀ ≡ a₁
    Tm-≡ ∣∣₀₁ = Tm-≡₀ (Πᵢ-≡ Π-≡ λ γᴵ → ∣∣₀₁ γᴵ)

    Tm-≡ᵈ :
      {a₀ : Tm Γ A₀} {a₁ : Tm Γ A₁} (A₀₁ : A₀ ≡ A₁) →
      (∀ {I} (γᴵ : ∣ Γ ∣ I) → ∣ a₀ ∣ γᴵ ≡[ ap-∣∣ᵀ-A A₀₁ ] ∣ a₁ ∣ γᴵ) →
      a₀ ≡[ ap-Tm A₀₁ ] a₁
    Tm-≡ᵈ refl = Tm-≡

  infixl 9 _[_]ᵗ
  _[_]ᵗ : Tm Γ A → (γ : Sub Δ Γ) → Tm Δ (A [ γ ]ᵀ)
  ∣ a [ γ ]ᵗ ∣ δᴵ = ∣ a ∣ (∣ γ ∣ δᴵ)
  (a [ γ ]ᵗ) .⟨⟩ = pushr (apᵈ-∣∣ᵗ a (γ .⟨⟩) ∙ᵈ dep (a .⟨⟩))

  []ᵗ-∘ :
    (a : Tm Γ A) (γ : Sub Δ Γ) (δ : Sub Θ Δ) →
    a [ γ ∘ δ ]ᵗ ≡[ ap-Tm ([]ᵀ-∘ A γ δ) ] a [ γ ]ᵗ [ δ ]ᵗ
  []ᵗ-∘ {A} a γ δ = Tm-≡ᵈ ([]ᵀ-∘ A γ δ) λ θᴵ → reflᵈ

  []ᵗ-id : a [ id ]ᵗ ≡[ ap-Tm []ᵀ-id ] a
  []ᵗ-id = Tm-≡ᵈ []ᵀ-id λ γᴵ → reflᵈ

  opaque
    unfolding coe

    apᵈ-[]ᵗ :
      (A₀₁ : A₀ ≡ A₁) → a₀ ≡[ ap-Tm A₀₁ ] a₁ → (γ : Sub Δ Γ) →
      a₀ [ γ ]ᵗ ≡[ ap-Tm (ap-[]ᵀ A₀₁ γ) ] a₁ [ γ ]ᵗ
    apᵈ-[]ᵗ refl refl γ = refl

    apᵈ-[]ᵗᵣ :
      (a : Tm Γ A) (Δ₀₁ : Δ₀ ≡ Δ₁) (γ₀₁ : γ₀ ≡[ ap-Subₗ Δ₀₁ ] γ₁) →
      a [ γ₀ ]ᵗ ≡[ ap-Tm₂ Δ₀₁ (apᵈ-[]ᵀᵣ A Δ₀₁ γ₀₁) ] a [ γ₁ ]ᵗ
    apᵈ-[]ᵗᵣ a refl refl = refl

    apᵈ-[]ᵗ-ΓΔ :
      (Γ₀₁ : Γ₀ ≡ Γ₁) (A₀₁ : A₀ ≡[ ap-Ty Γ₀₁ ] A₁) → a₀ ≡[ ap-Tm₂ Γ₀₁ A₀₁ ] a₁ →
      (Δ₀₁ : Δ₀ ≡ Δ₁) (γ₀₁ : γ₀ ≡[ ap-Sub Δ₀₁ Γ₀₁ ] γ₁) →
      a₀ [ γ₀ ]ᵗ ≡[ ap-Tm₂ Δ₀₁ (apᵈ-[]ᵀ Γ₀₁ A₀₁ Δ₀₁ γ₀₁) ] a₁ [ γ₁ ]ᵗ
    apᵈ-[]ᵗ-ΓΔ refl refl refl refl refl = refl

  ◇ : Con
  ∣ ◇ ∣ I = ⊤
  ◇ ! ⋆ ⟨ f ⟩ = ⋆
  ◇ .⟨⟩-∘ = refl
  ◇ .⟨⟩-id = refl

  ε : Sub Γ ◇
  ∣ ε ∣ γᴵ = ⋆
  ε .⟨⟩ = refl

  ε-∘ : (γ : Sub Δ Γ) → ε ∘ γ ≡ ε
  ε-∘ γ = refl

  ◇-η : id ≡ ε
  ◇-η = refl

  infixl 2 _▹_
  _▹_ : (Γ : Con) → Ty Γ → Con
  ∣ Γ ▹ A ∣ I = Σ (∣ Γ ∣ I) (∣ A ∣ I)
  (Γ ▹ A) ! (γᴵ L., aᴵ) ⟨ f ⟩ = Γ ! γᴵ ⟨ f ⟩ L., A ! aᴵ ⟨ f ⟩
  (Γ ▹ A) .⟨⟩-∘ = Σ-≡ (Γ .⟨⟩-∘) (A .⟨⟩-∘)
  (Γ ▹ A) .⟨⟩-id = Σ-≡ (Γ .⟨⟩-id) (A .⟨⟩-id)

  p : Sub (Γ ▹ A) Γ
  ∣ p ∣ = fst
  p .⟨⟩ = refl

  q : Tm (Γ ▹ A) (A [ p ]ᵀ)
  ∣ q ∣ = snd
  q .⟨⟩ = pushr reflᵈ

  infixl 4 _,_
  _,_ : (γ : Sub Δ Γ) → Tm Δ (A [ γ ]ᵀ) → Sub Δ (Γ ▹ A)
  ∣ γ , a ∣ δᴵ = ∣ γ ∣ δᴵ L., ∣ a ∣ δᴵ
  (γ , a) .⟨⟩ = Σ-≡ (γ .⟨⟩) (pullr (a .⟨⟩))

  infixl 4 _,⟨_⟩_
  _,⟨_⟩_ : (γ : Sub Δ Γ) (A : Ty Γ) → Tm Δ (A [ γ ]ᵀ) → Sub Δ (Γ ▹ A)
  γ ,⟨ A ⟩ a = γ , a

  ▹-β₁ : (γ : Sub Δ Γ) (A : Ty Γ) (a : Tm Δ (A [ γ ]ᵀ)) → p ∘ (γ ,⟨ A ⟩ a) ≡ γ
  ▹-β₁ γ A a = refl

  ▹-β₂ :
    (γ : Sub Δ Γ) (A : Ty Γ) (a : Tm Δ (A [ γ ]ᵀ)) →
    q [ γ ,⟨ A ⟩ a ]ᵗ ≡[ ap-Tm (sym ([]ᵀ-∘ A p (γ ,⟨ A ⟩ a))) ] a
  ▹-β₂ γ A a = Tm-≡ᵈ (sym ([]ᵀ-∘ A p (γ ,⟨ A ⟩ a))) λ γᴵ → reflᵈ

  ,-∘ :
    (γ : Sub Δ Γ) (a : Tm Δ (A [ γ ]ᵀ)) (δ : Sub Θ Δ) →
    (γ ,⟨ A ⟩ a) ∘ δ ≡ (γ ∘ δ , coe (ap-Tm (sym ([]ᵀ-∘ A γ δ))) (a [ δ ]ᵗ))
  ,-∘ {A} γ a δ = Sub-≡ λ θᴵ → Σ-≡ refl (apᵈ-∣∣ᵗ-A (sym ([]ᵀ-∘ A γ δ)) refl)

  ▹-η : id {Γ ▹ A} ≡ (p , q)
  ▹-η = refl

  opaque
    unfolding coe

    ap-▹ : A₀ ≡ A₁ → (Γ ▹ A₀) ≡ (Γ ▹ A₁)
    ap-▹ refl = refl

    apᵈ-p : (A₀₁ : A₀ ≡ A₁) → p ≡[ ap-Subₗ (ap-▹ A₀₁) ] p
    apᵈ-p refl = refl

    apᵈ-q :
      (A₀₁ : A₀ ≡ A₁) →
      q ≡[ ap-Tm₂ (ap-▹ A₀₁) (apᵈ-[]ᵀ-AΔ {γ₀ = p} A₀₁ (ap-▹ A₀₁) (apᵈ-p A₀₁)) ]
      q
    apᵈ-q refl = refl

    apᵈ-, :
      {A : Ty Γ} {a₀ : Tm Δ₀ (A [ γ₀ ]ᵀ)} {a₁ : Tm Δ₁ (A [ γ₁ ]ᵀ)}
      (Δ₀₁ : Δ₀ ≡ Δ₁) (γ₀₁ : γ₀ ≡[ ap-Subₗ Δ₀₁ ] γ₁) →
      a₀ ≡[ ap-Tm₂ Δ₀₁ (apᵈ-[]ᵀᵣ A Δ₀₁ γ₀₁) ] a₁ →
      (γ₀ ,⟨ A ⟩ a₀) ≡[ ap-Subₗ Δ₀₁ ] (γ₁ ,⟨ A ⟩ a₁)
    apᵈ-, refl refl refl = refl

  infixl 10 _⁺
  _⁺ : (γ : Sub Δ Γ) → Sub (Δ ▹ A [ γ ]ᵀ) (Γ ▹ A)
  _⁺ {A} γ = γ ∘ p , coe (ap-Tm (sym ([]ᵀ-∘ A γ p))) q

  infixl 10 _⁺⟨_⟩
  _⁺⟨_⟩ : (γ : Sub Δ Γ) (A : Ty Γ) → Sub (Δ ▹ A [ γ ]ᵀ) (Γ ▹ A)
  γ ⁺⟨ A ⟩ = γ ⁺

  ⁺-∘ :
    (γ : Sub Δ Γ) (δ : Sub Θ Δ) →
    (γ ∘ δ) ⁺⟨ A ⟩ ≡[ ap-Subₗ (ap-▹ ([]ᵀ-∘ A γ δ)) ] γ ⁺ ∘ δ ⁺
  ⁺-∘ {A} γ δ =
    apᵈ-,
      (ap-▹ ([]ᵀ-∘ A γ δ))
      ( apᵈ-∘ (γ ∘ δ) (ap-▹ ([]ᵀ-∘ A γ δ)) (apᵈ-p ([]ᵀ-∘ A γ δ)) ∙ᵈ
        dep
          ( sym (assoc γ δ p) ∙
            ap-∘ γ
              (sym
                (▹-β₁ (δ ∘ p) (A [ γ ]ᵀ)
                  (coe (ap-Tm (sym ([]ᵀ-∘ (A [ γ ]ᵀ) δ p))) q))) ∙
        assoc γ p (δ ⁺⟨ A [ γ ]ᵀ ⟩)))
      (splitlr
        ( apᵈ-q ([]ᵀ-∘ A γ δ) ∙ᵈ
          mergel (symᵈ (▹-β₂ (δ ∘ p) (A [ γ ]ᵀ) (coe _ q))) ∙ᵈ
          apᵈ-[]ᵗ (sym ([]ᵀ-∘ A γ p)) refl (δ ⁺⟨ A [ γ ]ᵀ ⟩))) ∙ᵈ
    dep (sym (,-∘ (γ ∘ p) (coe _ q) (δ ∘ p , coe _ q)))

  ⁺-id : {A : Ty Γ} → id ⁺⟨ A ⟩ ≡[ ap-Subₗ (ap-▹ []ᵀ-id) ] id
  ⁺-id = apᵈ-, (ap-▹ []ᵀ-id) (dep idl ∙ᵈ apᵈ-p []ᵀ-id) (splitl (apᵈ-q []ᵀ-id))

  opaque
    unfolding coe

    apᵈ-⁺ :
      (γ₀₁ : γ₀ ≡ γ₁) → γ₀ ⁺⟨ A ⟩ ≡[ ap-Subₗ (ap-▹ (ap-[]ᵀᵣ A γ₀₁)) ] γ₁ ⁺⟨ A ⟩
    apᵈ-⁺ refl = refl

    apᵈ-⁺ᵣ :
      {A₀ A₁ : Ty Γ} (γ : Sub Δ Γ) (A₀₁ : A₀ ≡ A₁) →
      γ ⁺⟨ A₀ ⟩ ≡[ ap-Sub (ap-▹ (ap-[]ᵀ A₀₁ γ)) (ap-▹ A₀₁) ] γ ⁺⟨ A₁ ⟩
    apᵈ-⁺ᵣ γ refl = refl

  yᵒ : C.Ob → Con
  ∣ yᵒ I ∣ J = C.Hom J I
  yᵒ I ! f ⟨ g ⟩ = f C.∘ g
  yᵒ I .⟨⟩-∘ = C.assoc
  yᵒ I .⟨⟩-id = C.idr

  yʰ : C.Hom J I → Sub (yᵒ J) (yᵒ I)
  ∣ yʰ f ∣ g = f C.∘ g
  yʰ f .⟨⟩ = C.assoc

  yʰ-∘ : yʰ (f C.∘ g) ≡ yʰ f ∘ yʰ g
  yʰ-∘ = Sub-≡ λ h → sym C.assoc

  yʰ-id : yʰ (C.id {I}) ≡ id
  yʰ-id = Sub-≡ λ f → C.idl

  yl : ∣ Γ ∣ I → Sub (yᵒ I) Γ
  ∣ yl {Γ} γᴵ ∣ f = Γ ! γᴵ ⟨ f ⟩
  yl {Γ} γᴵ .⟨⟩ = Γ .⟨⟩-∘

  yl-⟨⟩ : ∀ {γᴵ} → yl {Γ} (Γ ! γᴵ ⟨ f ⟩) ≡ yl γᴵ ∘ yʰ f
  yl-⟨⟩ {Γ} = Sub-≡ λ g → sym (Γ .⟨⟩-∘)

  U : Ty Γ
  ∣ U ∣ I γᴵ = Ty (yᵒ I)
  U ! Aᴵ ⟨ f ⟩ = Aᴵ [ yʰ f ]ᵀ
  U .⟨⟩-∘ {f} {g} {aᴵ = Aᴵ} = dep (ap-[]ᵀᵣ Aᴵ yʰ-∘ ∙ []ᵀ-∘ Aᴵ (yʰ f) (yʰ g))
  U .⟨⟩-id {aᴵ = Aᴵ} = dep (ap-[]ᵀᵣ Aᴵ yʰ-id ∙ []ᵀ-id)

  El : Tm Γ U → Ty Γ
  ∣ El A ∣ I γᴵ = ∣ ∣ A ∣ γᴵ ∣ I C.id
  El A ._!_⟨_⟩ {γᴵ} aᴵ f =
    coe
      (ap-∣∣ᵀ (∣ A ∣ γᴵ) (C.idl ∙ sym C.idr) ∙ ap-∣∣ᵀ-A (sym (A .⟨⟩)))
      (∣ A ∣ γᴵ ! aᴵ ⟨ f ⟩)
  El A .⟨⟩-∘ {γᴵ} =
    splitlr
      ( ∣ A ∣ γᴵ .⟨⟩-∘ ∙ᵈ
        apᵈ-⟨⟩ᵀₗ (∣ A ∣ γᴵ) (C.idl ∙ sym C.idr) refl ∙ᵈ
        mergel (apᵈ-⟨⟩ᵀₗ-A (sym (A .⟨⟩)) (splitl refl)))
  El A .⟨⟩-id {γᴵ} = splitl (∣ A ∣ γᴵ .⟨⟩-id)

  c : Ty Γ → Tm Γ U
  ∣ c A ∣ γᴵ = A [ yl γᴵ ]ᵀ
  c A .⟨⟩ {f} {γᴵ} = ap-[]ᵀᵣ A yl-⟨⟩ ∙ []ᵀ-∘ A (yl γᴵ) (yʰ f)

  El-yl : {γᴵ : ∣ Γ ∣ I} (A : Tm Γ U) → El A [ yl γᴵ ]ᵀ ≡ ∣ A ∣ γᴵ
  El-yl {γᴵ} A =
    Ty-≡
      (λ I f → ap-∣∣ᵀ-A (A .⟨⟩) ∙ ap-∣∣ᵀ (∣ A ∣ γᴵ) C.idr)
      (λ aᴵ₀₁ g →
        splitl
          (splitl
            ( merger (apᵈ-⟨⟩ᵀₗ-A (A .⟨⟩) refl) ∙ᵈ
              apᵈ-⟨⟩ᵀₗ (∣ A ∣ γᴵ) C.idr (splitl aᴵ₀₁))))

  U-β : {A : Ty Γ} → El (c A) ≡ A
  U-β {Γ} {A} =
    Ty-≡
      (λ I γᴵ → ap-∣∣ᵀ A (Γ .⟨⟩-id))
      (λ aᴵ₀₁ f → splitl (splitl (apᵈ-⟨⟩ᵀₗ A (Γ .⟨⟩-id) aᴵ₀₁)))

  U-η : {A : Tm Γ U} → A ≡ c (El A)
  U-η {A} = Tm-≡ λ γᴵ → sym (El-yl A)

  Π : (A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
  ∣ Π A B ∣ I γᴵ = Tm (yᵒ I ▹ A [ yl γᴵ ]ᵀ) (B [ yl γᴵ ⁺ ]ᵀ)
  Π A B ._!_⟨_⟩ {γᴵ} bᴵ f =
    coe
      (ap-Tm₂
        (ap-▹ (sym ([]ᵀ-∘ A (yl γᴵ) (yʰ f)) ∙ ap-[]ᵀᵣ A (sym yl-⟨⟩)))
        ( dep (sym ([]ᵀ-∘ B (yl γᴵ ⁺) (yʰ f ⁺))) ∙ᵈ
          apᵈ-[]ᵀᵣ B
            (ap-▹ (sym ([]ᵀ-∘ A (yl γᴵ) (yʰ f)) ∙ ap-[]ᵀᵣ A (sym yl-⟨⟩)))
            (symᵈ (⁺-∘ (yl γᴵ) (yʰ f)) ∙ᵈ apᵈ-⁺ (sym yl-⟨⟩))))
      (bᴵ [ yʰ f ⁺ ]ᵗ)
  Π A B .⟨⟩-∘ {f} {g} {γᴵ} {aᴵ} =
    splitlr
      ( apᵈ-[]ᵗᵣ aᴵ
          (ap-▹
            (ap-[]ᵀᵣ (A [ yl γᴵ ]ᵀ) yʰ-∘ ∙ []ᵀ-∘ (A [ yl γᴵ ]ᵀ) (yʰ f) (yʰ g)))
          (apᵈ-⁺ yʰ-∘ ∙ᵈ ⁺-∘ (yʰ f) (yʰ g)) ∙ᵈ
        []ᵗ-∘ aᴵ (yʰ f ⁺) (yʰ g ⁺) ∙ᵈ
        apᵈ-[]ᵗ-ΓΔ
          {γ₀ = yʰ g ⁺} {γ₁ = yʰ g ⁺}
          (ap-▹ (sym ([]ᵀ-∘ A (yl γᴵ) (yʰ f)) ∙ ap-[]ᵀᵣ A (sym yl-⟨⟩)))
          ( dep (sym ([]ᵀ-∘ B (yl γᴵ ⁺) (yʰ f ⁺))) ∙ᵈ
            apᵈ-[]ᵀᵣ B
              (ap-▹ (sym ([]ᵀ-∘ A (yl γᴵ) (yʰ f)) ∙ ap-[]ᵀᵣ A (sym yl-⟨⟩)))
              (symᵈ (⁺-∘ (yl γᴵ) (yʰ f)) ∙ᵈ apᵈ-⁺ (sym yl-⟨⟩)))
          refl
          (ap-▹
            (ap-[]ᵀ
              (sym ([]ᵀ-∘ A (yl γᴵ) (yʰ f)) ∙ ap-[]ᵀᵣ A (sym yl-⟨⟩))
              (yʰ g)))
          (apᵈ-⁺ᵣ (yʰ g)
            (sym ([]ᵀ-∘ A (yl γᴵ) (yʰ f)) ∙ ap-[]ᵀᵣ A (sym yl-⟨⟩))))
  Π A B .⟨⟩-id {γᴵ} {aᴵ} =
    splitl
      ( apᵈ-[]ᵗᵣ aᴵ
          (ap-▹ (ap-[]ᵀᵣ (A [ yl γᴵ ]ᵀ) yʰ-id ∙ []ᵀ-id))
          (apᵈ-⁺ yʰ-id ∙ᵈ ⁺-id) ∙ᵈ
        []ᵗ-id)

  app : Tm Γ (Π A B) → Tm (Γ ▹ A) B
  ∣ app {Γ} {A} {B} t ∣ (γᴵ L., aᴵ) =
    coe
      (ap-∣∣ᵀ B
        (Σ-≡ (Γ .⟨⟩-id)
          (merger (apᵈ-∣∣ᵗ-A ([]ᵀ-∘ A (yl γᴵ) p) (splitl reflᵈ)))))
      (∣ ∣ t ∣ γᴵ ∣ (C.id L., coe (ap-∣∣ᵀ A (sym (Γ .⟨⟩-id))) aᴵ))
  app {Γ} {A} {B} t .⟨⟩ {f} {γᴵ = γᴵ L., aᴵ} =
    apᵈ-∣∣ᵗ-Γ
      (ap-▹ (ap-[]ᵀᵣ A yl-⟨⟩ ∙ []ᵀ-∘ A (yl γᴵ) (yʰ f)))
      ( apᵈ-[]ᵀᵣ B
          (ap-▹ (ap-[]ᵀᵣ A yl-⟨⟩ ∙ []ᵀ-∘ A (yl γᴵ) (yʰ f)))
          (apᵈ-⁺ yl-⟨⟩ ∙ᵈ ⁺-∘ (yl γᴵ) (yʰ f)) ∙ᵈ
        dep ([]ᵀ-∘ B (yl γᴵ ⁺) (yʰ f ⁺)))
      (pullr (t .⟨⟩))
      (Σ-≡ᵈ
        (λ g → ap-∣∣ᵀ A (sym (Γ .⟨⟩-∘)))
        refl
        (splitl (apᵈ-⟨⟩ᵀᵣ A (sym C.idr)))) ∙ᵈ
    apᵈ-∣∣ᵗ (∣ t ∣ γᴵ)
      (Σ-≡ (C.idr ∙ sym C.idl)
        (splitr
          ( apᵈ-∣∣ᵗ-A ([]ᵀ-∘ (A [ yl γᴵ ]ᵀ) (yʰ f) p) (splitl reflᵈ) ∙ᵈ
            apᵈ-⟨⟩ᵀ A (sym (Γ .⟨⟩-id)) refl C.idr))) ∙ᵈ
    pullr (∣ t ∣ γᴵ .⟨⟩) ∙ᵈ
    apᵈ-⟨⟩ᵀₗ B
      (Σ-≡ (Γ .⟨⟩-id)
        (merger (apᵈ-∣∣ᵗ-A ([]ᵀ-∘ A (yl γᴵ) p) (splitl reflᵈ))))
      refl

  lam : Tm (Γ ▹ A) B → Tm Γ (Π A B)
  ∣ lam b ∣ γᴵ = b [ yl γᴵ ⁺ ]ᵗ
  lam {A} b .⟨⟩ {f} {γᴵ} =
    pushr
      ( apᵈ-[]ᵗᵣ b
          (ap-▹ (ap-[]ᵀᵣ A yl-⟨⟩ ∙ []ᵀ-∘ A (yl γᴵ) (yʰ f)))
          (apᵈ-⁺ yl-⟨⟩ ∙ᵈ ⁺-∘ (yl γᴵ) (yʰ f)) ∙ᵈ
        []ᵗ-∘ b (yl γᴵ ⁺) (yʰ f ⁺))

  app-yl :
    {γᴵ : ∣ Γ ∣ I} (B : Ty (Γ ▹ A)) (t : Tm Γ (Π A B)) →
    app {B = B} t [ yl γᴵ ⁺ ]ᵗ ≡ ∣ t ∣ γᴵ
  app-yl {Γ} {A} {γᴵ} B t = Tm-≡ λ (f L., aᴵ) →
    apᵈ-∣∣ᵗ-Γ
      (ap-▹ (ap-[]ᵀᵣ A yl-⟨⟩ ∙ []ᵀ-∘ A (yl γᴵ) (yʰ f)))
      ( apᵈ-[]ᵀᵣ B
          (ap-▹ (ap-[]ᵀᵣ A yl-⟨⟩ ∙ []ᵀ-∘ A (yl γᴵ) (yʰ f)))
          (apᵈ-⁺ yl-⟨⟩ ∙ᵈ ⁺-∘ (yl γᴵ) (yʰ f)) ∙ᵈ
        dep ([]ᵀ-∘ B (yl γᴵ ⁺) (yʰ f ⁺)))
      (pullr (t .⟨⟩))
      (Σ-≡ᵈ
        (λ g → ap-∣∣ᵀ A (sym (Γ .⟨⟩-∘)))
        refl
        (splitlr (apᵈ-∣∣ᵗ-A ([]ᵀ-∘ A (yl γᴵ) p) (splitl reflᵈ)))) ∙ᵈ
    apᵈ-∣∣ᵗ (∣ t ∣ γᴵ)
      (Σ-≡ C.idr
        (merger
          {A₂₁ = ap-∣∣ᵀ A (ap-⟨⟩ᶜᵣ Γ (sym C.idr))}
          (apᵈ-∣∣ᵗ-A ([]ᵀ-∘ (A [ yl γᴵ ]ᵀ) (yʰ f) p) (splitl reflᵈ))))

  Π-β : {b : Tm (Γ ▹ A) B} → app (lam b) ≡ b
  Π-β {Γ} {A} {b} = Tm-≡ λ (γᴵ L., aᴵ) →
    apᵈ-∣∣ᵗ b
      (Σ-≡ (Γ .⟨⟩-id) (merger (apᵈ-∣∣ᵗ-A ([]ᵀ-∘ A (yl γᴵ) p) (splitl reflᵈ))))

  Π-η : {t : Tm Γ (Π A B)} → t ≡ lam {B = B} (app t)
  Π-η {B} {t} = Tm-≡ λ γᴵ → sym (app-yl B t)
