{-# OPTIONS --prop #-}

-- list of tricks:
-- * pointfree
-- * parametricity definition of naturality and type restriction
-- * Yoneda
-- * equation in SProp
-- * local universe

open import Agda.Primitive
open import Cat

postulate
  C' : Cat
C : Cat
C = strictify C'
open Cat.Cat C

infix 4 _≡_
infixl 5 _,Σ_
data _≡_ {i}{A : Set i}(a : A) : A → Prop i where
  refl : a ≡ a
record ⊤ : Set where
record ⊤p : Prop where
record Σ {i}(A : Set i){j}(B : A → Set j) : Set (i ⊔ j) where
  constructor _,Σ_
  field
    π₁ : A
    π₂ : B π₁
open Σ
-- ,= : ∀{i}{A : Set i}{j}{B : A → Set j}{a₀ a₁ : A}(a₌ : a₀ ≡ a₁){b₀ : B a₀}{b₁ : B a₁} → b₀ ≡ b₁
record Lift {i}(A : Prop i) : Set i where
  constructor mk
  field un : A
open Lift
cong : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong f refl = refl
_◾_ : ∀{i}{A : Set i}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
refl ◾ refl = refl
_⁻¹ : ∀{i}{A : Set i}{a a' : A} → a ≡ a' → a' ≡ a
refl ⁻¹ = refl

infixl 7 _∘_

record Con : Set₁ where
  field
    Γ    : Ob → Set
    _[_] : ∀{I} → Γ I → ∀{J} → Hom J I → Γ J
    [∘]  : ∀{I}(γI : Γ I){J}(f : Hom J I){K}(g : Hom K J) → γI [ f ∘c g ] ≡ γI [ f ] [ g ] -- TODO: change this to pointfree, then we try2fix should be fine
    [id] : ∀{I}(γI : Γ I) → γI [ idc ] ≡ γI
  infix 8 _[_]
open Con renaming (Γ to ∣_∣; _[_] to _∶_[_])

◆ : Con
∣_∣ ◆ I = ⊤
_∶_[_] ◆ _ _ = _
[∘] ◆ _ _ _ = refl
[id] ◆ _ = refl

yOb : Ob → Con
∣ yOb I ∣ J = Hom J I
yOb I ∶ f [ g ] = f ∘c g
[∘] (yOb I) f g h = refl
[id] (yOb I) f = refl

record Sub (Δ Γ : Con) : Set where
  field
    γ   : ∀{I} → ∣ Δ ∣ I → ∣ Γ ∣ I
    nat : ∀{I}{δI : ∣ Δ ∣ I}{J}(f : Hom J I) → Γ ∶ γ δI [ f ] ≡ γ (Δ ∶ δI [ f ])
open Sub renaming (γ to ∣_∣) public

_∘_ : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
∣ γ ∘ δ ∣ θI = ∣ γ ∣ (∣ δ ∣ θI)
nat (γ ∘ δ) f = {!!}

id : ∀{Γ} → Sub Γ Γ
∣ id ∣ γI = γI
nat id f = refl

yHom : ∀{J I} → Hom J I → Sub (yOb J) (yOb I)
∣ yHom f ∣ g = f ∘c g
nat (yHom f) g = refl

yl : ∀{Γ I} → ∣ Γ ∣ I → Sub (yOb I) Γ
∣ yl {Γ} γI ∣ f = Γ ∶ γI [ f ]
nat (yl {Γ} γI) {_}{f} g = [∘] Γ γI f g ⁻¹

⟨_⟩ : Con → Con
∣ ⟨ Γ ⟩ ∣ I = Sub (yOb I) Γ
⟨ Γ ⟩ ∶ γI [ f ]  = γI ∘ yHom f
[∘] ⟨ Γ ⟩ _ _ _ = refl
[id] ⟨ Γ ⟩ _ = refl

mk⟨_⟩ : (Γ : Con) → Sub Γ ⟨ Γ ⟩
∣ mk⟨ Γ ⟩ ∣ γI = yl γI
nat mk⟨ Γ ⟩ f = {!!}

un⟨_⟩ : (Γ : Con) → Sub ⟨ Γ ⟩ Γ
∣ un⟨ Γ ⟩ ∣ γI = ∣ γI ∣ idc
nat un⟨ Γ ⟩ {_}{γI} f = nat γI f

⟨_⟩β : (Γ : Con) → un⟨ Γ ⟩ ∘ mk⟨ Γ ⟩ ≡ id
⟨ Γ ⟩β = {!!}

⟨_⟩η : (Γ : Con) → mk⟨ Γ ⟩ ∘ un⟨ Γ ⟩ ≡ id
⟨ Γ ⟩η = {!!}

Sub' : Con → Con → Set
Sub' Δ Γ = Sub ⟨ Δ ⟩ ⟨ Γ ⟩

ass : ∀{Γ Δ}{γ : Sub' Δ Γ}{Θ}{δ : Sub' Θ Δ}{Ξ}{θ : Sub' Ξ Θ} → (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
ass = refl

idl : ∀{Γ Δ}{γ : Sub Δ Γ} → id ∘ γ ≡ γ
idl = refl

idr : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘ id ≡ γ
idr = refl

yid : ∀{I} → yHom (idc {I}) ≡ id
yid = refl

yHom' : ∀{J I} → Hom J I → Sub' (yOb J) (yOb I)
∣ yHom' f ∣ g = yHom f ∘ g
nat (yHom' f) g = refl

yl' : ∀{Γ I} → ∣ Γ ∣ I → Sub' (yOb I) Γ
∣ yl' {Γ} γI ∣ f = yl γI ∘ f
nat (yl' {Γ} γI) {_}{f} g = refl

record Ty (Γ : Con) : Set₁ where
  field
    A     : (I : Ob) → ∣ ⟨ Γ ⟩ ∣ I → Set
    _[_]  : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I} → A I γI → ∀{J}(f : Hom J I) → A J (⟨ Γ ⟩ ∶ γI [ f ])
    [∘]   : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I}(aI : A I γI){J}(f : Hom J I){K}(g : Hom K J) → aI [ f ∘c g ] ≡ (aI [ f ]) [ g ]
    [id]  : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I}(aI : A I γI) → aI [ idc ] ≡ aI
  infix 8 _[_]
open Ty renaming (A to ∣_∣; _[_] to _∶_[_])

record Tm (Γ : Con)(A : Ty Γ) : Set where
  field
    a   : ∀{I}(γI : ∣ ⟨ Γ ⟩ ∣ I) → ∣ A ∣ I γI
    nat : ∀{I}{γI : ∣ ⟨ Γ ⟩ ∣ I}{J}(f : Hom J I) → A ∶ (a γI) [ f ] ≡ a (⟨ Γ ⟩ ∶ γI [ f ])
open Tm renaming (a to ∣_∣)

_[_]T : ∀{Γ} → Ty Γ → ∀{Δ} → Sub' Δ Γ → Ty Δ
∣ _[_]T {Γ} A {Δ} γ ∣ I δI = ∣ A ∣ I (un⟨ Γ ⟩ ∘ γ ∘ mk⟨ Δ ⟩ ∘ δI)
(A [ γ ]T) ∶ aI [ f ] = A ∶ aI [ f ]
[∘] (A [ γ ]T) aI f g = {!!}
[id] (A [ γ ]T) aI = {!!}

_[_]t : ∀{Γ}{A : Ty Γ} → Tm Γ A → ∀{Δ}(γ : Sub' Δ Γ) → Tm Δ (A [ γ ]T)
∣ _[_]t {Γ}{A} a {Δ} γ ∣ δI = ∣ a ∣ (un⟨ Γ ⟩ ∘ γ ∘ mk⟨ Δ ⟩ ∘ δI)
nat (a [ γ ]t) = {!!}

module try1 where
  [∘]T : ∀{Γ}{A : Ty Γ}{Δ}{γ : Sub' Δ Γ}{Θ}{δ : Sub' Θ Δ} → A [ γ ∘ δ ]T ≡ A [ γ ]T [ δ ]T
  [∘]T = {!!}

  [id]T : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
  [id]T = {!!}

  -- [∘]t : ∀{Γ}{A : Ty Γ}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → a [ γ ∘ δ ]t ≡ a [ γ ]t [ δ ]t
  -- [∘]t = refl

  -- [id]t : ∀{Γ}{A : Ty Γ}{a : Tm Γ A} → a [ id ]t ≡ a
  -- [id]t = refl

  _▹_ : (Γ : Con)(A : Ty Γ) → Con
  ∣ Γ ▹ A ∣ I = Σ (∣ ⟨ Γ ⟩ ∣ I) (∣ A ∣ I)
  (Γ ▹ A) ∶ (γI ,Σ aI) [ f ] = (γI ∘ yHom f) ,Σ A ∶ aI [ f ]
  [id] (Γ ▹ A) _ = {!!}
  [∘]  (Γ ▹ A) = {!!}

  _,_ : ∀{Γ Δ}(γ : Sub' Δ Γ){A : Ty Γ} → Tm Δ (A [ γ ]T) → Sub' Δ (Γ ▹ A)
  ∣ ∣ _,_ {Γ}{Δ} γ {A} a ∣ δI ∣ f = un⟨ Γ ⟩ ∘ γ ∘ mk⟨ Δ ⟩ ∘ δI ∘ yHom f ,Σ ∣ a ∣ (δI ∘ yHom f)
  nat (∣ γ , a ∣ δI) = {!!}
  nat (γ , a) f = {!!}

  p : ∀{Γ}{A : Ty Γ} → Sub' (Γ ▹ A) Γ
  ∣ ∣ p {Γ} {A} ∣ γaI ∣ f = ∣ π₁ (∣ γaI ∣ idc) ∣ f -- ∣ π₁ (∣ γaI ∣ f) ∣ idc
  nat (∣ p {Γ} {A} ∣ γaI) = {!!}
  nat (p {Γ} {A}) = {!!}

  q : ∀{Γ}{A : Ty Γ} → Tm (Γ ▹ A) (A [ p {A = A} ]T)
  ∣ q {Γ} {A} ∣ γaI = {!!} -- {!∣ π₂ (∣ γaI ∣ idc) ∣ (yHom idc)!}
  nat (q {Γ} {A}) = {!!}

module try2 where
  record Ty' (Γ : Con) : Set₁ where
    field
      con : Con
      sub : Sub' Γ con
      ty  : Ty con
  open Ty' public

  _[_]T' : ∀{Γ} → Ty' Γ → ∀{Δ} → Sub' Δ Γ → Ty' Δ
  con (A [ γ ]T') = con A
  sub (A [ γ ]T') = sub A ∘ γ
  ty (A [ γ ]T') = ty A

  [∘]T : ∀{Γ}{A : Ty' Γ}{Δ}{γ : Sub' Δ Γ}{Θ}{δ : Sub' Θ Δ} → A [ γ ∘ δ ]T' ≡ A [ γ ]T' [ δ ]T'
  [∘]T = refl

  [id]T : ∀{Γ}{A : Ty' Γ} → A [ id ]T' ≡ A
  [id]T = refl

  record Tm' (Γ : Con)(A : Ty' Γ) : Set where
    field
      tm  : Tm (con A) (ty A)
      sub : Sub' Γ (con A)
  open Tm'
  
  _[_]t' : ∀{Γ}{A : Ty' Γ} → Tm' Γ A → ∀{Δ}(γ : Sub' Δ Γ) → Tm' Δ (A [ γ ]T')
  tm (a [ γ ]t') = tm a
  sub (a [ γ ]t') = sub a ∘ γ

  [∘]t : ∀{Γ}{A : Ty' Γ}{a : Tm' Γ A}{Δ}{γ : Sub' Δ Γ}{Θ}{δ : Sub' Θ Δ} → a [ γ ∘ δ ]t' ≡ a [ γ ]t' [ δ ]t'
  [∘]t = refl

  [id]t : ∀{Γ}{A : Ty' Γ}{a : Tm' Γ A} → a [ id ]t' ≡ a
  [id]t = refl

  _▹_ : (Γ : Con) → Ty Γ → Con
  ∣ Γ ▹ A ∣ I = {!!}
  _∶_[_] (Γ ▹ A) = {!!}
  [∘] (Γ ▹ A) = {!!}
  [id] (Γ ▹ A) = {!!}
