{-# OPTIONS --prop --rewriting #-}

open import Agda.Primitive

-- we postulate coe, and we only use rewriting for its computation rule on refl

infixl 2 _◼_
infix 5 _⁻¹
infix 4 _≡_
infixl 5 _,Σ_
data _≡_ {i}{A : Set i}(a : A) : A → Prop i where
  refl : a ≡ a
_◼_ : ∀{i}{A : Set i}{a a' a'' : A} → a ≡ a' → a' ≡ a'' → a ≡ a''
refl ◼ e = e
_⁻¹ : ∀{i}{A : Set i}{a a' : A} → a ≡ a' → a' ≡ a
refl ⁻¹ = refl
record ⊤ : Set where
record ⊤p : Prop where
record Σ {i}(A : Set i){j}(B : A → Set j) : Set (i ⊔ j) where
  constructor _,Σ_
  field
    π₁ : A
    π₂ : B π₁
open Σ
record Lift {i}(A : Prop i) : Set i where
  constructor mk
  field un : A
open Lift
cong : ∀{i j}{A : Set i}{B : Set j}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong f refl = refl

postulate coe     : ∀{ℓ}{A B : Set ℓ} → A ≡ B → A → B
postulate coerefl : ∀{ℓ}{A : Set ℓ}{e : A ≡ A}{a : A} → coe e a ≡ a
{-# BUILTIN REWRITE _≡_ #-}
{-# REWRITE coerefl   #-}

postulate funExtᵢ : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}{f g : {a : A} → B a} → (∀ {x} → f {x} ≡ g {x}) → (λ {x} → f {x}) ≡ (λ {x} → g {x})
postulate funExt : ∀{ℓ ℓ'}{A : Set ℓ}{B : A → Set ℓ'}{f g : (a : A) → B a} → (∀ x → f x ≡ g x) → f ≡ g

transp : ∀{ℓ}{A : Set ℓ}{ℓ'}(P : A → Set ℓ'){a a' : A} → a ≡ a' → P a → P a'
transp P e p = coe (cong P e) p

record Model {i}{j}{k}{l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _∘_
  infixl 5 _▹_
  infixl 5 _,[_]_
  field
    Con      : Set i
    Sub      : Con → Con → Set j
    _∘_      : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
    ass      : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{Ξ}{θ : Sub Ξ Θ} → (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
    id       : ∀{Γ} → Sub Γ Γ
    idl      : ∀{Γ Δ}{γ : Sub Δ Γ} → id ∘ γ ≡ γ
    idr      : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘ id ≡ γ
    ◇        : Con
    ε        : ∀{Γ} → Sub Γ ◇
    ◇η       : ∀{Γ}{σ : Sub Γ ◇} → σ ≡ ε
    Ty       : Con → Set k
    _[_]T    : ∀{Γ} → Ty Γ → ∀{Δ} → Sub Δ Γ → Ty Δ
    [∘]T     : ∀{Γ}{A : Ty Γ}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → A [ γ ∘ δ ]T ≡ A [ γ ]T [ δ ]T
    [id]T    : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
    Tm       : (Γ : Con) → Ty Γ → Set l
    _[_][_]t : ∀{Γ}{A : Ty Γ} → Tm Γ A → ∀{Δ}(γ : Sub Δ Γ){A' : Ty Δ} → A [ γ ]T ≡ A' → Tm Δ A'
    [∘]t     : ∀{Γ}{A : Ty Γ}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → a [ γ ∘ δ ][ [∘]T ]t ≡ a [ γ ][ refl ]t [ δ ][ refl ]t
    [id]t    : ∀{Γ}{A : Ty Γ}{a : Tm Γ A} → a [ id ][ [id]T ]t ≡ a
    _▹_      : (Γ : Con) → Ty Γ → Con
    _,[_]_   : ∀{Γ Δ}(γ : Sub Δ Γ) → ∀{A A'} → A [ γ ]T ≡ A' → Tm Δ A' → Sub Δ (Γ ▹ A)
    p        : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q        : ∀{Γ A A'} → (A [ p ]T) ≡ A' → Tm (Γ ▹ A) A'
    ▹β₁      : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{a : Tm Δ (A [ γ ]T)} → p ∘ (γ ,[ refl ] a) ≡ γ
    ▹β₂      : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{a : Tm Δ (A [ γ ]T)} → q refl [ γ ,[ refl ] a ][ [∘]T ⁻¹ ◼ cong (A [_]T) ▹β₁ ]t ≡ a
    ▹η       : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → (p ∘ γa ,[ refl ] q refl [ γa ][ [∘]T ⁻¹ ]t) ≡ γa
    Π        : ∀{Γ}(A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
    Π[]      : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{Δ}{γ : Sub Δ Γ} → (Π A B) [ γ ]T ≡ Π (A [ γ ]T) (B [ γ ∘ p ,[ [∘]T ] q refl ]T)

record StrictModel {i}{j}{k}{l} : Set (lsuc (i ⊔ j ⊔ k ⊔ l)) where
  infixl 6 _∘_
  infixl 5 _,_
  infixl 6 _[_]T _[_]t
  infixl 5 _▹_
  field
    Con      : Set i
    Sub      : Con → Con → Set j
    _∘_      : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
    ass      : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{Ξ}{θ : Sub Ξ Θ} → (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
    id       : ∀{Γ} → Sub Γ Γ
    idl      : ∀{Γ Δ}{γ : Sub Δ Γ} → id ∘ γ ≡ γ
    idr      : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘ id ≡ γ
    ◇        : Con
    ε        : ∀{Γ} → Sub Γ ◇
    ◇η       : ∀{Γ}{σ : Sub Γ ◇} → σ ≡ ε
    Ty       : Con → Set k
    _[_]T    : ∀{Γ} → Ty Γ → ∀{Δ} → Sub Δ Γ → Ty Δ
    [∘]T     : ∀{Γ}{A : Ty Γ}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → A [ γ ∘ δ ]T ≡ A [ γ ]T [ δ ]T
    [id]T    : ∀{Γ}{A : Ty Γ} → A [ id ]T ≡ A
    Tm       : (Γ : Con) → Ty Γ → Set l
    _[_]t    : ∀{Γ}{A : Ty Γ} → Tm Γ A → ∀{Δ}(γ : Sub Δ Γ) → Tm Δ (A [ γ ]T)
    [∘]t     : ∀{Γ}{A : Ty Γ}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → transp (Tm Θ) [∘]T (a [ γ ∘ δ ]t) ≡ a [ γ ]t [ δ ]t
    [id]t    : ∀{Γ}{A : Ty Γ}{a : Tm Γ A} → transp (Tm Γ) [id]T (a [ id ]t) ≡ a
    _▹_      : (Γ : Con) → Ty Γ → Con
    _,_      : ∀{Γ Δ}(γ : Sub Δ Γ){A} → Tm Δ (A [ γ ]T) → Sub Δ (Γ ▹ A)
    p        : ∀{Γ A} → Sub (Γ ▹ A) Γ
    q        : ∀{Γ A} → Tm (Γ ▹ A) (A [ p ]T)
    ▹β₁      : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{a : Tm Δ (A [ γ ]T)} → p ∘ (γ , a) ≡ γ
    ▹β₂      : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{a : Tm Δ (A [ γ ]T)} → transp (Tm Δ) ([∘]T ⁻¹ ◼ cong (A [_]T) ▹β₁) (q [ γ , a ]t) ≡ a
    ▹η       : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → γa ≡ ((p ∘ γa) , transp (Tm Δ) ([∘]T ⁻¹) (q [ γa ]t))
    Π        : ∀{Γ}(A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
    Π[]      : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{Δ}{γ : Sub Δ Γ} → (Π A B) [ γ ]T ≡ Π (A [ γ ]T) (B [ (γ ∘ p) , transp (Tm (Δ ▹ A [ γ ]T)) ([∘]T ⁻¹) q ]T)

-- first we strictify only the category part, and obtain a new model
module StrictifyCat (C : Model {lzero}{lzero}{lzero}{lzero}) where
  module C = Model C
  Con = C.Con
  Ty  = C.Ty
  Tm  = C.Tm
  ◇   = C.◇
  _▹_ = C._▹_
  infixl 6 _∘_
  infixl 5 _▹_
  infixl 5 _,[_]_

  -- Sub Δ Γ := yΔ ̇→ yΓ
  record Sub (Δ Γ : Con) : Set where
    field
      γ   : ∀{Θ : Con} → C.Sub Θ Δ → C.Sub Θ Γ
      nat : ∀{Θ}{δ : C.Sub Θ Δ}{Ξ}{θ : C.Sub Ξ Θ} → γ δ C.∘ θ ≡ γ (δ C.∘ θ)
  open Sub renaming (γ to ∣_∣)

  infix 4 ∣_∣≡
  ∣_∣≡ : ∀{Γ Δ}{σ₀ σ₁ : Sub Δ Γ} → (λ {Θ} → ∣ σ₀ ∣ {Θ}) ≡ ∣ σ₁ ∣ → σ₀ ≡ σ₁
  ∣ refl ∣≡ = refl

  _∘_ : ∀{Γ Δ} → Sub Δ Γ → ∀{Θ} → Sub Θ Δ → Sub Θ Γ
  ∣ γ ∘ δ ∣ θ = ∣ γ ∣ (∣ δ ∣ θ)
  nat (γ ∘ δ) {Θ} {δ₁} {Ξ} {θ} = nat γ {_} {∣ δ ∣ δ₁} ◼ cong ∣ γ ∣ (nat δ)

  ass : ∀{Γ Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}{Ξ}{θ : Sub Ξ Θ} → (γ ∘ δ) ∘ θ ≡ γ ∘ (δ ∘ θ)
  ass = refl

  id : ∀{Γ} → Sub Γ Γ
  ∣ id ∣ γ = γ
  nat id = refl

  idl : ∀{Γ Δ}{γ : Sub Δ Γ} → id {Γ} ∘ γ ≡ γ
  idl = refl

  idr : ∀{Γ Δ}{γ : Sub Δ Γ} → γ ∘ id ≡ γ
  idr = refl

  ε : ∀{Γ} → Sub Γ ◇
  ∣ ε ∣ _ = C.ε
  nat ε = C.◇η

  ◇η : ∀{Γ}{σ : Sub Γ ◇} → σ ≡ ε
  ◇η {σ = σ} = ∣ (funExtᵢ λ {_} → funExt λ δ → C.◇η) ∣≡

  _[_]T : ∀{Γ} → Ty Γ → ∀{Δ} → Sub Δ Γ → Ty Δ
  A [ γ ]T = A C.[ ∣ γ ∣ C.id ]T

  [∘]T : ∀{Γ}{A : Ty Γ}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → A [ γ ∘ δ ]T ≡ A [ γ ]T [ δ ]T
  [∘]T {A = A} {_} {γ = γ} {_} {δ} = cong (A C.[_]T) (cong ∣ γ ∣ (C.idl ⁻¹) ◼ nat γ ⁻¹) ◼ C.[∘]T
  [id]T : ∀{Γ}{A : Ty Γ} → A [ id {Γ} ]T ≡ A
  [id]T = C.[id]T

  [∘]T-gen : ∀{Γ}{A : Ty Γ}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}
             {A' : Ty Δ} (e0 : A [ γ ]T ≡ A') {A'' : Ty Θ} (e1 : A' [ δ ]T ≡ A'')
             → A [ γ ∘ δ ]T ≡ A''
  [∘]T-gen {A = A} {γ = γ} {δ = δ} refl refl = [∘]T {A = A} {γ = γ} {δ = δ}

  _[_][_]t : ∀{Γ}{A : Ty Γ} → Tm Γ A → ∀{Δ}(γ : Sub Δ Γ){A' : Ty Δ} → A [ γ ]T ≡ A' → Tm Δ A'
  a [ γ ][ e ]t = a C.[ ∣ γ ∣ C.id ][ e ]t

  _[_][-]t≡ : ∀{Γ}{A : Ty Γ}{a a' : Tm Γ A} → a ≡ a' → ∀{Δ}{γ γ' : Sub Δ Γ} → γ ≡ γ' → {A' : Ty Δ}{e : A [ γ ]T ≡ A'}{e' : A [ γ' ]T ≡ A'} → a [ γ ][ e ]t ≡ a' [ γ' ][ e' ]t
  refl [ refl ][-]t≡ = refl

  _[_][-]t≡C : ∀{Γ}{A : C.Ty Γ}{a a' : C.Tm Γ A} → a ≡ a' → ∀{Δ}{γ γ' : C.Sub Δ Γ} → γ ≡ γ' → {A' : C.Ty Δ}{e : A C.[ γ ]T ≡ A'}{e' : A C.[ γ' ]T ≡ A'} → a C.[ γ ][ e ]t ≡ a' C.[ γ' ][ e' ]t
  refl [ refl ][-]t≡C = refl

  [∘]t     : ∀{Γ}{A : Ty Γ}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ} → a [ γ ∘ δ ][ [∘]T {A = A}{γ = γ}{δ = δ} ]t ≡ a [ γ ][ refl ]t [ δ ][ refl ]t
  [∘]t {a = a} {_} {γ} {_} {δ} = {! cong (λ x → a C.[ x ][ _ ]t) ((γ .nat ◼ cong ∣ γ ∣ C.idl) ⁻¹)!} ◼ C.[∘]t
  [id]t    : ∀{Γ}{A : Ty Γ}{a : Tm Γ A} → a [ id ][ [id]T ]t ≡ a
  [id]t = C.[id]t
  _,[_]_   : ∀{Γ Δ}(γ : Sub Δ Γ) → ∀{A A'} → A [ γ ]T ≡ A' → Tm Δ A' → Sub Δ (Γ ▹ A)
  ∣ γ ,[ e ] a ∣ = λ δ → ∣ γ ∣ δ C.,[ cong (_ C.[_]T) (cong ∣ γ ∣ (C.idl ⁻¹) ◼ nat γ ⁻¹) ◼ C.[∘]T ◼ cong C._[ δ ]T e ] (a C.[ δ ][ refl ]t)
  nat (γ ,[ e ] a) = {! !}

  [∘]t-gen : ∀{Γ}{A : Ty Γ}{a : Tm Γ A}{Δ}{γ : Sub Δ Γ}{Θ}{δ : Sub Θ Δ}
             {A' : Ty Δ} (e0 : A [ γ ]T ≡ A') {A'' : Ty Θ} (e1 : A' [ δ ]T ≡ A'')
           → a [ γ ∘ δ ][ [∘]T-gen {A = A}{γ = γ}{δ = δ} e0 e1 ]t ≡ a [ γ ][ e0 ]t [ δ ][ e1 ]t
  [∘]t-gen {a = a} {γ = γ} {δ = δ} refl refl = [∘]t {a = a} {γ = γ} {δ = δ}

  p        : ∀{Γ A} → Sub (Γ ▹ A) Γ
  ∣ p ∣ γa = C.p C.∘ γa
  nat p = C.ass
  q        : ∀{Γ A A'} → (A [ p ]T) ≡ A' → Tm (Γ ▹ A) A'
  q {A = A} e = C.q (cong (A C.[_]T) (C.idr ⁻¹) ◼ e)

  q≡C : ∀{Γ}{A : C.Ty Γ}{A'}{e e' : (A C.[ C.p ]T) ≡ A'} → C.q e ≡ C.q e'
  q≡C = refl

  _,[-]_≡ : ∀{Γ Δ}{γ γ' : Sub Δ Γ} → γ ≡ γ' → ∀{A A'}{a a' : Tm Δ A'} → a ≡ a' → {e : A [ γ ]T ≡ A'}{e' : A [ γ' ]T ≡ A'} → γ ,[ e ] a ≡ γ' ,[ e' ] a'
  refl ,[-] refl ≡ = refl

  _,[-]_≡C : ∀{Γ Δ}{γ γ' : C.Sub Δ Γ} → γ ≡ γ' → ∀{A A'}{a a' : C.Tm Δ A'} → a ≡ a' → {e : A C.[ γ ]T ≡ A'}{e' : A C.[ γ' ]T ≡ A'} → γ C.,[ e ] a ≡ γ' C.,[ e' ] a'
  refl ,[-] refl ≡C = refl

  ▹β₁      : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{a : Tm Δ (A [ γ ]T)} → p ∘ (γ ,[ refl ] a) ≡ γ
  ▹β₁ {Γ} {Δ} {γ} {A} {a} = ∣ (funExtᵢ (λ {Θ} → funExt λ δ → {! C.▹β₁ {γ = ∣ γ ∣ δ}{a = a C.[ δ ][ ? ]t}  !})) ∣≡
  ▹β₂      : ∀{Γ Δ}{γ : Sub Δ Γ}{A}{a : Tm Δ (A [ γ ]T)} → q refl [ γ ,[ refl ] a ][ [∘]T {A = A}{γ = p}{δ = (γ ,[ refl ] a)} ⁻¹ ◼ cong (A [_]T) {_} {γ} ▹β₁ ]t ≡ a
  ▹β₂ {Γ} {Δ} {γ} {A} {a} = _[_][-]t≡ {_} {A [ p ]T} {q {Γ} {A} {A [ p ]T} refl} {q refl} refl {Δ} {γ ,[ refl ] a} {γ ,[ refl ] a} refl {A [ γ ]T} {[∘]T {γ = p} {δ = γ ,[ refl ] a} ⁻¹ ◼ cong (A [_]T) (▹β₁ {γ = γ})} {[∘]T {γ = p} {δ = γ ,[ refl ] a} ⁻¹ ◼ cong (A [_]T) (▹β₁ {γ = γ})} ◼ {!   !}
    {-
    _[_][-]t≡ {a = q refl} {q refl} refl {_} {γ ,[ refl ] a} {γ ,[ refl ] a} refl {_} {_} {
    cong (λ x → ((A C.[ x ]T) C.[ ∣ γ ∣ C.id C.,[ cong (C._[_]T A) (cong ∣ γ ∣ (C.idl ⁻¹) ◼ nat γ ⁻¹) ◼ C.[∘]T ◼ cong (C._[ C.id ]T) refl ] (a C.[ C.id ][ refl ]t) ]T)) C.idr
    ◼ C.[∘]T ⁻¹
    ◼ (cong (A C.[_]T) (cong (C.p C.∘_) {!  !}) ◼ {!   !})} ◼ {!   !}
    -}
    -- C.[id]t {Δ} {A [ γ ]T} {a}
    -- C.▹β₂ {Γ} {Δ} {∣ γ ∣ C.id} {A} {a}
  ▹η       : ∀{Γ Δ A}{γa : Sub Δ (Γ ▹ A)} → (p ∘ γa ,[ refl ] q refl [ γa ][ [∘]T {γ = p} {δ = γa}  ⁻¹ ]t) ≡ γa
  ▹η {Γ} {Δ} {A} {γa} = {!  nat γa !}

  Π        : ∀{Γ}(A : Ty Γ) → Ty (Γ ▹ A) → Ty Γ
  Π = C.Π
  Π[]      : ∀{Γ}{A : Ty Γ}{B : Ty (Γ ▹ A)}{Δ}{γ : Sub Δ Γ} → (Π A B) [ γ ]T ≡ Π (A [ γ ]T) (B [ γ ∘ p ,[ [∘]T {γ = γ} {δ = p} ] q refl ]T)
  Π[] = {!C.Π[]!}

  M : Model {lzero}{lzero}{lzero}{lzero}
  M = record
        { Con = Con
        ; Sub = Sub
        ; _∘_ = _∘_
        ; ass = λ {_}{_}{γ}{_}{δ}{_}{θ} → ass {_}{_}{γ}{_}{δ}{_}{θ}
        ; id = id
        ; idl = idl
        ; idr = idr
        ; ◇ = ◇
        ; ε = ε
        ; ◇η = ◇η
        ; Ty = Ty
        ; _[_]T = _[_]T
        ; [∘]T = λ {x1}{x2}{x3}{x4}{x5}{x6} → [∘]T {x1}{x2}{x3}{x4}{x5}{x6}
        ; [id]T = [id]T
        ; Tm = Tm
        ; _[_][_]t = _[_][_]t
        ; [∘]t = λ {x1}{x2}{x3}{x4}{x5}{x6}{x7} → [∘]t {x1}{x2}{x3}{x4}{x5}{x6}{x7}
        ; [id]t = [id]t
        ; _▹_ = _▹_
        ; _,[_]_ = _,[_]_
        ; p = p
        ; q = q
        ; ▹β₁ = ▹β₁
        ; ▹β₂ = λ {x1}{x2}{x3}{x4} → ▹β₂ {x1}{x2}{x3}{x4}
        ; ▹η = ▹η
        ; Π = Π
        ; Π[] = λ {x1}{x2}{x3}{x4}{x5} → Π[] {x1}{x2}{x3}{x4}{x5}
        }

-- next step: apply the prefascist model construction to the StrictifyCat.M

module StrictifyUsingPrefascist (M : Model {lzero}{lzero}{lzero}{lzero}) where

  -- TODO: copy the prefascist stuff from Prefascist3

  -- strict CwF (except η) of prefascist sets over SrictifyCat.M

  -- this CwF has U,El coming from M

  variable i j k : Level

  Ob  = Model.Con M
  -- Ob  = Model.Con (StrictifyCat.M M)
  variable x y z : Ob
  Hom = Model.Sub (StrictifyCat.M M)
  module C where
    infixl 9 _∘_
    _∘_ : Hom y x → Hom z y → Hom z x
    f ∘ g = Model._∘_ (StrictifyCat.M M) f g
    id : Hom x x
    id = Model.id (StrictifyCat.M M)
    _▹_ : (x : Ob) → Model.Ty (StrictifyCat.M M) x → Ob
    _▹_ = Model._▹_ (StrictifyCat.M M)
    p : ∀{x A} → Hom (x ▹ A) x
    p = Model.p (StrictifyCat.M M)
    q : ∀{x A A'} → (Model._[_]T (StrictifyCat.M M) A p) ≡ A' → Model.Tm (StrictifyCat.M M) (x ▹ A) A'
    q = Model.q (StrictifyCat.M M)

  ≡-elim :
    {A : Set i} {a₀ : A}
    (P : ∀ a₁ → a₀ ≡ a₁ → Prop j) →
    P a₀ refl →
    ∀ {a₁} a₀₁ → P a₁ a₀₁
  ≡-elim P p refl = p

  ≡-elimₛ :
    {A : Set i} {a₀ : A}
    (P : ∀ a₁ → a₀ ≡ a₁ → Set j) →
    P a₀ refl →
    ∀ {a₁} a₀₁ → P a₁ a₀₁
  ≡-elimₛ {A = A}{a₀} P p a₀₁ = transp (λ a → (e : a₀ ≡ a) → P a e) a₀₁ (λ _ → p) a₀₁

  ap : ∀ {A : Set i} {B : Set j} (f : A → B) {x y} → x ≡ y → f x ≡ f y
  ap = cong

  postulate
    funext :
      {A : Set i} {B : A → Set j} {f g : ∀ a → B a} → (∀ a → f a ≡ g a) → f ≡ g
    funextᵢ :
      {A : Set i} {B : A → Set j} {f g : ∀ {a} → B a} →
      (∀ {a} → f {a} ≡ g {a}) →
      (λ {a} → f {a}) ≡ (λ {a} → g {a})

  sym : ∀ {A : Set i} {x y : A} → x ≡ y → y ≡ x
  sym refl = refl

  record ⊤ₚ : Prop where constructor ⋆

  record Y
    (x : Ob)
    (A : ∀ {y} → Hom y x → Set i)
    (A-rel : ∀ {y} (f : Hom y x) → (∀ {z} (g : Hom z y) → A (f C.∘ g)) → Prop i)
    : Set i
    where
    field
      a : (f : Hom y x) → A f
      rel : (f : Hom y x) → A-rel f λ g → a (f C.∘ g)
  open Y public renaming (a to ∣_∣)

  record Con (i : Level) : Set (lsuc i) where
    field
      Γ : Ob → Set i
      rel : (∀ {y} → Hom y x → Γ y) → Prop i
  open Con public renaming (Γ to ∣_∣)

  private variable
    Γ Δ Θ Ξ : Con i

  -- converting Γ to ordinary presheaf, and this is the action on objects
  Yᶜ : Con i → Ob → Set i
  Yᶜ Γ x = Y x (λ {y} _ → ∣ Γ ∣ y) (λ _ → Γ .rel)

  Yᶜ-≡ : {γ₀ γ₁ : Yᶜ Γ x} → (λ {x} → ∣ γ₀ ∣ {x}) ≡ ∣ γ₁ ∣ → γ₀ ≡ γ₁
  Yᶜ-≡ refl = refl

  infixl 9 _|ᶜ_
  _|ᶜ_ : Yᶜ Γ x → Hom y x → Yᶜ Γ y
  ∣ γ |ᶜ f ∣ g = ∣ γ ∣ (f C.∘ g)
  (γ |ᶜ f) .rel g = γ .rel (f C.∘ g)

  record Sub (Δ : Con i) (Γ : Con j) : Set (i ⊔ j) where
    field
      γ : Yᶜ Δ x → ∣ Γ ∣ x                           -- this is not simply a natural transformation from Yᶜ Δ to Yᶜ Γ (not even this first component)
      rel : (δ : Yᶜ Δ x) → Γ .rel λ f → γ (δ |ᶜ f)
  open Sub public renaming (γ to ∣_∣)

  Sub-≡ :
    {γ₀ γ₁ : Sub Δ Γ} → (λ {x} → ∣ γ₀ ∣ {x}) ≡ (λ {x} → ∣ γ₁ ∣ {x}) → γ₀ ≡ γ₁
  Sub-≡ refl = refl

  -- converting a Sub into a natural transformation, only action on objects
  Yˢ : Sub Δ Γ → Yᶜ Δ x → Yᶜ Γ x
  ∣ Yˢ γ δ ∣ f = ∣ γ ∣ (δ |ᶜ f)
  Yˢ γ δ .rel f = γ .rel (δ |ᶜ f)

  infixl 9 _∘_
  _∘_ : Sub Δ Γ → Sub Θ Δ → Sub Θ Γ
  ∣ γ ∘ δ ∣ θ = ∣ γ ∣ (Yˢ δ θ)
  (γ ∘ δ) .rel θ = γ .rel (Yˢ δ θ)

  comp : ∀ (Γ : Con i) {Δ : Con i} → Sub Δ Γ → ∀{Θ : Con i} → Sub Θ Δ → Sub Θ Γ
  comp Γ γ δ = _∘_ {Γ = Γ} γ δ
  syntax comp Γ γ δ = γ ∘[ Γ ] δ

  assoc : (γ : Sub Δ Γ) (δ : Sub Θ Δ) (θ : Sub Ξ Θ) → γ ∘ (δ ∘ θ) ≡ (γ ∘ δ) ∘ θ
  assoc γ δ θ = refl

  id : Sub Γ Γ
  ∣ id ∣ γ = ∣ γ ∣ C.id
  id .rel γ = γ .rel C.id

  idr : (γ : Sub Δ Γ) → γ ∘ id ≡ γ
  idr γ = refl

  idl : (γ : Sub Δ Γ) → id ∘ γ ≡ γ
  idl γ = refl

  record Ty (Γ : Con i) (j : Level) : Set (i ⊔ lsuc j) where
    field
      A : Yᶜ Γ x → Set j -- ordinary family
      rel : (γ : Yᶜ Γ x) → (∀ {y} (f : Hom y x) → A (γ |ᶜ f)) → Prop j
  open Ty public renaming (A to ∣_∣)

  private variable
    A B : Ty Γ i

  Yᵀ : Ty Γ j → Yᶜ Γ x → Set j
  Yᵀ {x = x} A γ = Y x (λ f → ∣ A ∣ (γ |ᶜ f)) (λ f → A .rel (γ |ᶜ f))

  infixl 9 ⟨_,_⟩_|ᵀ_
  ⟨_,_⟩_|ᵀ_ : (A : Ty Γ i) (γ : Yᶜ Γ x) → Yᵀ A γ → (f : Hom y x) → Yᵀ A (γ |ᶜ f)
  ∣ ⟨ A , γ ⟩ a |ᵀ f ∣ g = ∣ a ∣ (f C.∘ g)
  (⟨ A , γ ⟩ a |ᵀ f) .rel g = a .rel (f C.∘ g)

  infixl 9 _[_]ᵀ
  _[_]ᵀ : Ty Γ i → Sub Δ Γ → Ty Δ i
  ∣ A [ γ ]ᵀ ∣ δ = ∣ A ∣ (Yˢ γ δ)
  (A [ γ ]ᵀ) .rel δ = A .rel (Yˢ γ δ)

  []ᵀ-∘ :
    (A : Ty Γ i) (γ : Sub Δ Γ) (δ : Sub Θ Δ) → A [ γ ∘ δ ]ᵀ ≡ A [ γ ]ᵀ [ δ ]ᵀ
  []ᵀ-∘ A γ δ = refl

  []ᵀ-id : (A : Ty Γ i) → A [ id ]ᵀ ≡ A
  []ᵀ-id A = refl

  record Tm (Γ : Con i) (A : Ty Γ j) : Set (i ⊔ j) where
    field
      a : (γ : Yᶜ Γ x) → ∣ A ∣ γ -- "in the domain you always convert into ordinary presheaves"
      rel : (γ : Yᶜ Γ x) → A .rel γ λ f → a (γ |ᶜ f)
  open Tm public renaming (a to ∣_∣)

  Tm-≡ : {a₀ a₁ : Tm Γ A} → (λ {x} → ∣ a₀ ∣ {x}) ≡ (λ {x} → ∣ a₁ ∣ {x}) → a₀ ≡ a₁
  Tm-≡ refl = refl

  Yᵗ : Tm Γ A → (γ : Yᶜ Γ x) → Yᵀ A γ
  ∣ Yᵗ a γ ∣ f = ∣ a ∣ (γ |ᶜ f)
  Yᵗ a γ .rel f = a .rel (γ |ᶜ f)

  infixl 9 _[_]ᵗ
  _[_]ᵗ : Tm Γ A → (γ : Sub Δ Γ) → Tm Δ (A [ γ ]ᵀ)
  ∣ a [ γ ]ᵗ ∣ δ = ∣ a ∣ (Yˢ γ δ)
  (a [ γ ]ᵗ) .rel δ = a .rel (Yˢ γ δ)

  []ᵗ-∘ :
    (a : Tm Γ A) (γ : Sub Δ Γ) (δ : Sub Θ Δ) → a [ γ ∘ δ ]ᵗ ≡ a [ γ ]ᵗ [ δ ]ᵗ
  []ᵗ-∘ a γ δ = refl

  []ᵗ-id : {A : Ty Γ i} (a : Tm Γ A) → a [ id ]ᵗ ≡ a
  []ᵗ-id a = refl

  record ∣▹∣ (Γ : Con i) (A : Ty Γ j) (x : Ob) : Set (i ⊔ j) where
    field
      con : Yᶜ Γ x
      ty : ∣ A ∣ con
  open ∣▹∣ public

  ∣▹∣-≡ :
    {γa₀ γa₁ : ∣▹∣ Γ A x}
    (con-≡ : γa₀ .con ≡ γa₁ .con) →
    ≡-elimₛ (λ γ _ → ∣ A ∣ γ) (γa₀ .ty) con-≡ ≡ γa₁ .ty →
    γa₀ ≡ γa₁
  ∣▹∣-≡ refl refl = refl

  record ▹-rel
    {Γ : Con i} {A : Ty Γ j} (γa : ∀ {y} → Hom y x → ∣▹∣ Γ A y) : Prop (i ⊔ j)
    where
    field
      con :
        (λ {y} (f : Hom y x) {z} (g : Hom z y) → ∣ γa f .con ∣ g) ≡
        (λ {y} (f : Hom y x) {z} (g : Hom z y) → ∣ γa (f C.∘ g) .con ∣ C.id)
      ty :
        A .rel
          record
            { a = λ f → ∣ γa f .∣▹∣.con ∣ C.id
            ; rel = λ f →
              ≡-elim (λ γ _ → Γ .rel (γ f)) (γa f .∣▹∣.con .rel C.id) con }
          λ f → ≡-elimₛ (λ γ _ → ∣ A ∣ γ) (γa f .ty) (Yᶜ-≡ (ap (λ γ → γ f) con))
  open ▹-rel public

  infixl 2 _▹_
  _▹_ : (Γ : Con i) → Ty Γ j → Con (i ⊔ j)
  ∣ Γ ▹ A ∣ = ∣▹∣ Γ A
  (Γ ▹ A) .rel = ▹-rel

  Y▹ : (γ : Yᶜ Γ x) → Yᵀ A γ → Yᶜ (Γ ▹ A) x
  ∣ Y▹ γ a ∣ f .con = γ |ᶜ f
  ∣ Y▹ γ a ∣ f .ty = ∣ a ∣ f
  Y▹ γ a .rel f .con = refl
  Y▹ γ a .rel f .ty = a .rel f

  p : Sub (Γ ▹ A) Γ
  ∣ p ∣ γa = ∣ ∣ γa ∣ C.id .con ∣ C.id
  p {Γ = Γ} .rel γa =
    ≡-elim
      (λ γ _ → Γ .rel (γ C.id))
      (∣ γa ∣ C.id .con .rel C.id)
      (γa .rel C.id .con)

  q : Tm (Γ ▹ A) (A [ p ]ᵀ)
  ∣ q {A = A} ∣ γa =
    ≡-elimₛ
      (λ γ _ → ∣ A ∣ γ)
      (∣ γa ∣ C.id .ty)
      (Yᶜ-≡ (ap (λ γ → γ C.id) (γa .rel C.id .con)))
  q .rel γa = γa .rel C.id .ty

  infixl 4 _,_
  _,_ : (γ : Sub Δ Γ) → Tm Δ (A [ γ ]ᵀ) → Sub Δ (Γ ▹ A)
  ∣ γ , a ∣ δ .con = Yˢ γ δ
  ∣ γ , a ∣ δ .ty = ∣ a ∣ δ
  (γ , a) .rel δ .con = refl
  (_,_ {A = A} γ a) .rel δ .ty = a .rel δ

  infixl 4 _,⟨_⟩_
  _,⟨_⟩_ : (γ : Sub Δ Γ) (A : Ty Γ i) → Tm Δ (A [ γ ]ᵀ) → Sub Δ (Γ ▹ A)
  γ ,⟨ A ⟩ a = γ , a

  ,-∘ :
    (γ : Sub Δ Γ) (a : Tm Δ (A [ γ ]ᵀ)) (δ : Sub Θ Δ) →
    (γ ,⟨ A ⟩ a) ∘ δ ≡ (γ ∘ δ , a [ δ ]ᵗ)
  ,-∘ γ a δ = refl

  ▹-β₁ : (γ : Sub Δ Γ) (a : Tm Δ (A [ γ ]ᵀ)) → p ∘ (γ ,⟨ A ⟩ a) ≡ γ
  ▹-β₁ γ a = refl

  ▹-β₂ : (γ : Sub Δ Γ) (a : Tm Δ (A [ γ ]ᵀ)) → q [ γ ,⟨ A ⟩ a ]ᵗ ≡ a
  ▹-β₂ γ a = refl

  ▹-η : {A : Ty Γ i} → (p ,⟨ A ⟩ q) ≡ id
  ▹-η =
    Sub-≡
      (funextᵢ
        (funext λ γa →
          sym (∣▹∣-≡ (Yᶜ-≡ (ap (λ γ → γ C.id) (γa .rel C.id .con))) refl)))

  infixl 4 _↑
  _↑ : (γ : Sub Δ Γ) → Sub (Δ ▹ A [ γ ]ᵀ) (Γ ▹ A)
  γ ↑ = γ ∘ p , q

  ◇ : Con lzero
  ∣ ◇ ∣ x = ⊤
  ◇ .rel γ = ⊤ₚ

  ε : Sub Γ ◇
  ∣ ε ∣ γ = _
  ε .rel γ = ⋆

  ε-∘ : (γ : Sub Δ Γ) → ε ∘ γ ≡ ε
  ε-∘ γ = refl

  ◇-η : ε ≡ id
  ◇-η = refl

  record ∣U∣ (i : Level) (x : Ob) : Set (lsuc i) where
    field
      t : Hom y x → Set i   -- this is simpler than just Ty (yoneda x)
      rel : (∀ {y} (f : Hom y x) → t f) → Prop i
  open ∣U∣ public renaming (t to ∣_∣)

  ∣U∣-≡ :
    {A₀ A₁ : ∣U∣ i x}
    (A-≡ : (λ {y} → ∣ A₀ ∣ {y}) ≡ (λ {y} → ∣ A₁ ∣ {y})) →
    ≡-elimₛ (λ A _ → (∀ {y} (f : Hom y x) → A f) → Prop i) (A₀ .rel) A-≡ ≡
    A₁ .rel →
    A₀ ≡ A₁
  ∣U∣-≡ refl refl = refl

  U : (i : Level) → Ty Γ (lsuc i)
  ∣ U i ∣ {x} γ = ∣U∣ i x
  U i .rel {x} γ A =
    (λ {y} (f : Hom y x) {z} (g : Hom z y) → ∣ A f ∣ g) ≡
    (λ {y} (f : Hom y x) {z} (g : Hom z y) → ∣ A (f C.∘ g) ∣ C.id)

  U-[] : (i : Level) (γ : Sub Δ Γ) → U i [ γ ]ᵀ ≡ U i
  U-[] i γ = refl

  El : Tm Γ (U i) → Ty Γ i
  ∣ El A ∣ γ = ∣ ∣ A ∣ γ ∣ C.id
  El A .rel γ a =
    ∣ A ∣ γ .rel λ f → ≡-elimₛ (λ a _ → a C.id f) (a f) (sym (A .rel γ))

  El-[] : (A : Tm Γ (U i)) (γ : Sub Δ Γ) → El A [ γ ]ᵀ ≡ El (A [ γ ]ᵗ)
  El-[] A γ = refl

  c : Ty Γ i → Tm Γ (U i)
  ∣ ∣ c A ∣ γ ∣ f = ∣ A ∣ (γ |ᶜ f)
  ∣ c A ∣ γ .rel a = A .rel γ a
  c A .rel γ = refl

  c-[] : (A : Ty Γ i) (γ : Sub Δ Γ) → c A [ γ ]ᵗ ≡ c (A [ γ ]ᵀ)
  c-[] A γ = refl

  U-β : (A : Ty Γ i) → El (c A) ≡ A
  U-β A = refl
  {-
  U-η : (A : Tm Γ (U i)) → c (El A) ≡ A
  U-η A = Tm-≡ (funextᵢ (funext λ γ → sym (∣U∣-≡ (ap (λ a → a C.id) (A .rel γ)) {!refl!})))
  -}
  Π : (A : Ty Γ i) → Ty (Γ ▹ A) j → Ty Γ (i ⊔ j)
  ∣ Π A B ∣ γ = (a : Yᵀ A γ) → ∣ B ∣ (Y▹ γ a)
  Π A B .rel γ t = (a : Yᵀ A γ) → B .rel (Y▹ γ a) λ f → t f (⟨ A , γ ⟩ a |ᵀ f)

  Π-[] :
    (A : Ty Γ i) (B : Ty (Γ ▹ A) j) (γ : Sub Δ Γ) →
    Π A B [ γ ]ᵀ ≡ Π (A [ γ ]ᵀ) (B [ γ ↑ ]ᵀ)
  Π-[] A B γ = refl

  app :
    {A : Ty Γ i} (B : Ty (Γ ▹ A) j) →
    Tm Γ (Π A B) → (a : Tm Γ A) → Tm Γ (B [ id , a ]ᵀ)
  ∣ app B t a ∣ γ = ∣ t ∣ γ (Yᵗ a γ)
  app B t a .rel γ = t .rel γ (Yᵗ a γ)

  app-[] :
    {A : Ty Γ i} (B : Ty (Γ ▹ A) j) →
    (t : Tm Γ (Π A B)) (a : Tm Γ A) (γ : Sub Δ Γ) →
    app B t a [ γ ]ᵗ ≡
    app (B [ γ ↑ ]ᵀ) (t [ γ ]ᵗ) (a [ γ ]ᵗ)
  app-[] B t a γ = refl

  lam : Tm (Γ ▹ A) B → Tm Γ (Π A B)
  ∣ lam b ∣ γ a = ∣ b ∣ (Y▹ γ a)
  lam b .rel γ a = b .rel (Y▹ γ a)

  lam-[] : (b : Tm (Γ ▹ A) B) (γ : Sub Δ Γ) → lam b [ γ ]ᵗ ≡ lam (b [ γ ↑ ]ᵗ)
  lam-[] b γ = refl

  Π-β :
    {A : Ty Γ i} (B : Ty (Γ ▹ A) j) (b : Tm (Γ ▹ A) B) (a : Tm Γ A) →
    app B (lam b) a ≡ b [ id , a ]ᵗ
  Π-β B b a = refl

  Π-η :
    {Γ : Con i} {A : Ty Γ j} (B : Ty (Γ ▹ A) k) (t : Tm Γ (Π A B)) →
    lam (app (B [ p ↑ ]ᵀ) (t [ p ]ᵗ) (q {A = A})) ≡ t
  Π-η B t = refl

  -- here we convert the presheaf of types in our input model into a prefascist set
  CTy : Con lzero
  CTy = record { Γ = X ; rel = λ {p} s → ∀{q}(α : Hom q p) → (s C.id [ α ]X) ≡ s α } -- following https://pujet.fr/pdf/presheaf_translation.pdf, middle of page 4
    where
      X = Model.Ty (StrictifyCat.M M)
      _[_]X = Model._[_]T (StrictifyCat.M M)

  CTm : Ty CTy lzero
  CTm = record { A = λ {Γ} t → Xtm Γ (∣ t ∣ C.id)
               ; rel = λ {Γ} t s →  ∀{Δ}(σ : Hom Δ Γ) → (s C.id [ σ ][ eq σ t ]tX) ≡ s σ }
    where
      Xtm = Model.Tm (StrictifyCat.M M)
      _[_]X = Model._[_]T (StrictifyCat.M M)
      _[_][_]tX = Model._[_][_]t (StrictifyCat.M M)

      eq : ∀ {I J} (f : Hom J I) (t : Yᶜ CTy I) → (∣ t ∣ C.id) [ f ]X ≡ ∣ t ∣ f
      eq = λ f t → t .rel C.id f

  Tyᵒ : Con i → Set i
  Tyᵒ Γ = Sub Γ CTy

  _[_]ᵀᵒ : ∀{Γ : Con i} → Tyᵒ Γ → ∀{Δ : Con i} → Sub Δ Γ → Tyᵒ Δ
  Aᵒ [ γ ]ᵀᵒ = Aᵒ ∘ γ

  [∘]ᵀᵒ  : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ}{Δ : Con i}{γ : Sub Δ Γ}{Θ : Con i}{δ : Sub Θ Δ} → Aᵒ [ γ ∘ δ ]ᵀᵒ ≡ Aᵒ [ γ ]ᵀᵒ [ δ ]ᵀᵒ
  [∘]ᵀᵒ  = refl

  [id]ᵀᵒ : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ} → Aᵒ [ id ]ᵀᵒ ≡ Aᵒ
  [id]ᵀᵒ = refl

  Tmᵒ : (Γ : Con i) → Tyᵒ Γ → Set i
  Tmᵒ Γ Aᵒ = Tm Γ (CTm [ Aᵒ ]ᵀ)

  _[_]ᵗᵒ : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ} → Tmᵒ Γ Aᵒ → ∀{Δ : Con i}(γ : Sub Δ Γ) → Tmᵒ Δ (Aᵒ [ γ ]ᵀᵒ)
  a [ γ ]ᵗᵒ = a [ γ ]ᵗ

  [∘]ᵗᵒ : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ}{aᵒ : Tmᵒ Γ Aᵒ}{Δ : Con i}{γ : Sub Δ Γ}{Θ : Con i}{δ : Sub Θ Δ} → _[_]ᵗᵒ {Aᵒ = Aᵒ} aᵒ (γ ∘[ Γ ] δ) ≡ _[_]ᵗᵒ {Aᵒ = Aᵒ [ γ ]ᵀᵒ} (_[_]ᵗᵒ {Aᵒ = Aᵒ} aᵒ γ) δ
  [∘]ᵗᵒ = refl

  [id]ᵗᵒ : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ}{aᵒ : Tmᵒ Γ Aᵒ} → _[_]ᵗᵒ {Aᵒ = Aᵒ} aᵒ id ≡ aᵒ
  [id]ᵗᵒ = refl

  _▹ᵒ_ : (Γ : Con i)(A : Tyᵒ Γ) → Con i
  Γ ▹ᵒ Aᵒ = Γ ▹ (CTm [ Aᵒ ]ᵀ)

  _,ᵒ_ : ∀{Γ Δ : Con i}(γ : Sub Δ Γ){Aᵒ : Tyᵒ Γ} → Tmᵒ Δ (Aᵒ [ γ ]ᵀᵒ) → Sub Δ (Γ ▹ᵒ Aᵒ)
  _,ᵒ_ γ {Aᵒ} aᵒ = _,_ {A = CTm [ Aᵒ ]ᵀ} γ aᵒ

  pᵒ : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ} → Sub (Γ ▹ᵒ Aᵒ) Γ
  pᵒ {Aᵒ = Aᵒ} = p {A = CTm [ Aᵒ ]ᵀ}

  qᵒ : ∀{Γ : Con i}{Aᵒ : Tyᵒ Γ} → Tmᵒ (Γ ▹ᵒ Aᵒ) (Aᵒ [ pᵒ {Aᵒ = Aᵒ} ]ᵀᵒ)
  qᵒ {Aᵒ = Aᵒ} = q {A = CTm [ Aᵒ ]ᵀ}

  ▹ᵒβ₁ : ∀{Γ Δ : Con i}{γ : Sub Δ Γ}{Aᵒ : Tyᵒ Γ}{aᵒ : Tmᵒ Δ (Aᵒ [ γ ]ᵀᵒ)}→ pᵒ {Aᵒ = Aᵒ} ∘[ Γ ] (_,ᵒ_ γ {Aᵒ = Aᵒ} aᵒ) ≡ γ
  ▹ᵒβ₁ = refl

  ▹ᵒβ₂ : ∀{Γ Δ : Con i}{γ : Sub Δ Γ}{Aᵒ : Tyᵒ Γ}{aᵒ : Tmᵒ Δ (Aᵒ [ γ ]ᵀᵒ)}→ _[_]ᵗᵒ {Aᵒ = Aᵒ [ pᵒ {Aᵒ = Aᵒ} ]ᵀᵒ} (qᵒ {Aᵒ = Aᵒ}) (_,ᵒ_ γ {Aᵒ = Aᵒ} aᵒ) ≡ aᵒ
  ▹ᵒβ₂ = refl

  -- The following definition for Π⁺ is the most naive one

  -- Π⁺ : ∀{Γ : Con i} → (A : Sub Γ CTy) → Ty (Γ ▹ (CTm [ A ]ᵀ)) lzero → Ty Γ lzero
  -- Π⁺ {Γ} A B = Π (CTm [ A ]ᵀ) B

  -- Alternatively, we can use the fact that A is a representable type to give another description that is closer to the initial model
  -- It's unfortunately not very straightforward, but I cannot find a cleaner formulation

  Π⁺ : (A : Sub Γ CTy) → Ty (Γ ▹ (CTm [ A ]ᵀ)) lzero → Ty Γ lzero
  Π⁺ {Γ = Γ} Aᵒ B = record { A = λ γ → Yᵀ B (Y▹ (γ |ᶜ C.p) (q' γ))
                           ; rel = λ {x} γ b → ∀ {y} (f : Hom y x) → transp ∣ B ∣ (ext-eq γ f) (∣ b C.id ∣ (extend γ f)) ≡ ∣ b f ∣ C.id }
    where
      _[_]X = Model._[_]T (StrictifyCat.M M)
      _[_][_]tX = Model._[_][_]t (StrictifyCat.M M)
      _,[_]X_ = Model._,[_]_ (StrictifyCat.M M)

      Aᵒeq : ∀ {I J} (f : Hom J I) (γ : Yᶜ Γ I) → (∣ Aᵒ ∣ γ) [ f ]X ≡ ∣ Aᵒ ∣ (γ |ᶜ f)
      Aᵒeq f γ = (Yˢ Aᵒ γ) .rel C.id f

      q' : ∀ {I} (γ : Yᶜ Γ I) → Yᵀ (CTm [ Aᵒ ]ᵀ) (γ |ᶜ C.p) -- {I C.▹ ∣ Aᵒ ∣ γ}
      q' γ = (record { a = λ {J} f → (C.q (Aᵒeq C.p γ)) [ f ][ Aᵒeq f (γ |ᶜ C.p) ]tX
                     ; rel = λ {J} f g → (StrictifyCat.[∘]t-gen M {γ = f} {δ = g}) (Aᵒeq f (γ |ᶜ C.p)) (Aᵒeq g (γ |ᶜ C.p |ᶜ f)) ⁻¹ })

      extend : ∀ {I J} (γ : Yᶜ Γ I) (f : Hom J I) → Hom (J C.▹ ∣ Aᵒ ∣ (γ |ᶜ f)) (I C.▹ ∣ Aᵒ ∣ γ)
      extend γ f = (f C.∘ C.p) ,[ StrictifyCat.[∘]T M {γ = f} {δ = C.p} ]X C.q (cong (λ x → x [ C.p ]X) ((Aᵒeq f γ) ⁻¹))

      ext-eq : ∀ {I J} (γ : Yᶜ Γ I) (f : Hom J I) → (Y▹ (γ |ᶜ C.p) (q' γ) |ᶜ (extend γ f)) ≡ (Y▹ (γ |ᶜ f |ᶜ C.p) (q' (γ |ᶜ f)))
      ext-eq γ f = {!!} -- that one would probably be definitional if ▹β₁ was...

    --   remember that
    --   ∀ {I J} (γ : Yᶜ Γ I) (f : Hom J (I C.▹ ∣ Aᵒ ∣ γ))
    --         → (Y▹ (γ |ᶜ C.p) (q' γ)) |ᶜ f ≡ Y▹ (γ |ᶜ C.p |ᶜ f) (⟨ CTm [ Aᵒ ]ᵀ , γ |ᶜ C.p ⟩ q' γ |ᵀ f)

  Π⁺[] : ∀ {Γ} {A : Sub Γ CTy} {B : Ty (Γ ▹ (CTm [ A ]ᵀ)) lzero} {Δ} {γ : Sub Δ Γ} → (Π⁺ A B) [ γ ]ᵀ ≡ Π⁺ (A ∘ γ) (B [ (_,_ {A = CTm [ A ]ᵀ} (γ ∘[ Γ ] p {A = CTm [ A ∘[ CTy ] γ ]ᵀ}) (q {A = CTm [ A ∘[ CTy ] γ ]ᵀ})) ]ᵀ)
  Π⁺[] = refl


  {-
  lam⁺ : ∀{Γ}{Aᵒ : Sub Γ CTy}{B : Ty (Γ ▹ (CTm [ Aᵒ ]T))} → Tm (Γ ▹ (CTm [ Aᵒ ]T)) B → Tm Γ (Π⁺ Aᵒ B)
  lam⁺ {Γ}{Aᵒ} t = record { a = λ γI → ∣ t ∣ (Γ ∶ γI [ C.p ] ,Σ C.q (nat Aᵒ {γf = refl})) ; nat = {!!} }



  -- Π⁺ should be strict, probably we need to start with a strict CwF for this

  CΠ : Sub (CTy ▹ Π⁺ id (K CTy)) CTy
  CΠ = record { a = λ (A ,Σ B) → C.Π A B ; nat = {!!} }
  -}
