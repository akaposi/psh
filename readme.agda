{-# OPTIONS --prop --rewriting #-}

module readme where

-- Tools and postulates (funext, etc)
import lib

-- Definition of a CwF with dependent products and booleans (incl. large elimination)
import model

-- Definition of CwF morphisms
import morphism

-- Definition of dependent CwFs, and pulling them back along morphisms
import depModel

-- Initial CwF ("syntax") with its elimination principles
import initialObs

-- Strictification of the substitution associativity and unit laws of a CwF
-- + definition of a morphism from the strictified CwF to the original CwF
import strictifyCat
import strictifyCatMorphism

-- Strictification of most administrative equations using prefascist sets
-- + definition of a morphism from the strictified CwF to the contextual core of the original CwF
import strictifyPrefascist
import strictifyPrefascistMorphism

-- Proving that the strictification morphisms are in fact isomorphisms
import strictifyIso

-- strictification of the initial model using prefascist sets, it
-- includes dependent model over the strict initial model and its
-- induction principle
import initialStrict

-- some tests for definitional equalities in the strict syntax
import test

-- a canonicity proof
import canon

-- TODO:
-- * derive elimination principle for initialStrict
